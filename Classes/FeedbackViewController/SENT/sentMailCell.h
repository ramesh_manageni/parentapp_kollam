//
//  sentMailCell.h
//  ParentApp
//
//  Created by Redbytes on 27/08/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface sentMailCell : UITableViewCell
@property(strong,nonatomic)UILabel *mailDateLabel;
@property(strong,nonatomic)UILabel *mailTimeLabel;
@property(strong,nonatomic)UILabel *mailContentLabel;
@property(strong,nonatomic)UILabel *mailSubjectLabel;
@property(strong,nonatomic)UIButton *tickButton;
@end
