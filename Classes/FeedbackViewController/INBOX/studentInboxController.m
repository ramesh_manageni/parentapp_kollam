//
//  AttendanceViewController.m
//  ParentApp
//
//  Created by Redbytes on 03/01/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "studentInboxController.h"
#import "ChangePasswordViewController.h"
#import "Constant.h"
#import "AttendanceWebservice.h"
#import "AttendanceTableViewController.h"
#import "AddCalenderEvents.h"
#import "AppDelegate.h"

#import "Parent.h"
#import "StudentDetails.h"
#import "AttendanceDetail.h"
#import "NSData+Base64.h"
#import "EventDetails.h"

#import "AttenCalenderVC.h"


//SliderMenu

#import "DXSemiViewControllerCategory.h"
#import "DXSubClassSemiViewController.h"
#import "DXSubclassSemiTableViewController.h"
#import "MenuViewController.h"
@interface studentInboxController ()

@end

@implementation studentInboxController
@synthesize tableviewObject;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setTitle:@"Communication"];
    NSLog(@"%@",studentArrayCoreData);
    NSLog(@"%@",parentArrayCoreData);
     self.navigationController.navigationBarHidden=NO;
    [self.navigationItem setHidesBackButton:YES];
    //self.navigationItem.rightBarButtonItem = [self addRightBarButtonItem];
    self.navigationItem.leftBarButtonItem = [self addLeftBarButtonItem];
    self.navigationItem.rightBarButtonItem = [self addRightBarButtonItem];

    // Do any additional setup after loading the view from its nib.
    [self.view setBackgroundColor:[UIColor blackColor]];
    UIScreen *mainScreen = [UIScreen mainScreen];
    CGFloat scale = ([mainScreen respondsToSelector:@selector(scale)] ? mainScreen.scale : 1.0f);
    CGFloat pixelHeight = (CGRectGetHeight(mainScreen.bounds) * scale);
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        if (scale == 2.0f)
        {
            if (pixelHeight == 960.0f)
            {
                tableviewObject=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, 320, 480-49) style:UITableViewStyleGrouped];
            }
            
            else if (pixelHeight == 1136.0f)
            {
                tableviewObject=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, 320, 568-49) style:UITableViewStyleGrouped];
            }
        }
    }

    [tableviewObject setBackgroundColor:[UIColor clearColor]];
    tableviewObject.separatorColor = [UIColor clearColor];
    tableviewObject.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    tableviewObject.delegate=self;
    tableviewObject.dataSource=self;
    [self.view addSubview:tableviewObject];
    [tableviewObject reloadData];
}

-(void) viewDidAppear:(BOOL)animated
{
    [ParseClass incrementView];
}

-(void)viewWillAppear:(BOOL)animated
{
    [tableviewObject reloadData];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(UIBarButtonItem *)addLeftBarButtonItem
{
    UIButton *backButton = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 30.0f, 30.0f)];
    UIImage *backImage = [UIImage imageNamed:@"back button icon.png"] ;
    [backButton setBackgroundImage:backImage  forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    return backButtonItem;
    
}
-(UIBarButtonItem *)addRightBarButtonItem
{
    UIButton *backButton = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 30.0f, 30.0f)];
    UIImage *backImage = [UIImage imageNamed:@"list button icon.png"] ;
    [backButton setBackgroundImage:backImage  forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(settingButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    return backButtonItem;
    
}
-(void)backButtonClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)settingButtonClicked
{
    if(semiLeft!=nil)
    {
        [semiLeft dismissSemi:nil];
    }
    semiLeft = [[DXSubclassSemiTableViewController alloc] init];
    semiLeft.delegate=self;
    semiLeft.semiTitleLabel.text = @"SemiLeftTableView";
     semiLeft.tableViewRowHeight = 80.0f/2;
    [semiLeft.view setBackgroundColor:[UIColor clearColor]];
    
    self.leftSemiViewController = semiLeft;
    
}

#pragma mark Table View Datasource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	//tableviewObject = (UITableView *)tableView;
    return 75;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
	return [studentArrayCoreData count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
	return 1;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    AttendanceCustomCell *cell = (AttendanceCustomCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[AttendanceCustomCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
		[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    
    // Configure the cell...
	
    //	cell.labelName.text=[nameArray objectAtIndex:indexPath.section];
    //    cell.labelAdminNumber.text=[adminNumberArray objectAtIndex:indexPath.section
    //                                ];
    //
//    cell.labelName.text=[[[responseArrayLogin objectAtIndex:indexPath.section] valueForKey:@"student_details"] valueForKey:@"local_name"];
//    cell.labelAdminNumber.text=[[[responseArrayLogin objectAtIndex:indexPath.section] valueForKey:@"student_details"] valueForKey:@"admission_no"];
//   
    
    cell.labelName.text=[[studentArrayCoreData objectAtIndex:indexPath.section] local_name];
    cell.labelAdminNumber.text=[[studentArrayCoreData objectAtIndex:indexPath.section] admission_no];
    
    NSMutableArray *tempArray=[[[NSUserDefaults standardUserDefaults] objectForKey:@"attendanceArray"] mutableCopy];

    for(int i=0;i<[tempArray count];i++)
    {
        if([[[tempArray objectAtIndex:i] objectForKey:@"stdId"] isEqualToString:[[studentArrayCoreData objectAtIndex:indexPath.section] id_student]])
        {
           // NSString *temp=@"%";
            cell.labelAttendance.text=[[tempArray objectAtIndex:i] objectForKey:@"count"];
            cell.labelAttendance.text=[NSString stringWithFormat:@"No. of absent days:%@",[[tempArray objectAtIndex:i] objectForKey:@"count"]];
            
        }
    }
    
    
   // cell.labelAttendance.text=@"Attendance:65%";
    
    if([[studentArrayCoreData objectAtIndex:indexPath.section] photo].length>0)
    {
        NSData *imageData = [NSData dataFromBase64String:[[studentArrayCoreData objectAtIndex:indexPath.section] photo]];
        cell.objImageViewStudent.image=[UIImage imageWithData:imageData];
    }
   

    return cell;
}

#pragma mark Table View Delegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    MailViewController *objMailViewController = [[MailViewController alloc]initWithNibName:@"MailViewController" bundle:nil];
    
    objMailViewController.stdID=[[studentArrayCoreData objectAtIndex:indexPath.section] id_student];
    
    objMailViewController.delegate= self.delegate;

    [self.navigationController pushViewController: objMailViewController animated:NO];
    
}


#pragma mark SliderMenuDelegate
-(void)didSelectSliderMenuButton:(NSInteger)rowcount
{
   // NSLog(@"ROWCOUNT=== %d",rowcount);
    if(rowcount==0)
    {
        [self.navigationController popToViewController:objHomeViewController animated:YES];
        
    }
    else
    {
        [self.navigationController popToViewController:objHomeViewController animated:NO];
        
    }
    if ([_delegate respondsToSelector:@selector(didSelectCommunicationRow:)])
    {
        [_delegate didSelectCommunicationRow:rowcount];
    }

}

#pragma mark Delegate

- (id)delegate
{
	return  _delegate;
}

- (void)setDelegate:(id)new_delegate
{
	_delegate = new_delegate;
}
@end
