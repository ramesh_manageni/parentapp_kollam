//
//  ResultCustomCell.m
//  ParentApp
//
//  Created by Redbytes on 03/01/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//
#define kFontName @"BradleyHandITCTT-Bold"

#import "ResultCustomCell.h"

@implementation ResultCustomCell

@synthesize labelGrade,labelExamName,objImageViewBG;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        labelExamName =[[UILabel alloc]initWithFrame:CGRectMake(60, 12, 160, 30)];
		[labelExamName setFont:[UIFont fontWithName:kFontName size:15]];
		[labelExamName setBackgroundColor:[UIColor clearColor]];
        [labelExamName setTextColor:[UIColor whiteColor]];
        
        labelGrade =[[UILabel alloc]initWithFrame:CGRectMake(250, 12, 70, 30)];
		[labelGrade setFont:[UIFont fontWithName:kFontName size:15]];
		[labelGrade setBackgroundColor:[UIColor clearColor]];
        [labelGrade setTextColor:[UIColor whiteColor]];
        
        objImageViewBG=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 54)];
        [objImageViewBG setImage:[UIImage imageNamed:@"result tab 1.png"]];
        [objImageViewBG setBackgroundColor:[UIColor blackColor]];
        
        
        [self.contentView addSubview:objImageViewBG];
        
        [self.contentView addSubview:labelExamName];
		[self.contentView addSubview:labelGrade];
        [self.contentView setBackgroundColor:[UIColor blackColor]];
    }
    return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
