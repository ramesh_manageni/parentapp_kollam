    //
//  MailViewController.m
//  ParentApp
//
//  Created by RedBytes on 05/08/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#define kFontName @"BradleyHandITCTT-Bold"

#import "MailViewController.h"
#import "Constant.h"
#import "MailCustomCell.h"
#import "sentMailCell.h"
#import "MailSliderCustomCell.h"
#import "InboxDetailViewController.h"
#import "FeedbackViewController.h"
#import "Parent.h"
#import "ComposeViewController.h"
#import "DeleteInboxMessage.h"
#import "SentMessageDetail.h"
#import "AppDelegate.h"
#import "ReadInboxMessage.h"

@interface MailViewController ()<UIGestureRecognizerDelegate>
{
    UIView *customNavigationBarView,*sliderView,*gestureView,*customTabbar;
    UILabel *customeViewTitleLabel;
    BOOL isSliderEnable;
    NSMutableArray *inboxArray,*sentArray,*deleteArray,*dateArray,*readArray;
    NSArray *timeArray;
    
    CGRect theNewFrame;
    CGRect theOldFrame;
}

@end

@implementation MailViewController
@synthesize myTableView,stdID;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    
    isInbox=YES;
    
    [self setTitle:@"Mail"];
    
    self.navigationController.navigationBarHidden=NO;
    [self.navigationController.navigationBar setBackgroundColor:[UIColor colorWithRed:246/255.0 green:246/255.0 blue:246/255.0 alpha:1]];
    [self.navigationItem setHidesBackButton:YES];
    self.navigationItem.leftBarButtonItem = [self addLeftBarButtonItem];
    self.navigationItem.rightBarButtonItem = [self addRightBarButtonItem];
    
    readArray = [[NSMutableArray alloc]init];
    inboxArray = [[NSMutableArray alloc] initWithObjects:@"Inbox 1",@"Inbox 2",@"Inbox 3",@"Inbox 4",@"Inbox 5",@"Inbox 6",@"Inbox 7",@"Inbox 8",@"Inbox 9",@"Inbox 10",@"Inbox 11",@"Inbox 12", nil];
    sentArray = [[NSMutableArray alloc] initWithObjects:@"Sent 1",@"Sent 2",@"Sent 3",@"Sent 4",@"Sent 5", @"Inbox 1",@"Inbox 2",@"Inbox 3",@"Inbox 4",@"Inbox 5",@"Inbox 6",nil];
    
    deleteArray = [[NSMutableArray alloc] init];
    
    customNavigationBarView = [[UIView alloc] initWithFrame:CGRectMake(00,65,320,50)];
    customNavigationBarView.backgroundColor = [UIColor colorWithRed:234/255.0 green:234/255.0 blue:234/255.0 alpha:1];
    customeViewTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(110,9,100,30)];
    [customeViewTitleLabel setTextAlignment:NSTextAlignmentCenter];
    
    customeViewTitleLabel.text = @"Inbox";
    
    [customeViewTitleLabel setFont:[UIFont systemFontOfSize:20]];
    [customeViewTitleLabel setTextColor:[UIColor blackColor]];
    
    [customNavigationBarView addSubview:customeViewTitleLabel];
    [self.view addSubview:customNavigationBarView];
    
    self.view.backgroundColor=[UIColor clearColor];
    
    #pragma  implemented By rizwan
    #pragma setting the tableView according to screen size
    UIScreen *mainScreen = [UIScreen mainScreen];
    CGFloat scale = ([mainScreen respondsToSelector:@selector(scale)] ? mainScreen.scale : 1.0f);
    CGFloat pixelHeight = (CGRectGetHeight(mainScreen.bounds) * scale);
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        if (scale == 2.0f)
        {
            if (pixelHeight == 960.0f)
            {
                self.myTableView=[[UITableView alloc]initWithFrame:CGRectMake(0, 111, 320, 480-165) style:UITableViewStyleGrouped];
                customTabbar = [[UIView alloc] initWithFrame:CGRectMake(00,387,320,47)];
            }
            
            else if (pixelHeight == 1136.0f)
            {
                self.myTableView=[[UITableView alloc]initWithFrame:CGRectMake(0, 111, 320, 568-206) style:UITableViewStyleGrouped];
                customTabbar = [[UIView alloc] initWithFrame:CGRectMake(00,475,320,47)];
            }
        }
    }
    
    customTabbar.backgroundColor =[UIColor colorWithRed:234/255.0 green:234/255.0 blue:234/255.0 alpha:1];
    [self addSettingButton];
    [self addDeleteButton];
    
    //self.myTableView=[[UITableView alloc]initWithFrame:CGRectMake(00,95,320,300) style:UITableViewStyleGrouped];
    [self.myTableView setBackgroundColor:[UIColor redColor]];
    self.myTableView.delegate=self;
    self.myTableView.dataSource=self;
    self.myTableView.showsVerticalScrollIndicator=YES;
    self.myTableView.backgroundColor=[UIColor clearColor];
    [self.myTableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    
    [self.view addSubview:self.myTableView];
    [self.view addSubview:customTabbar];
    
    [self addSliderView];
    
    //****************************************************
    
    //    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] init];
    //    tapGesture.delegate = self;
    //    tapGesture.numberOfTapsRequired = 1;
    //    tapGesture.numberOfTouchesRequired = 1;
    //    [tapGesture addTarget:self action:@selector(handleTap:)];
    //    //theOldFrame = [sliderView frame];
    //    //theOldFrame.origin.x=-280;
    //    [self.view addGestureRecognizer:tapGesture];
    
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    // Regiser for HUD callbacks so we can remove it from the window at the right time
    HUD.delegate = self;
    msgInboxUnreadCount = 0;
}
#pragma  implemented By rizwan
-(void)viewWillAppear:(BOOL)animated{
    
    [deleteArray removeAllObjects];
    self.navigationController.navigationBarHidden=NO;
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    InboxWebservice *objInboxWebservice=[[InboxWebservice alloc]init];
    objInboxWebservice.delegate=self;
    [objInboxWebservice InboxMethodForUsername:[[parentArrayCoreData objectAtIndex:0] user_name]];
    [HUD show:YES];
    
    isDeleteMessage=@"";
    isDeleteSent = @"";
    
    if([isDeleteSent isEqualToString:@"YES"])
    {
        
    }
    
    SentItemWebservices *objSentItemWebservice=[[SentItemWebservices alloc]init];
    objSentItemWebservice.delegate=self;
    [objSentItemWebservice SentItemMethodForUsername:[[parentArrayCoreData objectAtIndex:0] user_name]];
    [HUD show:YES];
    [myTableView reloadData];

}
//************************

-(void) addSliderView
{
    gestureView = [[UIView alloc] initWithFrame:CGRectMake(-320,115,320,self.view.frame.size.height-95)];
    gestureView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.65f];
    [self.view addSubview:gestureView];

    sliderView = [[UIView alloc] initWithFrame:CGRectMake(00,00,320,self.view.frame.size.height-95)];
    sliderView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:1.0f];
    [gestureView addSubview:sliderView];
    
    UIView *borderView = [[UIView alloc] initWithFrame:CGRectMake(219,00,01,self.view.frame.size.height-95)];
    borderView.backgroundColor = [UIColor whiteColor];
    //[sliderView addSubview:borderView];
    
    theOldFrame = [gestureView frame];
    theOldFrame.origin.x=-320;
    
    self.sliderTableView = [[UITableView alloc] initWithFrame:CGRectMake(20,-4,300, 200)];
    self.sliderTableView.delegate = self;
    self.sliderTableView.dataSource = self;
    self.sliderTableView.backgroundColor = [UIColor clearColor];
    //[self.sliderTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    self.sliderTableView.scrollEnabled = NO;
    [sliderView addSubview:self.sliderTableView];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] init];
    tapGesture.delegate = self;
    tapGesture.numberOfTapsRequired = 1;
    tapGesture.numberOfTouchesRequired = 1;
    [tapGesture addTarget:self action:@selector(handleTap:)];
    [gestureView addGestureRecognizer:tapGesture];
}

-(void)handleTap:(id)sender
{
    if(isSliderEnable)
    {
        [UIView animateWithDuration:0.3 animations:^{gestureView.frame = theOldFrame;}];
        isSliderEnable = NO;
        [customNavigationBarView addSubview:deleteButton];
    }
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    customeViewTitleLabel.text=@"";
    if([touch.view isKindOfClass:[UITableViewCell class]]) {
        return NO;
    }
    // UITableViewCellContentView => UITableViewCell
    if([touch.view.superview isKindOfClass:[UITableViewCell class]])
    {
        return NO;
    }
    // UITableViewCellContentView => UITableViewCellScrollView => UITableViewCell
    if([touch.view.superview.superview isKindOfClass:[UITableViewCell class]]) {
        return NO;
    }
    return YES;
}

-(UIBarButtonItem *)addLeftBarButtonItem
{
    UIButton *backButton = [[UIButton alloc] initWithFrame: CGRectMake(0,0,30,30)];
    UIImage *backImage = [UIImage imageNamed:@"back button icon.png"] ;
    [backButton setBackgroundImage:backImage  forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    return backButtonItem;
}

-(UIBarButtonItem *)addRightBarButtonItem
{
    UIButton *backButton = [[UIButton alloc] initWithFrame: CGRectMake(00,0,30,30)];
    UIImage *backImage = [UIImage imageNamed:@"list button icon.png"] ;
    [backButton setBackgroundImage:backImage  forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(settingButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    return backButtonItem;
}

-(void)addDeleteButton
{
     deleteButton = [[UIButton alloc] initWithFrame: CGRectMake(280,10,30,30)];
    [deleteButton setBackgroundImage:[UIImage imageNamed:@"delete1.png"] forState:UIControlStateNormal];
    [deleteButton addTarget:self action:@selector(deleteButtonClickAction:) forControlEvents:UIControlEventTouchUpInside];
    [customNavigationBarView addSubview:deleteButton];
    
    UIButton *composeButton = [[UIButton alloc] initWithFrame: CGRectMake(278,5,30,30)];
    [composeButton setBackgroundImage:[UIImage imageNamed:@"compose1.png"]  forState:UIControlStateNormal];
    [composeButton addTarget:self action:@selector(composeButtonClickAction:) forControlEvents:UIControlEventTouchUpInside];
    [customTabbar addSubview:composeButton];
}

#pragma  implemented by rizwan

-(void)composeButtonClickAction:(id)sender
{
    ComposeViewController *objComposeViewController=[[ComposeViewController alloc] initWithNibName:@"ComposeViewController" bundle:nil];
    objComposeViewController.student_id=stdID;
    [self presentViewController:objComposeViewController animated:YES completion:nil];
}
-(IBAction)deleteButtonClickAction:(id)sender
{
    deletemessageCount=0;
    deleteArrayCount=[deleteArray count];
    
    if(deleteArray.count==0)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please select items to delete" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    else
    {
        [HUD show:YES];
        if (isInbox)
        {
            for(int i=0;i<[deleteArray count];i++)
            {
                DeleteInboxMessage *objDeleteInboxMessage=[[DeleteInboxMessage alloc]init];
                objDeleteInboxMessage.delegate =self;
                [objDeleteInboxMessage deleteMessageWithUsername:[[parentArrayCoreData objectAtIndex:0] user_name] pmId:[deleteArray objectAtIndex:i] Inbox_Sent:@"inbox"];
            }
        }
        else
        {
            for(int i=0;i<[deleteArray count];i++)
            {
                DeleteSentMessage *objDeleteSent = [[DeleteSentMessage alloc]init];
                objDeleteSent.delegate = self;
                [objDeleteSent deleteMessageWithUsername:[[parentArrayCoreData objectAtIndex:0] user_name] pmId:[deleteArray objectAtIndex:i] Sent:@"outbox"];
            }
        }
    }
    [deleteArray removeAllObjects];
    [myTableView reloadData];
}
//*************************************************

-(void)addSettingButton
{
    UIButton *settingButton = [[UIButton alloc] initWithFrame: CGRectMake(12,10,30,30)];
    [settingButton setBackgroundImage:[UIImage imageNamed:@"settingCommunication.png"]  forState:UIControlStateNormal];
    [settingButton addTarget:self action:@selector(settingButtonClickAction:) forControlEvents:UIControlEventTouchUpInside];
    [customNavigationBarView addSubview:settingButton];
}

-(IBAction)settingButtonClickAction:(id)sender
{
    if(isSliderEnable)
    {
        isSliderEnable= NO;
        customeViewTitleLabel.text=@"Inbox";
        //customeViewTitleLabel.text=@"";
        [deleteButton setHidden:NO];

        if (isInbox) {
            customeViewTitleLabel.text=@"Inbox";
         

        }
        else if(isSentbox)
        {
            customeViewTitleLabel.text=@"Sent Items";
            


        }
        
        
        [UIView animateWithDuration:0.3 animations:^{gestureView.frame = theOldFrame;}];
        //[customNavigationBarView addSubview:deleteButton];
    }
    else
    {
       
            customeViewTitleLabel.text = @"";
            
       
        [deleteButton setHidden:YES];
        [self.sliderTableView reloadData];
        theNewFrame = [gestureView frame];
        theNewFrame.origin.x = 0;
        [UIView animateWithDuration:0.3 animations:^{gestureView.frame = theNewFrame;}];
        isSliderEnable= YES;
       // [deleteButton removeFromSuperview];
    }
}

-(void)backButtonClicked
{
    [self.navigationController popViewControllerAnimated:NO];
}

-(void)settingButtonClicked
{
    if(semiLeft!=nil)
    {
        [semiLeft dismissSemi:nil];
    }
    semiLeft = [[DXSubclassSemiTableViewController alloc] init];
    semiLeft.delegate=self;
    semiLeft.semiTitleLabel.text = @"SemiLeftTableView";
    semiLeft.tableViewRowHeight = 80.0f/2;
    [semiLeft.view setBackgroundColor:[UIColor clearColor]];
    self.leftSemiViewController = semiLeft;
}

#pragma mark SliderMenuDelegate
-(void)didSelectSliderMenuButton:(NSInteger)rowcount
{
    // NSLog(@"ROWCOUNT=== %d",rowcount);
    if(rowcount==0)
    {
        [self.navigationController popToViewController:objHomeViewController animated:YES];
    }
    else
    {
        [self.navigationController popToViewController:objHomeViewController animated:NO];
    }
    if ([_delegate respondsToSelector:@selector(didSelectRowMail:)])
    {
        [_delegate didSelectRowMail:rowcount];
    }
}

- (id)delegate
{
	return _delegate;
}

- (void)setDelegate:(id)new_delegate
{
	_delegate = new_delegate;
}

#pragma mark UITableView Delegate And Datasource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == myTableView)
    {
        return 60;
    }
    else
    {
        return 60;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 4;
}

//-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
//{
//    UIImageView* separatorLineView = [[UIImageView alloc] initWithFrame:CGRectMake(00,00,320, 4)];/// change size as you need.
//    separatorLineView.backgroundColor = [UIColor clearColor];
//    separatorLineView.image = [UIImage imageNamed:@"line@2x.png"];
//    
//    return separatorLineView;
//}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	if (tableView == myTableView)
    {
        if(isInbox)
        {
            if([inboxResponse count]==0)
            {
                [deleteButton removeFromSuperview];
            }
            else
            {
                [deleteButton setHidden:NO];
                [customNavigationBarView addSubview:deleteButton];
            }

            return inboxResponse.count;
            
        }
        else
        {
            if([sentItemResponse count]==0)
            {
                  [deleteButton removeFromSuperview];
            }
            else
            {
                [deleteButton setHidden:NO];
                [customNavigationBarView addSubview:deleteButton];
            }
         
            return sentItemResponse.count;
            
        }
    }
    else
    {
        return 3;
    }
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == myTableView)
    {
        if(isInbox)
        {
            static NSString *CellIdentifier = @"MailCustomCell";
            MailCustomCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (cell == nil)
            {
                cell = [[MailCustomCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
                [cell setBackgroundColor:[UIColor clearColor]];
            }
            
            NSString *dateString = [[inboxResponse  objectAtIndex:indexPath.row] valueForKey:@"datestamp"];
            NSLog(@"%@",dateString);
            NSDateFormatter *stringFormate = [[NSDateFormatter alloc] init];
            [stringFormate setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            NSDate *finalDate = [[NSDate alloc] init];
            [stringFormate dateFromString:dateString];
            
            NSDateFormatter *dateFormate = [[NSDateFormatter alloc] init];
            [dateFormate setDateFormat:@"yyyy-MM-dd"];
            NSString *dateFinalStr = [dateFormate stringFromDate:finalDate];
            NSLog(@"%@",dateFinalStr);
            
            NSDateFormatter *timeFormate = [[NSDateFormatter alloc] init];
            [timeFormate setDateFormat:@"HH:mm a"];
            NSString *timeFinalStr = [timeFormate stringFromDate:finalDate];
            NSLog(@"%@",timeFinalStr);
            
            [cell.tickButton addTarget:self action:@selector(tickButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            cell.tickButton.tag=indexPath.row;
            cell.mailContentLabel.text = [[inboxResponse  objectAtIndex:indexPath.row] valueForKey:@"subject_name"];
            cell.mailDateLabel.text = dateFinalStr;//[[[inboxResponse  objectAtIndex:indexPath.row] valueForKey:@"datestamp"] componentsSeparatedByString:@" "][0];
            cell.mailTimeLabel.text = timeFinalStr;//[[[inboxResponse  objectAtIndex:indexPath.row] valueForKey:@"datestamp"] componentsSeparatedByString:@" "][1];
            cell.mailSubjectLabel.text=[[inboxResponse  objectAtIndex:indexPath.row] valueForKey:@"subject"];
            [cell.tickButton setImage:[UIImage imageNamed:@"check.png"] forState:UIControlStateNormal];
            
            if ([deleteArray containsObject:[[inboxResponse objectAtIndex:indexPath.row] valueForKey:@"pm_message_id"]])
            {
                [cell.tickButton setBackgroundImage:[UIImage imageNamed:@"selectcheck.png"] forState:UIControlStateNormal];
                cell.mailContentLabel.textColor=[UIColor redColor];
                cell.mailDateLabel.textColor=[UIColor redColor];
                cell.mailTimeLabel.textColor=[UIColor redColor];
                cell.mailSubjectLabel.textColor=[UIColor redColor];
            }
            else
            {
                [cell.tickButton setBackgroundImage:[UIImage imageNamed:@"check.png"] forState:UIControlStateNormal];
                cell.mailContentLabel.textColor=[UIColor darkGrayColor];
                cell.mailDateLabel.textColor=[UIColor lightGrayColor];
                cell.mailTimeLabel.textColor=[UIColor lightGrayColor];
                cell.mailSubjectLabel.textColor=[UIColor darkGrayColor];
                
                BOOL isReadMessage = [[[inboxResponse objectAtIndex:indexPath.row] valueForKey:@"read_flag"] intValue];
                if (!isReadMessage)
                {
                    cell.mailContentLabel.textColor=[UIColor blackColor];
                    cell.mailDateLabel.textColor=[UIColor darkGrayColor];
                    cell.mailTimeLabel.textColor=[UIColor darkGrayColor];//colorWithRed:98/255.0 green:98/255.0 blue:98/255.0 alpha:1];
                    cell.mailSubjectLabel.textColor=[UIColor darkGrayColor];//colorWithRed:98/255.0 green:98/255.0 blue:98/255.0 alpha:1];
                }
                
            }
            cell.backgroundColor=[UIColor whiteColor];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            return cell;
        }
        
        //***********************************************************
        else
        {
            static NSString *CellIdentifier = @"sentMailCell";
            sentMailCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (cell == nil)
            {
                cell = [[sentMailCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
                [cell setBackgroundColor:[UIColor clearColor]];
                //[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            }
            [cell.tickButton addTarget:self action:@selector(tickButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            cell.tickButton.tag=indexPath.row ;
            
            NSString *dateAndTimeString = [[sentItemResponse  objectAtIndex:indexPath.row] valueForKey:@"datestamp"];
            timeArray = [dateAndTimeString componentsSeparatedByString:@" "];
            NSString *timeString = [NSString stringWithFormat:@"%@ %@", [timeArray objectAtIndex:1], [timeArray objectAtIndex:2]];
            NSString *dateString = [NSString stringWithFormat:@"%@", [timeArray objectAtIndex:0]];
           
            // NSString *msg = [dateString lastObject];
           
            
            
//            NSArray *firstSplit = [str componentsSeparatedByString:@"|"];
//            NSAssert(firstSplit.count == 2, @"Oops! Parsed string had more than one |, no message or no numbers.");
//            NSString *msg = [firstSplit lastObject];
//            NSArray *numbers = [[firstSplit objectAtIndex:0] componentsSepratedByString:@","];
//            
//            // print out the numbers (as strings)
//            for(NSString *currentNumberString in number) {
//                NSLog(@"Number: %@", currentNumberString);
//            }
//            
            
            
//            NSDateFormatter *stringFormate = [[NSDateFormatter alloc] init];
//            [stringFormate setDateFormat:@"yyyy-mm-dd hh:mm:ss"];
//            NSDate *finalDate = [[NSDate alloc] init];
//           finalDate= [stringFormate dateFromString:dateString];
//            
//            NSDateFormatter *dateFormate = [[NSDateFormatter alloc] init];
//            [dateFormate setDateFormat:@"yyyy-MM-dd"];
//            NSString *dateFinalStr = [dateFormate stringFromDate:finalDate];
//            
//            NSDateFormatter *timeFormate = [[NSDateFormatter alloc] init];
//            [timeFormate setDateFormat:@"hh:mm a"];
//            NSString *timeFinalStr = [timeFormate stringFromDate:finalDate];
//           // timeArray = [timeFinalStr componentsSeparatedByString:@" "];
            
            cell.mailContentLabel.text = [[sentItemResponse  objectAtIndex:indexPath.row]objectForKey:@"subject_name"];// to_address];
            cell.mailDateLabel.text = dateString;
            cell.mailTimeLabel.text = timeString;
            cell.mailSubjectLabel.text=[[sentItemResponse  objectAtIndex:indexPath.row] objectForKey:@"subject"];//subject_sent_item];
            [cell.tickButton setImage:[UIImage imageNamed:@"check.png"] forState:UIControlStateNormal];
            if ([deleteArray containsObject:[[sentItemResponse objectAtIndex:indexPath.row] objectForKey:@"pm_message_id"]])//message_id]])
            {
                NSLog(@"DELETE MESSAGE with message id : %@",[[sentItemResponse objectAtIndex:indexPath.row]objectForKey:@"pm_message_id" ]);//message_id]);
                [cell.tickButton setBackgroundImage:[UIImage imageNamed:@"selectcheck.png"] forState:UIControlStateNormal];
                cell.mailContentLabel.textColor=[UIColor redColor];
                cell.mailDateLabel.textColor=[UIColor redColor];
                cell.mailTimeLabel.textColor=[UIColor redColor];
                cell.mailSubjectLabel.textColor=[UIColor redColor];
            }
            else
            {
                [cell.tickButton setBackgroundImage:[UIImage imageNamed:@"check.png"] forState:UIControlStateNormal];
                cell.mailContentLabel.textColor=[UIColor blackColor];
                cell.mailDateLabel.textColor=[UIColor lightGrayColor];
                cell.mailTimeLabel.textColor=[UIColor lightGrayColor];
                cell.mailSubjectLabel.textColor=[UIColor darkGrayColor];
            }
            cell.backgroundColor=[UIColor whiteColor];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            return cell;
        }
        
    }
    else
    {
        static NSString *CellIdentifier = @"MailSliderCustomCell";
        MailSliderCustomCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil)
        {
            cell = [[MailSliderCustomCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
            [cell setBackgroundColor:[UIColor clearColor]];
            //[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
        }
        if (indexPath.row == 0)
        {
            cell.count.text = @"";
            cell.contentTitle.text = @"Compose";
            [cell.contentTitle setTextColor:[UIColor blackColor]];
            cell.contentImageView.image = [UIImage imageNamed:@"compose1.png"];
        }
        else if (indexPath.row == 1)
        {
            cell.count.text = [NSString stringWithFormat:@"%d",msgInboxUnreadCount];
            NSLog(@"%d",msgInboxUnreadCount);
            //cell.count.text = [NSString stringWithFormat:@"%lu",(unsigned long)inboxResponse.count];
            [cell.count setTextColor:[UIColor grayColor]];
            cell.contentTitle.text = @"Inbox";
            [cell.contentTitle setTextColor:[UIColor blackColor]];
            cell.contentImageView.image = [UIImage imageNamed:@"inbox1.png"];
        }
        else if (indexPath.row == 2)
        {
            //[self getEntityCountFromSentMessageTable:@"SentMessageDetail"];
            cell.count.text = [NSString stringWithFormat:@"%lu",(unsigned long)msgSentTotalCount];
            [cell.count setTextColor:[UIColor grayColor]];
            cell.contentTitle.text = @"Sent Items";
            [cell.contentTitle setTextColor:[UIColor blackColor]];
            cell.contentImageView.image = [UIImage imageNamed:@"sent.png"];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        cell.backgroundColor=[UIColor whiteColor];
        return cell;
    }
}

#pragma  implemented by rizwan
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
   
    if (tableView == myTableView)
    {
        MailCustomCell *cell = (MailCustomCell *)[tableView cellForRowAtIndexPath:indexPath];
        
        InboxDetailViewController *objInboxDetailViewController = [[InboxDetailViewController alloc]initWithNibName:@"InboxDetailViewController" bundle:Nil];
       
        objInboxDetailViewController.delegate=self.delegate;
        NSLog(@"%@",cell.mailContentLabel.text);
//        objInboxDetailViewController.nameProperty=cell.mailContentLabel.text;
//        objInboxDetailViewController.dateProperty=cell.mailDateLabel.text;
//        objInboxDetailViewController.timeProperty=cell.mailTimeLabel.text;
//        objInboxDetailViewController.messageProperty=[[inboxResponse objectAtIndex:indexPath.row] valueForKey:@"message"];
        if(isSentbox)
        {
         objInboxDetailViewController.nameProperty=[[sentItemResponse objectAtIndex:indexPath.row]objectForKey:@"subject_name"];// to_address];
         objInboxDetailViewController.dateProperty=cell.mailDateLabel.text;
         objInboxDetailViewController.timeProperty=cell.mailTimeLabel.text;
         objInboxDetailViewController.messageProperty=[[sentItemResponse objectAtIndex:indexPath.row] objectForKey:@"message"];//messgae_sent];
         objInboxDetailViewController.isInbox=NO;
         objInboxDetailViewController.isSentbox=YES;
         objInboxDetailViewController.stdID_inboxDetail=stdID;
         objInboxDetailViewController.message_Id=[[sentItemResponse objectAtIndex:indexPath.row] objectForKey:@"pm_message_id"]; //message_id];
        }
        else if (isInbox)
        {
            [readArray addObject:[[inboxResponse objectAtIndex:indexPath.row] valueForKey:@"pm_message_id"]];

            objInboxDetailViewController.nameProperty=[[inboxResponse objectAtIndex:indexPath.row] valueForKey:@"subject_name"];
            objInboxDetailViewController.dateProperty=cell.mailDateLabel.text;
            objInboxDetailViewController.timeProperty=cell.mailTimeLabel.text;
            objInboxDetailViewController.messageProperty=[[inboxResponse objectAtIndex:indexPath.row] valueForKey:@"message"];
            objInboxDetailViewController.isInbox=YES;
            objInboxDetailViewController.isSentbox=NO;
            objInboxDetailViewController.stdID_inboxDetail=stdID;
            objInboxDetailViewController.message_Id=[[inboxResponse objectAtIndex:indexPath.row] valueForKey:@"pm_message_id"];
            objInboxDetailViewController.subjectProperty=[[inboxResponse objectAtIndex:indexPath.row] valueForKey:@"subject"];
            
            //WS for Read Message from Inbox
             if (![[[inboxResponse objectAtIndex:indexPath.row] valueForKey:@"read_flag"]intValue])
             {
            ReadInboxMessage *objReadInboxMessage=[[ReadInboxMessage alloc]init];
            objReadInboxMessage.delegate=self;
            [objReadInboxMessage readInboxMessageWithUsername:[[parentArrayCoreData objectAtIndex:0] user_name] pmId:[[inboxResponse objectAtIndex:indexPath.row] valueForKey:@"pm_message_id"]];
             }
        }
        [self.navigationController pushViewController:objInboxDetailViewController animated:YES];

        NSLog(@"%@",deleteArray);
    }

  // ****************************************
    else
    {
        
        if (indexPath.row == 0)
        {
            [deleteButton setHidden:NO];
            
            if (isInbox) {
                customeViewTitleLabel.text=@"Inbox";
                
                
            }
            else if(isSentbox)
            {
                customeViewTitleLabel.text=@"Sent Items";
                
                
                
            }

            //[customNavigationBarView addSubview:deleteButton];
            ComposeViewController *objComposeViewController=[[ComposeViewController alloc] initWithNibName:@"ComposeViewController" bundle:nil];
            objComposeViewController.student_id=stdID;
            [self presentViewController:objComposeViewController animated:YES completion:nil];
        }
        else if (indexPath.row == 1)
        {
            isInbox=YES;
            isSentbox=NO;
           // [customNavigationBarView addSubview:deleteButton];
            if (![customeViewTitleLabel.text isEqualToString:@"Inbox"])
            {
                [deleteArray removeAllObjects];
                customeViewTitleLabel.text = @"Inbox";
                //customeViewTitleLabel.text = @"";
                [self.myTableView reloadData];
            }
        }
        else if (indexPath.row == 2)
        {
            isInbox=NO;
            isSentbox=YES;
            SentItemWebservices *objSentItemWebservice=[[SentItemWebservices alloc]init];
            objSentItemWebservice.delegate=self;
            [objSentItemWebservice SentItemMethodForUsername:[[parentArrayCoreData objectAtIndex:0] user_name]];
            [HUD show:YES];
            //[customNavigationBarView addSubview:deleteButton];
            if (![customeViewTitleLabel.text isEqualToString:@"Sent Items"])
            {
                [deleteArray removeAllObjects];
                customeViewTitleLabel.text = @"Sent Items";
                //customeViewTitleLabel.text = @"";
                [self.myTableView reloadData];
            }
        }
        isSliderEnable= NO;
        [UIView animateWithDuration:0.3 animations:^{gestureView.frame = theOldFrame;}];
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma  Rizwan implemented code

-(void)tickButtonClicked:(UIButton *)sender
{
    //[[inboxResponse  objectAtIndex:indexPath.row] valueForKey:@"subject"]
    if (isInbox)
    {
        if ([sender backgroundImageForState:UIControlStateNormal]== [UIImage imageNamed:@"selectcheck.png"])
        {
            NSLog(@"%ld",(long)sender.tag);
            [deleteArray removeObject:[[inboxResponse objectAtIndex:sender.tag] valueForKey:@"pm_message_id"]];
            [sender setBackgroundImage:[UIImage imageNamed:@"check.png"] forState:UIControlStateNormal];
            //[sender setImage:[UIImage imageNamed:@"unselectedcheckbox.png"] forState:UIControlStateNormal];
        }
        else
        {
            NSLog(@"%ld",(long)sender.tag);
            [deleteArray addObject:[[inboxResponse objectAtIndex:sender.tag] valueForKey:@"pm_message_id"]];
            NSLog(@"%@",deleteArray);
            [sender setBackgroundImage:[UIImage imageNamed:@"selectcheck.png"] forState:UIControlStateNormal];
        }
    }
    else
    {
        if ([sender backgroundImageForState:UIControlStateNormal]== [UIImage imageNamed:@"selectcheck.png"])
        {
            
            NSLog(@"%ld",(long)sender.tag);
            
            [deleteArray removeObject:[[sentItemResponse  objectAtIndex:sender.tag] objectForKey:@"pm_message_id"]]; //message_id]];
            [sender setBackgroundImage:[UIImage imageNamed:@"check.png"] forState:UIControlStateNormal];
            //[sender setImage:[UIImage imageNamed:@"unselectedcheckbox.png"] forState:UIControlStateNormal];
        }
        else
        {
            NSLog(@"%ld",(long)sender.tag);
            [deleteArray addObject:[[sentItemResponse  objectAtIndex:sender.tag] objectForKey:@"pm_message_id"]];//message_id]];
            NSLog(@"%@",deleteArray);
            // sender.imageView.image = [UIImage imageNamed:@"selectedcheckbox.png"];
            [sender setBackgroundImage:[UIImage imageNamed:@"selectcheck.png"] forState:UIControlStateNormal];
        }
    }
    [self.myTableView reloadData];
}

#pragma mark InboxWebservice Delegate
-(void)serverResponseForInboxWebservice:(NSArray *)responseArray
{
    [HUD hide:YES];
    if([responseArray valueForKey:@"success"])
    {
        msgInboxUnreadCount = 0;
        NSLog(@"Respose of Inbox:  %@",responseArray);
        inboxResponse=[responseArray valueForKey:@"inbox"];
        
        for (NSDictionary *msgDict in inboxResponse)
        {
            BOOL isMsgRead = [[msgDict valueForKey:@"read_flag"] boolValue];
            if (!isMsgRead)
            {
                msgInboxUnreadCount++;
            }
        }
       communicationBadgeCount=msgInboxUnreadCount;

        [self.myTableView reloadData];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"there are no messages available" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        
    }
}
-(void)serverFailResponseForInboxwebservice
{
    [HUD hide:YES];

    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Server Response Failed" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}

#pragma mark SentMessage Core Data Methods
-(void)getEntityCountFromSentMessageTable:(NSString *)entityName
{
    // entityCount = 0;
   // sentItemResponse = [[NSMutableArray alloc]init];
    //NSError *error;
    
//    sentArray=[[NSMutableArray alloc] init];
//    
//    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
//    NSManagedObjectContext *context = [delegate managedObjectContext];
//    
//    NSFetchRequest *fetchReq = [[NSFetchRequest alloc]init];
//    
//    [fetchReq setEntity:[NSEntityDescription entityForName:entityName inManagedObjectContext:context]];
//    NSError *error;
//    
//    NSMutableArray *resultArray = [[NSMutableArray alloc]initWithArray:[context executeFetchRequest:fetchReq error:&error]];
//    NSLog(@"Result Array: %@",resultArray);
    
    
//    NSSortDescriptor* sortOrder = [NSSortDescriptor sortDescriptorWithKey:@"datestamp" ascending:NO];//@"date_sent"
//    NSArray *sortLocalArry= [sentItemResponse sortedArrayUsingDescriptors:[NSArray arrayWithObject: sortOrder]];
//    if(error==nil)
//    {
//        sentItemResponse=[NSMutableArray arrayWithArray:sortLocalArry];
//
//    }
}

-(void)deleteSentMessage:(NSString *)msgID
{
    
    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [delegate managedObjectContext];
    
    NSFetchRequest *fetchReq = [[NSFetchRequest alloc]init];
    
    [fetchReq setEntity:[NSEntityDescription entityForName:@"SentMessageDetail" inManagedObjectContext:context]];
    NSError *error;
    
    NSMutableArray *resultArray = [[NSMutableArray alloc]initWithArray:[context executeFetchRequest:fetchReq error:&error]];
    if(error==nil)
    {
        
        for(SentMessageDetail *sentdetails in resultArray)
        {
            if(sentdetails.message_id==msgID)
            {
                [context deleteObject:sentdetails];
            }
            
        }
      }
    
    deletemessageCount++;
   
      UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Message(s) deleted successfully" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        if(deleteArrayCount==1)
        {
            [alert show];
         }
        if(deleteArrayCount==deletemessageCount && deletemessageCount!=0 && deleteArrayCount!=0 )
        {
            [alert show];
        }

}

#pragma mark Delete Message Delegate
-(void)serverResponseForDeleteInboxMessage:(NSArray *)responseArray
{
    [HUD hide:YES];
    deletemessageCount++;
    if([responseArray valueForKey:@"success"])
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Message(s) deleted successfully" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        if(deleteArrayCount==1)
        {
            [alert show];
            //Fetch Inbox messages
            InboxWebservice *objInboxWebservice=[[InboxWebservice alloc]init];
            objInboxWebservice.delegate=self;
            [objInboxWebservice InboxMethodForUsername:[[parentArrayCoreData objectAtIndex:0] user_name]];
            [HUD show:YES];
        }
        if(deleteArrayCount==deletemessageCount && deletemessageCount!=0 && deleteArrayCount!=0 )
        {
            [alert show];
            //Fetch Inbox messages
            InboxWebservice *objInboxWebservice=[[InboxWebservice alloc]init];
            objInboxWebservice.delegate=self;
            [objInboxWebservice InboxMethodForUsername:[[parentArrayCoreData objectAtIndex:0] user_name]];
            [HUD show:YES];
        }
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Failed to delete message" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
}
-(void)serverFailResponseForIDeleteInboxMessage
{
    [HUD hide:YES];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Server Response Failed" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}

-(void)serverResponseForReadInboxMessage:(NSArray *)responseArray
{
    int intvalue=(int)[responseArray valueForKey:@"success"];
    if(intvalue)
    {
        
    }
    
}
-(void)serverFailResponseForIReadInboxMessage
{
    [HUD hide:YES];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Server Response Failed" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}


#pragma mark SentItemWebservices Delegate

-(void)serverResponseForSentItemWebservice:(NSArray *)responseArray
{
    [HUD hide:YES];
    if([responseArray valueForKey:@"success"])
    {
        msgSentTotalCount = 0;
        NSLog(@"Respose of SentItem:  %@",responseArray);
        sentItemResponse=[responseArray valueForKey:@"SendItems"];
        [self.sliderTableView reloadData];
        
        NSError *error;
        NSSortDescriptor *sortOrder = [NSSortDescriptor sortDescriptorWithKey:@"datestamp" ascending:NO];
        NSArray *sortLocalArry= [sentItemResponse sortedArrayUsingDescriptors:[NSArray arrayWithObject: sortOrder]];
        if(error==nil)
        {
            sentItemResponse=[NSMutableArray arrayWithArray:sortLocalArry];
        }
        msgSentTotalCount = [sentItemResponse count];
        
 

        [self.myTableView reloadData];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"there are no messages available" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }

}
-(void)serverFailResponseForSentItemwebservice
{
    [HUD hide:YES];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Server Response Failed" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}

#pragma DeleteSentmessage WS
-(void)serverResponseForDeleteSentMessage:(NSArray *)responseArray
{
    
    [HUD hide:YES];
    deletemessageCount++;
    if([responseArray valueForKey:@"success"])
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Message(s) deleted successfully" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        if(deleteArrayCount==1)
        {
            [alert show];
            //Fetch Inbox messages
            SentItemWebservices *objSentWebservice=[[SentItemWebservices alloc]init];
            objSentWebservice.delegate=self;
            [objSentWebservice SentItemMethodForUsername:[[parentArrayCoreData objectAtIndex:0] user_name]];
            [HUD show:YES];
        }
        if(deleteArrayCount==deletemessageCount && deletemessageCount!=0 && deleteArrayCount!=0 )
        {
            [alert show];
            //Fetch Inbox messages
            SentItemWebservices *objSentWebservice=[[SentItemWebservices alloc]init];
            objSentWebservice.delegate=self;
            [objSentWebservice SentItemMethodForUsername:[[parentArrayCoreData objectAtIndex:0] user_name]];
            [HUD show:YES];
        }
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Failed to delete message" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        
    }
}
-(void)serverFailResponseForDeleteSentMessage
{
    [HUD hide:YES];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Server Response Failed" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}

//******************************************
@end
