//
//  FeedbackViewController.h
//  ParentApp
//
//  Created by Redbytes on 03/01/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FeedbackWebservice.h"
#import "TeacherWebservice.h"
#import "MBProgressHUD.h"



#import "DXSemiViewControllerCategory.h"
#import "DXSubClassSemiViewController.h"
#import "DXSubclassSemiTableViewController.h"
@protocol MenuButtonFeedbackDelegate <NSObject>

-(void)didSelectRowFeedback:(NSInteger)rowcount;

@end



@interface FeedbackViewController : UIViewController<UITextFieldDelegate,UITextViewDelegate,FeedbackDelegate,MBProgressHUDDelegate,UIPickerViewDelegate,UIPickerViewDataSource,UITableViewDataSource,UITableViewDelegate,SliderMenuButtonDelegate,TeacherDelegate>
{
    MBProgressHUD *HUD;
    NSMutableArray *selectedStudents;
    NSString *isSelectAll;
    id _delegate;
    DXSubclassSemiTableViewController *semiLeft;
}
// keshav navigation bar
@property (strong, nonatomic) IBOutlet UIView *customNavBar;



@property (strong, nonatomic) IBOutlet UIScrollView *objScrollView;
@property (strong, nonatomic) IBOutlet UITextField *objTextName;
@property (strong, nonatomic) IBOutlet UITextField *objTextEmail;
@property (strong, nonatomic) IBOutlet UITextField *objTextSubject;
@property (strong, nonatomic) IBOutlet UITextView *objTextMessage;
@property (strong, nonatomic) IBOutlet UIButton *submitButton;
@property (strong, nonatomic) IBOutlet UITextField *defaultLabel;

//by Nihal
- (IBAction)objTextSubjectAction:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *objTextFeedbackAbout;
@property (weak, nonatomic) IBOutlet UITextField *objTextTeacher;

@property (retain, nonatomic) NSMutableArray *teacherNameArray;
@property (retain, nonatomic) NSMutableArray *subjectsNameArray;
//////// end Nihal ///////


@property(strong,nonatomic)UITableView *myTableView;
- (IBAction)submitButtonClicked:(id)sender;

- (id)delegate;
- (void)setDelegate:(id)new_delegate;

@end
