//
//  NoticeReadfromDeviceWebservice.m
//  ParentApp
//
//  Created by Imtiyaz on 8/12/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "NoticeReadfromDeviceWebservice.h"

@implementation NoticeReadfromDeviceWebservice
-(void)GetNoticeReadForProfileId:(NSString *)profileId circular_id:(NSString *)circular_id
{
    NSMutableDictionary *localDictionary=[[NSMutableDictionary alloc]init];
    
    
    
    
    
    
    [localDictionary setObject:profileId forKey:@"profile_id"];
    [localDictionary setObject:circular_id forKey:@"circular_id"];
    
    NSLog(@"Dict: %@",localDictionary);
    
    if (objConnectionManager!=nil)
    {
		objConnectionManager=nil;
	}
	objConnectionManager=[[ConnectionManager alloc]init];
    //  objConnectionManager.isSBTracker=@"YES";
     objConnectionManager.isNoticeReadWS=@"YES";
    [objConnectionManager getWebData:self commandName:@"ReadFromDevice" Requestparameter:localDictionary];
}

#pragma mark connection manager delegate
- (void)receivedData:(NSArray *)responseArray
{
    if ([_delegate respondsToSelector:@selector(serverResponseForNoticeReadfromDeviceWebservice:)])
    {
        [_delegate serverResponseForNoticeReadfromDeviceWebservice:(NSMutableDictionary *)responseArray];
    }
    else
    {
        [NSException raise:NSInternalInconsistencyException format:@"Delegate doesn't respond to serverResponseForCaptchaWebSerivce:"];
    }
}
-(void)failToReciveData
{
    if ([_delegate respondsToSelector:@selector(serverFailResponseForNoticeReadfromDeviceWebservice)])
    {
        [_delegate serverFailResponseForNoticeReadfromDeviceWebservice];
    }
}
#pragma mark - Delegate methods
- (id)delegate
{
	return  _delegate;
}

- (void)setDelegate:(id)new_delegate
{
	_delegate = new_delegate;
}


@end
