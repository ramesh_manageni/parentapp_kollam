//
//  AddCalenderEvents.m
//  AddCalendarEvents
//
//  Created by Chetan Zalake on 13/01/14.
//  Copyright (c) 2014 Chetan Zalake. All rights reserved.
//

#import "AddCalenderEvents.h"
#import "MBProgressHUD.h"

@interface AddCalenderEvents ()
{
    dispatch_queue_t backgroundQueue;
}
@property (nonatomic, strong) EKEventStore *eventStore;
@property (nonatomic, strong) EKCalendar *calendar;
@property (nonatomic, strong) NSString* eventTitle;
@property (nonatomic, strong) NSString* descriptionNote;
@property (nonatomic, strong) NSArray* absentDate;

@end

@implementation AddCalenderEvents



- (void) addEventsToCalender: (NSString*)eventTitle Description: (NSString *)descriptionNote Date: (NSArray*)absentDate
{
    _eventTitle = eventTitle;
    _descriptionNote = descriptionNote;
    _absentDate = absentDate;

    backgroundQueue = dispatch_queue_create("com.tinytapps.bgqueue", NULL);
    
    _eventStore = [[EKEventStore alloc] init] ;
    
    [self checkEventStoreAccessForCalendar];
}

#pragma mark -
#pragma mark Access Calendar

// Check the authorization status of our application for Calendar
-(void)checkEventStoreAccessForCalendar
{
    EKAuthorizationStatus status = [EKEventStore authorizationStatusForEntityType:EKEntityTypeEvent];
    
    switch (status)
    {
            // Update our UI if the user has granted access to their Calendar
        case EKAuthorizationStatusAuthorized:
            NSLog(@"Granted");
            [self accessGrantedForCalendar];
            break;
            // Prompt the user for access to Calendar if there is no definitive answer
        case EKAuthorizationStatusNotDetermined:
            NSLog(@"Grant");
            [self requestCalendarAccess];
            break;
            // Display a message if the user has denied or restricted access to Calendar
        case EKAuthorizationStatusDenied:
        case EKAuthorizationStatusRestricted:
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Privacy Warning" message:@"Permission was not granted for Calendar"
                                                           delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
            [alert show];
        }
            break;
        default:
            break;
    }
}

-(void)requestCalendarAccess
{
    [_eventStore requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error)
     {
         if (granted)
         {
             //    RootViewController * __weak weakSelf = self;
             // Let's ensure that our code will be executed from the main queue
             dispatch_async(dispatch_get_main_queue(), ^{
                 // The user has granted access to their Calendar; let's populate our UI with all events occuring in the next 24 hours.
                 [self accessGrantedForCalendar];
                 NSLog(@"Granted");
             });
         }
     }];
}


-(void) accessGrantedForCalendar
{
   // EKEventStore *eventStore = [[EKEventStore alloc] init];
    NSError *error = nil;
    EKCalendar *calendar = [EKCalendar calendarForEntityType:EKSourceTypeLocal eventStore:_eventStore];
    calendar.title = @"ParentAppCal";
    calendar.CGColor = [UIColor redColor].CGColor;
    BOOL result=NO;
    
    
    
   // EKSource *defaultSource = [_eventStore defaultCalendarForNewEvents].source;
    NSLog(@"Sources : %@",_eventStore.sources);
    EKSource *theSource = nil;
    
    for (EKSource *source in _eventStore.sources) {
        if (source.sourceType == EKSourceTypeLocal) {
            
            NSLog(@"Source : %@",source);
            theSource = source;
            break;
        }
    }
    
    if(_eventStore.sources == NULL)
    {
        
    }
    
    if (theSource) {
        calendar.source = theSource;
        
        for (EKCalendar *thisCalendar in [_eventStore calendarsForEntityType: EKEntityTypeEvent]) {
            EKCalendarType type = thisCalendar.type;
            
            NSLog(@"Type : %u Title : %@",type,thisCalendar.title);
            
            if([thisCalendar.title isEqualToString:@"ParentAppCal"])
            {
                result = YES;
                break;
            }
            
            
            
        }
    }
    else {
        NSLog(@"Error: Local source not available");
        return;
    }
    
    if(result == NO)
    {
        result = [_eventStore saveCalendar:calendar commit:YES error:&error];
    }
    
    
    
    
    if (result) {
        NSLog(@"Saved calendar to event store.");
        //      calendarIdentifier = calendar.calendarIdentifier;
    } else {
        NSLog(@"Error saving calendar: %@.", error);
    }
    
    
    
    
    //Adding Events
    
    if ([_eventStore respondsToSelector:@selector(requestAccessToEntityType:completion:)]){
        [_eventStore requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error)
         {
             if (!granted) { return; }
             NSLog(@"GRANTED: %c", granted);
             for (EKSource *source in _eventStore.sources)
             {
                 if (source.sourceType == EKSourceTypeLocal)
                 {
                     NSLog(@"I found it: %@",source);
                     
                     for (EKCalendar *thisCalendar in [_eventStore calendarsForEntityType: EKEntityTypeEvent]) {
                         EKCalendarType type = thisCalendar.type;
                         
                         NSLog(@"Type : %u Title : %@",type,thisCalendar.title);
                         
                         if([thisCalendar.title isEqualToString:@"ParentAppCal"])
                         {
                             
                          /*
                            __block int i;
                             
                             
                             dispatch_sync(backgroundQueue, ^(void) {
                                 for(i=0; i<_absentDate.count ;i++)
                                 {
                                     NSLog(@"i = %i",i);
                                     
                                     EKEvent *event = [EKEvent eventWithEventStore:_eventStore];
                                     event.title = @"Absent Notice";
                                     // NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
                                     NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
                                     [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                                     [formatter setTimeZone:[NSTimeZone systemTimeZone]];
                                     // [formatter setCalendar:gregorianCalendar];
                                     NSMutableString *dateStart1 = [[NSMutableString alloc]init];
                                     
                                     [dateStart1 appendString:[[_absentDate objectAtIndex:i] valueForKey:@"date_absent"]];//[formatter stringFromDate:date];
                                     NSLog(@"Date %@",dateStart1);
                                     
                                     [dateStart1 appendString:@" 08:00:00"];
                                     
                                     
                                     event.notes = [NSString stringWithFormat:@"%@ was absent",_descriptionNote];
                                     
                                     //  EKAlarm *reminder = [EKAlarm alarmWithRelativeOffset:-60];
                                     //  [event addAlarm:reminder];
                                     NSDate *dateStart =[formatter dateFromString:dateStart1] ;
                                     event.startDate = dateStart; //today
                                     event.endDate = [event.startDate dateByAddingTimeInterval:60*60*9];  //set 9 hour
                                     [event setCalendar:thisCalendar];
                                     __block NSError *err = nil;
                                     
                                    [_eventStore saveEvent:event span:EKSpanThisEvent commit:YES error:&err];
                                     
                                     
                                     
                                     
                                     NSString *savedEventId = event.eventIdentifier;  //this is so you can access this event later
                                     NSLog(@"Event Id :%@ Date:%@",savedEventId,dateStart);
                                     
                                     event = nil;
                                     
                                     

                                 }
                             });
                             
                             dispatch_async(dispatch_get_main_queue(), ^(void) {
                                 if(106 == _absentDate.count)
                                 {
                                      [NSThread sleepForTimeInterval:5.0];
                                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"calshow://"]];;
                                 }
                                 
                             });
                           */
                             int count;
                             for(count=0; count<_absentDate.count ;count++)
                                 {
                                     NSLog(@"i = %i",count);
                                     
                                 EKEvent *event = [EKEvent eventWithEventStore:_eventStore];
                                 event.title = @"Absent Notice";
                                // NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
                                 NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
                                [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                                [formatter setTimeZone:[NSTimeZone systemTimeZone]];
                                    // [formatter setCalendar:gregorianCalendar];
                                     NSMutableString *dateStart1 = [[NSMutableString alloc]init];
                                     
                                     //[dateStart1 appendString:[[_absentDate objectAtIndex:count] valueForKey:@"date_absent"]];//[formatter stringFromDate:date];
                                     NSLog(@"Date %@",dateStart1);
                                     
                                     [dateStart1 appendString:@" 08:00:00"];
                                
                                 
                                 event.notes = [NSString stringWithFormat:@"%@ was absent",_descriptionNote];
                                 
                               //  EKAlarm *reminder = [EKAlarm alarmWithRelativeOffset:-60];
                               //  [event addAlarm:reminder];
                                 NSDate *dateStart =[formatter dateFromString:dateStart1] ;
                                 event.startDate = dateStart; //today
                                 event.endDate = [event.startDate dateByAddingTimeInterval:60*60*9];  //set 9 hour
                                 [event setCalendar:thisCalendar];
                                  __block NSError *err = nil;
                                     
                                         [_eventStore saveEvent:event span:EKSpanThisEvent commit:YES error:&err];
                                     
                                     
                                 
                                     
                                 NSString *savedEventId = event.eventIdentifier;  //this is so you can access this event later
                                 NSLog(@"Event Id :%@ Date:%@",savedEventId,dateStart);
                                     
                                     event = nil;
                           
                                 }
                             
                             
                             if( count == _absentDate.count)
                             {
                                 [NSThread sleepForTimeInterval:5.0];
                                // [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"calshow://"]];;
                             }
                         }
                         
                     }
                     
                 }
             }
         }];
    }
    
    
    //[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"calshow://"]];
}


@end
