//
//  RegistrationWebservice.m
//  ParentApp
//
//  Created by Redbytes on 06/02/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "RegistrationWebservice.h"

#import "ConnectionManager.h"

@implementation RegistrationWebservice
-(id)init
{
	return self;
}


-(void)registerDeviceWithUsername:(NSString *)username password:(NSString *)password email:(NSString *)email contact:(NSString *)contact deviceOS:(NSString *)deviceOS deviceVenderId:(NSString *)deviceVenderId
{
    NSMutableString *strApplicationUUID=[[NSMutableString alloc]init];
    NSUUID *oNSUUID = [[UIDevice currentDevice] identifierForVendor];
    [strApplicationUUID setString:[oNSUUID UUIDString]];
    
    NSMutableDictionary *localDictionary=[[NSMutableDictionary alloc]init];
    
    if(email==nil)
    {
        email=@"";
    }
    if(contact==nil)
    {
        contact=@"";
    }
    if(username==nil)
    {
       username=@"";
    }
    if(password==nil)
    {
        // password=@"";
    }
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"apnsToken"]==nil)
    {
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"apnsToken"];
    }
   // [localDictionary setObject:username forKey:@"user_name"];
    //[localDictionary setObject:password forKey:@"password"];
    
    [localDictionary setObject:email forKey:@"user_email"];
    [localDictionary setObject:contact forKey:@"user_contact"];
    [localDictionary setObject:deviceOS forKey:@"device_os"];
    [localDictionary setObject:strApplicationUUID forKey:@"vendor_id"];
    [localDictionary setObject:username forKey:@"user_name"];
    [localDictionary setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"apnsToken"] forKey:@"device_token"];
    
    NSLog(@"Dictionary At Registration: %@",localDictionary);
    
    if (objConnectionManager!=nil)
        
    {
		objConnectionManager=nil;
	}
	objConnectionManager=[[ConnectionManager alloc]init];
    objConnectionManager.isRegistration=@"YES";
    
    [objConnectionManager getWebData:self commandName:@"register.php" Requestparameter:localDictionary];
}

#pragma mark connection manager delegate
- (void)receivedData:(NSArray *)responseArray
{
    if ([_delegate respondsToSelector:@selector(serverResponseForRegistrationWebservice:)])
    {
        [_delegate serverResponseForRegistrationWebservice:(NSMutableDictionary *)responseArray];
    }
    else
    {
        [NSException raise:NSInternalInconsistencyException format:@"Delegate doesn't respond to serverResponseForCaptchaWebSerivce:"];
    }
}
-(void)failToReciveData
{
    if ([_delegate respondsToSelector:@selector(serverFailResponseForRegistrationWebservice)])
    {
        [_delegate serverFailResponseForRegistrationWebservice];
    }
}
#pragma mark - Delegate methods
- (id)delegate
{
	return  _delegate;
}

- (void)setDelegate:(id)new_delegate
{
	_delegate = new_delegate;
}

@end
