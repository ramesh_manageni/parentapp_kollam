//
//  SentMessageDetail.h
//  ParentApp
//
//  Created by Redbytes on 29/08/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface SentMessageDetail : NSManagedObject

@property (nonatomic, retain) NSString * from;
@property (nonatomic, retain) NSString * message_id;
@property (nonatomic, retain) NSString * messgae_sent;
@property (nonatomic, retain) NSString * subject_sent_item;
@property (nonatomic, retain) NSString * to_address;
@property (nonatomic, retain) NSString * date_sent;
@property (nonatomic, retain) NSString * time_sent;

@end
