//
//  AttenCalenderVC.h
//  ParentApp
//
//  Created by Redbytes on 04/01/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>


#import "DXSemiViewControllerCategory.h"
#import "DXSubClassSemiViewController.h"
#import "DXSubclassSemiTableViewController.h"
@protocol MenuButtonCalenderAllViewDelegate <NSObject>

-(void)didSelectRowCalenderAllView:(NSInteger)rowcount;

@end

@interface AttenCalenderVC : UIViewController<SliderMenuButtonDelegate>
{
    int sundayCount;
    UIView *baseView;
    
    id _delegate;
    DXSubclassSemiTableViewController *semiLeft;

}
@property (strong, nonatomic) UIScrollView *scrollViewObject;
@property (weak, nonatomic) IBOutlet UIView *baseViewEvents;
@property (strong, nonatomic) IBOutlet UIView *eventShowView;

- (id)delegate;
- (void)setDelegate:(id)new_delegate;
@end
