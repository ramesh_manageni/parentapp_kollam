//
//  AttendanceViewController.m
//  ParentApp
//
//  Created by Redbytes on 03/01/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "AttendanceViewController.h"
#import "ChangePasswordViewController.h"
#import "Constant.h"
#import "AttendanceWebservice.h"
#import "AttendanceTableViewController.h"
#import "AddCalenderEvents.h"
#import "AppDelegate.h"

#import "Parent.h"
#import "StudentDetails.h"
#import "AttendanceDetail.h"
#import "NSData+Base64.h"
#import "EventDetails.h"

#import "AttenCalenderVC.h"


//SliderMenu

#import "DXSemiViewControllerCategory.h"
#import "DXSubClassSemiViewController.h"
#import "DXSubclassSemiTableViewController.h"
#import "MenuViewController.h"
@interface AttendanceViewController ()

@end

@implementation AttendanceViewController
@synthesize tableviewObject;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setTitle:@"Attendance"];
    NSLog(@"%@",studentArrayCoreData);
    NSLog(@"%@",parentArrayCoreData);
   // objAttendanceTableViewController=[[AttendanceTableViewController alloc]initWithNibName:@"AttendanceTableViewController" bundle:nil];
    
    //get total number of students in database
    
    
//    UIView *myView = [[UIView alloc] initWithFrame: CGRectMake(0, 0, 300, 30)];
//    UILabel *title = [[UILabel alloc] initWithFrame: CGRectMake(75, 0, 300, 30)];
//    
//    title.text = NSLocalizedString(@"Attendance", nil);
//    [title setTextColor:[UIColor whiteColor]];
//    [title setFont:[UIFont boldSystemFontOfSize:20.0]];
//    
//    [title setBackgroundColor:[UIColor clearColor]];
//    UIImage *image = [UIImage imageNamed:@"icon01.png"];
//    UIImageView *myImageView = [[UIImageView alloc] initWithImage:image];
//    
//    myImageView.frame = CGRectMake(45, 0, 30, 30);
//    myImageView.layer.cornerRadius = 5.0;
//    myImageView.layer.masksToBounds = YES;
//    myImageView.layer.borderColor = [UIColor lightGrayColor].CGColor;
//    myImageView.layer.borderWidth = 0.1;
//    
//    [myView addSubview:title];
//    [myView setBackgroundColor:[UIColor  clearColor]];
//    [myView addSubview:myImageView];
//    self.navigationItem.titleView = myView;
    
    self.navigationController.navigationBarHidden=NO;
    [self.navigationItem setHidesBackButton:YES];
    //self.navigationItem.rightBarButtonItem = [self addRightBarButtonItem];
    self.navigationItem.leftBarButtonItem = [self addLeftBarButtonItem];
    self.navigationItem.rightBarButtonItem = [self addRightBarButtonItem];

    // Do any additional setup after loading the view from its nib.
    [self.view setBackgroundColor:[UIColor blackColor]];

    
    UIScreen *mainScreen = [UIScreen mainScreen];
    CGFloat scale = ([mainScreen respondsToSelector:@selector(scale)] ? mainScreen.scale : 1.0f);
    CGFloat pixelHeight = (CGRectGetHeight(mainScreen.bounds) * scale);
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        if (scale == 2.0f)
        {
            if (pixelHeight == 960.0f)
            {
                tableviewObject=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, 320, 480-49) style:UITableViewStyleGrouped];
            }
            
            else if (pixelHeight == 1136.0f)
            {
                tableviewObject=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, 320, 568-49) style:UITableViewStyleGrouped];
            }
        }
    }

    [tableviewObject setBackgroundColor:[UIColor clearColor]];
    tableviewObject.separatorColor = [UIColor clearColor];
    tableviewObject.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    tableviewObject.delegate=self;
    tableviewObject.dataSource=self;
    [self.view addSubview:tableviewObject];
    [tableviewObject reloadData];
}

-(void) viewDidAppear:(BOOL)animated
{
    [ParseClass incrementView];
}

-(void)viewWillAppear:(BOOL)animated
{
    [tableviewObject reloadData];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(UIBarButtonItem *)addLeftBarButtonItem
{
    UIButton *backButton = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 30.0f, 30.0f)];
    UIImage *backImage = [UIImage imageNamed:@"back button icon.png"] ;
    [backButton setBackgroundImage:backImage  forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    return backButtonItem;
    
}
-(UIBarButtonItem *)addRightBarButtonItem
{
    UIButton *backButton = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 30.0f, 30.0f)];
    UIImage *backImage = [UIImage imageNamed:@"list button icon.png"] ;
    [backButton setBackgroundImage:backImage  forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(settingButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    return backButtonItem;
    
}
-(void)backButtonClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)settingButtonClicked
{
    if(semiLeft!=nil)
    {
        [semiLeft dismissSemi:nil];
    }
    semiLeft = [[DXSubclassSemiTableViewController alloc] init];
    semiLeft.delegate=self;
    semiLeft.semiTitleLabel.text = @"SemiLeftTableView";
     semiLeft.tableViewRowHeight = 80.0f/2;
    [semiLeft.view setBackgroundColor:[UIColor clearColor]];
    
    self.leftSemiViewController = semiLeft;
    
}

#pragma mark Table View Datasource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	//tableviewObject = (UITableView *)tableView;
    return 75;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
	return [studentArrayCoreData count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
	return 1;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    AttendanceCustomCell *cell = (AttendanceCustomCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[AttendanceCustomCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
		[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    
    // Configure the cell...
	
    //	cell.labelName.text=[nameArray objectAtIndex:indexPath.section];
    //    cell.labelAdminNumber.text=[adminNumberArray objectAtIndex:indexPath.section
    //                                ];
    //
//    cell.labelName.text=[[[responseArrayLogin objectAtIndex:indexPath.section] valueForKey:@"student_details"] valueForKey:@"local_name"];
//    cell.labelAdminNumber.text=[[[responseArrayLogin objectAtIndex:indexPath.section] valueForKey:@"student_details"] valueForKey:@"admission_no"];
//   
    
    cell.labelName.text=[[studentArrayCoreData objectAtIndex:indexPath.section] local_name];
    cell.labelAdminNumber.text=[[studentArrayCoreData objectAtIndex:indexPath.section] admission_no];
    
    NSMutableArray *tempArray=[[[NSUserDefaults standardUserDefaults] objectForKey:@"attendanceArray"] mutableCopy];

    for(int i=0;i<[tempArray count];i++)
    {
        if([[[tempArray objectAtIndex:i] objectForKey:@"stdId"] isEqualToString:[[studentArrayCoreData objectAtIndex:indexPath.section] id_student]])
        {
           // NSString *temp=@"%";
            cell.labelAttendance.text=[[tempArray objectAtIndex:i] objectForKey:@"count"];
            cell.labelAttendance.text=[NSString stringWithFormat:@"No. of absent days:%@",[[tempArray objectAtIndex:i] objectForKey:@"count"]];
            
        }
    }
    
    
   // cell.labelAttendance.text=@"Attendance:65%";
    
    if([[studentArrayCoreData objectAtIndex:indexPath.section] photo].length>0)
    {
        NSData *imageData = [NSData dataFromBase64String:[[studentArrayCoreData objectAtIndex:indexPath.section] photo]];
        cell.objImageViewStudent.image=[UIImage imageWithData:imageData];
    }
   

    return cell;
}

#pragma mark Table View Delegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //objAttendanceTableViewController.name=[[[responseArrayLogin objectAtIndex:indexPath.section] valueForKey:@"student_details"] valueForKey:@"local_name"];
    
    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
	[self.view addSubview:HUD];
	
	// Regiser for HUD callbacks so we can remove it from the window at the right time
	HUD.delegate = self;
	
	// Show the HUD while the provided method executes in a new thread
	//[HUD showWhileExecuting:@selector(myTask) onTarget:self withObject:nil animated:YES];
    
    delegate.HUD=HUD;
    [HUD show:YES];

    AttendanceWebservice *objAttendanceWebservice=[[AttendanceWebservice alloc]init];
    objAttendanceWebservice.delegate=self;
    [objAttendanceWebservice showAttendanceForUsername:[[parentArrayCoreData objectAtIndex:0] user_name] studentId:[[studentArrayCoreData objectAtIndex:indexPath.section] id_student]];
    //used stdName to display in cal...
    stdName=[[studentArrayCoreData objectAtIndex:indexPath.section] local_name];
    stdId=[[studentArrayCoreData objectAtIndex:indexPath.section] id_student];
}

#pragma mark AttendanceWebservice delegate
-(void)serverResponseForAttendanceWebservice:(NSArray*)responseArray
{
    [HUD hide:YES];
    if (responseArray!=nil)
    {
        //[self deleteAllObjects:@"AttendanceDetail"];
        [self loadData:responseArray];
        [self getEntityCount:@"StudentDetails"];
        [self getEventEntityCount:@"EventDetails"];
        NSLog(@"studentArray : %@",absentDatesArrayCoreData);
        
        AddCalenderEvents *objAddCalenderEvents=[[AddCalenderEvents alloc]init];
        [objAddCalenderEvents addEventsToCalender:@"Absent Notice" Description:stdName Date:datesArrayCoreData];
        AttenCalenderVC *objAttenCalenderVC=[[AttenCalenderVC alloc]init];
        objAttenCalenderVC.hidesBottomBarWhenPushed=NO;
        objAttenCalenderVC.delegate=self.delegate;
        
        [self.navigationController pushViewController:objAttenCalenderVC animated:NO];
    }
    else if(responseArray==nil)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Attendance not published yet" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
}
-(void)serverFailResponseForAttendanceWebservice
{
    [HUD hide:YES];
    
    [self getEntityCount:@"StudentDetails"];

    
    AttenCalenderVC *objAttenCalenderVC=[[AttenCalenderVC alloc]init];
    objAttenCalenderVC.hidesBottomBarWhenPushed=YES;
    objAttenCalenderVC.delegate=self.delegate;
    
    if([datesArrayCoreData count]>0)
    {
         [HUD hide:YES];
    
        [self.navigationController pushViewController:objAttenCalenderVC animated:NO];
     
        //  [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"calshow://"]];

    }
    else
    {
        [HUD hide:YES];
        
        if([isStudentDataAvailable isEqualToString:@"YES"])
       {

                 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Zero absent dates" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                 [alert show];
        }
        else if([isStudentDataAvailable isEqualToString:@"NO"])
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"No information is available" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];

        }
        
        [self.navigationController pushViewController:objAttenCalenderVC animated:NO];

    }
    
   // [objAddCalenderEvents addEventsToCalender:@"Absent Notice" Description:stdName Date:datesArrayCoreData];


}
- (void) deleteAllObjects: (NSString *) entityDescription
{
    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [delegate managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityDescription inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    NSError *error;
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    
    
    for (NSManagedObject *managedObject in items) {
    	[context deleteObject:managedObject];
    	NSLog(@"%@ object deleted",entityDescription);
    }
    if (![context save:&error]) {
    	NSLog(@"Error deleting %@ - error:%@",entityDescription,error);
    }
}

// by nihal
-(void)getEventEntityCount:(NSString *)entityName
{
    datesArrayCoreData=[[NSMutableArray alloc] init];
    datesArrayForCalender=[[NSMutableArray alloc]init];
    eventsArrayCoredata = [[NSMutableArray alloc] init];
    
    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [delegate managedObjectContext];
    
    NSFetchRequest *fetchReq = [[NSFetchRequest alloc]init];
    
    [fetchReq setEntity:[NSEntityDescription entityForName:entityName inManagedObjectContext:context]];
    NSError *error;
    
    NSMutableArray *resultArray = [[NSMutableArray alloc]initWithArray:[context executeFetchRequest:fetchReq error:&error]];
    if(error==nil)
    {
        for(EventDetails *eventDetailsObject in resultArray)
        {
            NSLog(@"%@",eventDetailsObject.eventDate);
            if (eventDetailsObject.eventDate != nil)
            {
                [datesArrayCoreData addObject:eventDetailsObject.eventDate];
                
                NSMutableDictionary *tempDict = [[NSMutableDictionary alloc] init];
                [tempDict setValue:eventDetailsObject.eventName forKey:@"eventName"];
                [tempDict setValue:eventDetailsObject.eventLocation forKey:@"eventLocaion"];
                [tempDict setValue:eventDetailsObject.eventStartTime forKey:@"eventStartTime"];
                [tempDict setValue:eventDetailsObject.eventEndTime forKey:@"eventEndTime"];
                [tempDict setValue:eventDetailsObject.eventSubject forKey:@"eventSubject"];
                [tempDict setValue:eventDetailsObject.eventDate forKey:@"eventDate"];
                
                if (tempDict != nil)
                {
                    [eventsArrayCoredata addObject:tempDict];
                }
            }
        }
        eventDatesArrayCoredata = datesArrayCoreData;
    }
}

-(void)getEntityCount:(NSString *)entityName
{
    entityCount = 0;
    
    datesArrayCoreData=[[NSMutableArray alloc] init];
    datesArrayForCalender=[[NSMutableArray alloc]init];
    
    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [delegate managedObjectContext];
    
    NSFetchRequest *fetchReq = [[NSFetchRequest alloc]init];
    
    [fetchReq setEntity:[NSEntityDescription entityForName:entityName inManagedObjectContext:context]];
    NSError *error;
    
    NSMutableArray *resultArray = [[NSMutableArray alloc]initWithArray:[context executeFetchRequest:fetchReq error:&error]];
    if(error==nil)
    {
        NSSet *arr;
        for(StudentDetails *stdobject in resultArray)
        {
           if([stdobject.id_student isEqualToString:stdId])
           {
               arr=stdobject.studentToAttendance;
               NSLog(@"SET = %@",[arr allObjects]);
               //For displaying proper Alert at offline.
               isStudentDataAvailable=@"YES";
               
               for(AttendanceDetail *dates in [arr allObjects])
               {
                   NSMutableDictionary *tempDict=[[NSMutableDictionary alloc]init];
                   if (dates.date_absent != nil)
                   {
                       [tempDict setObject:dates.date_absent forKey:@"date_absent"];
                       [datesArrayCoreData addObject:tempDict];
                       [datesArrayForCalender addObject:dates.date_absent];
                       NSLog(@"Dates: %@",dates.date_absent);
                   }
               }
               absentDatesArrayCoreData=datesArrayForCalender;
               
/***************************************ATTENDANCE CPERCENTAGE*******************************************************/
               int result=(int)[datesArrayCoreData count];
               NSString *string = [NSString stringWithFormat:@"%d", result];

               NSMutableDictionary *attenPercentage=[[NSMutableDictionary alloc] init];
               [attenPercentage setObject:string forKey:@"count"];
               [attenPercentage setObject:stdId forKey:@"stdId"];
               if([[NSUserDefaults standardUserDefaults] valueForKey:@"isInitializedAttendanceArray"]==nil)
               {
                  NSMutableArray *attendanceArray=[[NSMutableArray alloc] init];
                   [[NSUserDefaults standardUserDefaults] setValue:@"YES" forKey:@"isInitializedAttendanceArray"];
                   
                   [[NSUserDefaults standardUserDefaults] setObject:attendanceArray forKey:@"attendanceArray"];
                   [[NSUserDefaults standardUserDefaults] synchronize];
                   
               }
               NSMutableArray *tempArray=[[[NSUserDefaults standardUserDefaults] objectForKey:@"attendanceArray"] mutableCopy];
               
               [tempArray addObject:attenPercentage];
               [[NSUserDefaults standardUserDefaults] setObject:tempArray forKey:@"attendanceArray"];
               [[NSUserDefaults standardUserDefaults] synchronize];

/********************************************************************************************************************/
              //  NSLog(@"%@ %lu std_id:%@",datesArrayForCalender,(unsigned long)datesArrayForCalender.count,stdId);
               break;
           }
//            NSMutableDictionary *tempDict=[[NSMutableDictionary alloc]init];
//            [tempDict setObject:dates.date_absent forKey:@"date_absent"];
//            [datesArrayCoreData addObject:tempDict];
//            [datesArrayForCalender addObject:dates.date_absent];
//            NSLog(@"Dates: %@",dates.date_absent);
            
        }
        
        
    }
    
}
-(int)calculateAttendancePercentage:(int)count
{
    int result;
    int presentDays;
    presentDays=300-count;
    result=(presentDays/300)*100;
    NSLog(@"RESULT ATTENDA:%d",result);

    return count;
}

//////// made changes by nihal/////////
-(void)loadData:response
{
    StudentDetails *std=[self getStudentEntityWithId];
    
    NSArray *eventsArray = [response objectForKey:@"Events"];
    NSLog(@"%@",eventsArray);
    
    NSArray *dateAbsentArray = [response objectForKey:@"attendence"];
    NSLog(@"%@",dateAbsentArray);

    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [delegate managedObjectContext];
    
    AttendanceDetail *dates = [NSEntityDescription insertNewObjectForEntityForName:@"AttendanceDetail" inManagedObjectContext:context];
    NSDictionary *dateAbsentDict = [[NSDictionary alloc] init];
    if(dateAbsentArray != nil)
    {
        for (dateAbsentDict in dateAbsentArray)
        {
            if ([dateAbsentDict objectForKey:@"date_absent"] != nil)
            {
                dates.date_absent = [dateAbsentDict objectForKey:@"date_absent"];
            }
        }
        if (dates != nil)
        {
            [std addStudentToAttendanceObject:dates];
        }
        dates = nil;
    }
    
    //remove all objects from Event table coredata
    NSFetchRequest * allEvents = [[NSFetchRequest alloc] init];
    [allEvents setEntity:[NSEntityDescription entityForName:@"EventDetails" inManagedObjectContext:context]];
    //[allEvents setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * cars = [context executeFetchRequest:allEvents error:&error];
    //error handling goes here
    for (NSManagedObject * car in cars) {
        [context deleteObject:car];
    }
    [context save:&error];
    ////////////////////////////////////////////////////
    
    NSDictionary *eventDict = [[NSDictionary alloc] init];
    if(eventsArray != nil)
    {
        for (eventDict in eventsArray)
        {
            EventDetails *dates1 = [NSEntityDescription insertNewObjectForEntityForName:@"EventDetails" inManagedObjectContext:context];
            dates1.eventDate = [eventDict objectForKey:@"start_date"];
            dates1.eventLocation = [eventDict objectForKey:@"location"];
            dates1.eventName = [eventDict objectForKey:@"description"];
            dates1.eventStartTime = [eventDict objectForKey:@"start_time"];
            dates1.eventEndTime = [eventDict objectForKey:@"end_time"];
            dates1.eventSubject = [eventDict objectForKey:@"subject"];
            
            NSError *err;
            [context save:&err];
            if(! [context save:&err] ){
                NSLog(@"Cannot save data: %@", [err localizedDescription]);
            }
        }
    }

    
//    for(int i=0;i<[response count];i++)
//    {
//        
//        AttendanceDetail *dates = [NSEntityDescription insertNewObjectForEntityForName:@"AttendanceDetail" inManagedObjectContext:context];
//
//        if([[response objectAtIndex:i] valueForKey:@"date_absent"]!=(NSString *)[NSNull null])
//        {
//            dates.date_absent=[[response objectAtIndex:i] valueForKey:@"date_absent"];
//        }
//        
//        [std addStudentToAttendanceObject:dates];
//        dates=nil;
//
//        
//    }
}
-(StudentDetails *)getStudentEntityWithId
{
    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [delegate managedObjectContext];
    
    NSFetchRequest *fetchReq = [[NSFetchRequest alloc]init];
    
    [fetchReq setEntity:[NSEntityDescription entityForName:@"StudentDetails" inManagedObjectContext:context]];
    NSError *error;
    
    NSMutableArray *resultArray = [[NSMutableArray alloc]initWithArray:[context executeFetchRequest:fetchReq error:&error]];
    if(error==nil)
    {
        for(StudentDetails *stdDetails in resultArray)
        {
            if([stdDetails.id_student isEqualToString:stdId])
            {
                //[stdDetails removeStudentToAttendance:stdDetails.studentToAttendance];
                return stdDetails;
            }
        }
       
    }

    return nil;
}

#pragma mark SliderMenuDelegate
-(void)didSelectSliderMenuButton:(NSInteger)rowcount
{
   // NSLog(@"ROWCOUNT=== %d",rowcount);
    if(rowcount==0)
    {
        [self.navigationController popToViewController:objHomeViewController animated:YES];
        
    }
    else
    {
        [self.navigationController popToViewController:objHomeViewController animated:NO];
        
    }
    if ([_delegate respondsToSelector:@selector(didSelectRow:)])
    {
        [_delegate didSelectRow:rowcount];
    }

}

#pragma mark Delegate

- (id)delegate
{
	return  _delegate;
}

- (void)setDelegate:(id)new_delegate
{
	_delegate = new_delegate;
}
@end
