//
//  AttendanceWebservice.m
//  ParentApp
//
//  Created by Redbytes on 07/01/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "AttendanceWebservice.h"
#import "ConnectionManager.h"

@implementation AttendanceWebservice

-(id)init
{
	return self;
}
-(void)showAttendanceForUsername:(NSString *)username studentId:(NSString *)studentId
{
    //
//    NSDateFormatter *dateFormatterTemp = [[NSDateFormatter alloc] init];
//    [dateFormatterTemp setDateFormat:@"yyyy-MM-dd"];
//    
//   NSDate *currentDate= [dateFormatterTemp dateFromString:[NSString stringWithFormat:@"%@-02-01",yearString]];
    
    student_Id=studentId;
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    currentDate = [formatter stringFromDate:[NSDate date]];
    NSLog(@".............%@",currentDate);

    lastUpdaeDateForAttendace=[NSUserDefaults standardUserDefaults];
    
    currentdateFromUserdefaults=[lastUpdaeDateForAttendace valueForKey:[NSString stringWithFormat:@"last_update_date_Attendance_%@",student_Id]];

    NSLog(@"DATE currentdateFromUserdefaults : %@",currentdateFromUserdefaults);
    
    NSMutableDictionary *localDictionary=[[NSMutableDictionary alloc]init];
    
    
    NSMutableString *strApplicationUUID=[[NSMutableString alloc]init];
    NSUUID *oNSUUID = [[UIDevice currentDevice] identifierForVendor];
    [strApplicationUUID setString:[oNSUUID UUIDString]];
    [localDictionary setObject:strApplicationUUID forKey:@"device_id"];

    [localDictionary setObject:username forKey:@"username"];
    [localDictionary setObject:studentId forKey:@"student_id"];
    if(currentdateFromUserdefaults==nil)
    {
        currentdateFromUserdefaults=@"";
    }
    [localDictionary setObject:currentdateFromUserdefaults forKey:@"last_update_date"];
    
   
   
    
    
    

    NSLog(@"Dict: %@",localDictionary);
    
    if (objConnectionManager!=nil)
    {
		objConnectionManager=nil;
	}
	objConnectionManager=[[ConnectionManager alloc]init];
    
    [objConnectionManager getWebData:self commandName:@"wsattendance" Requestparameter:localDictionary];
}

#pragma mark connection manager delegate
- (void)receivedData:(NSArray *)responseArray
{
    NSLog(@"Dict: %@",responseArray);
    [lastUpdaeDateForAttendace setValue:currentDate forKey:[NSString stringWithFormat:@"last_update_date_Attendance_%@",student_Id]];

    if ([_delegate respondsToSelector:@selector(serverResponseForAttendanceWebservice:)])
    {
        [_delegate serverResponseForAttendanceWebservice:responseArray];
    }
    else
    {
        [NSException raise:NSInternalInconsistencyException format:@"Delegate doesn't respond to serverResponseForCaptchaWebSerivce:"];
    }
}
-(void)failToReciveData
{
    if ([_delegate respondsToSelector:@selector(serverFailResponseForAttendanceWebservice)])
    {
        [_delegate serverFailResponseForAttendanceWebservice];
    }
}
#pragma mark - Delegate methods
- (id)delegate
{
	return  _delegate;
}

- (void)setDelegate:(id)new_delegate
{
	_delegate = new_delegate;
}



@end
