//
//  readInboxMessage.h
//  ParentApp
//
//  Created by Redbytes on 22/08/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ConnectionManager.h"


@protocol ReadInboxMessageDelegate <NSObject>

-(void)serverResponseForReadInboxMessage:(NSArray *)responseArray;
-(void)serverFailResponseForIReadInboxMessage;

@end
//********************************************************************

@class ConnectionManager;

@interface ReadInboxMessage : NSObject
{
    id _delegate;
    NSMutableDictionary *resultDictionary;
    ConnectionManager *objConnectionManager;
    
}

- (id)delegate;
- (void)setDelegate:(id)new_delegate;
-(void)readInboxMessageWithUsername:(NSString *)username pmId:(NSString *)pmID;

@end


