//
//  ForgotPasswordWebservice.m
//  ParentApp
//
//  Created by Redbytes on 08/02/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "ForgotPasswordWebservice.h"

#import "ConnectionManager.h"

@implementation ForgotPasswordWebservice
-(id)init
{
	return self;
}


-(void)ForgotPasswordForUsername:(NSString *)username withEmailId:(NSString *)email
{
    NSMutableDictionary *localDictionary=[[NSMutableDictionary alloc]init];
    
    NSMutableString *strApplicationUUID=[[NSMutableString alloc]init];
    NSUUID *oNSUUID = [[UIDevice currentDevice] identifierForVendor];
    [strApplicationUUID setString:[oNSUUID UUIDString]];
    
    [localDictionary setObject:username forKey:@"username"];
    [localDictionary setObject:email forKey:@"email"];
    [localDictionary setObject:strApplicationUUID forKey:@"device_id"];
    
    NSLog(@"Dict: %@",localDictionary);
    
    if (objConnectionManager!=nil)
    {
		objConnectionManager=nil;
	}
	objConnectionManager=[[ConnectionManager alloc]init];
    //  objConnectionManager.isSBTracker=@"YES";
    
    [objConnectionManager getWebData:self commandName:@"wsforgotpass" Requestparameter:localDictionary];

}

#pragma mark connection manager delegate
- (void)receivedData:(NSArray *)responseArray
{
    if ([_delegate respondsToSelector:@selector(serverResponseForForgotPasswordWebservice:)])
    {
        [_delegate serverResponseForForgotPasswordWebservice:(NSMutableDictionary *)responseArray];
    }
    else
    {
        [NSException raise:NSInternalInconsistencyException format:@"Delegate doesn't respond to serverResponseForCaptchaWebSerivce:"];
    }
}
-(void)failToReciveData
{
    if ([_delegate respondsToSelector:@selector(serverFailResponseForForgotPasswordWebservice)])
    {
        [_delegate serverFailResponseForForgotPasswordWebservice];
    }
}
#pragma mark - Delegate methods
- (id)delegate
{
	return  _delegate;
}

- (void)setDelegate:(id)new_delegate
{
	_delegate = new_delegate;
}

@end
