//
//  StudentResult.m
//  ParentApp
//
//  Created by Redbytes on 05/02/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "StudentResult.h"
#import "MarkDetails.h"
#import "StudentDetails.h"


@implementation StudentResult

@dynamic exam_code;
@dynamic exam_id;
@dynamic result_date;
@dynamic resultToMarks;
@dynamic resultToStudent;

@end
