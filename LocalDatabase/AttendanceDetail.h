//
//  AttendanceDetail.h
//  ParentApp
//
//  Created by Redbytes on 05/02/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class StudentDetails;

@interface AttendanceDetail : NSManagedObject

@property (nonatomic, retain) NSString * date_absent;
@property (nonatomic, retain) StudentDetails *attendanceToStudent;

@end
