//
//  AppDelegate.m
//  ParentApp
//
//  Created by Redbytes on 02/01/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "AppDelegate.h"
#import "Constant.h"
#import "Reachability.h"

//#import "TestFlight.h"
@implementation AppDelegate
@synthesize objLoginViewController;
@synthesize objNvc;
@synthesize HUD;
@synthesize objTabBarController,tabbarURLString;

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    
    //Nihal
    [Parse setApplicationId:@"8POePRuWWFdbWXY3f6N8lkdWZh4pwNdLwaAr3svT"
                  clientKey:@"oT5w8cjRGbWXrvUDYlKond55G5ZDNkIZeLpQD4OX"];
    
    countForSliderMenu=0;
    CountForSliderMenu=countForSliderMenu;
    
    //[TestFlight takeOff:@"7dc7daca-60f4-4253-aa71-0f11c2cc9172"];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
   
    // start of your application:didFinishLaunchingWithOptions // ...
    
    // The rest of your application:didFinishLaunchingWithOptions method// ...
    
    //[GMSServices provideAPIKey:@"AIzaSyCxqShJAFpLR3s_wi33A_IAc-bpJz8GKyc"];
 
    
    // PN
    [application registerForRemoteNotificationTypes:UIRemoteNotificationTypeBadge|UIRemoteNotificationTypeAlert|UIRemoteNotificationTypeSound];
    
    //rEMOVE bADGE FOR App.
    application.applicationIconBadgeNumber = 0;
    //[[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    
    objTabBarController = [[UITabBarController alloc] init];
    objTabBarController.delegate=self;
    
    UIColor *color1 = [self changeBgTo:@"#717171"];
    [objTabBarController.tabBar setBarTintColor:color1];
   // UIColor *color2 = [UIColor colorWithWhite:0 alpha:0.9];
    [objTabBarController.tabBar setBackgroundColor:color1];
    
    objLoginViewController=[[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
    self.objNvc=[[UINavigationController alloc] initWithRootViewController:objLoginViewController];
    
    NSArray* controllers = [NSArray arrayWithObjects: self.objNvc, nil];
    [objTabBarController setViewControllers:controllers animated:YES];
    
    // Assigning Tabbar Image from Parse Server
    Reachability *internetReachability = [Reachability reachabilityForInternetConnection];
    [internetReachability startNotifier];
    [self updateInterfaceWithReachability:internetReachability];
    
    UITapGestureRecognizer *tabGesture = [[UITapGestureRecognizer alloc] init];
    tabGesture.numberOfTapsRequired = 1;
    tabGesture.numberOfTouchesRequired = 1;
    [tabGesture addTarget:self action:@selector(tabbarClicked:)];
    [[UITabBar appearance] addGestureRecognizer:tabGesture];

    NSDictionary *remoteNotif = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    if([[NSUserDefaults standardUserDefaults] valueForKey:@"isInitialized"]==nil)
    {
         notificationArray=[[NSMutableArray alloc] init];
        [[NSUserDefaults standardUserDefaults] setValue:@"YES" forKey:@"isInitialized"];
        
        [[NSUserDefaults standardUserDefaults] setObject:notificationArray forKey:@"notificationArray"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    NSLog(@"REmoTE Notification: %@  NOTIFICATION ARRAY:-%@",remoteNotif.description,[[NSUserDefaults standardUserDefaults] objectForKey:@"notificationArray"]);

    if(remoteNotif)
    {
        //Handle remote notification
        [self handlePushNotification:remoteNotif];
       
    }
    return YES;
}

- (void)updateInterfaceWithReachability:(Reachability *)reachability
{
    NetworkStatus netStatus = [reachability currentReachabilityStatus];
    switch (netStatus)
    {
        case NotReachable:
        {
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0];
            NSString *fileName = [documentsDirectory stringByAppendingFormat:@"/tabbarImage.jpg"];
            
            UIImage *tabbarImage=[UIImage imageWithData:[NSData dataWithContentsOfFile:fileName]];
            [[UITabBar appearance] setBackgroundImage:tabbarImage];
            
            [self.window addSubview:objTabBarController.view];
            [self.window makeKeyAndVisible];

            break;
        }
        case ReachableViaWWAN:
        {
            PFQuery *query = [PFQuery queryWithClassName:@"Kollam"];
            [query getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
                if (!error)
                {
                    tabbarURLString = [object objectForKey:@"tabLink"];
                    PFFile *imageFile = [object objectForKey:@"tabImage"];
                    [imageFile getDataInBackgroundWithBlock:^(NSData *dataImage, NSError *error)
                     {
                         if (!error)
                         {
                             UIImage *tabbarImage = [UIImage imageWithData:dataImage];
                             [[UITabBar appearance] setBackgroundImage:tabbarImage];
                             
                             [self.window addSubview:objTabBarController.view];
                             [self.window makeKeyAndVisible];
                             
                             NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                             NSString *documentsDirectory = [paths objectAtIndex:0];
                             NSString *fileName = [documentsDirectory stringByAppendingFormat:@"/tabbarImage.jpg"];
                             [dataImage writeToFile:fileName atomically:YES];
                         }
                     }];
                }
            }];
            break;
        }
        case ReachableViaWiFi:
        {
            PFQuery *query = [PFQuery queryWithClassName:@"Kollam"];
            [query getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
                if (!error)
                {
                    tabbarURLString = [object objectForKey:@"tabLink"];
                    PFFile *imageFile = [object objectForKey:@"tabImage"];
                    [imageFile getDataInBackgroundWithBlock:^(NSData *dataImage, NSError *error)
                     {
                         if (!error)
                         {
                             UIImage *tabbarImage = [UIImage imageWithData:dataImage];
                             [[UITabBar appearance] setBackgroundImage:tabbarImage];
                             
                             [self.window addSubview:objTabBarController.view];
                             [self.window makeKeyAndVisible];
                             
                             NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                             NSString *documentsDirectory = [paths objectAtIndex:0];
                             NSString *fileName = [documentsDirectory stringByAppendingFormat:@"/tabbarImage.jpg"];
                             [dataImage writeToFile:fileName atomically:YES];
                         }
                     }];
                }
            }];
            break;
        }
        default:
        {
            break;
        }
    }
}

-(void)tabbarClicked:(id)sender
{
    [ParseClass incrementClick];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:tabbarURLString]];
}

-(void)handlePushNotification:(NSDictionary *)dict
{
    NSMutableArray *tempArray=[[[NSUserDefaults standardUserDefaults] objectForKey:@"notificationArray"] mutableCopy];
    if([tempArray count]>=10)
    {
        [tempArray removeLastObject];
    }
    [tempArray insertObject:dict atIndex:0];
    [[NSUserDefaults standardUserDefaults] setObject:tempArray forKey:@"notificationArray"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    NSLog(@"NOTIFICATION ARRAY: %@",[[NSUserDefaults standardUserDefaults] objectForKey:@"notificationArray"]);
}

-(UIColor *)changeBgTo:(NSString *)hexCode
{
    unsigned long red, green, blue;
    sscanf([hexCode UTF8String], "#%2lX%2lX%2lX", &red, &green, &blue);
    UIColor *color = [UIColor colorWithRed:red/255.0 green:green/255.0 blue:blue/255.0 alpha:0.99];
    return color;
}

- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController
{
    return NO;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [[delegate HUD] hide:YES];
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    //by nihal
    //PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    //[currentInstallation setDeviceTokenFromData:deviceToken];
    //[currentInstallation saveInBackground];

    
    NSString *deviceToken1 = [[deviceToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    deviceToken1 = [deviceToken1 stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSLog(@"devidce Token %@",deviceToken1);
    
        //NSLog(@"didRegisterForRemoteNotificationsWithDeviceToken: %@", deviceToken);
    
    
    NSString * token = [NSString stringWithFormat:@"%@", deviceToken];
    //Format token as you need:
    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
    token = [token stringByReplacingOccurrencesOfString:@">" withString:@""];
    token = [token stringByReplacingOccurrencesOfString:@"<" withString:@""];
    
    [[NSUserDefaults standardUserDefaults] setObject:token forKey:@"apnsToken"]; //save token to resend it if request fails
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"apnsTokenSentSuccessfully"]; // set flag for request status


  }

- (void)application:(UIApplication*)application didReceiveRemoteNotification:
(NSDictionary*)userInfo
{
    //Nihal
    //[PFPush handlePush:userInfo];
    
    NSLog(@"Received Notification");
    
    NSLog(@"remote notification: %@",[userInfo description]);
    NSDictionary *apsInfo = [userInfo objectForKey:@"aps"];
    
    NSString *alert = [apsInfo objectForKey:@"alert"];
    NSLog(@"Received Push Alert: %@", alert);
    
    NSString *sound = [apsInfo objectForKey:@"sound"];
    NSLog(@"Received Push Sound: %@", sound);
    
    NSString *badge = [apsInfo objectForKey:@"badge"];
    NSLog(@"Received Push Badge: %@", badge);
    
    //application.applicationIconBadgeNumber = 100;
    
    UIApplicationState state = [application applicationState];
    if (state == UIApplicationStateActive)
    {
        //What you want to do when your app was active and it got push notification
        [self handlePushNotification:userInfo];
    }
    else if (state == UIApplicationStateInactive)
    {
        //What you want to do when your app was in background and it got push notification
        [self handlePushNotification:userInfo];
    }
}

-(void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    NSLog(@"ERROR:- %@",error);

}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [[delegate HUD] hide:YES];
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    }

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    //[[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    application.applicationIconBadgeNumber = 0;
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    [self saveContext];
}

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"LocalDatabaseModel" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"LocalDatabaseModel.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}



@end
