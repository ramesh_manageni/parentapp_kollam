//
//  NoticeBoardCustomCell.h
//  ParentApp
//
//  Created by Redbytes on 14/02/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NoticeBoardCustomCell : UITableViewCell
{
    UILabel *NoticeBoardName;
    
}
@property(strong,nonatomic)UILabel *NoticeBoardName;
@property(strong,nonatomic)UIImageView *objImageViewBG;
@property(strong,nonatomic)UIImageView *objImageViewStudent;

@end