//
//  EventDetails.m
//  ParentApp
//
//  Created by RedBytes on 09/06/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "EventDetails.h"


@implementation EventDetails

@dynamic eventDate;
@dynamic eventEndTime;
@dynamic eventLocation;
@dynamic eventName;
@dynamic eventStartTime;
@dynamic eventSubject;

@end
