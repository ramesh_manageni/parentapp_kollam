//
//  SBMapWebservice.m
//  ParentApp
//
//  Created by Redbytes on 22/01/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "SBMapWebservice.h"

#import "ConnectionManager.h"

@implementation SBMapWebservice

-(id)init
{
	return self;
}


-(void)getMapDetailsForIMEI:(NSString *)imei_number
{
    NSMutableDictionary *localDictionary=[[NSMutableDictionary alloc]init];
    
    [localDictionary setObject:imei_number forKey:@"imei"];
    
    NSLog(@"Dict: %@",localDictionary);
    
    if (objConnectionManager!=nil)
    {
		objConnectionManager=nil;
	}
	objConnectionManager=[[ConnectionManager alloc]init];
    objConnectionManager.isSBTracker=@"YES";
    
    [objConnectionManager getWebData:self commandName:@"single" Requestparameter:localDictionary];

    
}

#pragma mark connection manager delegate
- (void)receivedData:(NSArray *)responseArray
{
    if ([_delegate respondsToSelector:@selector(serverResponseForSBMapWebservice:)])
    {
        [_delegate serverResponseForSBMapWebservice:(NSMutableDictionary *)responseArray];
    }
    else
    {
        [NSException raise:NSInternalInconsistencyException format:@"Delegate doesn't respond to serverResponseForCaptchaWebSerivce:"];
    }
}
-(void)failToReciveData
{
    if ([_delegate respondsToSelector:@selector(serverFailResponseForSBMapWebservice)])
    {
        [_delegate serverFailResponseForSBMapWebservice];
    }
}
#pragma mark - Delegate methods
- (id)delegate
{
	return  _delegate;
}

- (void)setDelegate:(id)new_delegate
{
	_delegate = new_delegate;
}



@end
