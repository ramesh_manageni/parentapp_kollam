//
//  FeedbackViewController.m
//  ParentApp
//
//  Created by Redbytes on 03/01/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "FeedbackViewController.h"
#import "ChangePasswordViewController.h"
#import "Constant.h"
#import "StudentDetails.h"
#import "Parent.h"

@interface FeedbackViewController ()
{
    UIPickerView *myPickerView;
    int teacher_subjectCount;
    UIButton* customButton;
}

@end

@implementation FeedbackViewController
@synthesize objTextEmail,objTextMessage,objTextName,objTextSubject,objTextTeacher,objTextFeedbackAbout,objScrollView,defaultLabel,myTableView,teacherNameArray,subjectsNameArray;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    selectedStudents=[[NSMutableArray alloc] init];
    
    [self setTitle:@"Feedback"];

    self.navigationController.navigationBarHidden=NO;
    [self.navigationItem setHidesBackButton:YES];
    //self.navigationItem.rightBarButtonItem = [self addRightBarButtonItem];
    self.navigationItem.leftBarButtonItem = [self addLeftBarButtonItem];
    self.navigationItem.rightBarButtonItem = [self addRightBarButtonItem];
    
    //Custom bar at top of view
    UIButton *backButton = [[UIButton alloc] initWithFrame: CGRectMake(15, 30, 30.0f, 30.0f)];
    UIImage *backImage = [UIImage imageNamed:@"back button icon.png"] ;
    [backButton setBackgroundImage:backImage  forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backButtonComposeClicked) forControlEvents:UIControlEventTouchUpInside];
    
    [self.customNavBar addSubview:backButton];
    [self.customNavBar setBackgroundColor:[UIColor darkGrayColor]];
    
    [self.objScrollView setScrollEnabled:YES];
    objTextMessage.delegate=self;
    objTextName.delegate=self;
    objTextEmail.delegate=self;
    objTextSubject.delegate=self;
    self.defaultLabel.delegate=self;
    //by nihal
    objTextFeedbackAbout.delegate = self;
    objTextTeacher.delegate = self;
    
    if ([objTextName respondsToSelector:@selector(setAttributedPlaceholder:)])
    {
        UIColor *color = [UIColor lightGrayColor];
        objTextName.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Student name" attributes:@{NSForegroundColorAttributeName: color}];
    }
//    if ([objTextEmail respondsToSelector:@selector(setAttributedPlaceholder:)])
//    {
//        UIColor *color = [UIColor lightGrayColor];
//        objTextEmail.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Email address" attributes:@{NSForegroundColorAttributeName: color}];
//        //objTextEmail.text = @"test@gmail.com";
//    }
    if ([objTextSubject respondsToSelector:@selector(setAttributedPlaceholder:)])
    {
        UIColor *color = [UIColor lightGrayColor];
        objTextSubject.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Select Topic" attributes:@{NSForegroundColorAttributeName: color}];
        objTextSubject.userInteractionEnabled = NO;
    }
    if ([self.defaultLabel respondsToSelector:@selector(setAttributedPlaceholder:)])
    {
        UIColor *color = [UIColor lightGrayColor];
        self.defaultLabel.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Message" attributes:@{NSForegroundColorAttributeName: color}];
    }
    if ([self.objTextTeacher respondsToSelector:@selector(setAttributedPlaceholder:)])
    {
        UIColor *color = [UIColor lightGrayColor];
        self.objTextTeacher.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Feedback About" attributes:@{NSForegroundColorAttributeName: color}];
    }
//    if ([objTextTeacher respondsToSelector:@selector(setAttributedPlaceholder:)])
//    {
//        UIColor *color = [UIColor lightGrayColor];
//        objTextTeacher.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Teacher Name" attributes:@{NSForegroundColorAttributeName: color}];
//    }
    
    myPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 200, 320, 200)];
    myPickerView.delegate = self;
    myPickerView.showsSelectionIndicator = YES;
    
    
    self.myTableView=[[UITableView alloc]initWithFrame:CGRectMake(5, 200, 300, 200) style:UITableViewStyleGrouped];
    self.myTableView.delegate=self;
    self.myTableView.dataSource=self;
    self.myTableView.showsVerticalScrollIndicator=YES;
    self.myTableView.backgroundColor=[UIColor clearColor];
    
    
    //objTextName.inputView = myPickerView;
    objTextName.inputView=self.myTableView;
    objTextSubject.inputView = myPickerView;
    
    UIToolbar *myToolbar = [[UIToolbar alloc] initWithFrame:
                            CGRectMake(0,0, 320, 44)]; //should code with variables to support view resizing
    UIBarButtonItem *doneButton =
    [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                  target:self action:@selector(inputAccessoryViewDidFinish)];
    
    UIBarButtonItem *doneButton1 =
    [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                  target:self action:@selector(inputAccessoryViewDidFinish1)];
    
    UIToolbar *myToolbar1 = [[UIToolbar alloc] initWithFrame:
                             CGRectMake(0,0, 320, 44)]; //should code with variables to support view resizing
   
    //using default text field delegate method here, here you could call
    //myTextField.resignFirstResponder to dismiss the views
  
    UIBarButtonItem *flexibleSpace =  [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];

    customButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [customButton setTitle:@"Select All" forState:UIControlStateNormal];
    [customButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [customButton addTarget:self action:@selector(selectAllButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [customButton sizeToFit];
    UIBarButtonItem* customBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:customButton];
    
    UIButton* customButton1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [customButton1 setTitle:@"Cancel" forState:UIControlStateNormal];
    [customButton1 setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    [customButton1 addTarget:self action:@selector(cancelPickerButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [customButton1 sizeToFit];
    UIBarButtonItem* customBarButtonItem1 = [[UIBarButtonItem alloc] initWithCustomView:customButton1];
    
    [myToolbar setItems:[NSArray arrayWithObjects:doneButton,flexibleSpace,customBarButtonItem, nil] animated:NO];
    [myToolbar1 setItems:[NSArray arrayWithObjects:doneButton1,flexibleSpace,customBarButtonItem1, nil] animated:NO];

    objTextName.inputAccessoryView = myToolbar;
    objTextSubject.inputAccessoryView = myToolbar1;
    
    if([[parentArrayCoreData objectAtIndex:0] email] != nil && [[parentArrayCoreData objectAtIndex:0] email].length > 0 )
    {
        objTextEmail.text=[[parentArrayCoreData objectAtIndex:0] email];
        [objTextEmail setEnabled:NO];
    }
    else
    {
        [objTextEmail setEnabled:YES];
    }
}

-(void) viewDidAppear:(BOOL)animated
{
    [ParseClass incrementView];
}

-(void)inputAccessoryViewDidFinish
{
   // [self clearTextFields];
    [self.view endEditing:YES];
    isSelectAll=@"NO";
    
    NSMutableString *str=[[NSMutableString alloc] init];
    for(int i=0;i<[selectedStudents count];i++)
    {
        [str appendFormat:@"(%d)%@ ",i+1,[[selectedStudents objectAtIndex:i] local_name]];
    }
    
    objTextName.text=str;
    //NSLog(@"Selected students %@",selectedStudents);
    
    if (selectedStudents.count == 1)
    {
        objTextSubject.text = nil;
        objTextTeacher.text = nil;
        objTextSubject.userInteractionEnabled = YES;
        TeacherWebservice *objectTeacherWebservice = [[TeacherWebservice alloc] init];
        objectTeacherWebservice.delegate = self;
        [objectTeacherWebservice showTeacherForUsername:[[parentArrayCoreData objectAtIndex:0] valueForKey:@"user_name"] studentId:[[selectedStudents objectAtIndex:0] id_student]];
    }
    else if (selectedStudents.count == 0)
    {
        objTextSubject.text = nil;
        objTextTeacher.text = nil;
        objTextSubject.userInteractionEnabled = NO;
    }
    else
    {
        objTextSubject.text = @"Not Allowed";
        objTextTeacher.text = @"Not Allowed";
        objTextSubject.userInteractionEnabled = NO;
    }
    
}

-(void)inputAccessoryViewDidFinish1
{
    [self.view endEditing:YES];
    if ([subjectsNameArray objectAtIndex:teacher_subjectCount] != nil)
    {
        objTextSubject.text= [subjectsNameArray objectAtIndex:teacher_subjectCount];

    }
    if ([teacherNameArray objectAtIndex:teacher_subjectCount] != (NSString *)[NSNull null])
    {
        objTextTeacher.text = [teacherNameArray objectAtIndex:teacher_subjectCount];
    }
    else
        objTextTeacher.text = @"Feedback About";
        //
}

-(void)selectAllButtonClicked:(UIButton *)sender
{
    if([sender.titleLabel.text isEqualToString:@"Select All"])
    {
        [sender setTitle:@"None" forState:UIControlStateNormal];
        [sender setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        isSelectAll=@"YES";

    }
    else
    {
        [sender setTitle:@"Select All" forState:UIControlStateNormal];
        [sender setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];

        isSelectAll=@"NO";

    }
    [myTableView reloadData];
}

-(void)cancelPickerButtonClicked:(UIButton *)sender
{
    [self.view endEditing:YES];
    [self clearTextFields];
}

#pragma mark UITableView Delegate And Datasource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	
    return 35;
	
	
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#pragma  rizwan
    tableView.separatorStyle=UITableViewCellSeparatorStyleSingleLine;
    
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
	return [studentArrayCoreData count];
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"FriendCellIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
        [cell setBackgroundColor:[UIColor clearColor]];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    cell.textLabel.textAlignment = NSTextAlignmentCenter;
    cell.textLabel.text=[[studentArrayCoreData objectAtIndex:indexPath.row] local_name];
    cell.textLabel.font = [UIFont fontWithName:@"BradleyHandITCTT-Bold" size:18.0];

    if([isSelectAll isEqualToString:@"YES"])
    {
        [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
        if ([selectedStudents containsObject:[studentArrayCoreData objectAtIndex:indexPath.row]])
        {
            [selectedStudents removeObject:[studentArrayCoreData objectAtIndex:indexPath.row]];
        }
        [selectedStudents addObject:[studentArrayCoreData objectAtIndex:indexPath.row]];
    }
    if([isSelectAll isEqualToString:@"NO"])
    {
        [cell setAccessoryType:UITableViewCellAccessoryNone];
        [selectedStudents removeAllObjects];
        
    }
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([tableView cellForRowAtIndexPath:indexPath].accessoryType == UITableViewCellAccessoryCheckmark)
    {
        [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryNone;
        if ([selectedStudents containsObject:[studentArrayCoreData objectAtIndex:indexPath.row]])
        {
            [selectedStudents removeObject:[studentArrayCoreData objectAtIndex:indexPath.row]];
            if(selectedStudents.count == 0)
            {
                [customButton setTitle:@"Select All" forState:UIControlStateNormal];
                [customButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
            }
        }
        
    }
    else
    {
        [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryCheckmark;
        [selectedStudents addObject:[studentArrayCoreData objectAtIndex:indexPath.row]];
        if(selectedStudents.count == studentArrayCoreData.count)
        {
            [customButton setTitle:@"None" forState:UIControlStateNormal];
            [customButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
            
        }
    }
}


#pragma mark UIPickerView Datasource and Delefgate

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    // Handle the selection
    teacher_subjectCount = (int)row;
}

// tell the picker how many rows are available for a given component
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
    return [subjectsNameArray count];
}

// tell the picker how many components it will have
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

// tell the picker the title for a given component
//- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
//    NSString *title;
//    title = [[studentArrayCoreData objectAtIndex:row] local_name];
//    
//    return title;
//}

// tell the picker the width of each row for a given component
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
    int sectionWidth = 300;
    
    return sectionWidth;
}
- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel* tView = (UILabel*)view;
    if (!tView){
        tView = [[UILabel alloc] init];
        tView.font = [UIFont fontWithName:@"BradleyHandITCTT-Bold" size:18.0];
        tView.text = [subjectsNameArray objectAtIndex:row];
        tView.textAlignment = NSTextAlignmentCenter;
    }
    // Fill the label text here
    return tView;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)submitButtonClicked:(id)sender
{
   if([self validateFields])
   {
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
	[self.view addSubview:HUD];
	
	// Regiser for HUD callbacks so we can remove it from the window at the right time
	HUD.delegate = self;
	
	// Show the HUD while the provided method executes in a new thread
	//[HUD showWhileExecuting:@selector(myTask) onTarget:self withObject:nil animated:YES];
    [HUD show:YES];

    
    FeedbackWebservice *objFeedbackWebservice=[[FeedbackWebservice alloc]init];
    objFeedbackWebservice.delegate=self;
    [objFeedbackWebservice sendFeedbackWithStudentName:objTextName.text email:objTextEmail.text subject:objTextSubject.text message:objTextMessage.text profileId:[[parentArrayCoreData objectAtIndex:0] valueForKey:@"profile_id"] username:[[parentArrayCoreData objectAtIndex:0] valueForKey:@"user_name"]];
    
    
    [self.view endEditing:YES];
    [objScrollView setContentOffset:CGPointMake(0,-60) animated:YES];

   }


}
-(void)clearTextFields
{
    objTextName.text=@"";
    //objTextEmail.text=@"";
    objTextSubject.text=@"";
    objTextMessage.text=@"";
    objTextTeacher.text=@"";
    objTextFeedbackAbout.text=@"";
    

    defaultLabel.hidden=NO;
    
    
}
-(UIBarButtonItem *)addLeftBarButtonItem
{
    UIButton *backButton = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 30.0f, 30.0f)];
    UIImage *backImage = [UIImage imageNamed:@"back button icon.png"] ;
    [backButton setBackgroundImage:backImage  forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    return backButtonItem;
    
}
-(UIBarButtonItem *)addRightBarButtonItem
{
    
    
    UIButton *backButton = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 30.0f, 30.0f)];
    UIImage *backImage = [UIImage imageNamed:@"list button icon.png"] ;
    [backButton setBackgroundImage:backImage  forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(settingButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    return backButtonItem;
    
}
// keshav
-(void)backButtonComposeClicked
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
}



-(void)backButtonClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)settingButtonClicked
{
    
    
    if(semiLeft!=nil)
    {
        [semiLeft dismissSemi:nil];
    }

    //[self.navigationController popToViewController:objHomeViewController animated:YES];
    semiLeft = [[DXSubclassSemiTableViewController alloc] init];
    semiLeft.delegate=self;
    
    semiLeft.semiTitleLabel.text = @"SemiLeftTableView";
    semiLeft.tableViewRowHeight = 80.0f/2;
    [semiLeft.view setBackgroundColor:[UIColor clearColor]];
    self.leftSemiViewController = semiLeft;
    
    
}
#pragma mark UITextView delegates

- (void)textViewDidBeginEditing:(UITextView *)textView
{
#pragma rizwan
    
    UIScreen *mainScreen = [UIScreen mainScreen];
    CGFloat scale = ([mainScreen respondsToSelector:@selector(scale)] ? mainScreen.scale : 1.0f);
    CGFloat pixelHeight = (CGRectGetHeight(mainScreen.bounds) * scale);
    
    if(objTextMessage==textView)
    {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
        {
            if (scale == 2.0f)
            {
                if (pixelHeight == 960.0f)
                {
                    [self.objScrollView setContentOffset:CGPointMake(0,230) animated:YES];
                    
                }
                
                else if (pixelHeight == 1136.0f)
                {
                    [self.objScrollView setContentOffset:CGPointMake(0,165) animated:YES];
                    
                }
                
            }
            
        }
        
    }
    

    //*************************
    
//    self.defaultLabel.hidden = YES;
//    [self.objScrollView setContentOffset:CGPointMake(0,130) animated:YES];
}

- (void)textViewDidChange:(UITextView *)txtView
{
    self.defaultLabel.hidden = ([txtView.text length] > 0);
}

- (void)textViewDidEndEditing:(UITextView *)txtView
{
    self.defaultLabel.hidden = ([txtView.text length] > 0);
}
- (BOOL)textView:(UITextView *)txtView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if( [text rangeOfCharacterFromSet:[NSCharacterSet newlineCharacterSet]].location == NSNotFound )
    {

        return YES;

    }
    [txtView resignFirstResponder];
    [self.objScrollView setContentOffset:CGPointMake(0,15) animated:YES];

    return NO;
}
#pragma mark UITextFeild delegates

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [textField setTextColor:[UIColor whiteColor]];
    UIScreen *mainScreen = [UIScreen mainScreen];
    CGFloat scale = ([mainScreen respondsToSelector:@selector(scale)] ? mainScreen.scale : 1.0f);
    CGFloat pixelHeight = (CGRectGetHeight(mainScreen.bounds) * scale);
    
    
    if(objTextTeacher==textField)
    {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
        {
            if (scale == 2.0f)
            {
                if (pixelHeight == 960.0f)
                {
                    [self.objScrollView setContentOffset:CGPointMake(0,190) animated:YES];
                    
                }
                
                else if (pixelHeight == 1136.0f)
                {
                    [self.objScrollView setContentOffset:CGPointMake(0,165) animated:YES];
                    
                }
                
            }
            
        }
        
    }
    
    
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    [self.objScrollView setContentOffset:CGPointMake(0,15) animated:YES];

    return YES;
}




#pragma mark FeedbackWebservice Delegate
-(void)serverResponseForFeedbackWebservice:(NSArray*)responseArray
{
    [HUD hide:YES];
   if (responseArray!=nil)
    {

        NSLog(@"Responce in FeebackVC: %@",[responseArray valueForKey:@"succmsg"]  );
       
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:[responseArray valueForKey:@"succmsg"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert setTag:1];
        [alert show];

        // NSLog(@"Server Responce: %@",responseArrayLogin);
    }
    else if(responseArray==nil)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Your feedback not sent" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        
        [alert show];

    }

    
}
-(void)serverFailResponseForFeedbackWebservice
{
    [HUD hide:YES];

    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Server Response Failed" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];


    
}
#pragma mark Alert View Delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    //[self.navigationController popViewControllerAnimated:YES];
    if(alertView.tag==1)
    {
     [self clearTextFields];
    }


}

-(BOOL)validateFields
{
    if (objTextName.text.length == 0 || objTextName == nil)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"NOTE" message:@"Please enter student name" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return NO;
        
    }
//    else if (objTextEmail.text.length == 0 || objTextEmail == nil)
//    {
//        
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"NOTE" message:@"Please enter email " delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
//        [alert show];
//        return NO;
//        
//        
//        
//    }

    else if (objTextSubject.text.length == 0 || objTextSubject == nil)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"NOTE" message:@"Please enter subject name" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        return NO;
        
    }
    else if (objTextTeacher.text.length == 0 || objTextTeacher == nil)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"NOTE" message:@"Please enter feedback about" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        return NO;
        
    }
    else if (objTextMessage.text.length == 0 || objTextMessage == nil)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"NOTE" message:@"Please enter your message" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        return NO;
        
    }
//        else if (![self validateEmail:objTextEmail.text])
//    {
//        [objTextEmail setTextColor:[UIColor redColor]];
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"NOTE" message: @"Please enter valid email address" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
//        [alert show];
//        return NO;
//        
//        
//        
//    }
    
    return YES;
    
    
}
//-(BOOL) validateEmail: (NSString *) candidate
//{
//    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
//    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex]; //  return 0;
//    return [emailTest evaluateWithObject:candidate];
//}

#pragma mark SliderMenuDelegate
-(void)didSelectSliderMenuButton:(NSInteger)rowcount
{
    // NSLog(@"ROWCOUNT=== %d",rowcount);
    if(rowcount==0)
    {
        [self.navigationController popToViewController:objHomeViewController animated:YES];
        
    }
    else
    {
        [self.navigationController popToViewController:objHomeViewController animated:NO];
        
    }
    if ([_delegate respondsToSelector:@selector(didSelectRowFeedback:)])
    {
        [_delegate didSelectRowFeedback:rowcount];
    }
    
}

#pragma mark Delegate

- (id)delegate
{
	return  _delegate;
}

- (void)setDelegate:(id)new_delegate
{
	_delegate = new_delegate;
}

- (IBAction)objTextSubjectAction:(id)sender {
}

#pragma mark Teacher Webservice Delegate
-(void)serverResponseForTeacherWebservice:(NSArray*)responseArray
{
    //[HUD hide:YES];
    subjectsNameArray = [[NSMutableArray alloc] init];
    teacherNameArray = [[NSMutableArray alloc] init];
    
    if (responseArray != nil)
    {
        for (NSDictionary *tempDict in responseArray)
        {
            [subjectsNameArray addObject:[tempDict valueForKey:@"subject"]];
            [teacherNameArray addObject:[tempDict valueForKey:@"teacher"]];
        }
    }
    
    
    [subjectsNameArray insertObject:@"General Feedback" atIndex:0];
    [teacherNameArray insertObject:@"Class Teacher" atIndex:0];
    
    NSLog(@"%@",subjectsNameArray);
    NSLog(@"%@",teacherNameArray);
    
    [myPickerView reloadAllComponents];
}
-(void)serverFailResponseForTeacherWebservice
{
    [HUD hide:YES];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Server Response Failed" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}
@end
