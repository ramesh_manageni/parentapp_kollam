//
//  FeedbackWebservice.m
//  ParentApp
//
//  Created by Redbytes on 07/01/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "FeedbackWebservice.h"

#import "ConnectionManager.h"

@implementation FeedbackWebservice

-(id)init
{
	return self;
}
-(void)sendFeedbackWithStudentName:(NSString *)studentNameString email:(NSString *)emailString subject:(NSString *)subjectString message:(NSString *)messageString profileId:(NSString *)profileId username:(NSString *)username
{
    NSMutableDictionary *localDictionary=[[NSMutableDictionary alloc]init];
   
    
    NSMutableString *strApplicationUUID=[[NSMutableString alloc]init];
    NSUUID *oNSUUID = [[UIDevice currentDevice] identifierForVendor];
    [strApplicationUUID setString:[oNSUUID UUIDString]];
    [localDictionary setObject:strApplicationUUID forKey:@"device_id"];

    [localDictionary setObject:studentNameString forKey:@"studentNameString"];
    [localDictionary setObject:emailString forKey:@"emailString"];
    [localDictionary setObject:subjectString forKey:@"subjectString"];
    [localDictionary setObject:messageString forKey:@"messageString"];
    [localDictionary setObject:profileId forKey:@"profileId"];
    [localDictionary setObject:username forKey:@"username"];
    NSLog(@"Dict: %@",localDictionary);
    
    if (objConnectionManager!=nil)
    {
		objConnectionManager=nil;
	}
	objConnectionManager=[[ConnectionManager alloc]init];
    
    [objConnectionManager getWebData:self commandName:@"wsparents_feedback" Requestparameter:localDictionary];

    
}

#pragma mark connection manager delegate
- (void)receivedData:(NSArray *)responseArray
{
    if ([_delegate respondsToSelector:@selector(serverResponseForFeedbackWebservice:)])
    {
        [_delegate serverResponseForFeedbackWebservice:responseArray];
    }
    else
    {
        [NSException raise:NSInternalInconsistencyException format:@"Delegate doesn't respond to serverResponseForCaptchaWebSerivce:"];
    }
}
-(void)failToReciveData
{
    if ([_delegate respondsToSelector:@selector(serverFailResponseForFeedbackWebservice)])
    {
        [_delegate serverFailResponseForFeedbackWebservice];
    }
}
#pragma mark - Delegate methods
- (id)delegate
{
	return  _delegate;
}

- (void)setDelegate:(id)new_delegate
{
	_delegate = new_delegate;
}



@end
