//
//  SliderMenuCustomCell.m
//  ParentApp
//
//  Created by Redbytes on 03/04/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "SliderMenuCustomCell.h"

@implementation SliderMenuCustomCell

@synthesize objImageViewBG;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        objImageViewBG=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 70/2, 70/2)];
        //[objImageViewBG setImage:[UIImage imageNamed:@"attendance tab.png"]];
        [objImageViewBG setBackgroundColor:[UIColor clearColor]];
        [self.contentView addSubview:objImageViewBG];
        [self.contentView setBackgroundColor:[UIColor clearColor]];
        [self setBackgroundColor:[UIColor clearColor]];
        
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
