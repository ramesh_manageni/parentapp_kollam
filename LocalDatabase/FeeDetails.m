//
//  FeeDetails.m
//  ParentApp
//
//  Created by RedBytes on 09/06/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "FeeDetails.h"
#import "StudentDetails.h"


@implementation FeeDetails

@dynamic act;
@dynamic adm;
@dynamic bus;
@dynamic due_amount;
@dynamic due_date;
@dynamic hos;
@dynamic id_fee;
@dynamic student_id;
@dynamic tc;
@dynamic tut;
@dynamic otherFee;
@dynamic feeToStudent;

@end
