//
//  deleteInboxMessage.h
//  ParentApp
//
//  Created by Redbytes on 22/08/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ConnectionManager.h"


@protocol DeleteInboxMessageDelegate <NSObject>

-(void)serverResponseForDeleteInboxMessage:(NSArray *)responseArray;
-(void)serverFailResponseForIDeleteInboxMessage;

@end
//********************************************************************

@class ConnectionManager;

@interface DeleteInboxMessage : NSObject
{
    id _delegate;
    NSMutableDictionary *resultDictionary;
    ConnectionManager *objConnectionManager;
    
}
@property (strong,nonatomic) NSArray *deleteArray1;
- (id)delegate;
- (void)setDelegate:(id)new_delegate;
-(void)deleteMessageWithUsername:(NSString *)username pmId:(NSString *)pmMessageID Inbox_Sent:(NSString *)inbox_sent;

@end

