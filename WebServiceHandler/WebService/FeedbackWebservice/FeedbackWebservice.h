//
//  FeedbackWebservice.h
//  ParentApp
//
//  Created by Redbytes on 07/01/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol FeedbackDelegate <NSObject>

-(void)serverResponseForFeedbackWebservice:(NSArray*)responseArray;
-(void)serverFailResponseForFeedbackWebservice;

@end
//********************************************************************

@class ConnectionManager;
@interface FeedbackWebservice : NSObject
{
    id _delegate;
    NSMutableDictionary *resultDictionary;
    ConnectionManager *objConnectionManager;
}
- (id)delegate;
- (void)setDelegate:(id)new_delegate;
-(void)sendFeedbackWithStudentName:(NSString *)studentNameString email:(NSString *)emailString subject:(NSString *)subjectString message:(NSString *)messageString profileId:(NSString *)profileId username:(NSString *)username;
@end
