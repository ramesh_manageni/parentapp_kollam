
//
//  LoginViewController.m
//  ParentApp
//
//  Created by Redbytes on 02/01/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "LoginViewController.h"
#import "Parent.h"
#import "StudentDetails.h"
#import "AppDelegate.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    studentArray=[[NSMutableArray alloc]init];
    parentArray=[[NSMutableArray alloc]init];
    
    self.navigationController.navigationBar.barTintColor= [UIColor darkGrayColor];
    self.navigationController.navigationBarHidden=YES;
    
    objTextId.delegate=self;
    objTextPassword.delegate=self;
    
    NSArray * keys = [[NSArray alloc] initWithObjects:NSForegroundColorAttributeName, NSUnderlineStyleAttributeName, nil];
    NSArray * objects = [[NSArray alloc] initWithObjects:[UIColor whiteColor], [NSNumber numberWithInt:NSUnderlineStyleSingle], nil];
    NSDictionary * linkAttributes = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
    forgotPasswordLabel.attributedText = [[NSAttributedString alloc] initWithString:@"* Forgot your password ?"
                                                                         attributes:linkAttributes];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(forgotPassButtonClicked)];
    [forgotPasswordLabel addGestureRecognizer:tap];
    
    if ([objTextId respondsToSelector:@selector(setAttributedPlaceholder:)])
    {
        UIColor *color = [UIColor lightGrayColor];
        // [objTextId setFont:[UIFont fontWithName:@"Bradley Hand ITC" size:12.0]];
        objTextId.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Login Id" attributes:@{NSForegroundColorAttributeName: color}];
    }
    if ([objTextPassword respondsToSelector:@selector(setAttributedPlaceholder:)])
    {
        UIColor *color = [UIColor lightGrayColor];
        objTextPassword.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Password" attributes:@{NSForegroundColorAttributeName: color}];
    }
    
    NSUserDefaults *preference=[NSUserDefaults standardUserDefaults];
    if([[preference valueForKey:@"login"] isEqualToString:@"YES"])
    {
        [self getEntityCount:@"Parent"];
        
        LoginWebservice *objLoginWebservice = [[LoginWebservice alloc]init];
        objLoginWebservice.delegate = self;
        NSLog(@"%@",[[parentArrayCoreData objectAtIndex:0] user_name]);
        NSLog(@"%@",[[parentArrayCoreData objectAtIndex:0] passwords]);
        
        
        [objLoginWebservice loginToServserWithId:[[parentArrayCoreData objectAtIndex:0] user_name] :[[parentArrayCoreData objectAtIndex:0] passwords]];
        
        HUD = [[MBProgressHUD alloc] initWithView:self.view];
        [self.view addSubview:HUD];
        
        // Regiser for HUD callbacks so we can remove it from the window at the right time
        HUD.delegate = self;
        
        // Show the HUD while the provided method executes in a new thread
        //[HUD showWhileExecuting:@selector(myTask) onTarget:self withObject:nil animated:YES];
        [HUD show:YES];
    }
    
    self.imageViewEdsys.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapGestureEnlarge = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(onClickImageEdsys:)];
    tapGestureEnlarge.numberOfTapsRequired = 1;
    [self.imageViewEdsys addGestureRecognizer:tapGestureEnlarge];
}

-(void) viewDidAppear:(BOOL)animated
{
    [ParseClass incrementView];
}

-(void)onClickImageEdsys:(id)sender
{
    NSLog(@"EDSYS");
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"http://www.edsys.in/"]];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden=YES;
    
    objTextId.text=@"";
    objTextPassword.text=@"";
    [self.view endEditing:YES];
}
-(void)backButtonClicked:(UIBarButtonItem *)sender
{
    
}
-(void)settingButtonClicked:(UIBarButtonItem *)sender
{
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    UIScreen *mainScreen = [UIScreen mainScreen];
    CGFloat scale = ([mainScreen respondsToSelector:@selector(scale)] ? mainScreen.scale : 1.0f);
    CGFloat pixelHeight = (CGRectGetHeight(mainScreen.bounds) * scale);
    
    
    if(objTextId==textField)
    {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
        {
            if (scale == 2.0f)
            {
                if (pixelHeight == 960.0f)
                {
                    [objScrollView setContentOffset:CGPointMake(0,120) animated:YES];
                    
                }
                
                else if (pixelHeight == 1136.0f)
                {
                    [objScrollView setContentOffset:CGPointMake(0,30) animated:YES];
                    
                }
                
            }
            
        }
        
        
    }
    if(objTextPassword==textField)
    {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
        {
            if (scale == 2.0f)
            {
                if (pixelHeight == 960.0f)
                {
                    [objScrollView setContentOffset:CGPointMake(0,120) animated:YES];
                    
                }
                
                else if (pixelHeight == 1136.0f)
                {
                    [objScrollView setContentOffset:CGPointMake(0,30) animated:YES];
                    
                }
                
            }
            
        }
        
    }
    
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    [objScrollView setContentOffset:CGPointMake(0,0) animated:YES];
    
    return YES;
}

- (IBAction)cancelButtonClicked:(id)sender
{
    NSLog(@"cancel clicked..!!!");
    objTextId.text=@"";
    objTextPassword.text=@"";
    
    [self.view endEditing:YES];
    
    [objScrollView setContentOffset:CGPointMake(0,-20) animated:YES];
    
}

- (IBAction)submitButtonClicked:(id)sender
{
    
    if([self validateFields])
    {
        
        [self.view endEditing:YES];
        
        LoginWebservice *objLoginWebservice = [[LoginWebservice alloc]init];
        objLoginWebservice.delegate = self;
        [objLoginWebservice loginToServserWithId:objTextId.text :objTextPassword.text];
        
        HUD = [[MBProgressHUD alloc] initWithView:self.view];
        [self.view addSubview:HUD];
        
        // Regiser for HUD callbacks so we can remove it from the window at the right time
        HUD.delegate = self;
        
        // Show the HUD while the provided method executes in a new thread
        //[HUD showWhileExecuting:@selector(myTask) onTarget:self withObject:nil animated:YES];
        [HUD show:YES];
    }
}

- (void)forgotPassButtonClicked
{
    NSLog(@"Forgot password link tapped..!!!");
    UIAlertView *alertView = [[UIAlertView alloc]
                              initWithTitle:@"Forgot Password"
                              message:@"Please enter your username and email"
                              delegate:self
                              cancelButtonTitle:@"Cancel"
                              otherButtonTitles:@"Ok", nil];
    [alertView setAlertViewStyle:UIAlertViewStyleLoginAndPasswordInput];
    alertView.delegate=self;
    alertView.tag=100;
    /* Display a numerical keypad for this text field */
    UITextField *textFieldUsername = [alertView textFieldAtIndex:0];
    textFieldUsername.placeholder=@"Username";
    [textFieldUsername setFont:[UIFont fontWithName:@"BradleyHandITCTT-Bold" size:14.0]];
    
    UITextField *textFieldEmail = [alertView textFieldAtIndex:1];
    textFieldEmail.keyboardType = UIKeyboardTypeEmailAddress;
    textFieldEmail.placeholder=@"Email";
    textFieldEmail.secureTextEntry=NO;
    [textFieldEmail setFont:[UIFont fontWithName:@"BradleyHandITCTT-Bold" size:14.0]];
    [alertView show];
}

#pragma mark UIAlertView Delegates

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    [objScrollView setContentOffset:CGPointMake(0,-20) animated:YES];
    
    if(alertView.tag==100)
    {
        if(buttonIndex==1)
        {
            UITextField *textFieldUsername = [alertView textFieldAtIndex:0];
            UITextField *textFieldEmail = [alertView textFieldAtIndex:1];
            
            ForgotPasswordWebservice *objForgotPasswordWebservice=[[ForgotPasswordWebservice alloc]init];
            objForgotPasswordWebservice.delegate=self;
            [objForgotPasswordWebservice ForgotPasswordForUsername:textFieldUsername.text withEmailId:textFieldEmail.text];
            
            HUD = [[MBProgressHUD alloc] initWithView:self.view];
            [self.view addSubview:HUD];
            
            // Regiser for HUD callbacks so we can remove it from the window at the right time
            HUD.delegate = self;
            
            // Show the HUD while the provided method executes in a new thread
            //[HUD showWhileExecuting:@selector(myTask) onTarget:self withObject:nil animated:YES];
            [HUD show:YES];
        }
    }
    else
    {
        [HUD hide:YES];
        
    }
    
}
#pragma mark LoginWebservice Delegate
-(void)serverResponseForLoginWebservice:(NSArray *)responseArray
{
       [HUD hide:YES];
    if (responseArray!=nil)
    {
        NSLog(@"RES : %@",responseArray);
        //#warning mark check response is nil or "success=0"
        if([responseArray isKindOfClass:[NSArray class]])
        {
            [self deleteAllObjects:@"Parent"];
            [self loadData:responseArray];
            [self getEntityCount:@"Parent"];
            objMenuViewController=[[MenuViewController alloc] initWithNibName:@"MenuViewController" bundle:nil];
            //Add object to global object for get back to home.
            objHomeViewController=objMenuViewController;
            
            [self.navigationController pushViewController:objMenuViewController animated:NO];
            //save value in userdefaults for one time login
            NSUserDefaults *preference=[NSUserDefaults standardUserDefaults];
            [preference setValue:@"YES" forKey:@"login"];
            
            NSLog(@"Server Responce At login: %@",responseArrayLogin);
            
            if(![[[parentArrayCoreData objectAtIndex:0] user_name]isEqualToString:@"12345"])
            {
                if(![[[responseArray valueForKey:@"is_register"] objectAtIndex:0]intValue])
                {
                    RegistrationWebservice *objRegistrationWebservice=[[RegistrationWebservice alloc]init];
                    objRegistrationWebservice.delegate=self;
                    ///////nihal
                     [objRegistrationWebservice registerDeviceWithUsername:[[parentArrayCoreData objectAtIndex:0] user_name] password:[[parentArrayCoreData objectAtIndex:0] passwords] email:[[parentArrayCoreData objectAtIndex:0] email] contact:[[parentArrayCoreData objectAtIndex:0] mobile1] deviceOS:@"IOS" deviceVenderId:@""];
                }
            }
        }
        else if(![[responseArray valueForKey:@"success"] intValue])
        {
            if([[responseArray valueForKey:@"status"] isEqualToString:@"false"])
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:[responseArray valueForKey:@"message"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alert show];
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Invalid username or password" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alert show];
            }
        }
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Invalid username or password" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    
}
-(void)serverFailResponseForLoginWebservice
{
    [HUD hide:YES];
    [self getEntityCount:@"Parent"];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Server response failed\nAccessing local data" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
    
    if([parentArrayCoreData count]>0)
    {
        if([objTextId.text isEqualToString:[[parentArrayCoreData objectAtIndex:0] user_name]] && [objTextPassword.text isEqualToString:[[parentArrayCoreData objectAtIndex:0] passwords]])
        {
            
            objMenuViewController=[[MenuViewController alloc] initWithNibName:@"MenuViewController" bundle:nil];
            [self.navigationController pushViewController:objMenuViewController animated:NO];
            objHomeViewController=objMenuViewController;
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Invalid username or password" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        }
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Local database is empty" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
}

#pragma mark ForgotPasswordWebservice Delegates
-(void)serverResponseForForgotPasswordWebservice:(NSMutableDictionary*)responseArray
{
    NSLog(@"RESPONSE FOR ForgotPassword : %@",responseArray);
    if ([[responseArray valueForKey:@"success"] intValue])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Your Password is mailed to you.\nPlease check your mail." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        alert.delegate=self;
    }
    if (![[responseArray valueForKey:@"success"] intValue])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Please try again with valid username and email" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        alert.delegate=self;
    }
}

-(void)serverFailResponseForForgotPasswordWebservice
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Server response failed" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}

#pragma mark RegistrationWebservice Delegates
-(void)serverResponseForRegistrationWebservice:(NSMutableDictionary*)responseArray
{
    NSLog(@"RESPONSE FOR REGISTRATION : %@",responseArray);
    NSUserDefaults *registration=[NSUserDefaults standardUserDefaults];
    
    if([[responseArray valueForKey:@"status"] isEqualToString:@"true"])
    {
        [registration setObject:@"YES" forKey:@"isRegisterd"];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!\nDevice Registration" message:[responseArray valueForKey:@"message"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    else
    {
        [registration setObject:@"NO" forKey:@"isRegisterd"];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!\nDevice Registration" message:[responseArray valueForKey:@"message"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        
    }
}
//#warning ADD RESPONSE TO USERDEFAULTS.
-(void)serverFailResponseForRegistrationWebservice
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Server response failed" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}

-(BOOL)validateFields
{
    if (objTextId.text.length == 0 || objTextId == nil)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"NOTE" message:@"Please enter user name" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        return NO;
        
    }
    else if (objTextPassword.text.length == 0 || objTextPassword == nil)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"NOTE" message:@"Please enter password" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        return NO;
        
    }
    return YES;
}

- (void) deleteAllObjects: (NSString *) entityDescription
{
    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [delegate managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityDescription inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    NSError *error;
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    
    
    for (NSManagedObject *managedObject in items) {
    	[context deleteObject:managedObject];
    	NSLog(@"%@ object deleted",entityDescription);
    }
    if (![context save:&error]) {
    	NSLog(@"Error deleting %@ - error:%@",entityDescription,error);
    }
    
}
-(void)loadData:response
{
    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [delegate managedObjectContext];
    Parent *p1 = [NSEntityDescription insertNewObjectForEntityForName:@"Parent" inManagedObjectContext:context];
    if([[response objectAtIndex:0] valueForKey:@"detailed_name"]!=(NSString *)[NSNull null])
    {
        p1.detailed_name=[[response objectAtIndex:0] valueForKey:@"detailed_name"];
    }
    if([[response objectAtIndex:0] valueForKey:@"id"]!=(NSString *)[NSNull null])
    {
        p1.id_parent=[[response objectAtIndex:0] valueForKey:@"id"];
    }
    if([[response objectAtIndex:0] valueForKey:@"institution_id"]!=(NSString *)[NSNull null])
    {
        p1.institution_id=[[response objectAtIndex:0] valueForKey:@"institution_id"];
    }
    if([[response objectAtIndex:0] valueForKey:@"institution_name"]!=(NSString *)[NSNull null])
    {
        p1.institution_name=[[response objectAtIndex:0] valueForKey:@"institution_name"];
    }
    
    if([[response objectAtIndex:0] valueForKey:@"login_name"]!=(NSString *)[NSNull null])
    {
        p1.login_name=[[response objectAtIndex:0] valueForKey:@"login_name"];
    }
    if([[response objectAtIndex:0] valueForKey:@"login_pwd"]!=(NSString *)[NSNull null])
    {
        p1.login_pwd=[[response objectAtIndex:0] valueForKey:@"login_pwd"];
    }
    if([[response objectAtIndex:0] valueForKey:@"passwords"]!=(NSString *)[NSNull null])
    {
        p1.passwords=[[response objectAtIndex:0] valueForKey:@"passwords"];
    }
    if([[response objectAtIndex:0] valueForKey:@"profile_id"]!=(NSString *)[NSNull null])
    {
        p1.profile_id=[[response objectAtIndex:0] valueForKey:@"profile_id"];
    }
    if([[response objectAtIndex:0] valueForKey:@"remarks"]!=(NSString *)[NSNull null])
    {
        p1.remarks=[[response objectAtIndex:0] valueForKey:@"remarks"];
    }
    if([[response objectAtIndex:0] valueForKey:@"role_id"]!=(NSString *)[NSNull null])
    {
        p1.role_id=[[response objectAtIndex:0] valueForKey:@"role_id"];
    }
    if([[response objectAtIndex:0] valueForKey:@"role_type_id"]!=(NSString *)[NSNull null])
    {
        p1.role_type_id=[[response objectAtIndex:0] valueForKey:@"role_type_id"];
    }
    if([[response objectAtIndex:0] valueForKey:@"status_flag"]!=(NSString *)[NSNull null])
    {
        p1.status_flag=[[response objectAtIndex:0] valueForKey:@"status_flag"];
    }
    if([[response objectAtIndex:0] valueForKey:@"style_id"]!=(NSString *)[NSNull null])
    {
        p1.style_id=[[response objectAtIndex:0] valueForKey:@"style_id"];
    }
    if([[response objectAtIndex:0] valueForKey:@"updated_by"]!=(NSString *)[NSNull null])
    {
        p1.updated_by=[[response objectAtIndex:0] valueForKey:@"updated_by"];
    }
    if([[response objectAtIndex:0] valueForKey:@"updated_date"]!=(NSString *)[NSNull null])
    {
        p1.updated_date=[[response objectAtIndex:0] valueForKey:@"updated_date"];
    }
    if([[response objectAtIndex:0] valueForKey:@"user_id"]!=(NSString *)[NSNull null])
    {
        p1.user_id=[[response objectAtIndex:0] valueForKey:@"user_id"];
    }
    if([[response objectAtIndex:0] valueForKey:@"user_name"]!=(NSString *)[NSNull null])
    {
        p1.user_name=[[response objectAtIndex:0] valueForKey:@"user_name"];
    }
    if([[response objectAtIndex:0] valueForKey:@"user_role_id"]!=(NSString *)[NSNull null])
    {
        p1.user_role_id=[[response objectAtIndex:0] valueForKey:@"user_role_id"];
    }
    if([[response objectAtIndex:0] valueForKey:@"valid_from"]!=(NSString *)[NSNull null])
    {
        p1.valid_from=[[response objectAtIndex:0] valueForKey:@"valid_from"];
    }
    if([[response objectAtIndex:0] valueForKey:@"email"]!=(NSString *)[NSNull null])
    {
        p1.email=[[response objectAtIndex:0] valueForKey:@"email"];
    }
    if([[response objectAtIndex:0] valueForKey:@"mobile1"]!=(NSString *)[NSNull null])
    {
        p1.mobile1=[[response objectAtIndex:0] valueForKey:@"mobile1"];
    }
    
    
    for(int i=0;i<[response count];i++)
    {
        
        StudentDetails *s1 = [NSEntityDescription insertNewObjectForEntityForName:@"StudentDetails" inManagedObjectContext:context];
        if([[[response objectAtIndex:i] valueForKey:@"student_details"] valueForKey:@"admission_no"]!=(NSString *)[NSNull null])
        {
            s1.admission_no=[[[response objectAtIndex:i] valueForKey:@"student_details"] valueForKey:@"admission_no"];
        }
        if([[[response objectAtIndex:i] valueForKey:@"student_details"] valueForKey:@"due_amount"]!=(NSString *)[NSNull null])
        {
            s1.due_amount=[[[response objectAtIndex:i] valueForKey:@"student_details"] valueForKey:@"due_amount"];
        }
        if([[[response objectAtIndex:i] valueForKey:@"student_details"] valueForKey:@"due_date"]!=(NSString *)[NSNull null])
        {
            s1.due_date=[[[response objectAtIndex:i] valueForKey:@"student_details"] valueForKey:@"due_date"];
        }
        if([[[response objectAtIndex:i] valueForKey:@"student_details"] valueForKey:@"id"]!=(NSString *)[NSNull null])
        {
            s1.id_student=[[[response objectAtIndex:i] valueForKey:@"student_details"] valueForKey:@"id"];
        }
        if([[[response objectAtIndex:i] valueForKey:@"student_details"] valueForKey:@"local_name"]!=(NSString *)[NSNull null])
        {
            s1.local_name=[[[response objectAtIndex:i] valueForKey:@"student_details"] valueForKey:@"local_name"];
        }
        if([[[response objectAtIndex:i] valueForKey:@"student_details"] valueForKey:@"photo"]!=(NSString *)[NSNull null])
        {
            if([[[response objectAtIndex:0] valueForKey:@"passwords"]  isEqual: @"12345"])
            {
                NSString *string = [NSString stringWithFormat:@"testImage%d.jpg",i] ;//]@"testImage@d.jpg",;
                UIImage *image = [UIImage imageNamed:string];
                NSData *imgData= UIImageJPEGRepresentation(image,1.0);
                NSString *str = [imgData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
                NSString *newString = [[str componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@" "];
                s1.photo = newString;
            }
            else
            {
                s1.photo=[[[response objectAtIndex:i] valueForKey:@"student_details"] valueForKey:@"photo"];
            }
        }
        //Add s1 to p1
        [p1 addParentToChildObject:s1];
        s1=nil;
        
        
    }
    
    NSError *err;
    
    if( ! [context save:&err] ){
        NSLog(@"Cannot save data: %@", [err localizedDescription]);
    }
    
    //    Parent *p1 = [NSEntityDescription insertNewObjectForEntityForName:@"Parent" inManagedObjectContext:managedObjectContext];
    //    AppDelegate *appDelegate =
    //    [[UIApplication sharedApplication] delegate];
    //
    //    NSManagedObjectContext *context =
    //    [appDelegate managedObjectContext];
    //    p1.Mothername= @"Asha";
    //    p1.FatherName = @"Radhe";
    //
    //    Child *c1 = [NSEntityDescription insertNewObjectForEntityForName:@"Child" inManagedObjectContext:self.managedObjectContext];
    //    c1.ChildName = @"Radix";
    //
    //    [p1 addParentToChildObject:c1];
    //
    //    Parent *p2 = [NSEntityDescription insertNewObjectForEntityForName:@"Parent" inManagedObjectContext:self.managedObjectContext];
    //    p2.Mothername = @"Chichi";
    //    p2.FatherName = @"Goku";
    //
    //    Child *c2 = [NSEntityDescription insertNewObjectForEntityForName:@"Child" inManagedObjectContext:self.managedObjectContext];
    //    c2.ChildName = @"Gohan";
    //
    //    [p2 addParentToChildObject:c2];
    
    
    //    if([self.managedObjectContext hasChanges])
    //    {
    //        [self.managedObjectContext save:nil];
    //    }
}

-(NSArray*)retriveData
{
    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [delegate managedObjectContext];
    NSError *error;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Parent" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    return fetchedObjects;
}

-(void)getEntityCount:(NSString *)entityName
{
    entityCount = 0;
    [studentArray removeAllObjects];
    [parentArray removeAllObjects];
    
    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [delegate managedObjectContext];
    
    NSFetchRequest *fetchReq = [[NSFetchRequest alloc]init];
    
    [fetchReq setEntity:[NSEntityDescription entityForName:entityName inManagedObjectContext:context]];
    NSError *error;
    
    NSMutableArray *resultArray = [[NSMutableArray alloc]initWithArray:[context executeFetchRequest:fetchReq error:&error]];
    if(error==nil)
    {
        for(Parent *pnt in resultArray)
        {
            [parentArray addObject:pnt];
            
            for(StudentDetails *std in pnt.parentToChild)
            {
                [studentArray addObject:std];
            }
        }
        
    }
    studentArrayCoreData=studentArray;
    parentArrayCoreData=parentArray;
    NSLog(@"Entity Array:%@ Parent: %@",studentArrayCoreData,parentArrayCoreData);
}


#pragma rizwan

- (IBAction)videoButtonClicked:(id)sender
{
    int k=0;
    if(k==0)
    {
        
        NSString *urlStr = [[NSBundle mainBundle] pathForResource:@"parentAppVideo.mp4" ofType:nil];
        
        NSURL *url = [NSURL fileURLWithPath:urlStr];
        moviePlayer = [[MPMoviePlayerController alloc] initWithContentURL:url];
        [self.view addSubview:moviePlayer.view];
        
        UIScreen *mainScreen = [UIScreen mainScreen];
        CGFloat scale = ([mainScreen respondsToSelector:@selector(scale)] ? mainScreen.scale : 1.0f);
        CGFloat pixelHeight = (CGRectGetHeight(mainScreen.bounds) * scale);
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
        {
            if (scale == 2.0f)
            {
                if (pixelHeight == 960.0f)
                {
                    [moviePlayer.view setFrame:CGRectMake(0, 20, 320, 480-69)];
                    
                }
                
                else if (pixelHeight == 1136.0f)
                {
                    [moviePlayer.view setFrame:CGRectMake(0, 20, 320, 568-69)];
                }
            }
            
        }
        // moviePlayer.view.frame = CGRectMake(0, 0, 300, 400);
        [moviePlayer play];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(moviePlaybackComplete:)
                                                     name:MPMoviePlayerPlaybackDidFinishNotification
                                                   object:moviePlayer];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(doneButtonClick:)
                                                     name:MPMoviePlayerWillExitFullscreenNotification
                                                   object:nil];
        
    }
    
    
}
-(void)doneButtonClick:(NSNotification*)aNotification
{
    MPMoviePlayerViewController *moviePlayerController = [aNotification object];

   
        // Your done button action here
        [moviePlayerController.view removeFromSuperview];

}

- (void)moviePlaybackComplete:(NSNotification *)notification
{
    
    MPMoviePlayerViewController *moviePlayerController = [notification object];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:MPMoviePlayerPlaybackDidFinishNotification
                                                  object:moviePlayerController];
    [moviePlayerController.view removeFromSuperview];
    
    //i++;
    
}

//***********************************


@end
