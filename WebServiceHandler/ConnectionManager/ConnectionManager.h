//
//  ConnectionManager.h
//  MovilTicket
//
//  Created by Neha Salankar on 04/02/13.
//  Copyright (c) 2013 Sachin. All rights reserved.
//

#import <Foundation/Foundation.h>
//This protocal recive the responce from webservice.

@protocol connectionDelegate <NSObject>
@required
- (void)receivedData:(NSArray *)responseArray;     //The method performs the action of receiving the data
-(void)failToReciveData; //responseFailedWithError  //The method gets invoked on failure of receiving the data
@end

//=============================================


@interface ConnectionManager : NSObject
{
    //Objects for storing data
    id _delegate;
    
    //collecting the response data
	NSMutableData *responseMutableData;
    
    //Object for managing the URL connection
    NSURLConnection *serverConnectionObj;
    
    UIImageView *uploadMultipleImages;
    NSData *imageData;
    UIImage *userImage;
    NSString *selectImageName;
    NSArray *checkSelectedArray;
    UIImage *img;
	int expectedData;
	int downloadedData;
    
    // Global *GlobalObj;
    NSString *base64String;
}

@property(nonatomic,strong)NSString *isSBTracker;
@property(nonatomic,strong)NSString *isThisUrl;

@property(nonatomic,strong)NSString *isRegistration;
@property(nonatomic,strong)NSString *isInboxWS;
@property(nonatomic,strong)NSString *isReadInboxMessageWS;
@property(nonatomic,strong)NSString *isDeleteInboxMessageWS;
@property(nonatomic,strong)NSString *isComposeMessageWS;
@property(nonatomic,strong)NSString *isSentWS;

@property(nonatomic,strong)NSString *isNoticeCountWS;
@property(nonatomic,strong)NSString *isNoticeReadWS;

//The method performs the action of Returning Delegate
- (id)delegate;

//The method performs the action of setting Delegate
- (void)setDelegate:(id)new_delegate;

//The method performs the action of passing Webservice Response
- (void)setResponseFromServerToServiceClass:(NSArray *)responseArray;
//The method performs the action of Showing alert Message.
//-(void)alertViewMethod:(NSString *)alertTitle :(NSString *)message;

- (void)getWebData:(id)aDelegate commandName:(NSString*)commandName Requestparameter:(NSMutableDictionary*)theRequestParameters;



- (NSString*)cleanSOAPResponseforJSON:(NSString*)SOAPResponse;
-(void)stopLoading;
- (NSString *)filterSpecialSymbols:(NSString *)string;
@end

