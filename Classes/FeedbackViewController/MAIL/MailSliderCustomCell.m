//
//  MailSliderCustomCell.m
//  ParentApp
//
//  Created by RedBytes on 06/08/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#define kFontName @"BradleyHandITCTT-Bold"

#import "MailSliderCustomCell.h"

@implementation MailSliderCustomCell
@synthesize count,contentImageView,contentTitle;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        contentImageView = [[UIImageView alloc] initWithFrame:CGRectMake(00,10,40,40)];
        [contentImageView setBackgroundColor:[UIColor clearColor]];
        
        contentTitle =[[UILabel alloc]initWithFrame:CGRectMake(50,15,100,30)];
		[contentTitle setBackgroundColor:[UIColor clearColor]];
        [contentTitle setTextColor:[UIColor whiteColor]];
        [contentTitle setTextAlignment:NSTextAlignmentLeft];
        count =[[UILabel alloc]initWithFrame:CGRectMake(250,15,30,30)];
        [count setTextColor:[UIColor whiteColor]];
        [count setTextAlignment:NSTextAlignmentCenter];
        
//        UIImageView* separatorLineView = [[UIImageView alloc] initWithFrame:CGRectMake(00,56,200,4)];/// change size as you need.
//        separatorLineView.backgroundColor = [UIColor clearColor];
//        separatorLineView.image = [UIImage imageNamed:@"line@2x.png"];
        
        [self.contentView addSubview:contentImageView];
        [self.contentView addSubview:contentTitle];
        [self.contentView addSubview:count];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
