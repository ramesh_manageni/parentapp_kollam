//
//  ComposeViewController.m
//  ParentApp
//
//  Created by Redbytes on 22/08/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "ComposeViewController.h"
#import "TeacherWebservice.h"

#import "Constant.h"
#import "StudentDetails.h"
#import "Parent.h"
#import "TeacherDetails.h"
#import "ComposeMessage.h"
#import "AppDelegate.h"
#import "SentMessageDetail.h"

#include <stdlib.h>


@interface ComposeViewController ()
{
    UIPickerView *myPickerView;
    int teacher_subjectCount;
    NSString *teacherEmailString;
    
}

@end

@implementation ComposeViewController

@synthesize subjectsNameArray,teacherNameArray,student_id;

@synthesize textFieldAddTopic,textFieldSubject,textViewMessage,textFieldStudent,myTableView,labelDefault,isFromReplyButton,to_subject,to_address,to_message;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    
    [self getEntityCountFromSentMessageTable:@"SentMessageDetail"];
    myRandomNumberArray=sentArray;
    
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    
    // Regiser for HUD callbacks so we can remove it from the window at the right time
    
    HUD.delegate = self;
    
    
    selectedStudents=[[NSMutableArray alloc] init];
    
    self.labelDefault.text=@"Message";
    myPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 200, 320, 200)];
    myPickerView.delegate = self;
    myPickerView.showsSelectionIndicator = YES;
    [myPickerView setBackgroundColor:[UIColor colorWithRed:246/255.0 green:246/255.0 blue:246/255.0 alpha:1.0]];
    
    textFieldAddTopic.inputView = myPickerView;
    
    
    UIBarButtonItem *flexibleSpace =  [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    
    UIBarButtonItem *doneButton1 =
    [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                  target:self action:@selector(inputAccessoryViewDidFinish1)];
    
    UIToolbar *myToolbar1 = [[UIToolbar alloc] initWithFrame:
                             CGRectMake(0,0, 320, 44)]; //should code with variables to support view resizing
    
    //using default text field delegate method here, here you could call
    //myTextField.resignFirstResponder to dismiss the views
    
    
    UIButton* customButton1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [customButton1 setTitle:@"Cancel" forState:UIControlStateNormal];
    [customButton1 setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    [customButton1 addTarget:self action:@selector(cancelPickerButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [customButton1 sizeToFit];
    UIBarButtonItem* customBarButtonItem1 = [[UIBarButtonItem alloc] initWithCustomView:customButton1];
    
    [myToolbar1 setItems:[NSArray arrayWithObjects:doneButton1,flexibleSpace,customBarButtonItem1, nil] animated:NO];
    
    
    textFieldAddTopic.inputAccessoryView = myToolbar1;
    textFieldAddTopic.inputAccessoryView.backgroundColor=[UIColor darkGrayColor];
    
    NSMutableArray *tempArray=[self getEntityCount:@"StudentDetails"];
    if(tempArray.count==0)
    {
        TeacherWebservice *objTeacherWebservice=[[TeacherWebservice alloc] init];
        objTeacherWebservice.delegate=self;
        [objTeacherWebservice showTeacherForUsername:[[parentArrayCoreData objectAtIndex:0] user_name] studentId:student_id];
        
        // Show the HUD while the provided method executes in a new thread
        //[HUD showWhileExecuting:@selector(myTask) onTarget:self withObject:nil animated:YES];
        [HUD show:YES];
       
    }
    else
    {
        subjectsNameArray=tempArray;
    }
    
    if([isFromReplyButton isEqualToString:@"YES"])
    {
        textFieldAddTopic.text=self.to_address;
        textFieldSubject.text=self.to_subject;
        textViewMessage.text=self.to_message;
        self.labelDefault.hidden = ([textViewMessage.text length] > 0);
        isFromReplyButton=@"";
    }
    else
    {
        //[textViewMessage addSubview:labelDefault];
    }
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)addTopicButtonClicked:(id)sender
{
    [self.view addSubview:myPickerView];
    
}

- (IBAction)sendButtonClicked:(id)sender
{
    if([self validateFields])
    {
        
        ComposeMessage *objComposeMessage=[[ComposeMessage alloc] init];
        objComposeMessage.delegate=self;
//               [objComposeMessage composeMessgaeFrom:@"8119161" toMessage:[[subjectsNameArray objectAtIndex:teacher_subjectCount] login_name] message:textViewMessage.text withSubject:textFieldSubject.text withStatus:YES];
        
        NSLog(@"%@",[parentArrayCoreData valueForKey:@"user_name"]);
        
        [objComposeMessage composeMessgaeFrom:[[parentArrayCoreData valueForKey:@"user_name"]objectAtIndex:0] toMessage:[[subjectsNameArray objectAtIndex:teacher_subjectCount] login_name] SubjectName:textFieldAddTopic.text message:textViewMessage.text withSubject:textFieldSubject.text withstudent_ID:self.student_id withStatus:YES];
        
        //[objComposeMessage composeMessgaeFrom:@"8119161" toMessage:@"angel" SubjectName:textFieldAddTopic.text message:textViewMessage.text withSubject:textFieldSubject.text withStatus:true];
        
       // [[parentArrayCoreData valueForKey:@"user_name"]objectAtIndex:0]
        
       // [objComposeMessage composeMessgaeFrom:@"8119161" toMessage:textFieldAddTopic.text message:textViewMessage.text withSubject:textFieldSubject.text withStatus:true];
        //[HUD showWhileExecuting:@selector(myTask) onTarget:self withObject:nil animated:YES];
        [HUD show:YES];
    }
    
}
-(BOOL)validateFields
{
    if(textFieldAddTopic.text.length==0)
    {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Alert" message:@"Please select to address from list" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        return NO;
    }
    if(textFieldSubject.text.length==0)
    {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Alert" message:@"Please enter subject" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        return NO;
    }
    if(textViewMessage.text.length==0)
    {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Alert" message:@"Please enter message" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        return NO;
    }
    
    return YES;
}
- (IBAction)cancelButtonClicked:(id)sender
{
    [self.view endEditing:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

#pragma mark UIPickerView Datasource and Delefgate

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    // Handle the selection
    teacher_subjectCount = (int)row;
}

// tell the picker how many rows are available for a given component
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
    return [subjectsNameArray count];
}

// tell the picker how many components it will have
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

// tell the picker the title for a given component
//- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
//    NSString *title;
//    title = [subjectsNameArray objectAtIndex:row];
//
//    return title;
//}

// tell the picker the width of each row for a given component
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
    int sectionWidth = 300;
    
    return sectionWidth;
}
- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel* tView = (UILabel*)view;
    if (!tView){
        tView = [[UILabel alloc] init];
        tView.font = [UIFont systemFontOfSize:14.0];
        tView.text = [[subjectsNameArray objectAtIndex:row] subject];
        tView.textAlignment = NSTextAlignmentCenter;
    }
    // Fill the label text here
    return tView;
}

#pragma mark InputAccessory Methods
-(void)inputAccessoryViewDidFinish1
{
    [self.view endEditing:YES];
    
    if ([subjectsNameArray objectAtIndex:teacher_subjectCount] != nil)
    {
        textFieldAddTopic.text= [[subjectsNameArray objectAtIndex:teacher_subjectCount] valueForKey:@"subject"];
        NSLog(@"%@",[[subjectsNameArray objectAtIndex:teacher_subjectCount] valueForKey:@"teacher_email"]);
        teacherEmailString = [[subjectsNameArray objectAtIndex:teacher_subjectCount] valueForKey:@"login_name"];
    }
    
    //
}
-(void)cancelPickerButtonClicked:(UIButton *)sender
{
    [self.view endEditing:YES];
}

#pragma mark UITableView Delegate And Datasource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 35;
	
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#pragma  rizwan
    tableView.separatorStyle=UITableViewCellSeparatorStyleSingleLine;
    
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
	return [studentArrayCoreData count];
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"FriendCellIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
        [cell setBackgroundColor:[UIColor clearColor]];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    cell.textLabel.textAlignment = NSTextAlignmentCenter;
    cell.textLabel.text=[[studentArrayCoreData objectAtIndex:indexPath.row] local_name];
    cell.textLabel.font = [UIFont systemFontOfSize:14.0];
    
    if([isSelectAll isEqualToString:@"YES"])
    {
        [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
        if ([selectedStudents containsObject:[studentArrayCoreData objectAtIndex:indexPath.row]])
        {
            [selectedStudents removeObject:[studentArrayCoreData objectAtIndex:indexPath.row]];
        }
        [selectedStudents addObject:[studentArrayCoreData objectAtIndex:indexPath.row]];
    }
    if([isSelectAll isEqualToString:@"NO"])
    {
        [cell setAccessoryType:UITableViewCellAccessoryNone];
        [selectedStudents removeAllObjects];
        
    }
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([tableView cellForRowAtIndexPath:indexPath].accessoryType == UITableViewCellAccessoryCheckmark)
    {
        [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryNone;
        if ([selectedStudents containsObject:[studentArrayCoreData objectAtIndex:indexPath.row]])
        {
            [selectedStudents removeObject:[studentArrayCoreData objectAtIndex:indexPath.row]];
            if(selectedStudents.count == 0)
            {
                [customButton setTitle:@"Select All" forState:UIControlStateNormal];
                [customButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
            }
        }
        
    }
    else
    {
        [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryCheckmark;
        [selectedStudents addObject:[studentArrayCoreData objectAtIndex:indexPath.row]];
        if(selectedStudents.count == studentArrayCoreData.count)
        {
            [customButton setTitle:@"None" forState:UIControlStateNormal];
            [customButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
            
        }
    }
}

#pragma mark Teacher Webservice Delegate
-(void)serverResponseForTeacherWebservice:(NSArray*)responseArray
{
    [HUD hide:YES];
    subjectsNameArray = [[NSMutableArray alloc] init];
    teacherNameArray = [[NSMutableArray alloc] init];
    
    if (responseArray != nil)
    {
        //        for (NSDictionary *tempDict in responseArray)
        //        {
        //            [subjectsNameArray addObject:[tempDict valueForKey:@"subject"]];
        //            [teacherNameArray addObject:[tempDict valueForKey:@"teacher"]];
        //        }
        
       // subjectsNameArray=[responseArray mutableCopy];
        [self loadData:responseArray];
        // [self getEntityCount:@"StudentDetails"];
    }
    
    NSMutableDictionary *tempDict=[[NSMutableDictionary alloc] init];
    [tempDict setValue:@"General Feedback" forKey:@"subject"];
    
    //   [subjectsNameArray insertObject:tempDict atIndex:0];
    // [teacherNameArray insertObject:@"Class Teacher" atIndex:0];
    
    NSLog(@"%@",subjectsNameArray);
    NSLog(@"%@",teacherNameArray);
    
    [myPickerView reloadAllComponents];
}
-(void)serverFailResponseForTeacherWebservice
{
    [HUD hide:YES];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Server Response Failed" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}

-(void)clearTextFields
{
    textFieldStudent.text=@"";
    //objTextEmail.text=@"";
    textFieldSubject.text=@"";
    textViewMessage.text=@"";
    textFieldAddTopic.text=@"";
//self.textFieldAddTopic.text=@"";
    labelDefault.hidden=NO;
}

#pragma mark UITextView delegates

- (void)textViewDidBeginEditing:(UITextView *)textView
{
#pragma rizwan
    
    UIScreen *mainScreen = [UIScreen mainScreen];
    CGFloat scale = ([mainScreen respondsToSelector:@selector(scale)] ? mainScreen.scale : 1.0f);
    CGFloat pixelHeight = (CGRectGetHeight(mainScreen.bounds) * scale);
    
    if(textViewMessage==textView)
    {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
        {
            if (scale == 2.0f)
            {
                if (pixelHeight == 960.0f)
                {
                    [self.scrollView setContentOffset:CGPointMake(0,230) animated:YES];
                    
                }
                
                else if (pixelHeight == 1136.0f)
                {
                    [self.scrollView setContentOffset:CGPointMake(0,165) animated:YES];
                    
                }
                
            }
            
        }
        
    }
    
    
    //*************************
    
    self.labelDefault.hidden = YES;
    [self.scrollView setContentOffset:CGPointMake(0,100) animated:YES];
}

- (void)textViewDidChange:(UITextView *)txtView
{
    self.labelDefault.hidden = ([txtView.text length] > 0);
}

- (void)textViewDidEndEditing:(UITextView *)txtView
{
    self.labelDefault.hidden = ([txtView.text length] > 0);
}
- (BOOL)textView:(UITextView *)txtView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if( [text rangeOfCharacterFromSet:[NSCharacterSet newlineCharacterSet]].location == NSNotFound )
    {
        
        return YES;
        
    }
    [txtView resignFirstResponder];
    [self.scrollView setContentOffset:CGPointMake(0,0) animated:YES];
    
    return NO;
}
#pragma mark UITextFeild delegates

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if(textField==textFieldAddTopic)
    {
        subjectsNameArray=[self getEntityCount:@"StudentDetails"];
    }
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    [self.scrollView setContentOffset:CGPointMake(0,0) animated:YES];
    
    return YES;
}
- (int)getRandomNumber:(int)from to:(int)to {
    return (int)from + arc4random() % (to-from+1);
}
- (void)getNewUniqueRandomNumber {
    int randomIndex = [self getRandomNumber:1 to:1000];
    for (int i = 0; i < [myRandomNumberArray count]; i++) {
        if ([[myRandomNumberArray objectAtIndex:i]intValue] == randomIndex) { //number is already in array
            randomIndex = 0; //let's set it to 0
        }
    }
    if (randomIndex == 0) {
        [self getNewUniqueRandomNumber]; //repeat, as we didn't get a random number this time
    }
    else {
        [myRandomNumberArray addObject:[NSNumber numberWithInt:randomIndex]];
        //add your code here to proceed
        NSLog(@"myRandomNumberArray %@",myRandomNumberArray.description);
        
    }
}
#pragma mark ComposeWS Delegate
-(void)serverResponseForComposeMessage:(NSArray *)responseArray
{
    [HUD hide:YES];
    if([[responseArray valueForKey:@"success"] boolValue])
    {
        [self getNewUniqueRandomNumber];
        int r = (int)[myRandomNumberArray lastObject];
        // generate number up to 42 (limit)
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"hh:mm:ss a"];
        NSString *currentTime= [formatter stringFromDate:[NSDate date]];
        NSLog(@"Today's Date and Time: %@", [formatter stringFromDate:[NSDate date]]);
        
//        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//        dateFormatter.dateFormat = @"hh:mm:ss a";
//        [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
//        [dateFormatter setTimeStyle:NSDateFormatterMediumStyle];
//        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
//        NSString *currentTime = [dateFormatter stringFromDate:[NSDate date]];
//        dateFormatter = nil;
        
        NSDate *currDate = [NSDate date];
        NSDateFormatter *dateFormatterDate = [[NSDateFormatter alloc]init];
        [dateFormatterDate setDateFormat:@"dd/MM/YY"];
        NSString *dateString = [dateFormatterDate stringFromDate:currDate];
        NSLog(@"%@",dateString);
        
        [sentMessageDict setObject:[NSString stringWithFormat:@"%i",r] forKey:@"message_id"];
        [sentMessageDict setObject:currentTime forKey:@"time_sent"];
        [sentMessageDict setObject:dateString forKey:@"date_sent"];

        [self loadDataToSentMessageTable:sentMessageDict];
       
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Message sent successfully" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        alert.tag=100;
        [alert show];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Message not sent" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        alert.tag=200;
        [alert show];
    }
    
    NSLog(@" %@",responseArray);
}
-(void)serverFailResponseForIComposeMessage
{
    [HUD hide:YES];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Server Response Failed" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
    
}
#pragma mark UIAlertView Delegates

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if(alertView.tag==100)
    {
        [self dismissViewControllerAnimated:YES completion:nil];
        
    }
    else
    {
        
    }
    
}
#pragma mark Core Data Methods
-(void)loadData:response
{
    
    StudentDetails *std=[self getStudentEntityWithId];
    
    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [delegate managedObjectContext];
    
    // StudentDetails *std = [NSEntityDescription insertNewObjectForEntityForName:@"StudentDetails" inManagedObjectContext:context];
    for(int i=0;i<[response count];i++)
    {
        TeacherDetails *teacherdetail = [NSEntityDescription insertNewObjectForEntityForName:@"TeacherDetails" inManagedObjectContext:context];

        if([[response objectAtIndex:i] valueForKey:@"login_name"]!=(NSString *)[NSNull null])
        {
            teacherdetail.login_name=[[response objectAtIndex:i] valueForKey:@"login_name"];
        }
        if([[response objectAtIndex:i] valueForKey:@"subject"]!=(NSString *)[NSNull null])
        {
            teacherdetail.subject=[[response objectAtIndex:i] valueForKey:@"subject"];
        }
        if([[response objectAtIndex:i] valueForKey:@"teacher"]!=(NSString *)[NSNull null])
        {
            teacherdetail.teacher=[[response objectAtIndex:i] valueForKey:@"teacher"];
        }
        if([[response objectAtIndex:i] valueForKey:@"teacher_email"]!=(NSString *)[NSNull null])
        {
            teacherdetail.teacher_email=[[response objectAtIndex:i] valueForKey:@"teacher_email"];
        }
        
        [std addStudentToTeacherObject:teacherdetail];
        teacherdetail=nil;

    }
    
    NSError *err;
    
    if( ! [context save:&err] ){
        NSLog(@"Cannot save data: %@", [err localizedDescription]);
    }
    
}
-(StudentDetails *)getStudentEntityWithId
{
    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [delegate managedObjectContext];
    
    NSFetchRequest *fetchReq = [[NSFetchRequest alloc]init];
    
    [fetchReq setEntity:[NSEntityDescription entityForName:@"StudentDetails" inManagedObjectContext:context]];
    NSError *error;
    
    NSMutableArray *resultArray = [[NSMutableArray alloc]initWithArray:[context executeFetchRequest:fetchReq error:&error]];
    if(error==nil)
    {
        for(StudentDetails *stdDetails in resultArray)
        {
            if([stdDetails.id_student isEqualToString:student_id])
            {
                [stdDetails removeStudentToTeacher:stdDetails.studentToTeacher];
                return stdDetails;
            }
        }
        
    }
    
    return nil;
}
-(NSMutableArray *)getEntityCount:(NSString *)entityName
{
   // entityCount = 0;
    
    teacherDetailsArray=[[NSMutableArray alloc] init];
    
    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [delegate managedObjectContext];
    
    NSFetchRequest *fetchReq = [[NSFetchRequest alloc]init];
    
    [fetchReq setEntity:[NSEntityDescription entityForName:entityName inManagedObjectContext:context]];
    NSError *error;
    
    NSMutableArray *resultArray = [[NSMutableArray alloc]initWithArray:[context executeFetchRequest:fetchReq error:&error]];
    NSLog(@"Result Array: %@",resultArray);
    if(error==nil)
    {
        NSSet *set;
        
        for(StudentDetails *stddetails in resultArray)
        {
            if([stddetails.id_student isEqualToString:student_id])
            {
                set=stddetails.studentToTeacher;
                for(TeacherDetails *teacher in [set allObjects])
                {
                    NSLog(@"Fee details %@",teacher.login_name);
                    [teacherDetailsArray addObject:teacher];
                    
                }
            }
            
        }
        return teacherDetailsArray;
        
    }
    return nil;
}
#pragma mark Sent Messages Core Data Methods
-(void)loadDataToSentMessageTable:response
{
    
    
    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [delegate managedObjectContext];
    
    // StudentDetails *std = [NSEntityDescription insertNewObjectForEntityForName:@"StudentDetails" inManagedObjectContext:context];
   
    SentMessageDetail *sentMessageDetail = [NSEntityDescription insertNewObjectForEntityForName:@"SentMessageDetail" inManagedObjectContext:context];
        
    if([response valueForKey:@"From"]!=(NSString *)[NSNull null])
    {
        sentMessageDetail.from=[response valueForKey:@"From"];
    }
    if([response valueForKey:@"Subject"]!=(NSString *)[NSNull null])
    {
        sentMessageDetail.subject_sent_item=[response valueForKey:@"Subject"];
    }
    if([response valueForKey:@"Message"]!=(NSString *)[NSNull null])
    {
        sentMessageDetail.messgae_sent=[response valueForKey:@"Message"];
    }
    if([response valueForKey:@"To"]!=(NSString *)[NSNull null])
    {
        sentMessageDetail.to_address=[response valueForKey:@"To"];
    }
    if([response valueForKey:@"message_id"]!=(NSString *)[NSNull null])
    {
        sentMessageDetail.message_id=[response valueForKey:@"message_id"];
    }
    if([response valueForKey:@"date_sent"]!=(NSString *)[NSNull null])
    {
        sentMessageDetail.date_sent=[response valueForKey:@"date_sent"];
    }
    if([response valueForKey:@"time_sent"]!=(NSString *)[NSNull null])
    {
        sentMessageDetail.time_sent=[response valueForKey:@"time_sent"];
    }

    NSError *err;
    
    if( ! [context save:&err] ){
        NSLog(@"Cannot save data: %@", [err localizedDescription]);
    }
    
}

#pragma mark SentMessage Core Data Methods
-(void)getEntityCountFromSentMessageTable:(NSString *)entityName
{
    // entityCount = 0;
    
    sentArray=[[NSMutableArray alloc] init];
    
    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [delegate managedObjectContext];
    
    NSFetchRequest *fetchReq = [[NSFetchRequest alloc]init];
    
    [fetchReq setEntity:[NSEntityDescription entityForName:entityName inManagedObjectContext:context]];
    NSError *error;
    
    NSMutableArray *resultArray = [[NSMutableArray alloc]initWithArray:[context executeFetchRequest:fetchReq error:&error]];
    NSLog(@"Result Array: %@",resultArray);
    if(error==nil)
    {
        for(SentMessageDetail *sentMsg in resultArray)
        {
            [sentArray addObject:[sentMsg message_id]];
        }
       // sentArray=resultArray;
        
    }
}



@end
