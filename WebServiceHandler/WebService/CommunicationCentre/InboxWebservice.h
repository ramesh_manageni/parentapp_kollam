//
//  InboxWebservice.h
//  ParentApp
//
//  Created by Imtiyaz on 8/8/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ConnectionManager.h"


@protocol InboxDelegate <NSObject>

-(void)serverResponseForInboxWebservice:(NSArray *)responseArray;
-(void)serverFailResponseForInboxwebservice;

@end
//********************************************************************

@class ConnectionManager;

@interface InboxWebservice : NSObject
{
        id _delegate;
    NSMutableDictionary *resultDictionary;
    ConnectionManager *objConnectionManager;

}

- (id)delegate;
- (void)setDelegate:(id)new_delegate;
-(void)InboxMethodForUsername:(NSString *)username ;

@end


