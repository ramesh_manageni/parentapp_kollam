//
//  CustomNavigation.m
//  ParentApp
//
//  Created by Redbytes on 06/01/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "CustomNavigation.h"

@implementation CustomNavigation
@synthesize tempVC;
-(UIBarButtonItem *)addLeftBarButtonItem:(id)sender
{
    self.tempVC=(UIViewController *)sender;
    UIButton *backButton = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 30.0f, 30.0f)];
    UIImage *backImage = [UIImage imageNamed:@"back button icon.png"] ;
    [backButton setBackgroundImage:backImage  forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    return backButtonItem;

}
-(UIBarButtonItem *)addRightBarButtonItem:(id)sender
{
    
    
    UIButton *backButton = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 30.0f, 30.0f)];
    UIImage *backImage = [UIImage imageNamed:@"list button icon.png"] ;
    [backButton setBackgroundImage:backImage  forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(settingButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    return backButtonItem;

}
-(void)backButtonClicked
{

    [self.tempVC.navigationController popViewControllerAnimated:YES];
}
-(void)settingButtonClicked
{
    NSLog(@"setting Button Clicked");

}


@end
