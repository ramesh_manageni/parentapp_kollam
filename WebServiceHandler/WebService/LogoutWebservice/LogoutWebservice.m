//
//  LogoutWebservice.m
//  ParentApp
//
//  Created by Redbytes on 24/01/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "LogoutWebservice.h"
#import "ConnectionManager.h"

@implementation LogoutWebservice
-(id)init
{
	return self;
}


-(void)logoutFromServerWithUsername:(NSString *)username
{
    NSMutableDictionary *localDictionary=[[NSMutableDictionary alloc]init];
    
    
    NSMutableString *strApplicationUUID=[[NSMutableString alloc]init];
    NSUUID *oNSUUID = [[UIDevice currentDevice] identifierForVendor];
    [strApplicationUUID setString:[oNSUUID UUIDString]];
    [localDictionary setObject:strApplicationUUID forKey:@"device_id"];

    [localDictionary setObject:username forKey:@"username"];
    
    
    NSLog(@"Dict: %@",localDictionary);
    
    if (objConnectionManager!=nil)
    {
		objConnectionManager=nil;
	}
	objConnectionManager=[[ConnectionManager alloc]init];
    //  objConnectionManager.isSBTracker=@"YES";
    
    [objConnectionManager getWebData:self commandName:@"WSlogout" Requestparameter:localDictionary];
    

}


#pragma mark connection manager delegate
- (void)receivedData:(NSArray *)responseArray
{
    if ([_delegate respondsToSelector:@selector(serverResponseForLogoutWebservice:)])
    {
        [_delegate serverResponseForLogoutWebservice:(NSMutableDictionary *)responseArray];
    }
    else
    {
        [NSException raise:NSInternalInconsistencyException format:@"Delegate doesn't respond to serverResponseForCaptchaWebSerivce:"];
    }
}
-(void)failToReciveData
{
    if ([_delegate respondsToSelector:@selector(serverFailResponseForLogoutWebservice)])
    {
        [_delegate serverFailResponseForLogoutWebservice];
    }
}
#pragma mark - Delegate methods
- (id)delegate
{
	return  _delegate;
}

- (void)setDelegate:(id)new_delegate
{
	_delegate = new_delegate;
}

@end
