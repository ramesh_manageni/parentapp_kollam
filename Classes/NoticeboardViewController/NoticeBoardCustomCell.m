//
//  NoticeBoardCustomCell.m
//  ParentApp
//
//  Created by Redbytes on 14/02/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//
#define kFontName @"BradleyHandITCTT-Bold"

#import "NoticeBoardCustomCell.h"

@implementation NoticeBoardCustomCell

@synthesize NoticeBoardName,objImageViewBG,objImageViewStudent;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        NoticeBoardName =[[UILabel alloc]initWithFrame:CGRectMake(130, 12, 145, 50)];
		[NoticeBoardName setFont:[UIFont fontWithName:kFontName size:17]];
		[NoticeBoardName setBackgroundColor:[UIColor clearColor]];
        [NoticeBoardName setTextColor:[UIColor whiteColor]];
        [NoticeBoardName setTextAlignment:NSTextAlignmentCenter];
        
       
        objImageViewBG=[[UIImageView alloc] initWithFrame:CGRectMake(25, 0, 270, 75)];
        [objImageViewBG setImage:[UIImage imageNamed:@"attendance tab.png"]];
        [objImageViewBG setBackgroundColor:[UIColor grayColor]];
        objImageViewBG.layer.cornerRadius = 17.0;
        
        objImageViewStudent=[[UIImageView alloc] initWithFrame:CGRectMake(49, 12, 56, 52)];
        [objImageViewStudent setBackgroundColor:[UIColor greenColor]];
        [objImageViewStudent setImage:[UIImage imageNamed:@"pdf (1).png"]];
        

        [self.contentView addSubview:objImageViewBG];
        [self.contentView addSubview:objImageViewStudent];

        [self.contentView addSubview:NoticeBoardName];
        [self.contentView setBackgroundColor:[UIColor blackColor]];
        
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
