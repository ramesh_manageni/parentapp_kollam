
//
//  ConnectionManager.m
//  MovilTicket
//
//  Created by Neha Salankar on 04/02/13.yt
//  Copyright (c) 2013 Sachin. All rights reserved.
//

#import "ConnectionManager.h"
@implementation ConnectionManager
@synthesize isSBTracker,isRegistration,isThisUrl,isInboxWS,isReadInboxMessageWS,isDeleteInboxMessageWS,isComposeMessageWS,isNoticeCountWS,isNoticeReadWS,isSentWS;

#pragma mark -
#pragma mark Connection delegate for login service
-(id)init
{
    //GlobalObj = [Global sharedInstance];
	return self;
}
- (void)getWebData:(id)aDelegate commandName:(NSString*)commandName Requestparameter:(NSMutableDictionary*)theRequestParameters
{
    
    NSString *BASE_URL_GLOBAL = @"http://schoolccesoftware.com/oxfkparentapp/index.php/";
    
    if([isSBTracker isEqualToString:@"YES"])
    {
        BASE_URL_GLOBAL = @"http://54.209.147.134:8080/vehicleTracking/index.php/api/";
        isSBTracker=@"";
    }
    if([isRegistration isEqualToString:@"YES"])
    {
        BASE_URL_GLOBAL = @"http://schoolccesoftware.com/oxfkparentapp/PNGW/APIs/";
        isRegistration=@"";
        
    }
    if([isInboxWS isEqualToString:@"YES"]|| [isSentWS isEqualToString:@"YES"]|| [isReadInboxMessageWS isEqualToString:@"YES"]||[isDeleteInboxMessageWS isEqualToString:@"YES"]||[isComposeMessageWS isEqualToString:@"YES"] || [isNoticeCountWS isEqualToString:@"YES"] || [isNoticeReadWS isEqualToString:@"YES"])
    {
        BASE_URL_GLOBAL = @"http://schoolccesoftware.com/oxfordkollam/login/";
        isInboxWS=@"";
        isReadInboxMessageWS=@"";
        isDeleteInboxMessageWS=@"";
        isComposeMessageWS=@"";
        isNoticeCountWS=@"";
        isNoticeReadWS=@"";
        isSentWS =@"";
        
    }

   
    //To kill existing webservice call.
    [self stopLoading];
	
	[self setDelegate:aDelegate];
    
    NSString* url=[NSString stringWithFormat:@"%@%@",BASE_URL_GLOBAL,commandName];

    NSLog(@"getLocation URL :- %@",url);
    
	NSMutableURLRequest *theRequest=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]
															cachePolicy:NSURLRequestReloadIgnoringLocalCacheData
														timeoutInterval:60.0];
	//do post request for parameter passing
    [theRequest setHTTPMethod:@"POST"];
    
    NSError *error=nil;

    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:theRequestParameters
                                                       options:NSJSONWritingPrettyPrinted     error:&error];

    [theRequest setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    
    //set post data of request
    [theRequest setHTTPBody:jsonData];

	
    //clearing up the server connection and response object
    if (serverConnectionObj != nil)
    {
        serverConnectionObj = nil;
    }
    serverConnectionObj = [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
    if (responseMutableData !=nil)
    {
        responseMutableData = nil;
    }
	responseMutableData = [[NSMutableData alloc] init];
}

#pragma mark NSURLConnection delegate method starts
-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
	expectedData = (int)response.expectedContentLength;
	[responseMutableData setLength: 0];
    

}
-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
	[responseMutableData appendData:data];
    NSString *tempStr=[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    [self cleanSOAPResponseforJSON:tempStr];
    NSLog(@"CLEAN :%@",tempStr);
    
    NSLog(@"STRING DATA  : %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);

}
-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    if ([_delegate respondsToSelector:@selector(failToReciveData)])
    {
        [_delegate failToReciveData];
    }
    else
    {
        [NSException raise:NSInternalInconsistencyException format:@"Delegate doesn't respond to receivedData:"];
    }
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSError* error;
    NSMutableArray*   json = [NSJSONSerialization
                              JSONObjectWithData:responseMutableData
                              options:kNilOptions
                              error:&error];
    //NSLog(@"responseString: %@",json);
    [self setResponseFromServerToServiceClass:json];
    NSLog(@"ERROR: %@",error.description);
}

#pragma mark - NSURLConnection delegate method ends
-(void)stopLoading
{
    if(serverConnectionObj)
    {
        [serverConnectionObj cancel];
    }
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex==0)
    {
        if ([_delegate respondsToSelector:@selector(failToReciveData)])
        {
            [_delegate failToReciveData];
        }
    }
}
- (void)setResponseFromServerToServiceClass:(NSArray *)responseArray
{
    if ([_delegate respondsToSelector:@selector(receivedData:)])
	{
        [_delegate receivedData:responseArray];
	}
    else
	{
       // [CommonUtility showAlertView:@"Delegate doesn't respond to receivedData:" title:kTitleAlert delegateClass:nil];
	}
}


- (void)receivedData:(NSDictionary *)responseDictionary
{
    
}

#pragma mark - Delegate methods
- (id)delegate
{
	return _delegate;
}

- (void)setDelegate:(id)new_delegate
{
	_delegate = new_delegate;
}
#pragma mark - Clean SOAPResponsefor JSON

- (NSString*)cleanSOAPResponseforJSON:(NSString*)SOAPResponse
{
	NSScanner *thescanner;
	NSString *text = nil;
	
	thescanner = [NSScanner scannerWithString:SOAPResponse];
	
	while ([thescanner isAtEnd] == NO)
	{
		[thescanner scanUpToString:@"<" intoString:NULL];
		[thescanner scanUpToString:@">" intoString:&text];
		SOAPResponse = [SOAPResponse stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%@>", text] withString:
						@""];
	}
	return SOAPResponse;
}
-(void)dealloc
{
    if(serverConnectionObj != nil)
    {
        serverConnectionObj = nil;
    }
}
- (NSString *)filterSpecialSymbols:(NSString *)string
{
    string=[string stringByReplacingOccurrencesOfString:@"&#165;" withString:@"¥"];
    string=[string stringByReplacingOccurrencesOfString:@"&#163;" withString:@"£"];
    string=[string stringByReplacingOccurrencesOfString:@"&#8364;" withString:@"€"];
    return string;
}
@end
