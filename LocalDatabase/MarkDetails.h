//
//  MarkDetails.h
//  ParentApp
//
//  Created by Redbytes on 05/02/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class StudentResult;

@interface MarkDetails : NSManagedObject

@property (nonatomic, retain) NSString * batch_id;
@property (nonatomic, retain) NSString * entry_status;
@property (nonatomic, retain) NSString * exam;
@property (nonatomic, retain) NSString * exam_id;
@property (nonatomic, retain) NSString * grade;
@property (nonatomic, retain) NSString * mark;
@property (nonatomic, retain) NSString * mark_t;
@property (nonatomic, retain) NSString * max_mark_t;
@property (nonatomic, retain) NSString * order_id;
@property (nonatomic, retain) NSString * student_id;
@property (nonatomic, retain) NSString * subject;
@property (nonatomic, retain) NSString * subject_id;
@property (nonatomic, retain) NSString * subject_mark_id;
@property (nonatomic, retain) StudentResult *marksToResult;

@end
