//
//  InboxWebservice.m
//  ParentApp
//
//  Created by Imtiyaz on 8/8/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "InboxWebservice.h"


@implementation InboxWebservice

-(void)InboxMethodForUsername:(NSString *)username
{
    
    NSMutableDictionary *localDictionary=[[NSMutableDictionary alloc]init];
    
    
    //NSString *appVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    
    
    NSMutableString *strApplicationUUID=[[NSMutableString alloc]init];
    NSUUID *oNSUUID = [[UIDevice currentDevice] identifierForVendor];
    [strApplicationUUID setString:[oNSUUID UUIDString]];
    
   // [localDictionary setObject:strApplicationUUID forKey:@"device_id"];
    
    
#pragma hardcoded values
    
	[localDictionary setObject:username forKey:@"username"];
    [localDictionary setObject:institute_id forKey:@"institute_id"];
    
    

    //[localDictionary setObject:appVersion forKey:@"app_version"];
    
    
    NSLog(@"Dict: %@",localDictionary);
    
    if (objConnectionManager!=nil)
    {
		objConnectionManager=nil;
	}
	objConnectionManager=[[ConnectionManager alloc]init];
   // objConnectionManager.isThisUrl=@"http://schoolccesoftware.com/";
    
    objConnectionManager.isInboxWS=@"YES";
    [objConnectionManager getWebData:self commandName:@"phorum/Inbox" Requestparameter:localDictionary];
    
 //http://schoolccesoftware.com/oxforddemo/login/Phorum/Inbox
}
#pragma mark connection manager delegate
- (void)receivedData:(NSArray *)responseArray
{
    if ([_delegate respondsToSelector:@selector(serverResponseForInboxWebservice:)])
    {
        [_delegate serverResponseForInboxWebservice:responseArray];
    }
    else
    {
        [NSException raise:NSInternalInconsistencyException format:@"Delegate doesn't respond to serverResponseForCaptchaWebSerivce:"];
    }
}
-(void)failToReciveData
{
    if ([_delegate respondsToSelector:@selector(serverFailResponseForInboxwebservice)])
    {
        [_delegate serverFailResponseForInboxwebservice];
    }
}
#pragma mark - Delegate methods
- (id)delegate
{
	return  _delegate;
}

- (void)setDelegate:(id)new_delegate
{
	_delegate = new_delegate;
}

@end
