//
//  FeeViewController.h
//  ParentApp
//
//  Created by Redbytes on 03/01/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FeeWebservice.h"
#import "MBProgressHUD.h"

#import "DXSemiViewControllerCategory.h"
#import "DXSubClassSemiViewController.h"
#import "DXSubclassSemiTableViewController.h"
@protocol MenuButtonFeesDelegate <NSObject>

-(void)didSelectRowFees:(NSInteger)rowcount;

@end

@interface FeeViewController : UIViewController<FeeDelegate,MBProgressHUDDelegate,SliderMenuButtonDelegate>
{
    MBProgressHUD *HUD;
    int entityCount;
    NSMutableArray *feeDetailsArray;
    id _delegate;
    DXSubclassSemiTableViewController *semiLeft;

}
@property (strong, nonatomic) IBOutlet UILabel *labelDuedate;
@property (strong, nonatomic) IBOutlet UILabel *studentName;
@property (strong, nonatomic) IBOutlet UILabel *labelTutionFee;
@property (strong, nonatomic) IBOutlet UILabel *labelBusFee;
@property (strong, nonatomic) IBOutlet UILabel *labelAdminFee;
@property (strong, nonatomic) IBOutlet UILabel *labelActFee;
@property (strong, nonatomic) IBOutlet UILabel *labelOtherFee;
@property (strong, nonatomic) IBOutlet UILabel *labelTotalFee;

@property(strong,nonatomic)NSString *username;
@property(strong,nonatomic)NSString *student_id;
@property(strong,nonatomic)NSString *student_name;
@property NSInteger index;




- (id)delegate;
- (void)setDelegate:(id)new_delegate;




@end
