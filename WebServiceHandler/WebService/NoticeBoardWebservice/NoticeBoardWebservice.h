//
//  NoticeBoardWebservice.h
//  ParentApp
//
//  Created by Redbytes on 24/01/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol NoticeBoardDelegate <NSObject>

-(void)serverResponseForNoticeBoardWebservice:(NSMutableDictionary*)responseArray;
-(void)serverFailResponseForNoticeBoardWebservice;

@end
//********************************************************************

@class ConnectionManager;

@interface NoticeBoardWebservice : NSObject
{
    id _delegate;
    NSMutableDictionary *resultDictionary;
    ConnectionManager *objConnectionManager;
}
- (id)delegate;
- (void)setDelegate:(id)new_delegate;
-(void)GetNoticeForUsername:(NSString *)username profileId:(NSString *)profileId;
@end
