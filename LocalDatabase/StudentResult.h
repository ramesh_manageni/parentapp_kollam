//
//  StudentResult.h
//  ParentApp
//
//  Created by Redbytes on 05/02/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class MarkDetails, StudentDetails;

@interface StudentResult : NSManagedObject

@property (nonatomic, retain) NSString * exam_code;
@property (nonatomic, retain) NSString * exam_id;
@property (nonatomic, retain) NSString * result_date;
@property (nonatomic, retain) NSSet *resultToMarks;
@property (nonatomic, retain) StudentDetails *resultToStudent;
@end

@interface StudentResult (CoreDataGeneratedAccessors)

- (void)addResultToMarksObject:(MarkDetails *)value;
- (void)removeResultToMarksObject:(MarkDetails *)value;
- (void)addResultToMarks:(NSSet *)values;
- (void)removeResultToMarks:(NSSet *)values;

@end
