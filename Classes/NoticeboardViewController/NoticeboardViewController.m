//
//  NoticeboardViewController.m
//  ParentApp
//
//  Created by Redbytes on 10/01/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "NoticeboardViewController.h"
#import "Constant.h"
#import "Parent.h"
#import "StudentDetails.h"
#import "NoticePDFViewController.h"

#import "DXSemiViewControllerCategory.h"
#import "DXSubClassSemiViewController.h"
#import "DXSubclassSemiTableViewController.h"



@interface NoticeboardViewController ()
{
    int readNoticeCount;
    NSMutableArray *noticeDataArray;
    NSString *path;
}
@end

@implementation NoticeboardViewController
@synthesize controller,webView,tableviewObject;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setTitle:@"Noticeboard"];
    
    self.navigationController.navigationBarHidden=NO;
    
    [self.navigationItem setHidesBackButton:YES];
   // self.navigationItem.rightBarButtonItem = [self addRightBarButtonItem];
    self.navigationItem.leftBarButtonItem = [self addLeftBarButtonItem];
    self.navigationItem.rightBarButtonItem = [self addRightBarButtonItem];
    [self.view setBackgroundColor:[UIColor blackColor]];

    
    UIScreen *mainScreen = [UIScreen mainScreen];
    CGFloat scale = ([mainScreen respondsToSelector:@selector(scale)] ? mainScreen.scale : 1.0f);
    CGFloat pixelHeight = (CGRectGetHeight(mainScreen.bounds) * scale);
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        if (scale == 2.0f)
        {
            if (pixelHeight == 960.0f)
            {
                tableviewObject=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, 320, 480-49) style:UITableViewStyleGrouped];
            }
            
            else if (pixelHeight == 1136.0f)
            {
                tableviewObject=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, 320, 568-49) style:UITableViewStyleGrouped];
            }
        }
        
    }
    
    
    [tableviewObject setBackgroundColor:[UIColor clearColor]];
    tableviewObject.separatorColor = [UIColor clearColor];
    tableviewObject.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    tableviewObject.delegate=self;
    tableviewObject.dataSource=self;
    [self.view addSubview:tableviewObject];
    
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
	[self.view addSubview:HUD];
	
	// Regiser for HUD callbacks so we can remove it from the window at the right time
	HUD.delegate = self;
	
	// Show the HUD while the provided method executes in a new thread
	//[HUD showWhileExecuting:@selector(myTask) onTarget:self withObject:nil animated:YES];
    [HUD show:YES];
    
    NoticeBoardWebservice *objNoticeBoardWebservice=[[NoticeBoardWebservice alloc]init];
    objNoticeBoardWebservice.delegate=self;
    [objNoticeBoardWebservice GetNoticeForUsername:[[parentArrayCoreData objectAtIndex:0] user_name] profileId:[[parentArrayCoreData objectAtIndex:0] profile_id]];

    // Create Plist
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"/PDFFolder"];
    path = [dataPath stringByAppendingPathComponent:@"noticeboard.plist"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath: path])
    {
        path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"noticeboard.plist"] ];
    }
    noticeDataArray = [[NSMutableArray alloc] init];
}

-(void) viewDidAppear:(BOOL)animated
{
    //[ParseClass incrementView];
}

-(void) viewWillAppear:(BOOL)animated
{
    noticeBoardBadgeCount = 0;
    [tableviewObject reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(UIBarButtonItem *)addLeftBarButtonItem
{
    UIButton *backButton = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 30.0f, 30.0f)];
    UIImage *backImage = [UIImage imageNamed:@"back button icon.png"] ;
    [backButton setBackgroundImage:backImage  forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    return backButtonItem;
}

-(UIBarButtonItem *)addRightBarButtonItem
{
    UIButton *backButton = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 30.0f, 30.0f)];
    UIImage *backImage = [UIImage imageNamed:@"list button icon.png"] ;
    [backButton setBackgroundImage:backImage  forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(settingButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    return backButtonItem;
    
}
-(void)backButtonClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)settingButtonClicked
{
    
    if(semiLeft!=nil)
    {
        [semiLeft dismissSemi:nil];
    }

    //[self.navigationController popToViewController:objHomeViewController animated:YES];
    semiLeft = [[DXSubclassSemiTableViewController alloc] init];
    semiLeft.delegate=self;
    
    semiLeft.semiTitleLabel.text = @"SemiLeftTableView";
    semiLeft.tableViewRowHeight = 80.0f/2;
    [semiLeft.view setBackgroundColor:[UIColor clearColor]];
    self.leftSemiViewController = semiLeft;
    
}
#pragma mark NoticeBoardWebservise Delegates
-(void)serverResponseForNoticeBoardWebservice:(NSMutableArray*)responseArray;
{
//    NSMutableArray *tempArray = [[NSMutableArray alloc] init];
//    NSMutableDictionary *temp = [[NSMutableDictionary alloc] init];
//    [temp setObject:@"https://gradcollege.okstate.edu/sites/default/files/PDF_linking.pdf" forKey:@"circular"];
//    NSMutableDictionary *temp1 = [[NSMutableDictionary alloc] init];
//    [temp1 setObject:@"http://www.antennahouse.com/XSLsample/pdf/sample-link_1.pdf" forKey:@"circular"];
//    
//    [tempArray addObject:temp];
//    [tempArray addObject:temp1];
//    
//    responseArray = tempArray;
    
    //// Removing Old PDF Files's folder from document directory
    NSFileManager *fm = [NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"/PDFFolder"];
    NSError *error = nil;
    [fm removeItemAtPath:dataPath error:&error];
    
    [HUD hide:YES];
    if (responseArray!=nil)
    {
        NSLog(@"Server Responce: %@",responseArray);
        noticeboardArray = [[NSMutableArray alloc] initWithArray:responseArray];
        [[NSUserDefaults standardUserDefaults] setObject:noticeboardArray forKey:@"noticeboard_list"];
        [tableviewObject reloadData];
        for(int i=0;i<[noticeboardArray count];i++)
        {
            NSString *filename;
            if ([noticeboardArray valueForKey:@"circular"] != nil)
            {
                NSString *urlString = [[noticeboardArray objectAtIndex:i] valueForKey:@"circular"];
                NSArray *parts = [urlString componentsSeparatedByString:@"/"];
                filename = [parts objectAtIndex:[parts count]-1];
            }
            
            
            
            NSURL  *url = [NSURL URLWithString:[[noticeboardArray objectAtIndex:i] valueForKey:@"circular"]];
            NSData *urlData = [NSData dataWithContentsOfURL:url];
            
            if (urlData)
            {
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                NSString *documentsDirectory = [paths objectAtIndex:0];
                NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"/PDFFolder"];
                
                if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
                {
                    NSError *error;
                    [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error];
                }
                
                NSString *filePath = [NSString stringWithFormat:@"%@/%@", dataPath,filename];
                [urlData writeToFile:filePath atomically:YES];
            }
        }
    }
    else if(responseArray==nil)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Notice not published yet" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
}

-(void)serverFailResponseForNoticeBoardWebservice;
{
    [HUD hide:YES];
    noticeboardArray = [[NSMutableArray alloc] initWithArray:((NSMutableArray *) [[NSUserDefaults standardUserDefaults] objectForKey:@"noticeboard_list"])];
    if(noticeboardArray==nil)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Server response failed" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    else
    {
        [tableviewObject reloadData];
        
    }
}

#pragma mark Table View Datasource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	tableviewObject = (UITableView *)tableView;
    return 75;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
	return [noticeboardArray count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
	return 1;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    NoticeBoardCustomCell *cell = (NoticeBoardCustomCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[NoticeBoardCustomCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
		[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    
    // Configure the cell...
    NSString *urlString = [[noticeboardArray objectAtIndex:indexPath.section] valueForKey:@"circular"];
    NSArray *parts = [urlString componentsSeparatedByString:@"/"];
    NSString *filename = [parts objectAtIndex:[parts count]-1];
    
    NSLog(@"%@",filename);
	
    cell.NoticeBoardName.text=filename;
    
    NSArray *contentArray1 = [[NSArray alloc] init];
    contentArray1 = [NSArray arrayWithContentsOfFile:path];
    if ([contentArray1 containsObject:filename])
    {
        cell.objImageViewBG.backgroundColor = [UIColor blackColor];
    }
    if (cell.objImageViewBG.backgroundColor == [UIColor grayColor])
    {
        noticeBoardBadgeCount++;
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *urlString = [[noticeboardArray objectAtIndex:indexPath.section] valueForKey:@"circular"];
    NSArray *parts = [urlString componentsSeparatedByString:@"/"];
    NSString *filename = [parts objectAtIndex:[parts count]-1];
    
    NoticePDFViewController *objNoticePDFViewController=[[NoticePDFViewController alloc]initWithNibName:@"NoticePDFViewController" bundle:nil];
    objNoticePDFViewController.pdfNmaeNotice=filename;
    objNoticePDFViewController.delegate= self.delegate;
    [self.navigationController pushViewController:objNoticePDFViewController animated:YES];

    // Adding read notices into plist
    NSArray *contentArray= [NSArray arrayWithContentsOfFile:path];
    if (contentArray == nil)
    {
        [noticeDataArray addObject:filename];
        [noticeDataArray writeToFile:path atomically:YES];
    }
    else
    {
        if (![contentArray containsObject:filename])
        {
            noticeDataArray = [contentArray mutableCopy];
            [noticeDataArray addObject:filename];
            [noticeDataArray writeToFile:path atomically:YES];
        }
    }
}

#pragma mark SliderMenuDelegate
-(void)didSelectSliderMenuButton:(NSInteger)rowcount
{
    // NSLog(@"ROWCOUNT=== %d",rowcount);
    if(rowcount==0)
    {
        [self.navigationController popToViewController:objHomeViewController animated:YES];
        
    }
    else
    {
        [self.navigationController popToViewController:objHomeViewController animated:NO];
        
    }
    if ([_delegate respondsToSelector:@selector(didSelectRowNoticeboard:)])
    {
        [_delegate didSelectRowNoticeboard:rowcount];
    }

}

#pragma mark Delegate

- (id)delegate
{
	return  _delegate;
}

- (void)setDelegate:(id)new_delegate
{
	_delegate = new_delegate;
}





@end
