//
//  ExamGradeViewController.m
//  ParentApp
//
//  Created by Redbytes on 03/01/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "ExamGradeViewController.h"
#import "ChangePasswordViewController.h"
#import "Constant.h"
#import "SubjectGradeViewController.h"
#import "StudentResult.h"
#import "MarkDetails.h"
@interface ExamGradeViewController ()

@end

@implementation ExamGradeViewController

@synthesize tableviewObject;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        examNameArray=[[NSMutableArray alloc] init];
        examGradeArray=[[NSMutableArray alloc] initWithObjects:@"B+",@"A",@"B+",@"A+", nil];
        
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated
{
    [self setTitle:@"Exam Details"];

}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setTitle:@"Exam Details"];
    resultDict=(NSDictionary *)responseArrayResult;
    NSLog(@"Dict: %@",resultDict);
    
    [self setTitle:@""];
    for(NSString *key in [resultDict allKeys])
    {
        //NSLog(@"Single object in Dict:\n%@",[resultDict objectForKey:key]);
        [examNameArray addObject:[resultDict objectForKey:key]];
    }
    responseArraySubjectGrade=[[NSMutableArray alloc]initWithArray:examNameArray];
    
    self.navigationController.navigationBarHidden=NO;
    [self.navigationItem setHidesBackButton:YES];
    
    self.navigationItem.rightBarButtonItem = [self addRightBarButtonItem];
    self.navigationItem.leftBarButtonItem = [self addLeftBarButtonItem];

    // Do any additional setup after loading the view from its nib.
    [self.view setBackgroundColor:[UIColor blackColor]];
    
    
    UIScreen *mainScreen = [UIScreen mainScreen];
    CGFloat scale = ([mainScreen respondsToSelector:@selector(scale)] ? mainScreen.scale : 1.0f);
    CGFloat pixelHeight = (CGRectGetHeight(mainScreen.bounds) * scale);
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        if (scale == 2.0f)
        {
            if (pixelHeight == 960.0f)
            {
                tableviewObject=[[UITableView alloc]initWithFrame:CGRectMake(0, 109, 320, 480-158) style:UITableViewStylePlain];
            }
            
            else if (pixelHeight == 1136.0f)
            {
                tableviewObject=[[UITableView alloc]initWithFrame:CGRectMake(0, 109, 320, 568-158) style:UITableViewStylePlain];
            }
        }
        
    }

    [tableviewObject setBackgroundColor:[UIColor clearColor]];
    tableviewObject.separatorColor = [UIColor clearColor];
    tableviewObject.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    tableviewObject.delegate=self;
    tableviewObject.dataSource=self;
    [self.view addSubview:tableviewObject];
    
}

-(void) viewDidAppear:(BOOL)animated
{
    [ParseClass incrementView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UIBarButtonItem *)addLeftBarButtonItem
{
    UIButton *backButton = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 30.0f, 30.0f)];
    UIImage *backImage = [UIImage imageNamed:@"back button icon.png"] ;
    [backButton setBackgroundImage:backImage  forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    return backButtonItem;
    
}
-(UIBarButtonItem *)addRightBarButtonItem
{
    
    
    UIButton *backButton = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 30.0f, 30.0f)];
    UIImage *backImage = [UIImage imageNamed:@"list button icon.png"] ;
    [backButton setBackgroundImage:backImage  forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(settingButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    return backButtonItem;
    
}
-(void)backButtonClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)settingButtonClicked
{
    
    
    if(semiLeft!=nil)
    {
        [semiLeft dismissSemi:nil];
    }

    //[self.navigationController popToViewController:objHomeViewController animated:YES];
    semiLeft = [[DXSubclassSemiTableViewController alloc] init];
    semiLeft.delegate=self;
    
    semiLeft.semiTitleLabel.text = @"SemiLeftTableView";
    semiLeft.tableViewRowHeight = 80.0f/2;
    [semiLeft.view setBackgroundColor:[UIColor clearColor]];
    self.leftSemiViewController = semiLeft;
    
    
}
#pragma mark Table View Datasource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	tableviewObject = (UITableView *)tableView;
    return 54;
	
	
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
	return [examcodeArrayCoreData count];
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    ResultCustomCell *cell = (ResultCustomCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[ResultCustomCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
		[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    
    // Configure the cell...
	
	
    //cell.labelGrade.text=[examGradeArray objectAtIndex:indexPath.row];
                          
    cell.labelExamName.text=[[examcodeArrayCoreData objectAtIndex:indexPath.row] exam_code];

    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SubjectGradeViewController *objSubjectGradeViewController=[[SubjectGradeViewController alloc]initWithNibName:@"SubjectGradeViewController" bundle:nil];
    
    objSubjectGradeViewController.delegate=self.delegate;
    
    [objSubjectGradeViewController.subjectArray removeAllObjects];
    
    for(MarkDetails *marks in [[examcodeArrayCoreData objectAtIndex:indexPath.row] resultToMarks])
    {
        [objSubjectGradeViewController.subjectArray addObject:marks];
        NSLog(@"Subject Name: %@",marks.subject);
    }
    [self.navigationController pushViewController:objSubjectGradeViewController animated:YES];
    
}
#pragma mark SliderMenuDelegate
-(void)didSelectSliderMenuButton:(NSInteger)rowcount
{
    // NSLog(@"ROWCOUNT=== %d",rowcount);
    if(rowcount==0)
    {
        [self.navigationController popToViewController:objHomeViewController animated:YES];
        
    }
    else
    {
        [self.navigationController popToViewController:objHomeViewController animated:NO];
        
    }
    if ([_delegate respondsToSelector:@selector(didSelectRowExamGrade:)])
    {
        [_delegate didSelectRowExamGrade:rowcount];
    }
    
}

#pragma mark Delegate

- (id)delegate
{
	return  _delegate;
}

- (void)setDelegate:(id)new_delegate
{
	_delegate = new_delegate;
}



@end
