//
//  RegistrationWebservice.h
//  ParentApp
//
//  Created by Redbytes on 06/02/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol RegistrationWebserviceDelegate <NSObject>

-(void)serverResponseForRegistrationWebservice:(NSMutableDictionary*)responseArray;
-(void)serverFailResponseForRegistrationWebservice;

@end
//********************************************************************

@class ConnectionManager;

@interface RegistrationWebservice : NSObject
{
    id _delegate;
    NSMutableDictionary *resultDictionary;
    ConnectionManager *objConnectionManager;
}

- (id)delegate;
- (void)setDelegate:(id)new_delegate;
-(void)registerDeviceWithUsername:(NSString*)username password:(NSString*)password email:(NSString*)email contact:(NSString*)contact deviceOS:(NSString*)deviceOS deviceVenderId:(NSString*)deviceVenderId;

@end
