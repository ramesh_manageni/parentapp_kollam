//
//  AttendanceTableViewController.m
//  ParentApp
//
//  Created by Redbytes on 08/01/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "AttendanceTableViewController.h"
#import <CoreGraphics/CoreGraphics.h>
#import "CKCalendarView.h"
#import "Constant.h"
#import "CustomCellEvents.h"

@interface AttendanceTableViewController ()<CKCalendarDelegate,UIGestureRecognizerDelegate>
{
    UIView *alertView;
    UIView *customView,*animationView;
    //UITapGestureRecognizer *tapGesture;
    NSString *stringSelectedDate;
}

@property(nonatomic, weak) CKCalendarView *calendar;
@property(nonatomic, strong) UILabel *dateLabel;
@property(nonatomic, strong) NSDateFormatter *dateFormatter;
@property(nonatomic, strong) NSDate *minimumDate;
@property(nonatomic, strong) NSArray *appliedLeaveDates;
@property(nonatomic, strong) NSArray *schoolHolidayDates;
@property(nonatomic, strong) NSMutableArray *leaveDates;

@property(nonatomic, strong) NSMutableArray *eventDates;
@property(nonatomic, strong) NSMutableDictionary *eventDict;
@property(nonatomic, strong) NSMutableArray *selectedDateEventArray;

@end

@implementation AttendanceTableViewController
@synthesize _tableView;

- (id)init
{
    self = [super init];
    if (self)
    {
        CKCalendarView *calendar = [[CKCalendarView alloc] initWithStartDay:startSunday fromview:1];
        self.calendar = calendar;
        calendar.delegate = self;
        self.dateFormatter = [[NSDateFormatter alloc] init];
        [self.dateFormatter setDateFormat:@"yyyy-MM-dd"];
        self.minimumDate = [self.dateFormatter dateFromString:@"2000-12-31"];
        
        self.leaveDates=[[NSMutableArray alloc]init];
        for(int i=0;i<[absentDatesArrayCoreData count];i++)
        {
            [self.leaveDates addObject:[self.dateFormatter dateFromString:[absentDatesArrayCoreData objectAtIndex:i]]];
        }
        NSLog(@"%@",self.leaveDates);
        
        self.eventDates=[[NSMutableArray alloc]init];
        for(int i=0;i<[eventDatesArrayCoredata count];i++)
        {
            [self.eventDates addObject:[self.dateFormatter dateFromString:[eventDatesArrayCoredata objectAtIndex:i]]];
        }
        
        self.schoolHolidayDates = @[];
        self.appliedLeaveDates = @[];
        
        calendar.onlyShowCurrentMonth = NO;
        calendar.adaptHeightToNumberOfWeeksInMonth = YES;
        
        calendar.frame = CGRectMake(0, 65, 320, 0);
        [self.view addSubview:calendar];
        if(countOfNextMonth<10)
        {
            for(int k=(int)countOfNextMonth;k<10;k++)
            {
                [calendar gotoPreviousMonth];
            }
        }
        if(countOfNextMonth==11)
        {
            [calendar gotoNextMonth];
        }
        self.dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, CGRectGetMaxY(calendar.frame) + 4, self.view.bounds.size.width, 24)];
        // [self.view addSubview:self.dateLabel];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(localeDidChange) name:NSCurrentLocaleDidChangeNotification object:nil];
    }
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setTitle:@"Attendance Details"];
    self.navigationController.navigationBarHidden=NO;
    [self.navigationItem setHidesBackButton:YES];
    
    self.navigationItem.rightBarButtonItem = [self addRightBarButtonItem];
    self.navigationItem.leftBarButtonItem = [self addLeftBarButtonItem];
    
    // Do any additional setup after loading the view from its nib.
    [self.view setBackgroundColor:[UIColor blackColor]];
}

-(void) viewDidAppear:(BOOL)animated
{
    [ParseClass incrementView];
}

-(void) viewWillAppear:(BOOL)animated
{
    UIScreen *mainScreen = [UIScreen mainScreen];
    CGFloat scale = ([mainScreen respondsToSelector:@selector(scale)] ? mainScreen.scale : 1.0f);
    CGFloat pixelHeight = (CGRectGetHeight(mainScreen.bounds) * scale);
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        if (scale == 2.0f)
        {
            if (pixelHeight == 960.0f)
            {
                //self.eventsCustomBottomView.frame = CGRectMake(00,390, 320, 19);
                currentAttendanceTableVC = YES;
            }
            else
            {
                //self.eventsCustomBottomView.frame = CGRectMake(50,800, 320, 19);
                currentAttendanceTableVC = NO;
            }
        }
    }
    self.eventsCustomBottomView.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.1];
}

-(void) viewWillDisappear:(BOOL)animated
{
    currentAttendanceTableVC = NO;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
    } else {
        return YES;
    }
}

-(UIBarButtonItem *)addLeftBarButtonItem
{
    UIButton *backButton = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 30.0f, 30.0f)];
    UIImage *backImage = [UIImage imageNamed:@"back button icon.png"] ;
    [backButton setBackgroundImage:backImage  forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    return backButtonItem;
    
}
-(UIBarButtonItem *)addRightBarButtonItem
{
    UIButton *backButton = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 30.0f, 30.0f)];
    UIImage *backImage = [UIImage imageNamed:@"list button icon.png"] ;
    [backButton setBackgroundImage:backImage  forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(settingButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    return backButtonItem;
    
}
-(void)backButtonClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)settingButtonClicked
{
    if(semiLeft!=nil)
    {
        [semiLeft dismissSemi:nil];
    }

    semiLeft = [[DXSubclassSemiTableViewController alloc] init];
    semiLeft.delegate=self;
    
    semiLeft.semiTitleLabel.text = @"SemiLeftTableView";
    semiLeft.tableViewRowHeight = 80.0f/2;
    [semiLeft.view setBackgroundColor:[UIColor clearColor]];
    self.leftSemiViewController = semiLeft;
}
///////////////////////////////////////
- (void)localeDidChange {
    [self.calendar setLocale:[NSLocale currentLocale]];
}

- (BOOL)dateIsAppliedLeave:(NSDate *)date {
    for (NSDate *disabledDate in self.appliedLeaveDates) {
        if ([disabledDate isEqualToDate:date]) {
            return YES;
        }
    }
    return NO;
}
- (BOOL)dateIsHoliday:(NSDate *)date {
    for (NSDate *disabledDate in self.schoolHolidayDates) {
        if ([disabledDate isEqualToDate:date]) {
            return YES;
        }
    }
    return NO;
}
- (BOOL)dateIsLeave:(NSDate *)date {
    for (NSDate *disabledDate in self.leaveDates) {
        if ([disabledDate isEqualToDate:date]) {
            return YES;
        }
    }
    return NO;
}

-(BOOL)dateIsEvent:(NSDate *)date{
    for (NSDate *disabledDate in self.eventDates){
        if ([disabledDate isEqualToDate:date]){
            return YES;
        }
    }
    return NO;
}
-(UIColor *)changeBgTo:(NSString *)hexCode
{
    unsigned long red, green, blue;
    sscanf([hexCode UTF8String], "#%2lX%2lX%2lX", &red, &green, &blue);
    UIColor *color = [UIColor colorWithRed:red/255.0 green:green/255.0 blue:blue/255.0 alpha:1];
    
    return color;
}
#pragma mark -
#pragma mark - CKCalendarDelegate

- (void)calendar:(CKCalendarView *)calendar configureDateItem:(CKDateItem *)dateItem forDate:(NSDate *)date
{
    if(sundayCount==6||sundayCount==13||sundayCount==20||sundayCount==34||sundayCount==27||sundayCount==41||sundayCount==5||sundayCount==12||sundayCount==19||sundayCount==33||sundayCount==26||sundayCount==40)
    {
        if(![dateItem.textColor isEqual:[UIColor darkGrayColor]])
        {
            dateItem.backgroundColor = [self changeBgTo:@"#006bff"];
        }
        // dateItem.textColor = [UIColor whiteColor];
        
    }
    sundayCount++;
    if(sundayCount==42)
    {
        sundayCount=0;
    }
    // TODO: play with the coloring if we want to...
    if ([self dateIsHoliday:date])
    {
        dateItem.backgroundColor = [self changeBgTo:@"#006bff"];
        dateItem.textColor = [UIColor whiteColor];
    }
    if ([self dateIsLeave:date])
    {
        dateItem.backgroundColor = [self changeBgTo:@"#fe0000"];
        dateItem.textColor = [UIColor whiteColor];
    }
    if ([self dateIsAppliedLeave:date])
    {
        dateItem.backgroundColor = [self changeBgTo:@"#a700fe"];
        dateItem.textColor = [UIColor whiteColor];
    }
    if ([self dateIsEvent:date])
    {
        dateItem.backgroundColor = [UIColor greenColor];
        dateItem.textColor = [UIColor blueColor];
    }
}

// For custom alertView
// by nihal
//-(void) addGasture
//{
//    tapGesture = [[UITapGestureRecognizer alloc] init];
//    tapGesture.delegate = self;
//    tapGesture.numberOfTapsRequired = 1;
//    tapGesture.numberOfTouchesRequired = 1;
//    [tapGesture addTarget:self action:@selector(handleTap:)];
//    [alertView1 addGestureRecognizer:tapGesture];
//}

//-(void)handleTap:(id)sender
//{
//    [alertView removeFromSuperview];
//    [alertView1 removeFromSuperview];
//}

-(void) buttonPressMethod:(NSDate *)date
{
    stringSelectedDate = [self.dateFormatter stringFromDate:date];
    self.selectedDateEventArray = [[NSMutableArray alloc] init];
    
    for (NSDictionary *dict in eventsArrayCoredata)
    {
        //[self.eventDict setValue:[dict objectForKey:@"eventDate"] forKey:@"eventDate"];
        if ([[dict objectForKey:@"eventDate"] isEqualToString:stringSelectedDate])
        {
            self.eventDict = [[NSMutableDictionary alloc] init];
            [self.eventDict setValue:[dict objectForKey:@"eventDate"] forKey:@"eventDate"];
            [self.eventDict setValue:[dict objectForKey:@"eventLocaion"] forKey:@"eventLocaion"];
            [self.eventDict setValue:[dict objectForKey:@"eventName"] forKey:@"eventName"];
            [self.eventDict setValue:[dict objectForKey:@"eventStartTime"] forKey:@"eventStartTime"];
            [self.eventDict setValue:[dict objectForKey:@"eventEndTime"] forKey:@"eventEndTime"];
            [self.eventDict setValue:[dict objectForKey:@"eventSubject"] forKey:@"eventSubject"];
            
            [self.selectedDateEventArray addObject:self.eventDict];
        }
    }
    
    if ([self dateIsEvent:date])
    {
        if (self.selectedDateEventArray.count == 1)
        {
            animationView = [[UIView alloc] initWithFrame:CGRectMake(35,self.view.frame.size.height/2-55+32,252,115)];
            
            _tableView = [[UITableView alloc] initWithFrame:CGRectMake(00, 00, animationView.frame.size.width, animationView.frame.size.height) style:UITableViewStylePlain];
            _tableView.scrollEnabled = NO;
            
        }
        else if (self.selectedDateEventArray.count == 2)
        {
            animationView = [[UIView alloc] initWithFrame:CGRectMake(35,self.view.frame.size.height/2-100+32,252,200)];
            
            _tableView = [[UITableView alloc] initWithFrame:CGRectMake(00, 00, animationView.frame.size.width, animationView.frame.size.height) style:UITableViewStylePlain];
            _tableView.showsVerticalScrollIndicator=NO;
        }
        else
        {
            animationView = [[UIView alloc] initWithFrame:CGRectMake(35,self.view.frame.size.height/2-160+32,252,320)];
            _tableView = [[UITableView alloc] initWithFrame:CGRectMake(00, 00, animationView.frame.size.width, animationView.frame.size.height) style:UITableViewStylePlain];
            _tableView.showsVerticalScrollIndicator=NO;
        }
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.allowsSelection = NO;
        [_tableView setBackgroundColor:[UIColor clearColor]];
        
        customView = [[UIView alloc] initWithFrame:CGRectMake(00, 00, self.view.frame.size.width, self.view.frame.size.height)];
        customView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.85f];
        [self.view addSubview:customView];
        
        [customView addSubview:animationView];
        [animationView addSubview:_tableView];
        [self initialDelayEnded];
    }
}

-(void)initialDelayEnded
{
    animationView.transform = CGAffineTransformScale(CGAffineTransformMake(1.0, 0.0, 1.0, 0.0, 0.0, 00), 1.0, 0.0);
    //animationView.transform = CGAffineTransformRotate(animationView.transform, 180);
    animationView.alpha = 1.0;
    animationView.backgroundColor = [UIColor clearColor];
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:1/1.5];
    [UIView setAnimationDelegate:self];
    
    animationView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.0, 1.0);
    [UIView commitAnimations];
}


- (BOOL)calendar:(CKCalendarView *)calendar willSelectDate:(NSDate *)date
{
    //    return ![self dateIsDisabled:date];
    return NO;
}

- (void)calendar:(CKCalendarView *)calendar didSelectDate:(NSDate *)date
{
   // self.dateLabel.text = [self.dateFormatter stringFromDate:date];
}

- (BOOL)calendar:(CKCalendarView *)calendar willChangeToMonth:(NSDate *)date
{
    if ([date laterDate:self.minimumDate] == date) {
        self.calendar.backgroundColor = [UIColor clearColor];
        return YES;
    }
    else {
        self.calendar.backgroundColor = [UIColor redColor];
        return NO;
    }
}

- (void)calendar:(CKCalendarView *)calendar didLayoutInRect:(CGRect)frame
{
    //NSLog(@"calendar layout: %@", NSStringFromCGRect(frame));
}

#pragma mark SliderMenuDelegate
-(void)didSelectSliderMenuButton:(NSInteger)rowcount
{
    if(rowcount==0){
        [self.navigationController popToViewController:objHomeViewController animated:YES];
    }
    else{
        [self.navigationController popToViewController:objHomeViewController animated:NO];
        
    }
    
    if ([_delegate respondsToSelector:@selector(didSelectSliderMenuButton:)])
    {
        [_delegate didSelectRowCalenderSingleView:rowcount];
    }
}
#pragma mark Delegate
- (id)delegate{
	return  _delegate;
}
- (void)setDelegate:(id)new_delegate{
	_delegate = new_delegate;
}

#pragma mark TableView Delegate & Datasource ////by Nihal

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 35;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section

{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0,_tableView.frame.size.width, 35)];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0,_tableView.frame.size.width, 30)];
    [label setTextColor:[UIColor blackColor]];
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont boldSystemFontOfSize:18];
    
    UILabel *numEventLabel = [[UILabel alloc] initWithFrame:CGRectMake(_tableView.frame.size.width/2-45,25,90,10)];
    numEventLabel.textAlignment = NSTextAlignmentCenter;
    numEventLabel.font = [UIFont systemFontOfSize:9];
    numEventLabel.textColor = [UIColor darkGrayColor];
    numEventLabel.backgroundColor = [UIColor clearColor];
    //numEventLabel.layer.borderWidth = 2.0;
    //numEventLabel.layer.borderColor = [UIColor orangeColor].CGColor;
    
    
    
    
    //Convert date formate from string
    NSString *string = stringSelectedDate;
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
    [dateFormatter1 setDateFormat:@"yyyy-MM-dd"];
    NSDate *dateFromString = [[NSDate alloc] init];
    NSLog(@"%@",string);
    dateFromString = [dateFormatter1 dateFromString:string];
    [dateFormatter1 setDateFormat:@"dd-MM-yyyy"];

    //retrieving count of number of events
    NSString *eventLabel = @"No. of Events : ";
    NSString *eventCount = [NSString stringWithFormat: @"%lu",(unsigned long)self.selectedDateEventArray.count];
    
    [numEventLabel setText:[eventLabel stringByAppendingString:eventCount]];
    [label setText:[dateFormatter1 stringFromDate:dateFromString]];
    [view addSubview:label];
    [view addSubview:numEventLabel];
    [view setBackgroundColor:[UIColor whiteColor]];
    
    UIButton *closeAlertButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [closeAlertButton addTarget:self
                         action:@selector(closeAlertButtonAction:)
               forControlEvents:UIControlEventTouchUpInside];
    [closeAlertButton setBackgroundColor:[UIColor clearColor]];
    [closeAlertButton setTintColor:[UIColor redColor]];
    [closeAlertButton setTitle:@"Close" forState:UIControlStateNormal];
    closeAlertButton.frame = CGRectMake(_tableView.frame.size.width-45,4,45,20);
    
    [view addSubview:closeAlertButton];
    
    return view;

}

-(IBAction)closeAlertButtonAction:(id)sender

{
    animationView.backgroundColor = [UIColor clearColor];
    _tableView.backgroundColor = [UIColor clearColor];
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:1/1.5];
    [UIView setAnimationDelegate:self];
    animationView.transform = CGAffineTransformScale(CGAffineTransformMake(1.0, 0.0, 1.0, 0.0, 0.0, 0.0), 1.0, 0.0);
    //animationView.transform =CGAffineTransformScale(CGAffineTransformMake(00, 00, 00, 00, 550, 1000), 0.0, 0.0);
    [UIView commitAnimations];
    
    [self performSelector:@selector(removeView) withObject:nil afterDelay:0.8];
}

-(void) removeView
{
    [_tableView setHidden:YES];
    _tableView = nil;
    [customView removeFromSuperview];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    return self.selectedDateEventArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *TableViewCellIdentifier = @"Cell";
    
    CustomCellEvents *cell ;//= [tableView dequeueReusableCellWithIdentifier:TableViewCellIdentifier];
    if (cell == nil)
    {
        cell = [[CustomCellEvents alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:TableViewCellIdentifier];
        cell.backgroundColor = [UIColor orangeColor];
    }
    self.eventDict = [[NSMutableDictionary alloc] init];
    self.eventDict = [self.selectedDateEventArray objectAtIndex:indexPath.row];
    
    // SET EVENT SUBJECT
    NSString  *eventStr = [[NSString alloc] initWithFormat:@"Event:"];
    if ([[self.eventDict valueForKey:@"eventSubject"] length] == 0) {
        cell.eventNameLabel.text = [[eventStr stringByAppendingString:@" "] stringByAppendingString:@"no event name"];
    }
    else{
        cell.eventNameLabel.text = [[eventStr stringByAppendingString:@" "] stringByAppendingString:[self.eventDict valueForKey:@"eventSubject"]];
        cell.eventNameLabel.frame = cell.eventNameLabel.frame;
        cell.eventNameLabel.numberOfLines = 10;
    }
    
    //SET EVENT LOCATION
    NSString  *locationStr = [[NSString alloc] initWithFormat:@" Location:"];
    NSString *eventLocation = [[locationStr stringByAppendingString:@" "] stringByAppendingString:@"no location"];
    UIFont *eventLocationFont = [UIFont systemFontOfSize:13];
    CGSize constraint = CGSizeMake(250,1000);
    NSDictionary *locationAttributes = @{NSFontAttributeName: eventLocationFont};
    CGRect eventLocationRect = [eventLocation boundingRectWithSize:constraint
                                                           options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading)
                                                        attributes:locationAttributes
                                                           context:nil];

    
    if ([[self.eventDict valueForKey:@"eventLocaion"] length] == 0 )
    
    {
        cell.locationLabel.text = [[locationStr stringByAppendingString:@" "] stringByAppendingString:@"no location"];
    }
    else
    
    {
        cell.locationLabel.text = [[locationStr stringByAppendingString:@" "] stringByAppendingString:[self.eventDict valueForKey:@"eventLocaion"]];
        CGRect rect = cell.locationLabel.frame;
        NSLog(@"%f",cell.locationLabel.frame.size.height);
        
        rect.size.height = eventLocationRect.size.height + 6;
        NSLog(@"%f",rect.size.height);
        
        rect.origin.y = cell.eventNameLabel.frame.size.height;
        cell.locationLabel.numberOfLines = 10;
        cell.locationLabel.frame = rect;
        NSLog(@"%f,%f,%f,%f",cell.locationLabel.frame.origin.x,cell.locationLabel.frame.origin.y,cell.locationLabel.frame.size.width,cell.locationLabel.frame.size.height);
    }
    
    //SET EVENT DESCRIPTION
    NSString  *descStr = [[NSString alloc] initWithFormat:@" Description:"];
    NSString *eventDescription = [[descStr stringByAppendingString:@" "] stringByAppendingString:[self.eventDict valueForKey:@"eventName"]];
    CGRect eventDescriptionRect = [eventDescription boundingRectWithSize:constraint
                                                                 options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading)
                                                              attributes:locationAttributes
                                                                 context:nil];

    
    if ([[self.eventDict valueForKey:@"eventName"]length] == 0)
    {
        cell.descriptionLabel.text = [[descStr stringByAppendingString:@" "] stringByAppendingString:@"no description"];
    }
    else
    {
        cell.descriptionLabel.text = [[descStr stringByAppendingString:@" "] stringByAppendingString:[self.eventDict valueForKey:@"eventName"]];
        CGRect rect1 = cell.descriptionLabel.frame;
        
        rect1.size.height = eventDescriptionRect.size.height + 6 ;
        
        rect1.origin.y = cell.eventNameLabel.frame.size.height + cell.locationLabel.frame.size.height - 3 ;
        cell.descriptionLabel.numberOfLines = 10;
        cell.descriptionLabel.frame = rect1;
    }
    
    //SET EVENT DURATION
    NSString  *durationStr = [[NSString alloc] initWithFormat:@" Duration:"];
    NSString *eventDuration = [[[[durationStr stringByAppendingString:@" "] stringByAppendingString:[self.eventDict valueForKey:@"eventStartTime"]] stringByAppendingString:@" - "] stringByAppendingString:[self.eventDict valueForKey:@"eventEndTime"]];
    CGRect eventDurationRect = [eventDuration boundingRectWithSize:constraint
                                                           options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading)
                                                        attributes:locationAttributes
                                                           context:nil];
    
   
    if ([[self.eventDict valueForKey:@"eventStartTime"]length] == 0 && [[self.eventDict valueForKey:@"eventEndTime"]length] == 0)
    {
        cell.durationLabel.text = [[durationStr stringByAppendingString:@" "] stringByAppendingString:@"no duration"];
        
    }
    else
    {
        cell.durationLabel.text = [[[[durationStr stringByAppendingString:@" "] stringByAppendingString:[self.eventDict valueForKey:@"eventStartTime"]] stringByAppendingString:@" - "] stringByAppendingString:[self.eventDict valueForKey:@"eventEndTime"]];
        CGRect rect2 = cell.durationLabel.frame;
        rect2.size.height = eventDurationRect.size.height + 6;
        rect2.origin.y = cell.eventNameLabel.frame.size.height + cell.locationLabel.frame.size.height + cell.descriptionLabel.frame.size.height - 5;
        cell.durationLabel.numberOfLines = 10;
        cell.durationLabel.frame = rect2;
        NSLog(@"%f,%f,%f,%f",cell.durationLabel.frame.origin.x,cell.durationLabel.frame.origin.y,cell.durationLabel.frame.size.width,cell.durationLabel.frame.size.height);
    }
    return cell;
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.eventDict = [[NSMutableDictionary alloc] init];
    self.eventDict = [self.selectedDateEventArray objectAtIndex:indexPath.row];
   
    
    NSString *eventSubject = [self.eventDict valueForKey:@"eventSubject"];
    UIFont *eventFont = [UIFont systemFontOfSize:16];
    CGSize constraint = CGSizeMake(250,2000);
    NSDictionary *attributes = @{NSFontAttributeName: eventFont};
    CGRect eventSubjectRect = [eventSubject boundingRectWithSize:constraint
                                       options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading)
                                    attributes:attributes
                                       context:nil];
    NSLog(@"%f,%f,%f,%f",eventSubjectRect.origin.x,eventSubjectRect.origin.y,eventSubjectRect.size.width,eventSubjectRect.size.height);
    //cell.eventNameLabel.frame = eventSubjectRect;
    
    NSString  *locationStr = [[NSString alloc] initWithFormat:@"Location:"];
    NSString *eventLocation = [[locationStr stringByAppendingString:@" "] stringByAppendingString:[self.eventDict valueForKey:@"eventLocaion"]];
    NSLog(@"%@",eventLocation);
    UIFont *eventLocationFont = [UIFont systemFontOfSize:13];
    NSDictionary *locationAttributes = @{NSFontAttributeName: eventLocationFont};
    CGRect eventLocationRect = [eventLocation boundingRectWithSize:constraint
                                                         options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading)
                                                      attributes:locationAttributes
                                                         context:nil];
    NSLog(@"%f,%f,%f,%f",eventLocationRect.origin.x,eventLocationRect.origin.y,eventLocationRect.size.width,eventLocationRect.size.height);
    //cell.locationLabel.frame = eventLocationRect;
    
    
    NSString  *descStr = [[NSString alloc] initWithFormat:@"Description:"];
    NSString *eventDescription = [[descStr stringByAppendingString:@" "] stringByAppendingString:[self.eventDict valueForKey:@"eventName"]];
    NSLog(@"%@",eventDescription);

    CGRect eventDescriptionRect = [eventDescription boundingRectWithSize:constraint
                                                           options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading)
                                                        attributes:locationAttributes
                                                           context:nil];
    NSLog(@"%f,%f,%f,%f",eventDescriptionRect.origin.x,eventDescriptionRect.origin.y,eventDescriptionRect.size.width,eventDescriptionRect.size.height);
    
    NSString  *durationStr = [[NSString alloc] initWithFormat:@"Duration:"];
    NSString *eventDuration = [[[[durationStr stringByAppendingString:@" "] stringByAppendingString:[self.eventDict valueForKey:@"eventStartTime"]] stringByAppendingString:@" - "] stringByAppendingString:[self.eventDict valueForKey:@"eventEndTime"]];
    NSLog(@"%@",eventDuration);

    CGRect eventDurationRect = [eventDuration boundingRectWithSize:constraint
                                                                 options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading)
                                                              attributes:locationAttributes
                                                                 context:nil];
    NSLog(@"%f,%f,%f,%f",eventDurationRect.origin.x,eventDurationRect.origin.y,eventDurationRect.size.width,eventDurationRect.size.height);
    
    CGFloat cellHeight = eventSubjectRect.size.height + 5 + eventLocationRect.size.height + eventDescriptionRect.size.height + eventDurationRect.size.height + 5 + 9;
    
    if (indexPath.row == self.selectedDateEventArray.count - 1)
    {
        return cellHeight + 3;
    }
    else
    {
        return cellHeight ;
    }
}



@end