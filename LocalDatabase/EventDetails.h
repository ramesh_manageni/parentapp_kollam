//
//  EventDetails.h
//  ParentApp
//
//  Created by RedBytes on 09/06/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface EventDetails : NSManagedObject

@property (nonatomic, retain) NSString * eventDate;
@property (nonatomic, retain) NSString * eventEndTime;
@property (nonatomic, retain) NSString * eventLocation;
@property (nonatomic, retain) NSString * eventName;
@property (nonatomic, retain) NSString * eventStartTime;
@property (nonatomic, retain) NSString * eventSubject;

@end
