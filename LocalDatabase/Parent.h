//
//  Parent.h
//  ParentApp
//
//  Created by Redbytes on 05/02/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class StudentDetails;

@interface Parent : NSManagedObject

@property (nonatomic, retain) NSString * detailed_name;
@property (nonatomic, retain) NSString * id_parent;
@property (nonatomic, retain) NSString * institution_id;
@property (nonatomic, retain) NSString * institution_name;
@property (nonatomic, retain) NSString * login_name;
@property (nonatomic, retain) NSString * login_pwd;
@property (nonatomic, retain) NSString * passwords;
@property (nonatomic, retain) NSString * profile_id;
@property (nonatomic, retain) NSString * remarks;
@property (nonatomic, retain) NSString * role_id;
@property (nonatomic, retain) NSString * role_type_id;
@property (nonatomic, retain) NSString * status_flag;
@property (nonatomic, retain) NSString * style_id;
@property (nonatomic, retain) NSString * updated_by;
@property (nonatomic, retain) NSString * updated_date;
@property (nonatomic, retain) NSString * user_id;
@property (nonatomic, retain) NSString * user_name;
@property (nonatomic, retain) NSString * user_role_id;
@property (nonatomic, retain) NSString * valid_from;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSString * mobile1;
@property (nonatomic, retain) NSSet *parentToChild;
@end

@interface Parent (CoreDataGeneratedAccessors)

- (void)addParentToChildObject:(StudentDetails *)value;
- (void)removeParentToChildObject:(StudentDetails *)value;
- (void)addParentToChild:(NSSet *)values;
- (void)removeParentToChild:(NSSet *)values;

@end
