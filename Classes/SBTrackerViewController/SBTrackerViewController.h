//
//  SBTrackerViewController.h
//  ParentApp
//
//  Created by Redbytes on 22/01/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SBTrackerCustomCell.h"
#import "SBTrackerWebservice.h"

@interface SBTrackerViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,SBTrackerDelegate>
{
    SBTrackerCustomCell *objSBTrackerCustomCell;
    
}
@property(nonatomic,strong)	UITableView *tableviewObject;
@property(nonatomic,strong)NSMutableArray *vehicleInfoArray;
@end
