//
//  ForgotPasswordWebservice.h
//  ParentApp
//
//  Created by Redbytes on 08/02/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ForgotPasswordDelegate <NSObject>

-(void)serverResponseForForgotPasswordWebservice:(NSMutableDictionary*)responseArray;
-(void)serverFailResponseForForgotPasswordWebservice;

@end
//********************************************************************

@class ConnectionManager;

@interface ForgotPasswordWebservice : NSObject
{
    id _delegate;
    NSMutableDictionary *resultDictionary;
    ConnectionManager *objConnectionManager;
}
- (id)delegate;
- (void)setDelegate:(id)new_delegate;
-(void)ForgotPasswordForUsername:(NSString *)username withEmailId:(NSString *)email;
@end
