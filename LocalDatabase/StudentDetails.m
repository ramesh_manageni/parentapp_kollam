//
//  StudentDetails.m
//  ParentApp
//
//  Created by Redbytes on 27/08/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "StudentDetails.h"
#import "AttendanceDetail.h"
#import "FeeDetails.h"
#import "Parent.h"
#import "StudentResult.h"
#import "TeacherDetails.h"


@implementation StudentDetails

@dynamic admission_no;
@dynamic due_amount;
@dynamic due_date;
@dynamic id_student;
@dynamic local_name;
@dynamic photo;
@dynamic childToParent;
@dynamic studentToAttendance;
@dynamic studentToFee;
@dynamic studentToResult;
@dynamic studentToTeacher;

@end
