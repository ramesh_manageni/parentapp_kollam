//
//  AttendanceWebservice.h
//  ParentApp
//
//  Created by Redbytes on 07/01/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol AttendanceDelegate <NSObject>

-(void)serverResponseForAttendanceWebservice:(NSArray*)responseArray;
-(void)serverFailResponseForAttendanceWebservice;

@end
//********************************************************************

@class ConnectionManager;
@interface AttendanceWebservice : NSObject
{
    id _delegate;
    NSMutableDictionary *resultDictionary;
    ConnectionManager *objConnectionManager;
    NSString *currentdateFromUserdefaults;
    NSString *student_Id;
    NSUserDefaults *lastUpdaeDateForAttendace;
    NSString *currentDate;
}
- (id)delegate;
- (void)setDelegate:(id)new_delegate;
-(void)showAttendanceForUsername:(NSString *)username studentId:(NSString *)studentId;
@end
