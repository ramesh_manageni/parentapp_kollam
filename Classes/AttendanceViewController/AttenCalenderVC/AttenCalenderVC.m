//
//  AttenCalenderVC.m
//  ParentApp
//
//  Created by Redbytes on 04/01/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "AttenCalenderVC.h"

#import <CoreGraphics/CoreGraphics.h>
#import "CKCalendarView.h"
#import "Constant.h"
#import "AttendanceTableViewController.h"

@interface AttenCalenderVC ()<CKCalendarDelegate>

@property(nonatomic, weak) CKCalendarView *calendar;
@property(nonatomic, strong) UILabel *dateLabel;
@property(nonatomic, strong) NSDateFormatter *dateFormatter;
@property(nonatomic, strong) NSDate *minimumDate;
@property(nonatomic, strong) NSArray *appliedLeaveDates;
@property(nonatomic, strong) NSArray *schoolHolidayDates;
@property(nonatomic, strong) NSMutableArray *leaveDates;
@property(nonatomic, strong) NSMutableArray *eventDates;

@end

@implementation AttenCalenderVC
@synthesize scrollViewObject,baseViewEvents;

- (id)init
{
    self = [super init];
    if (self) {
        //[self.baseViewEvents removeFromSuperview];
        
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"AttenCalenderVC" owner:self options:nil];
        UIView *view = [[UIView alloc] init]; // or if it exists, MCQView *view = [[MCQView alloc] init];
        view = (UIView *)[nib objectAtIndex:1]; // or if it exists, (MCQView *)[nib objectAtIndex:0];
        
        UIScreen *mainScreen = [UIScreen mainScreen];
        CGFloat scale = ([mainScreen respondsToSelector:@selector(scale)] ? mainScreen.scale : 1.0f);
        CGFloat pixelHeight = (CGRectGetHeight(mainScreen.bounds) * scale);
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
        {
            if (scale == 2.0f)
            {
                if (pixelHeight == 960.0f)
                {
                    self.scrollViewObject=[[UIScrollView alloc]initWithFrame:CGRectMake(0,00,320,480)];
                    self.scrollViewObject.showsVerticalScrollIndicator=NO;
                    self.scrollViewObject.scrollEnabled=YES;
                    self.scrollViewObject.userInteractionEnabled=YES;
                    self.scrollViewObject.backgroundColor = [UIColor clearColor];
                    
                    baseView = [[UIView alloc] initWithFrame:CGRectMake(0,-45,320,428)];
                    [self.scrollViewObject addSubview:baseView];
                    [self.scrollViewObject addSubview:view];
                    view.frame = CGRectMake(00,390, 320, 19);
                    view.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.1];//[[UIColor grayColor] colorWithAlphaComponent:0.30f];
                    self.scrollViewObject.contentSize = CGSizeMake(320,440);

                    [self.view addSubview:self.scrollViewObject];
                }
                
                else if (pixelHeight == 1136.0f)
                {
                    self.scrollViewObject=[[UIScrollView alloc]initWithFrame:CGRectMake(0,00,320,568)];
                    self.scrollViewObject.showsVerticalScrollIndicator=NO;
                    self.scrollViewObject.scrollEnabled=NO;
                    self.scrollViewObject.userInteractionEnabled=YES;
                    self.scrollViewObject.backgroundColor = [UIColor clearColor];
                    
                    baseView = [[UIView alloc] initWithFrame:CGRectMake(0,-20, 320, 428)];
                    [self.scrollViewObject addSubview:baseView];
                    [self.scrollViewObject addSubview:view];
                    view.frame = CGRectMake(00,427, 320, 19);
                    view.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.1];//[[UIColor grayColor] colorWithAlphaComponent:0.30f];
                    
                    self.scrollViewObject.contentSize = CGSizeMake(320,568);
                    [self.view addSubview:self.scrollViewObject];
                }
            }
            
        }
        
        
        int h=65,w=17;
        
        for(int i=0;i<12;i++)
        {
            
            CKCalendarView *calendar = [[CKCalendarView alloc] initWithStartDay:startSunday fromview:12];
            self.calendar = calendar;
            calendar.delegate = self;
            
            self.dateFormatter = [[NSDateFormatter alloc] init];
            [self.dateFormatter setDateFormat:@"yyyy-MM-dd"];
            self.minimumDate = [self.dateFormatter dateFromString:@"2000-12-31"];
            
            self.leaveDates=[[NSMutableArray alloc]init];
            for(int i=0;i<[absentDatesArrayCoreData count];i++)
            {
                [self.leaveDates addObject:[self.dateFormatter dateFromString:[absentDatesArrayCoreData objectAtIndex:i]]];
            }
            
            self.eventDates=[[NSMutableArray alloc]init];
            for(int i=0;i<[eventDatesArrayCoredata count];i++)
            {
                [self.eventDates addObject:[self.dateFormatter dateFromString:[eventDatesArrayCoredata objectAtIndex:i]]];
            }
            
            self.schoolHolidayDates = @[];
            self.appliedLeaveDates = @[];
            
            calendar.onlyShowCurrentMonth = NO;
            calendar.adaptHeightToNumberOfWeeksInMonth = YES;
            
            calendar.frame = CGRectMake(5+w,0+h, 70, 70);
            
            UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            button.frame = CGRectMake(5+w,0+h, 75,90);
            [button setTitle:@"" forState:UIControlStateNormal];
            [button addTarget:self action:@selector(calenderButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [button setBackgroundColor:[UIColor clearColor]];
            [button setTag:i];
            // [button setImage:[UIImage imageNamed:@"image.png"] forState:UIControlStateNormal];
            
            w+=100;
            
            // Add calender to view.
            
            [baseView addSubview:calendar];
            [baseView addSubview: button];
            
            
            for(int k=10-i;k>0;k--)
            {
                [calendar gotoPreviousMonth];
            }
            if(i==11)
            {
                [calendar gotoNextMonth];
            }
            
            
            self.dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, CGRectGetMaxY(calendar.frame) + 4, self.view.bounds.size.width, 24)];
            // [self.view addSubview:self.dateLabel];
            
            
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(localeDidChange) name:NSCurrentLocaleDidChangeNotification object:nil];
            if(i==2)
            {
                w=17;
                h+=90;
                
            }
            if(i==5)
            {
                w=17;
                h+=90;
            }
            if(i==8)
            {
                w=17;
                h+=90;
            }
        }
        
    }
    return self;
    
}

-(void)calenderButtonClicked:(UIButton *)sender
{
    countOfNextMonth=sender.tag;
    AttendanceTableViewController *objAttendanceTableViewController=[[AttendanceTableViewController alloc]init];
    objAttendanceTableViewController.delegate=self.delegate;
    //NSLog(@"tag value at 12 : : %d",sender.tag);
     [self.navigationController pushViewController:objAttendanceTableViewController animated:YES];
     objAttendanceTableViewController.hidesBottomBarWhenPushed=NO;
    
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
}

-(void) viewDidAppear:(BOOL)animated
{
    [ParseClass incrementView];
}

-(void) viewWillAppear:(BOOL)animated
{
    [self setTitle:@"Attendance Details"];
    self.navigationController.navigationBarHidden=NO;
    [self.navigationItem setHidesBackButton:YES];
    
    self.navigationItem.rightBarButtonItem = [self addRightBarButtonItem];
    self.navigationItem.leftBarButtonItem = [self addLeftBarButtonItem];
    
    // Do any additional setup after loading the view from its nib.
    [self.view setBackgroundColor:[UIColor blackColor]];
    
    sundayCount=0;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
    } else {
        return YES;
    }
}

-(UIBarButtonItem *)addLeftBarButtonItem
{
    UIButton *backButton = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 30.0f, 30.0f)];
    UIImage *backImage = [UIImage imageNamed:@"back button icon.png"] ;
    [backButton setBackgroundImage:backImage  forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    return backButtonItem;
    
}
-(UIBarButtonItem *)addRightBarButtonItem
{
    UIButton *backButton = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 30.0f, 30.0f)];
    UIImage *backImage = [UIImage imageNamed:@"list button icon.png"] ;
    [backButton setBackgroundImage:backImage  forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(settingButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    return backButtonItem;
    
}
-(void)backButtonClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)settingButtonClicked
{
    if(semiLeft!=nil)
    {
        [semiLeft dismissSemi:nil];
    }
    semiLeft = [[DXSubclassSemiTableViewController alloc] init];
    semiLeft.delegate=self;
    
    semiLeft.semiTitleLabel.text = @"SemiLeftTableView";
    semiLeft.tableViewRowHeight = 80.0f/2;
    [semiLeft.view setBackgroundColor:[UIColor clearColor]];
    self.leftSemiViewController = semiLeft;
}
///////////////////////////////////////

- (void)localeDidChange {
    [self.calendar setLocale:[NSLocale currentLocale]];
}

- (BOOL)dateIsAppliedLeave:(NSDate *)date {
    for (NSDate *disabledDate in self.appliedLeaveDates) {
        if ([disabledDate isEqualToDate:date]) {
            return YES;
        }
    }
    return NO;
}
- (BOOL)dateIsHoliday:(NSDate *)date {
    for (NSDate *disabledDate in self.schoolHolidayDates) {
        if ([disabledDate isEqualToDate:date]) {
            return YES;
        }
    }
    return NO;
}
- (BOOL)dateIsLeave:(NSDate *)date {
    for (NSDate *disabledDate in self.leaveDates) {
        if ([disabledDate isEqualToDate:date]) {
            return YES;
        }
    }
    return NO;
}
- (BOOL)dateIsEvent:(NSDate *)date {
    for (NSDate *disabledDate in self.eventDates) {
        if ([disabledDate isEqualToDate:date]) {
            return YES;
        }
    }
    return NO;
}
-(UIColor *)changeBgTo:(NSString *)hexCode
{
    unsigned long red, green, blue;
    sscanf([hexCode UTF8String], "#%2lX%2lX%2lX", &red, &green, &blue);
    UIColor *color = [UIColor colorWithRed:red/255.0 green:green/255.0 blue:blue/255.0 alpha:1];
    
    return color;
}
#pragma mark -
#pragma mark - CKCalendarDelegate

- (void)calendar:(CKCalendarView *)calendar configureDateItem:(CKDateItem *)dateItem forDate:(NSDate *)date
{

    if(sundayCount==6||sundayCount==13||sundayCount==20||sundayCount==34||sundayCount==27||sundayCount==41||sundayCount==5||sundayCount==12||sundayCount==19||sundayCount==33||sundayCount==26||sundayCount==40)
    {
        if(![dateItem.textColor isEqual:[UIColor darkGrayColor]])
        {
        dateItem.backgroundColor = [self changeBgTo:@"#006bff"];
        }
       // dateItem.textColor = [UIColor whiteColor];
        
    }
    sundayCount++;
    if(sundayCount==42)
    {
        sundayCount=0;
    }
    // TODO: play with the coloring if we want to...
    if ([self dateIsHoliday:date])
    {
        dateItem.backgroundColor = [self changeBgTo:@"#006bff"];
        dateItem.textColor = [UIColor whiteColor];
    }
    
    if ([self dateIsLeave:date])
    {
        dateItem.backgroundColor = [self changeBgTo:@"#fe0000"];
        dateItem.textColor = [UIColor whiteColor];
    }
    if ([self dateIsAppliedLeave:date])
    {
        dateItem.backgroundColor = [self changeBgTo:@"#a700fe"];
        dateItem.textColor = [UIColor whiteColor];
    }
    if ([self dateIsEvent:date])
    {
        dateItem.backgroundColor = [UIColor greenColor];
        dateItem.textColor = [UIColor redColor];
    }
    
}

- (BOOL)calendar:(CKCalendarView *)calendar willSelectDate:(NSDate *)date {
    //    return ![self dateIsDisabled:date];
    return NO;
}

- (void)calendar:(CKCalendarView *)calendar didSelectDate:(NSDate *)date {
    // self.dateLabel.text = [self.dateFormatter stringFromDate:date];
}

- (BOOL)calendar:(CKCalendarView *)calendar willChangeToMonth:(NSDate *)date {
    if ([date laterDate:self.minimumDate] == date) {
        self.calendar.backgroundColor = [UIColor blackColor];
        return YES;
    } else {
        self.calendar.backgroundColor = [UIColor redColor];
        return NO;
    }
}

- (void)calendar:(CKCalendarView *)calendar didLayoutInRect:(CGRect)frame {
    NSLog(@"calendar layout: %@", NSStringFromCGRect(frame));
}

#pragma mark SliderMenuDelegate
-(void)didSelectSliderMenuButton:(NSInteger)rowcount
{
    // NSLog(@"ROWCOUNT=== %d",rowcount);
    if(rowcount==0)
    {
        [self.navigationController popToViewController:objHomeViewController animated:YES];

    }
    else
    {
        [self.navigationController popToViewController:objHomeViewController animated:NO];

    }
    if ([_delegate respondsToSelector:@selector(didSelectRowCalenderAllView:)])
    {
        [_delegate didSelectRowCalenderAllView:rowcount];
    }
    
}

#pragma mark Delegate

- (id)delegate
{
	return  _delegate;
}

- (void)setDelegate:(id)new_delegate
{
	_delegate = new_delegate;
}




@end