//
//  SentItemWebservices.h
//  ParentApp
//
//  Created by Redbytes on 11/09/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ConnectionManager.h"
#import "Parent.h"

@protocol SentItemDelegate <NSObject>

-(void)serverResponseForSentItemWebservice:(NSArray *)responseArray;
-(void)serverFailResponseForSentItemwebservice;

@end
//********************************************************************

@class ConnectionManager;
@interface SentItemWebservices:NSObject
{
    id _delegate;
    NSMutableDictionary *resultDictionary;
    ConnectionManager *objConnectionManager;
}

- (id)delegate;
- (void)setDelegate:(id)new_delegate;
-(void)SentItemMethodForUsername:(NSString *)username ;

@end
