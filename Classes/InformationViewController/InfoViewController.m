//
//  InfoViewController.m
//  ParentApp
//
//  Created by Redbytes on 19/02/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "InfoViewController.h"
#import "Constant.h"

@interface InfoViewController ()

@end

@implementation InfoViewController
@synthesize tableviewObject;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setTitle:@"Notification Details"];
    
    self.navigationController.navigationBarHidden=NO;
    
    [self.navigationItem setHidesBackButton:YES];
    self.navigationItem.rightBarButtonItem = [self addRightBarButtonItem];
    self.navigationItem.leftBarButtonItem = [self addLeftBarButtonItem];
    
    
    NotificationArray=[[[NSUserDefaults standardUserDefaults] objectForKey:@"notificationArray"] mutableCopy];
    
    [self.view setBackgroundColor:[UIColor blackColor]];
    
    
    UIScreen *mainScreen = [UIScreen mainScreen];
    CGFloat scale = ([mainScreen respondsToSelector:@selector(scale)] ? mainScreen.scale : 1.0f);
    CGFloat pixelHeight = (CGRectGetHeight(mainScreen.bounds) * scale);
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        if (scale == 2.0f)
        {
            if (pixelHeight == 960.0f)
            {
                tableviewObject=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, 320, 480-49) style:UITableViewStyleGrouped];
            }
            
            else if (pixelHeight == 1136.0f)
            {
                tableviewObject=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, 320, 568-49) style:UITableViewStyleGrouped];
            }
        }
        
    }
    
    
    [tableviewObject setBackgroundColor:[UIColor clearColor]];
    tableviewObject.separatorColor = [UIColor clearColor];
    tableviewObject.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    tableviewObject.delegate=self;
    tableviewObject.dataSource=self;
    [self.view addSubview:tableviewObject];
    
    if([NotificationArray count]==0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Notification" message:@"No Notification available" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert setDelegate:self];
        [alert setTag:100];
        [alert show];
        
    }
}

-(void) viewDidAppear:(BOOL)animated
{
    [ParseClass incrementView];
}

-(UIBarButtonItem *)addLeftBarButtonItem
{
    UIButton *backButton = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 30.0f, 30.0f)];
    UIImage *backImage = [UIImage imageNamed:@"back button icon.png"] ;
    [backButton setBackgroundImage:backImage  forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    return backButtonItem;
    
}
-(UIBarButtonItem *)addRightBarButtonItem
{
    UIButton *backButton = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 30.0f, 30.0f)];
    UIImage *backImage = [UIImage imageNamed:@"list button icon.png"] ;
    [backButton setBackgroundImage:backImage  forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(settingButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    return backButtonItem;
    
}
-(void)backButtonClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)settingButtonClicked
{
   
    
    if(semiLeft!=nil)
    {
        [semiLeft dismissSemi:nil];
    }
//[self.navigationController popToViewController:objHomeViewController animated:YES];
    semiLeft = [[DXSubclassSemiTableViewController alloc] init];
    semiLeft.delegate=self;
    
    semiLeft.semiTitleLabel.text = @"SemiLeftTableView";
    semiLeft.tableViewRowHeight = 80.0f/2;
    [semiLeft.view setBackgroundColor:[UIColor clearColor]];
    self.leftSemiViewController = semiLeft;
    
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark Alertview Delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    // the user clicked one of the Ok/Cancel buttons
    if(alertView.tag==100)
    {
        [self backButtonClicked];
    }
}

#pragma mark Table View Datasource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	tableviewObject = (UITableView *)tableView;
    return 75;
	
	
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
	return [NotificationArray count];
	//return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
	return 1;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    InfoCustomCell *cell = (InfoCustomCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[InfoCustomCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
		[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    
    // Configure the cell...
	
    cell.NotificationText.text=[[[NotificationArray objectAtIndex:indexPath.section] objectForKey:@"aps"] valueForKey:@"alert"];
   // cell.NotificationTime.text=@"2014-02-19 17:12:40";
   
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *tempDict=[NotificationArray objectAtIndex:indexPath.section];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Notification" message:[[tempDict objectForKey:@"aps"] valueForKey:@"alert"] delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
    [alert show];

    
}

#pragma mark SliderMenuDelegate
-(void)didSelectSliderMenuButton:(NSInteger)rowcount
{
    // NSLog(@"ROWCOUNT=== %d",rowcount);
    [self.navigationController popViewControllerAnimated:NO];
    if ([_delegate respondsToSelector:@selector(didSelectRowInfo:)])
    {
        [_delegate didSelectRowInfo:rowcount];
    }
    
}

#pragma mark Delegate

- (id)delegate
{
	return  _delegate;
}

- (void)setDelegate:(id)new_delegate
{
	_delegate = new_delegate;
}


@end
