//
//  TeacherDetails.m
//  ParentApp
//
//  Created by Redbytes on 27/08/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "TeacherDetails.h"
#import "StudentDetails.h"


@implementation TeacherDetails

@dynamic login_name;
@dynamic subject;
@dynamic teacher;
@dynamic teacher_email;
@dynamic teacherToStudent;

@end
