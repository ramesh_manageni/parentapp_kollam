//
//  SBTrackerWebservice.m
//  ParentApp
//
//  Created by Redbytes on 22/01/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "SBTrackerWebservice.h"

#import "ConnectionManager.h"

@implementation SBTrackerWebservice

-(id)init
{
	return self;
}

-(void)getVehicleInfoForStudent:(NSString *)stdAdmNo
{
    NSMutableDictionary *localDictionary=[[NSMutableDictionary alloc]init];
    
    [localDictionary setObject:stdAdmNo forKey:@"admission_num"];

    NSLog(@"Dict: %@",localDictionary);
    
    if (objConnectionManager!=nil)
    {
		objConnectionManager=nil;
	}
	objConnectionManager=[[ConnectionManager alloc]init];
    objConnectionManager.isSBTracker=@"YES";
    
    [objConnectionManager getWebData:self commandName:@"studentdetail" Requestparameter:localDictionary];


}

#pragma mark connection manager delegate
- (void)receivedData:(NSArray *)responseArray
{
    if ([_delegate respondsToSelector:@selector(serverResponseForSBTrackerWebservice:)])
    {
        [_delegate serverResponseForSBTrackerWebservice:(NSMutableDictionary *)responseArray];
    }
    else
    {
        [NSException raise:NSInternalInconsistencyException format:@"Delegate doesn't respond to serverResponseForCaptchaWebSerivce:"];
    }
}
-(void)failToReciveData
{
    if ([_delegate respondsToSelector:@selector(serverFailResponseForSBTrackerWebservice)])
    {
        [_delegate serverFailResponseForSBTrackerWebservice];
    }
}
#pragma mark - Delegate methods
- (id)delegate
{
	return  _delegate;
}

- (void)setDelegate:(id)new_delegate
{
	_delegate = new_delegate;
}



@end
