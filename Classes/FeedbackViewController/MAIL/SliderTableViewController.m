//
//  SliderTableViewController.m
//  ParentApp
//
//  Created by RedBytes on 05/08/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "SliderTableViewController.h"

@interface SliderTableViewController ()

@end

@implementation SliderTableViewController



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
