//
//  LoginWebservice.h
//  ParentApp
//
//  Created by Redbytes on 07/01/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol LoginDelegate <NSObject>

-(void)serverResponseForLoginWebservice:(NSArray*)responseArray;
-(void)serverFailResponseForLoginWebservice;

@end
//********************************************************************

@class ConnectionManager;
@interface LoginWebservice : NSObject
{
    id _delegate;
    NSMutableDictionary *resultDictionary;
    ConnectionManager *objConnectionManager;
}
- (id)delegate;
- (void)setDelegate:(id)new_delegate;
-(void)loginToServserWithId:(NSString *)userId :(NSString *)password;
@end
