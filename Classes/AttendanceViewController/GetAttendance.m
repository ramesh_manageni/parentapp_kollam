//
//  GetAttendance.m
//  ParentApp
//
//  Created by Redbytes on 16/01/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "GetAttendance.h"
#import "AttendanceWebservice.h"

@interface GetAttendance ()

@end

@implementation GetAttendance

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)getAttendanceForUser:(NSString *)username stdId:(NSString *)stdId
{
    
    AttendanceWebservice *objAttendanceWebservice=[[AttendanceWebservice alloc]init];
    objAttendanceWebservice.delegate=self;
    [objAttendanceWebservice showAttendanceForUsername:username studentId:stdId];
}
#pragma mark ResultWebservice delegate
-(void)serverResponseForAttendanceWebservice:(NSArray*)responseArray
{
    // [HUD hide:YES];
    
    if (responseArray!=nil)
    {
        absentCount=(int)[responseArray count];
      
        
    }
    else if(responseArray==nil)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Attendance not published yet" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    
    
}
-(void)serverFailResponseForAttendanceWebservice
{
   
    
    
}


@end
