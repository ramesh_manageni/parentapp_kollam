//
//  ResultViewController.m
//  ParentApp
//
//  Created by Redbytes on 03/01/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "ResultViewController.h"
#import "ExamGradeViewController.h"
#import "ChangePasswordViewController.h"
#import "Constant.h"
#import "FeeViewController.h"
#import "StudentResult.h"
#import "MarkDetails.h"
#import "AppDelegate.h"
#import "StudentDetails.h"
#import "Parent.h"
#import "NSData+Base64.h"
@interface ResultViewController ()

@end

@implementation ResultViewController
@synthesize tableviewObject;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    isFeeViewController=@"NO";

    stdResultarray=[[NSMutableArray alloc]init];
    
    self.navigationController.navigationBarHidden=NO;
    [self.navigationItem setHidesBackButton:YES];
   // self.navigationItem.rightBarButtonItem = [self addRightBarButtonItem];
    self.navigationItem.leftBarButtonItem = [self addLeftBarButtonItem];
    self.navigationItem.rightBarButtonItem=[self addRightBarButtonItem];
       // Do any additional setup after loading the view from its nib.
    [self.view setBackgroundColor:[UIColor blackColor]];
    
    
    UIScreen *mainScreen = [UIScreen mainScreen];
    CGFloat scale = ([mainScreen respondsToSelector:@selector(scale)] ? mainScreen.scale : 1.0f);
    CGFloat pixelHeight = (CGRectGetHeight(mainScreen.bounds) * scale);
    
       if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
        {
            if (scale == 2.0f)
            {
                if (pixelHeight == 960.0f)
                {
                    tableviewObject=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, 320, 480-49) style:UITableViewStyleGrouped];

                }
                
                else if (pixelHeight == 1136.0f)
                {
                    tableviewObject=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, 320, 568-49) style:UITableViewStyleGrouped];

                    
                }
                
            }
            
        }
        
    
    [tableviewObject setBackgroundColor:[UIColor clearColor]];
    tableviewObject.separatorColor = [UIColor clearColor];
    tableviewObject.separatorStyle = UITableViewCellSeparatorStyleNone;

    tableviewObject.delegate=self;
    tableviewObject.dataSource=self;
    [self.view addSubview:tableviewObject];
}

-(void) viewDidAppear:(BOOL)animated
{
    [ParseClass incrementView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UIBarButtonItem *)addLeftBarButtonItem
{
    UIButton *backButton = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 30.0f, 30.0f)];
    UIImage *backImage = [UIImage imageNamed:@"back button icon.png"] ;
    [backButton setBackgroundImage:backImage  forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    return backButtonItem;
    
}
-(UIBarButtonItem *)addRightBarButtonItem
{
    UIButton *backButton = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 30.0f, 30.0f)];
    UIImage *backImage = [UIImage imageNamed:@"list button icon.png"] ;
    [backButton setBackgroundImage:backImage  forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(settingButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    return backButtonItem;
}
-(void)backButtonClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)settingButtonClicked
{
    
    if(semiLeft!=nil)
    {
        [semiLeft dismissSemi:nil];
    }
    //[self.navigationController popToViewController:objHomeViewController animated:YES];
    semiLeft = [[DXSubclassSemiTableViewController alloc] init];
    semiLeft.delegate=self;
    
    semiLeft.semiTitleLabel.text = @"SemiLeftTableView";
    semiLeft.tableViewRowHeight = 80.0f/2;
    [semiLeft.view setBackgroundColor:[UIColor clearColor]];
    self.leftSemiViewController = semiLeft;
    
    
}


#pragma mark Table View Datasource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	tableviewObject = (UITableView *)tableView;
    return 75;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
	return [studentArrayCoreData count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
	return 1;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    AttendanceCustomCell *cell = (AttendanceCustomCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[AttendanceCustomCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
		[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    
    // Configure the cell...
	
//	cell.labelName.text=[nameArray objectAtIndex:indexPath.section];
//    cell.labelAdminNumber.text=[adminNumberArray objectAtIndex:indexPath.section
    
    cell.labelName.text=[[studentArrayCoreData objectAtIndex:indexPath.section] local_name];
    cell.labelAdminNumber.text=[[studentArrayCoreData objectAtIndex:indexPath.section] admission_no];
    if([[studentArrayCoreData objectAtIndex:indexPath.section] photo].length>0)
    {
        NSData *imageData = [NSData dataFromBase64String:[[studentArrayCoreData objectAtIndex:indexPath.section] photo]];
        cell.objImageViewStudent.image=[UIImage imageWithData:imageData];
    }


    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([_flag isEqualToString:@"YES"])
    {
        FeeViewController *objFeeViewController=[[FeeViewController alloc]initWithNibName:@"FeeViewController" bundle:nil];
        
        objFeeViewController.delegate=self.delegate;
        
        objFeeViewController.index=indexPath.section;
        objFeeViewController.username=[[parentArrayCoreData objectAtIndex:0] user_name];
        objFeeViewController.student_id=[[studentArrayCoreData objectAtIndex:indexPath.section] id_student];
        objFeeViewController.student_name=[[studentArrayCoreData objectAtIndex:indexPath.section] local_name];
        [self.navigationController pushViewController:objFeeViewController animated:YES];
        
    }
    else
    {
      HUD = [[MBProgressHUD alloc] initWithView:self.view];
	  [self.view addSubview:HUD];
	
	  // Regiser for HUD callbacks so we can remove it from the window at the right time
	  HUD.delegate = self;
	
	  // Show the HUD while the provided method executes in a new thread
	  //[HUD showWhileExecuting:@selector(myTask) onTarget:self withObject:nil animated:YES];
      [HUD show:YES];

        
    
      ResultWebservice *objResultWebservice=[[ResultWebservice alloc]init];
      objResultWebservice.delegate=self;
      [objResultWebservice showResultForUsername:[[parentArrayCoreData objectAtIndex:0] user_name] studentId:[[studentArrayCoreData objectAtIndex:indexPath.section] id_student] examId:@"0"];
        
        NSLog(@"user name:%@ student_id: %@ ",[[parentArrayCoreData objectAtIndex:0] user_name],[[studentArrayCoreData objectAtIndex:indexPath.section] id_student]);
        
        stdId=[[studentArrayCoreData objectAtIndex:indexPath.section] id_student];
      
    }

  }

#pragma mark ResultWebservice delegate
-(void)serverResponseForResultWebSerivce:(NSArray*)responseArray
{
    [HUD hide:YES];
    
    NSLog(@"Response in result_vc %lu", (unsigned long)[responseArray count]);
    
    if([responseArray count]==0)
    {
        ExamGradeViewController *objExamGradeViewController=[[ExamGradeViewController alloc]initWithNibName:@"ExamGradeViewController" bundle:nil];
        objExamGradeViewController.delegate=self.delegate;
        [self.navigationController pushViewController:objExamGradeViewController animated:YES];
        [self getEntityCount:@"StudentDetails"];
    }
    else if (responseArray!=nil)
    {
        NSString *str=[NSString stringWithFormat:@"%@",[responseArray valueForKey:@"success"]];
        //NSLog(@"%@",str);
        //NSLog(@"....... %d",[str length]);
        if([str length]==1)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:[responseArray valueForKey:@"msg"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        }
        else if ([str length]>0 && ![str isEqualToString:@"(null)"])
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:[responseArray valueForKey:@"success"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        }
        else
        {
            responseArrayResult=[[NSArray alloc] init];
            responseArrayResult=responseArray;
            responseArrayCoreData=[[NSMutableArray alloc]init];
            
            NSDictionary   *resultDict=(NSDictionary *)responseArray;
            NSLog(@"Dict: %@",resultDict);
            
            for(NSString *key in [resultDict allKeys])
            {
                // NSLog(@"Single object in Dict:\n%@",[resultDict objectForKey:key]);
                [responseArrayCoreData addObject:[resultDict objectForKey:key]];
            }
            
            
            // [self deleteAllObjects:@"StudentResult"];
            [self loadData:responseArrayCoreData];
            [self getEntityCount:@"StudentDetails"];
            
            ExamGradeViewController *objExamGradeViewController=[[ExamGradeViewController alloc]initWithNibName:@"ExamGradeViewController" bundle:nil];
            
            objExamGradeViewController.delegate=self.delegate;
            
            
            [self.navigationController pushViewController:objExamGradeViewController animated:YES];
            
        }
        // NSLog(@"Server Responce: %@",responseArrayLogin);
    }
    else if(responseArray==nil)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Result not published yet" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    
    
}
-(void)serverFailResponseForResultWebSerivce
{
    [HUD hide:YES];
    [self getEntityCount:@"StudentDetails"];

    //UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Server response failed\nAccessing local data" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
   // [alert show];
    
if([examcodeArrayCoreData count]>0)
{
    ExamGradeViewController *objExamGradeViewController=[[ExamGradeViewController alloc]initWithNibName:@"ExamGradeViewController" bundle:nil];
    
    objExamGradeViewController.delegate=self.delegate;

    [self.navigationController pushViewController:objExamGradeViewController animated:YES];
}
else
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Local database is empty" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}
    



}
- (void) deleteAllObjects: (NSString *) entityDescription
{
    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [delegate managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityDescription inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    NSError *error;
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    
    
    for (NSManagedObject *managedObject in items) {
    	[context deleteObject:managedObject];
    	NSLog(@"%@ object deleted",entityDescription);
    }
    if (![context save:&error]) {
    	NSLog(@"Error deleting %@ - error:%@",entityDescription,error);
    }
    
}
-(void)getEntityCount:(NSString *)entityName
{
    entityCount = 0;
    
    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [delegate managedObjectContext];
    
    NSFetchRequest *fetchReq = [[NSFetchRequest alloc]init];
    
    [fetchReq setEntity:[NSEntityDescription entityForName:entityName inManagedObjectContext:context]];
    NSError *error;
    
    NSMutableArray *resultArray = [[NSMutableArray alloc]initWithArray:[context executeFetchRequest:fetchReq error:&error]];
    if(error==nil)
    {
        [stdResultarray removeAllObjects];
        
        NSSet *set;
        
        for(StudentDetails *stddetails in resultArray)
        {
            if([stddetails.id_student isEqualToString:stdId])
            {
                set=stddetails.studentToResult;
                
                for(StudentResult *stdres in [set allObjects])
                {
                    [stdResultarray addObject:stdres];
                }
                
            
            }
//            for(MarkDetails *mark in std.resultToMarks)
//            {
//                NSLog(@"Subject : %@",mark.subject);
//            }
//            
//            NSLog(@"Exam Code:  %@  ",std.exam_code);
//            NSLog(@"exam_id:  %@  ",std.exam_id);
//            NSLog(@"result_date:  %@  ",std.result_date);
        }
        
        
        examcodeArrayCoreData=stdResultarray;
    }
//    studentArrayCoreData=studentArray;
//    parentArrayCoreData=parentArray;
//    NSLog(@"Entity Array:%@ Parent: %@",studentArrayCoreData,parentArrayCoreData);
    
}
-(void)loadData:response
{        NSLog(@"ResponseArrayCoreData:%@",responseArrayCoreData);

    StudentDetails *std=[self getStudentEntityWithId];

    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [delegate managedObjectContext];
    
  //  StudentDetails *st_detail = [NSEntityDescription insertNewObjectForEntityForName:@"StudentDetails" inManagedObjectContext:context];

    
    for(int i=0;i<[response count];i++)
    {
        StudentResult *stdres = [NSEntityDescription insertNewObjectForEntityForName:@"StudentResult" inManagedObjectContext:context];

        if([[response objectAtIndex:i] valueForKey:@"exam_code"]!=(NSString *)[NSNull null])
        {
            stdres.exam_code=[[response objectAtIndex:i] valueForKey:@"exam_code"];
        }
        if([[response objectAtIndex:i] valueForKey:@"exam_id"]!=(NSString *)[NSNull null])
        {
            stdres.exam_id=[[response objectAtIndex:i] valueForKey:@"exam_id"];
        }
        if([[response objectAtIndex:i] valueForKey:@"result_date"]!=(NSString *)[NSNull null])
        {
            stdres.result_date=[[response objectAtIndex:i] valueForKey:@"result_date"];
        }
        
        for(int j=0;j<[[[response objectAtIndex:i] valueForKey:@"mark_details"] count];j++)
        {
            
            MarkDetails *marks = [NSEntityDescription insertNewObjectForEntityForName:@"MarkDetails" inManagedObjectContext:context];
            
            if([[[[response objectAtIndex:i] valueForKey:@"mark_details"] objectAtIndex:j] valueForKey:@"batch_id"]!=(NSString *)[NSNull null])
            {
                marks.batch_id=[[[[response objectAtIndex:i] valueForKey:@"mark_details"] objectAtIndex:j] valueForKey:@"batch_id"];
            }
            
//            if([[[[response objectAtIndex:i] valueForKey:@"mark_details"] objectAtIndex:j] valueForKey:@"entry_status"]!=(NSString *)[NSNull null])
//            {
//                marks.entry_status=[[[[response objectAtIndex:i] valueForKey:@"mark_details"] objectAtIndex:j] valueForKey:@"entry_status"];
//            }
//            
            if([[[[response objectAtIndex:i] valueForKey:@"mark_details"] objectAtIndex:j] valueForKey:@"exam"]!=(NSString *)[NSNull null])
            {
                marks.exam=[[[[response objectAtIndex:i] valueForKey:@"mark_details"] objectAtIndex:j] valueForKey:@"exam"];
            }
            
            if([[[[response objectAtIndex:i] valueForKey:@"mark_details"] objectAtIndex:j] valueForKey:@"exam_id"]!=(NSString *)[NSNull null])
            {
                marks.exam_id=[[[[response objectAtIndex:i] valueForKey:@"mark_details"] objectAtIndex:j] valueForKey:@"exam_id"];
            }
            
            if([[[[response objectAtIndex:i] valueForKey:@"mark_details"] objectAtIndex:j] valueForKey:@"grade"]!=(NSString *)[NSNull null])
            {
                marks.grade=[[[[response objectAtIndex:i] valueForKey:@"mark_details"] objectAtIndex:j] valueForKey:@"grade"];
            }
            
            if([[[[response objectAtIndex:i] valueForKey:@"mark_details"] objectAtIndex:j] valueForKey:@"mark"]!=(NSString *)[NSNull null])
            {
                marks.mark=[[[[response objectAtIndex:i] valueForKey:@"mark_details"] objectAtIndex:j] valueForKey:@"mark"];
            }
            
            if([[[[response objectAtIndex:i] valueForKey:@"mark_details"] objectAtIndex:j] valueForKey:@"mark_t"]!=(NSString *)[NSNull null])
            {
                marks.mark_t=[[[[response objectAtIndex:i] valueForKey:@"mark_details"] objectAtIndex:j] valueForKey:@"mark_t"];
            }
            
            if([[[[response objectAtIndex:i] valueForKey:@"mark_details"] objectAtIndex:j] valueForKey:@"max_mark_t"]!=(NSString *)[NSNull null])
            {
                marks.max_mark_t=[[[[response objectAtIndex:i] valueForKey:@"mark_details"] objectAtIndex:j] valueForKey:@"max_mark_t"];
            }
            
//            if([[[[response objectAtIndex:i] valueForKey:@"mark_details"] objectAtIndex:j] valueForKey:@"order_id"]!=(NSString *)[NSNull null])
//            {
//                marks.batch_id=[[[[response objectAtIndex:i] valueForKey:@"mark_details"] objectAtIndex:j] valueForKey:@"order_id"];
//            }
//            
            if([[[[response objectAtIndex:i] valueForKey:@"mark_details"] objectAtIndex:j] valueForKey:@"student_id"]!=(NSString *)[NSNull null])
            {
                marks.student_id=[[[[response objectAtIndex:i] valueForKey:@"mark_details"] objectAtIndex:j] valueForKey:@"student_id"];
            }
            
            if([[[[response objectAtIndex:i] valueForKey:@"mark_details"] objectAtIndex:j] valueForKey:@"subject"]!=(NSString *)[NSNull null])
            {
                marks.subject=[[[[response objectAtIndex:i] valueForKey:@"mark_details"] objectAtIndex:j] valueForKey:@"subject"];
            }
            
            if([[[[response objectAtIndex:i] valueForKey:@"mark_details"] objectAtIndex:j] valueForKey:@"subject_id"]!=(NSString *)[NSNull null])
            {
                marks.subject_id=[[[[response objectAtIndex:i] valueForKey:@"mark_details"] objectAtIndex:j] valueForKey:@"subject_id"];
            }
            
            if([[[[response objectAtIndex:i] valueForKey:@"mark_details"] objectAtIndex:j] valueForKey:@"subject_mark_id"]!=(NSString *)[NSNull null])
            {
                marks.subject_mark_id=[[[[response objectAtIndex:i] valueForKey:@"mark_details"] objectAtIndex:j] valueForKey:@"subject_mark_id"];
            }
            
            
            [stdres addResultToMarksObject:marks];
            
            marks=nil;

        }
        
        
        [std addStudentToResultObject:stdres];
        
        stdres=nil;
    }
    NSError *err;
    
    if( ! [context save:&err] ){
        NSLog(@"Cannot save data: %@", [err localizedDescription]);
    }

}

-(StudentDetails *)getStudentEntityWithId
{
    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [delegate managedObjectContext];
    
    NSFetchRequest *fetchReq = [[NSFetchRequest alloc]init];
    
    [fetchReq setEntity:[NSEntityDescription entityForName:@"StudentDetails" inManagedObjectContext:context]];
    NSError *error;
    
    NSMutableArray *resultArray = [[NSMutableArray alloc]initWithArray:[context executeFetchRequest:fetchReq error:&error]];
    if(error==nil)
    {
        for(StudentDetails *stdDetails in resultArray)
        {
            if([stdDetails.id_student isEqualToString:stdId])
            {
                //[stdDetails removeStudentToResult:stdDetails.studentToResult];
                return stdDetails;
            }
        }
        
    }
    
    return nil;
}
#pragma mark SliderMenuDelegate
-(void)didSelectSliderMenuButton:(NSInteger)rowcount
{
    // NSLog(@"ROWCOUNT=== %d",rowcount);
    if(rowcount==0)
    {
        [self.navigationController popToViewController:objHomeViewController animated:YES];
        
    }
    else
    {
        [self.navigationController popToViewController:objHomeViewController animated:NO];
        
    }
    if ([_delegate respondsToSelector:@selector(didSelectRowResult:)])
    {
        [_delegate didSelectRowResult:rowcount];
    }
    
}

#pragma mark Delegate

- (id)delegate
{
	return  _delegate;
}

- (void)setDelegate:(id)new_delegate
{
	_delegate = new_delegate;
}



@end
