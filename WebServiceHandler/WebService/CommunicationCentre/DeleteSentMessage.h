//
//  DeleteSentMessage.h
//  ParentApp
//
//  Created by Tasneem on 17/09/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ConnectionManager.h"


@protocol DeleteSentMessageDelegate <NSObject>

-(void)serverResponseForDeleteSentMessage:(NSArray *)responseArray;
-(void)serverFailResponseForDeleteSentMessage;

@end
//********************************************************************

@class ConnectionManager;

@interface DeleteSentMessage : NSObject
{
    id _delegate;
    NSMutableDictionary *resultDictionary;
    ConnectionManager *objConnectionManager;
    
}
@property (strong,nonatomic) NSArray *deleteArray1;
- (id)delegate;
- (void)setDelegate:(id)new_delegate;
-(void)deleteMessageWithUsername:(NSString *)username pmId:(NSString *)pmMessageID Sent:(NSString *)sent;

@end
