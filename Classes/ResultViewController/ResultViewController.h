//
//  ResultViewController.h
//  ParentApp
//
//  Created by Redbytes on 03/01/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AttendanceCustomCell.h"
#import "ResultWebservice.h"
#import "MBProgressHUD.h"


#import "DXSemiViewControllerCategory.h"
#import "DXSubClassSemiViewController.h"
#import "DXSubclassSemiTableViewController.h"

@protocol MenuButtonResultDelegate <NSObject>

-(void)didSelectRowResult:(NSInteger)rowcount;

@end

@interface ResultViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,ResultDelegate,MBProgressHUDDelegate,SliderMenuButtonDelegate>
{
    AttendanceCustomCell *objAttendanceCustomCell;
     UITableView *tableviewObject;
    MBProgressHUD *HUD;
    
    NSMutableArray *responseArrayCoreData;
    int entityCount;
    
    NSMutableArray *stdResultarray;
    
    NSString *stdId;
    
    id _delegate;
    DXSubclassSemiTableViewController *semiLeft;

    
}
@property(nonatomic,strong)	UITableView *tableviewObject;
@property (strong, nonatomic) NSString *flag;

- (id)delegate;
- (void)setDelegate:(id)new_delegate;



@end
