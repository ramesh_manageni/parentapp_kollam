//
//  SBTrackerCustomCell.m
//  ParentApp
//
//  Created by Redbytes on 22/01/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//
#define kFontName @"BradleyHandITCTT-Bold"

#import "SBTrackerCustomCell.h"

@implementation SBTrackerCustomCell
@synthesize labelName,labelVehicleNumber,objImageViewBG,objImageViewStudent,labelVehicleImei;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        labelName =[[UILabel alloc]initWithFrame:CGRectMake(130, 3, 157, 30)];
		[labelName setFont:[UIFont fontWithName:kFontName size:12]];
		[labelName setBackgroundColor:[UIColor clearColor]];
        [labelName setTextColor:[UIColor whiteColor]];
        [labelName setTextAlignment:NSTextAlignmentCenter];
        
        labelVehicleNumber =[[UILabel alloc]initWithFrame:CGRectMake(130, 23, 157, 30)];
		[labelVehicleNumber setFont:[UIFont fontWithName:kFontName size:10]];
		[labelVehicleNumber setBackgroundColor:[UIColor clearColor]];
        [labelVehicleNumber setTextColor:[UIColor whiteColor]];
        [labelVehicleNumber setTextAlignment:NSTextAlignmentCenter];
        
        labelVehicleImei =[[UILabel alloc]initWithFrame:CGRectMake(130, 42, 157, 30)];
		[labelVehicleImei setFont:[UIFont fontWithName:kFontName size:10]];
		[labelVehicleImei setBackgroundColor:[UIColor clearColor]];
        [labelVehicleImei setTextColor:[UIColor whiteColor]];
        [labelVehicleImei setTextAlignment:NSTextAlignmentCenter];
        
        objImageViewBG=[[UIImageView alloc] initWithFrame:CGRectMake(25, 0, 270, 75)];
        [objImageViewBG setImage:[UIImage imageNamed:@"attendance tab.png"]];
        [objImageViewBG setBackgroundColor:[UIColor blackColor]];
        
        objImageViewStudent=[[UIImageView alloc] initWithFrame:CGRectMake(45, 5, 60, 64)];
        //[objImageViewStudent setBackgroundColor:[UIColor greenColor]];
        [objImageViewStudent setImage:[UIImage imageNamed:@"dummy1.jpg"]];
        [self setRoundedView:objImageViewStudent toDiameter:67.0];

        [self.contentView addSubview:objImageViewBG];
        [self.contentView addSubview:objImageViewStudent];
        
        [self.contentView addSubview:labelVehicleNumber];
		[self.contentView addSubview:labelName];
        [self.contentView addSubview:labelVehicleImei];
        [self.contentView setBackgroundColor:[UIColor blackColor]];
    }
    return self;
}


-(void)setRoundedView:(UIImageView *)roundedView toDiameter:(float)newSize;
{
    CGPoint saveCenter = roundedView.center;
    CGRect newFrame = CGRectMake(roundedView.frame.origin.x, roundedView.frame.origin.y, newSize, newSize);
    roundedView.frame = newFrame;
    roundedView.layer.cornerRadius = newSize / 2.0;
    roundedView.center = saveCenter;
    roundedView.clipsToBounds = YES;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
