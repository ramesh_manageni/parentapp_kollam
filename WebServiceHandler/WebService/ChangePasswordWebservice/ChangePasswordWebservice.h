//
//  ChangePasswordWebservice.h
//  ParentApp
//
//  Created by Redbytes on 24/01/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ChangePasswordDelegate <NSObject>

-(void)serverResponseForChangePasswordWebservice:(NSMutableDictionary*)responseArray;
-(void)serverFailResponseForChangePasswordWebservice;

@end
//********************************************************************

@class ConnectionManager;

@interface ChangePasswordWebservice : NSObject
{
    id _delegate;
    NSMutableDictionary *resultDictionary;
    ConnectionManager *objConnectionManager;
}
- (id)delegate;
- (void)setDelegate:(id)new_delegate;
-(void)ChangePasswordForUsername:(NSString *)username withPassword:(NSString *)newpassword profileId:(NSString *)profileId;
@end
