//
//  ChangePasswordWebservice.m
//  ParentApp
//
//  Created by Redbytes on 24/01/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "ChangePasswordWebservice.h"

#import "ConnectionManager.h"

@implementation ChangePasswordWebservice
-(id)init
{
	return self;
}


-(void)ChangePasswordForUsername:(NSString *)username withPassword:(NSString *)newpassword profileId:(NSString *)profileId
{
    NSMutableDictionary *localDictionary=[[NSMutableDictionary alloc]init];
    
    
    NSMutableString *strApplicationUUID=[[NSMutableString alloc]init];
    NSUUID *oNSUUID = [[UIDevice currentDevice] identifierForVendor];
    [strApplicationUUID setString:[oNSUUID UUIDString]];
    [localDictionary setObject:strApplicationUUID forKey:@"device_id"];

    [localDictionary setObject:username forKey:@"username"];
    [localDictionary setObject:newpassword forKey:@"new_password"];
    [localDictionary setObject:profileId forKey:@"current_password"];
    
    NSLog(@"Dict: %@",localDictionary);
    
    if (objConnectionManager!=nil)
    {
		objConnectionManager=nil;
	}
	objConnectionManager=[[ConnectionManager alloc]init];
    //  objConnectionManager.isSBTracker=@"YES";
    
    [objConnectionManager getWebData:self commandName:@"wschangepass" Requestparameter:localDictionary];
    

}

#pragma mark connection manager delegate
- (void)receivedData:(NSArray *)responseArray
{
    if ([_delegate respondsToSelector:@selector(serverResponseForChangePasswordWebservice:)])
    {
        [_delegate serverResponseForChangePasswordWebservice:(NSMutableDictionary *)responseArray];
    }
    else
    {
        [NSException raise:NSInternalInconsistencyException format:@"Delegate doesn't respond to serverResponseForCaptchaWebSerivce:"];
    }
}
-(void)failToReciveData
{
    if ([_delegate respondsToSelector:@selector(serverFailResponseForChangePasswordWebservice)])
    {
        [_delegate serverFailResponseForChangePasswordWebservice];
    }
}
#pragma mark - Delegate methods
- (id)delegate
{
	return  _delegate;
}

- (void)setDelegate:(id)new_delegate
{
	_delegate = new_delegate;
}

@end
