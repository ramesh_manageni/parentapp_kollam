//
//  LogoutWebservice.h
//  ParentApp
//
//  Created by Redbytes on 24/01/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol LogoutWebserviceDelegate <NSObject>

-(void)serverResponseForLogoutWebservice:(NSMutableDictionary*)responseArray;
-(void)serverFailResponseForLogoutWebservice;

@end
//********************************************************************

@class ConnectionManager;

@interface LogoutWebservice : NSObject
{
    id _delegate;
    NSMutableDictionary *resultDictionary;
    ConnectionManager *objConnectionManager;
}
- (id)delegate;
- (void)setDelegate:(id)new_delegate;
-(void)logoutFromServerWithUsername:(NSString *)username;
@end
