//
//  ParseClass.h
//  ParentApp
//
//  Created by RedBytes on 01/08/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ParseClass : NSObject

+(void) incrementClick;
+(void) incrementView;

@end
