//
//  NoticeboardViewController.h
//  ParentApp
//
//  Created by Redbytes on 10/01/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NoticeBoardWebservice.h"
#import "MBProgressHUD.h"
#import "NoticeBoardCustomCell.h"




#import "DXSemiViewControllerCategory.h"
#import "DXSubClassSemiViewController.h"
#import "DXSubclassSemiTableViewController.h"

@protocol MenuButtonNoticeDelegate <NSObject>
-(void)didSelectRowNoticeboard:(NSInteger)rowcount;
@end


@interface NoticeboardViewController : UIViewController<NoticeBoardDelegate,MBProgressHUDDelegate,UITableViewDataSource,UITableViewDelegate,SliderMenuButtonDelegate>
{
    MBProgressHUD *HUD;
    NoticeBoardCustomCell *objResultCustomCell;
    UITableView *tableviewObject;
    NSMutableArray *noticeboardArray;
    
    id _delegate;
    DXSubclassSemiTableViewController *semiLeft;


}
@property (nonatomic, strong) UIDocumentInteractionController *controller;
@property (nonatomic, strong) UIWebView *webView;
@property(nonatomic,strong)	UITableView *tableviewObject;


- (id)delegate;
- (void)setDelegate:(id)new_delegate;



@end
