//
//  InboxDetailViewController.m
//  ParentApp
//
//  Created by Imtiyaz on 8/7/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//
#define kFontName @"BradleyHandITCTT-Bold"
#import "InboxDetailViewController.h"
#import "AppDelegate.h"
#import "SentMessageDetail.h"

@interface InboxDetailViewController ()

@end

@implementation InboxDetailViewController

@synthesize mailTextView,isInbox,isSentbox,stdID_inboxDetail,message_Id,subjectProperty;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"%@",self.nameProperty);
    
    
    //[self setTitle:@"Mail"];
//    self.navigationController.navigationBarHidden=NO;
//    [self.navigationItem setHidesBackButton:YES];
    //self.navigationItem.leftBarButtonItem = [self addLeftBarButtonItem];
    //self.navigationItem.rightBarButtonItem = [self addRightBarButtonItem];
    
//#pragma code for navigation bar
//    [[UINavigationBar appearance]setBarTintColor:[UIColor colorWithRed:10/255.0 green:0/255.0 blue:0/255.0 alpha:1 ]];
//    [self.navigationItem setTitle:@"inbox"];
//    
//    
//    self.navigationController.navigationBarHidden=NO;
//    [self.navigationItem setHidesBackButton:YES];
//    
//    
//    UIButton *deltebutton = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 30.0f, 30.0f)];
//    UIImage *backImage = [UIImage imageNamed:@"delete@2x.png"] ;
//    [deltebutton setBackgroundImage:backImage  forState:UIControlStateNormal];
//    //  [backButton addTarget:self action:@selector(deleteButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
//    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:deltebutton];
//    
//    UIButton *sliderButton = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 30.0f, 30.0f)];
//    UIImage *backImage1 = [UIImage imageNamed:@"side slider button.png"] ;
//    [sliderButton setBackgroundImage:backImage1  forState:UIControlStateNormal];
//    [sliderButton addTarget:self action:@selector(sliderButtonClicked) forControlEvents:UIControlEventTouchUpInside];
//    UIBarButtonItem *backButtonItem1 = [[UIBarButtonItem alloc] initWithCustomView:sliderButton];
//    self.navigationItem.leftBarButtonItem=self.navigationItem.backBarButtonItem;
//    self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:backButtonItem1,backButtonItem,nil];
//    
//    UIButton *backButton = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 30.0f, 30.0f)];
//    UIImage *leftImage = [UIImage imageNamed:@"back button icon.png"] ;
//    [backButton setBackgroundImage:leftImage  forState:UIControlStateNormal];
//    [backButton addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
//    UIBarButtonItem *leftbackButton=[[UIBarButtonItem alloc]initWithCustomView:backButton];
//    self.navigationItem.leftBarButtonItem=leftbackButton;
//    
//    
    //***************************************************
 //    UIScreen *mainScreen = [UIScreen mainScreen];
//    CGFloat scale = ([mainScreen respondsToSelector:@selector(scale)] ? mainScreen.scale : 1.0f);
//    CGFloat pixelHeight = (CGRectGetHeight(mainScreen.bounds) * scale);
//    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
//    {
//        if (scale == 2.0f)
//        {
//            if (pixelHeight == 960.0f)
//            {
//                [self.view setFrame:CGRectMake(0, 7, 320, 480)];
//                
//            }
//            
//            else if (pixelHeight == 1136.0f)
//            {
//                [self.view setFrame:CGRectMake(0, 7, 320, 568)];
//            }
//        }
//        
//    }
    

#pragma  implemented By rizwan
    
    [self setTitle:@"Mail"];
    
    self.navigationController.navigationBarHidden=NO;
    [self.navigationController.navigationBar setBackgroundColor:[UIColor colorWithRed:246/255.0 green:246/255.0 blue:246/255.0 alpha:1]];
    [self.navigationItem setHidesBackButton:YES];
    self.navigationItem.leftBarButtonItem = [self addLeftBarButtonItem];
    self.navigationItem.rightBarButtonItem = [self addRightBarButtonItem];
    
#pragma setting the tableView according to screen size
    
    customNavigationBarView = [[UIView alloc] initWithFrame:CGRectMake(00,64,320,50)];
    customNavigationBarView.backgroundColor = [UIColor colorWithRed:234/255.0 green:234/255.0 blue:234/255.0 alpha:1];
    customeViewTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(110,9,100,30)];
    [customeViewTitleLabel setTextAlignment:NSTextAlignmentCenter];

    if(isSentbox)
    {
        //customeViewTitleLabel.text = @"Sent Items";

    }
    else if (isInbox)
    {
        //customeViewTitleLabel.text = @"Inbox";

    }
    [customeViewTitleLabel setFont:[UIFont systemFontOfSize:20]];
    [customeViewTitleLabel setTextColor:[UIColor blackColor]];
    
   // [customNavigationBarView addSubview:customeViewTitleLabel];
    [self.view addSubview:customNavigationBarView];
    
    
    
    self.view.backgroundColor=[UIColor clearColor];
    UIScreen *mainScreen = [UIScreen mainScreen];
    CGFloat scale = ([mainScreen respondsToSelector:@selector(scale)] ? mainScreen.scale : 1.0f);
    CGFloat pixelHeight = (CGRectGetHeight(mainScreen.bounds) * scale);
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        if (scale == 2.0f)
        {
            if (pixelHeight == 960.0f)
            {
//                mailTextView = [uimailview alloc]init
//                [mailTextView setFrame:CGRectMake(15, 167, 292, 500) ];
               // textView = [[UITextView alloc]initWithFrame:CGRectMake(0,30,320,400)];
                mailTextView =[[UITextView alloc]initWithFrame:CGRectMake(15, 167, 292, 220)];
                customTabbar = [[UIView alloc] initWithFrame:CGRectMake(00,387,320,47)];
                
            }
            
            else if (pixelHeight == 1136.0f)
            {
                mailTextView =[[UITextView alloc]initWithFrame:CGRectMake(15, 167, 292, 568-260)];
                customTabbar = [[UIView alloc] initWithFrame:CGRectMake(00,475,320,47)];

                
            }
        }
    }
    customTabbar.backgroundColor =[UIColor colorWithRed:234/255.0 green:234/255.0 blue:234/255.0 alpha:1];
    
    [self.view addSubview:customTabbar];
    
    [self addSettingButton];
    [self addDeleteButton];

    mailTextView.font = [UIFont systemFontOfSize:15];
    mailTextView.backgroundColor = [UIColor whiteColor];
    mailTextView.scrollEnabled = YES;
    mailTextView.pagingEnabled = YES;
    mailTextView.editable = NO;
    
    [self.view addSubview:mailTextView];
    
//    customNavigationBarView = [[UIView alloc] initWithFrame:CGRectMake(00,20,320,30)];
//    customNavigationBarView.backgroundColor = [UIColor darkGrayColor];
//    customeViewTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(140,00,60,30)];
//    customeViewTitleLabel.text = @"Inbox";
//  [customeViewTitleLabel setFont:[UIFont fontWithName:kFontName size:20]];
//    
//    [customeViewTitleLabel setTextColor:[UIColor whiteColor]];
//    
//    [customNavigationBarView addSubview:customeViewTitleLabel];
//    [self.view addSubview:customNavigationBarView];
////    [self addSettingButton];
//    [self addDeleteButton];
//
//    [self addLeftBarButtonItem];
//    [self addRightBarButtonItem];
    
    self.nameLabel.text =self.nameProperty;

    //    [self.nameLabel setFont:[UIFont systemFontOfSize:15]];
    
    self.dateLabel.text = self.dateProperty;
    [self.dateLabel setFont:[UIFont fontWithName:kFontName size:12]];
    
    self.timeLabel.text = self.timeProperty;
    [self.timeLabel setFont:[UIFont fontWithName:kFontName size:12]];
    
    self.mailTextView.text=self.messageProperty;
    
    // Do any additional setup after loading the view from its nib.
    
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    
    // Regiser for HUD callbacks so we can remove it from the window at the right time
    
    HUD.delegate = self;

}

-(void)viewWillAppear:(BOOL)animated
{
    [self.view setBackgroundColor:[UIColor whiteColor]];
    if(isSentbox)
    {
        self.toOrFromLabel.text=@"  To:";
    }
    else if (isInbox)
    {
        self.toOrFromLabel.text=@"From:";
    }
    
}
//-(void)addLeftBarButtonItem
//{
//    UIButton *backButton = [[UIButton alloc] initWithFrame: CGRectMake(0,0,30,30)];
//    UIImage *backImage = [UIImage imageNamed:@"back button icon.png"] ;
//    [backButton setBackgroundImage:backImage  forState:UIControlStateNormal];
//    [backButton addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
//    
//    [customNavigationBarView addSubview:backButton];
//}
//
//-(void)addRightBarButtonItem
//{
//    UIButton *backButton = [[UIButton alloc] initWithFrame: CGRectMake(280,0,30,30)];
//    UIImage *backImage = [UIImage imageNamed:@"list button icon.png"] ;
//    [backButton setBackgroundImage:backImage  forState:UIControlStateNormal];
//    //[backButton addTarget:self action:@selector(settingButtonClicked) forControlEvents:UIControlEventTouchUpInside];
//        [customNavigationBarView addSubview:backButton];
//}

-(UIBarButtonItem *)addLeftBarButtonItem
{
    UIButton *backButton = [[UIButton alloc] initWithFrame: CGRectMake(0,0,30,30)];
    UIImage *backImage = [UIImage imageNamed:@"back button icon.png"] ;
    [backButton setBackgroundImage:backImage  forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    return backButtonItem;
}

-(UIBarButtonItem *)addRightBarButtonItem
{
    UIButton *backButton = [[UIButton alloc] initWithFrame: CGRectMake(00,0,30,30)];
    UIImage *backImage = [UIImage imageNamed:@"list button icon.png"] ;
    [backButton setBackgroundImage:backImage  forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(settingButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    return backButtonItem;
}
-(void)addDeleteButton
{
    UIButton *deleteButton = [[UIButton alloc] initWithFrame: CGRectMake(280,10,30,30)];
    [deleteButton setBackgroundImage:[UIImage imageNamed:@"delete1.png"] forState:UIControlStateNormal];
    [deleteButton addTarget:self action:@selector(deleteButtonClickAction:) forControlEvents:UIControlEventTouchUpInside];
    [customNavigationBarView addSubview:deleteButton];
    
    
    UIButton *ReplyButton = [[UIButton alloc] initWithFrame: CGRectMake(10,5,30,30)];
    [ReplyButton setBackgroundImage:[UIImage imageNamed:@"reply.png"]  forState:UIControlStateNormal];
    [ReplyButton addTarget:self action:@selector(replyButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    if(isInbox)
    {
    [customTabbar addSubview:ReplyButton];
    }
    
    
    UIButton *composeButton = [[UIButton alloc] initWithFrame: CGRectMake(278,5,30,30)];
    [composeButton setBackgroundImage:[UIImage imageNamed:@"compose1.png"]  forState:UIControlStateNormal];
    [composeButton addTarget:self action:@selector(composeButtonClickAction:) forControlEvents:UIControlEventTouchUpInside];
    [customTabbar addSubview:composeButton];

}
-(void)addSettingButton
{
    UIButton *settingButton = [[UIButton alloc] initWithFrame: CGRectMake(12,10,30,30)];
    [settingButton setBackgroundImage:[UIImage imageNamed:@"settingCommunication.png"]  forState:UIControlStateNormal];
   // [settingButton addTarget:self action:@selector(settingButtonClickAction:) forControlEvents:UIControlEventTouchUpInside];
   // [customNavigationBarView addSubview:settingButton];
}

-(void)composeButtonClickAction:(id)sender
{
    ComposeViewController *objComposeViewController=[[ComposeViewController alloc] initWithNibName:@"ComposeViewController" bundle:nil];
    objComposeViewController.student_id=stdID_inboxDetail;
    [self presentViewController:objComposeViewController animated:YES completion:nil];
}
-(IBAction)deleteButtonClickAction:(id)sender
{
    if (isInbox)
    {
        DeleteInboxMessage *objDeleteInboxMessage=[[DeleteInboxMessage alloc]init];
        objDeleteInboxMessage .delegate =self;
        [objDeleteInboxMessage deleteMessageWithUsername:[[parentArrayCoreData objectAtIndex:0] user_name] pmId:message_Id Inbox_Sent:@"inbox"];
         [HUD show:YES];
    }
    else if (isSentbox)
    {
        DeleteSentMessage *objDeleteSent = [[DeleteSentMessage alloc]init];
        objDeleteSent.delegate = self;
        [objDeleteSent deleteMessageWithUsername:[[parentArrayCoreData objectAtIndex:0] user_name] pmId:message_Id Sent:@"outbox"];
         [HUD show:YES];
    }
}

//***********************************
-(void)settingButtonClicked
{
    if(semiLeft!=nil)
    {
        [semiLeft dismissSemi:nil];
    }
    semiLeft = [[DXSubclassSemiTableViewController alloc] init];
    semiLeft.delegate=self;
    semiLeft.semiTitleLabel.text = @"SemiLeftTableView";
    semiLeft.tableViewRowHeight = 80.0f/2;
    [semiLeft.view setBackgroundColor:[UIColor clearColor]];
    
    self.leftSemiViewController = semiLeft;
}

#pragma mark rizwan
-(void)sliderButtonClicked
{
    if(semiLeft!=nil)
    {
        [semiLeft dismissSemi:nil];
    }
    semiLeft = [[DXSubclassSemiTableViewController alloc] init];
    semiLeft.delegate=self;
    
    semiLeft.semiTitleLabel.text = @"SemiLeftTableView";
    semiLeft.tableViewRowHeight = 80.0f/2;
    [semiLeft.view setBackgroundColor:[UIColor clearColor]];
    self.leftSemiViewController = semiLeft;
    
}

#pragma pragma  mark SliderMenuDelegate
-(void)didSelectSliderMenuButton:(NSInteger)rowcount
{
    // NSLog(@"ROWCOUNT=== %d",rowcount);
    if(rowcount==0)
    {
        [self.navigationController popToViewController:objHomeViewController animated:YES];
    }
    else
    {
        [self.navigationController popToViewController:objHomeViewController animated:NO];
    }
    if ([_delegate respondsToSelector:@selector(didSelectRowInboxDetail:)])
    {
        [_delegate didSelectRowInboxDetail:rowcount];
    }
    
}
#pragma mark Delegate

- (id)delegate
{
	return  _delegate;
}

- (void)setDelegate:(id)new_delegate
{
	_delegate = new_delegate;
}

-(void)backButtonClicked
{
   [self.navigationController popViewControllerAnimated:YES];  
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
//- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
//{
//    
//}

- (IBAction)replyButtonAction:(id)sender
{
    
    ComposeViewController *objComposeViewController=[[ComposeViewController alloc] initWithNibName:@"ComposeViewController" bundle:nil];
    objComposeViewController.student_id=stdID_inboxDetail;
    objComposeViewController.to_address=self.nameProperty;
    objComposeViewController.to_subject=self.subjectProperty;
    objComposeViewController.to_message=self.messageProperty;
    objComposeViewController.isFromReplyButton=@"YES";
    [self presentViewController:objComposeViewController animated:YES completion:nil];
}

#pragma mark Delete Message Delegate
-(void)serverResponseForDeleteInboxMessage:(NSArray *)responseArray
{
    [HUD hide:YES];
    if([responseArray valueForKey:@"success"])
    {
        isDeleteMessage=@"YES";
        
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Message deleted successfully" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        alert.delegate=self;
        alert.tag=100;
    [alert show];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Failed to delete message" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        

    }
    
}
-(void)serverFailResponseForIDeleteInboxMessage
{
    [HUD hide:YES];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Server Response Failed" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}
#pragma mark Alertview Delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    // the user clicked one of the Ok/Cancel buttons
    if(alertView.tag==100)
    {
        [self backButtonClicked];
    }
}

#pragma DeleteSent Delegate
-(void)serverResponseForDeleteSentMessage:(NSArray *)responseArray
{
    
    [HUD hide:YES];
    if([responseArray valueForKey:@"success"])
    {
        isDeleteSent=@"YES";
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Message deleted successfully" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        alert.delegate=self;
        alert.tag=100;
        [alert show];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Failed to delete message" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }

}
-(void)serverFailResponseForDeleteSentMessage
{
    [HUD hide:YES];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Server Response Failed" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}

#pragma mark SentMessage Core Data Methods
-(void)deleteSentMessage:(NSString *)msgID
{
    
    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [delegate managedObjectContext];
    
    NSFetchRequest *fetchReq = [[NSFetchRequest alloc]init];
    
    [fetchReq setEntity:[NSEntityDescription entityForName:@"SentMessageDetail" inManagedObjectContext:context]];
    NSError *error;
    
    NSMutableArray *resultArray = [[NSMutableArray alloc]initWithArray:[context executeFetchRequest:fetchReq error:&error]];
    if(error==nil)
    {
        
        
        for(SentMessageDetail *sentdetails in resultArray)
        {
            if(sentdetails.message_id==msgID)
            {
                [context deleteObject:sentdetails];
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Message deleted successfully" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                alert.delegate=self;
                alert.tag=100;
                [alert show];

            }
            
        }
        
    }
}


@end
