//
//  MailViewController.h
//  ParentApp
//
//  Created by RedBytes on 05/08/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "FeedbackWebservice.h"
#import "TeacherWebservice.h"
#import "MBProgressHUD.h"
#import "DXSemiViewControllerCategory.h"
#import "DXSubClassSemiViewController.h"
#import "DXSubclassSemiTableViewController.h"
#import "InboxWebservice.h"
#import "ConnectionManager.h"
#import "DeleteInboxMessage.h"
#import "ReadInboxMessage.h"
#import "SentItemWebservices.h"
#import "DeleteSentMessage.h"

@protocol MenuButtonMailDelegate <NSObject>

-(void)didSelectRowMail:(NSInteger)rowcount;
@end


@interface MailViewController : UIViewController<InboxDelegate,SentItemDelegate,UITextFieldDelegate,UITextViewDelegate,MBProgressHUDDelegate,UITableViewDataSource,UITableViewDelegate,SliderMenuButtonDelegate,DeleteInboxMessageDelegate,DeleteSentMessageDelegate,ReadInboxMessageDelegate>
{
    MBProgressHUD *HUD;
    NSMutableArray *selectedStudents;
    NSString *isSelectAll;
    id _delegate;
    DXSubclassSemiTableViewController *semiLeft;
    
    BOOL isInbox;
    BOOL isSentbox;
    
    NSUInteger deletemessageCount;
    NSUInteger deleteArrayCount;
    UIButton *deleteButton;
    
    int msgInboxUnreadCount;
    NSUInteger msgSentTotalCount;
}

@property(strong,nonatomic)NSString *stdID;
@property(strong,nonatomic)UITableView *myTableView;
@property(strong,nonatomic)UITableView *sliderTableView;

- (id)delegate;
- (void)setDelegate:(id)new_delegate;

@end
