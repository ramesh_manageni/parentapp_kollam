//
//  DeleteSentMessage.m
//  ParentApp
//
//  Created by Tasneem on 17/09/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "DeleteSentMessage.h"

@implementation DeleteSentMessage

-(void)deleteMessageWithUsername:(NSString *)username pmId:(NSString *)pmMessageID Sent:(NSString *)sent
{
    
    NSMutableDictionary *localDictionary=[[NSMutableDictionary alloc]init];
    //NSString *appVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    
    NSMutableString *strApplicationUUID=[[NSMutableString alloc]init];
    NSUUID *oNSUUID = [[UIDevice currentDevice] identifierForVendor];
    [strApplicationUUID setString:[oNSUUID UUIDString]];
    
    // [localDictionary setObject:strApplicationUUID forKey:@"device_id"];
    
#pragma hardcoded values
    
	[localDictionary setObject:username forKey:@"username"];
	[localDictionary setObject:pmMessageID forKey:@"pm_message_id"];
    [localDictionary setObject:sent forKey:@"folder_name"];
    //[localDictionary setObject:appVersion forKey:@"app_version"];
    
    NSLog(@"Dict: %@",localDictionary);
    if (objConnectionManager!=nil)
    {
		objConnectionManager=nil;
	}
	objConnectionManager=[[ConnectionManager alloc]init];
    // objConnectionManager.isThisUrl=@"http://schoolccesoftware.com/";
    
    objConnectionManager.isDeleteInboxMessageWS=@"YES";
    [objConnectionManager getWebData:self commandName:@"phorum/deleteInbox" Requestparameter:localDictionary];
    
    //http://schoolccesoftware.com/oxforddemo/login/Phorum/Inbox
}
#pragma mark connection manager delegate
- (void)receivedData:(NSArray *)responseArray
{
    if ([_delegate respondsToSelector:@selector(serverResponseForDeleteSentMessage:)])
    {
        [_delegate serverResponseForDeleteSentMessage:responseArray];
    }
    else
    {
        [NSException raise:NSInternalInconsistencyException format:@"Delegate doesn't respond to serverResponseForCaptchaWebSerivce:"];
    }
}
-(void)failToReciveData
{
    if ([_delegate respondsToSelector:@selector(serverFailResponseForDeleteSentMessage)])
    {
        [_delegate serverFailResponseForDeleteSentMessage];
    }
}
#pragma mark - Delegate methods
- (id)delegate
{
	return  _delegate;
}

- (void)setDelegate:(id)new_delegate
{
	_delegate = new_delegate;
}

@end
