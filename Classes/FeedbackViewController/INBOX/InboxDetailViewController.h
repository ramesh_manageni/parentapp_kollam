//
//  InboxDetailViewController.h
//  ParentApp
//
//  Created by Imtiyaz on 8/7/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MailViewController.h"
#import "DXSemiTableViewController.h"
#import "DXSemiViewControllerCategory.h"
#import "DXSubClassSemiViewController.h"
#import "Constant.h"
#import "ComposeViewController.h"
#import "MBProgressHUD.h"
#import "DeleteSentMessage.h"


@protocol InboxDetailDelegate <NSObject>

-(void)didSelectRowInboxDetail:(NSInteger)rowcount;

@end

@interface InboxDetailViewController : UIViewController<UITextFieldDelegate,SliderMenuButtonDelegate,MBProgressHUDDelegate,UIAlertViewDelegate
>
{
    UIView *customNavigationBarView,*sliderView,*gestureView,*customTabbar;
    UILabel *customeViewTitleLabel;
    DXSubclassSemiTableViewController *semiLeft;
    id _delegate;
    
    MBProgressHUD *HUD;


}
@property (strong, nonatomic) IBOutlet UILabel *toOrFromLabel;
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UILabel *dateLabel;
@property (strong, nonatomic) IBOutlet UIView *detailView;
@property (strong, nonatomic) IBOutlet UILabel *timeLabel;
@property (strong, nonatomic)  UITextView *mailTextView;
- (IBAction)replyButtonAction:(id)sender;

- (id)delegate;
- (void)setDelegate:(id)new_delegate;

@property(weak,nonatomic)NSString *nameProperty;
@property(weak,nonatomic)NSString *dateProperty;
@property(weak,nonatomic)NSString *timeProperty;
@property(weak,nonatomic)NSString *messageProperty;
@property(weak,nonatomic)NSString *subjectProperty;

@property(strong,nonatomic) NSString *stdID_inboxDetail;
@property(strong,nonatomic) NSString *message_Id;
@property BOOL isInbox;
@property BOOL isSentbox;


@end
