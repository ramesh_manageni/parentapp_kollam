//
//  MailSliderCustomCell.h
//  ParentApp
//
//  Created by RedBytes on 06/08/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MailSliderCustomCell : UITableViewCell

@property(strong,nonatomic)UILabel *count;
@property(strong,nonatomic)UILabel *contentTitle;
@property(strong,nonatomic)UIImageView *contentImageView;

@end
