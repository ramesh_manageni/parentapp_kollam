//
//  MenuViewController.m
//  ParentApp
//
//  Created by Redbytes on 03/01/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "MenuViewController.h"
#import "Constant.h"
#import "LogoutWebservice.h"
#import "Parent.h"
#import "StudentDetails.h"
#import "MBProgressHUD.h"
#import "InfoViewController.h"
#import "ParseClass.h"
#import "MailViewController.h"

@interface MenuViewController ()

@end

@implementation MenuViewController
@synthesize logoutView,noBoardBadgeLabel,notificationBadgeLabel,inboxCountLabel;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    noticeBoardBadgeCount = 0;
    communicationBadgeCount= 0;
    
    //notificationBadgeCount = 100;
    
    //settings up badges count on the right top corner of buttons
    noBoardBadgeLabel.layer.cornerRadius = 11.0;
    noBoardBadgeLabel.layer.borderColor = [UIColor whiteColor].CGColor;
    noBoardBadgeLabel.layer.borderWidth = 2.0;
    
    notificationBadgeLabel.layer.cornerRadius = 11.0;
    notificationBadgeLabel.layer.borderColor = [UIColor whiteColor].CGColor;
    notificationBadgeLabel.layer.borderWidth = 2.0;
    
    inboxCountLabel.layer.cornerRadius = 11.0;
    inboxCountLabel.layer.borderColor = [UIColor whiteColor].CGColor;
    inboxCountLabel.layer.borderWidth = 2.0;
    
    [self setTitle:@"ParentApp"];
    
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont
                                                                           fontWithName:@"BradleyHandITCTT-Bold" size:22], NSFontAttributeName,
                                [UIColor whiteColor], NSForegroundColorAttributeName, nil];
    
    [[UINavigationBar appearance] setTitleTextAttributes:attributes];
    i=0;
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] init];
    tapGesture.delegate = self;
    tapGesture.numberOfTapsRequired = 1;
    tapGesture.numberOfTouchesRequired = 1;
    [tapGesture addTarget:self action:@selector(handleTap:)];
    theOldFrame = [self.logoutView frame];
    theOldFrame.origin.x=-280;
    [self.view addGestureRecognizer:tapGesture];
    
    self.logoutView.backgroundColor = [UIColor colorWithRed:0  green:0 blue:0 alpha:0.85];
    
    
    // Do any additional setup after loading the view from its nib.
    self.navigationController.navigationBarHidden=NO;
    [self.navigationItem setHidesBackButton:YES];
    self.navigationItem.rightBarButtonItem = [self addLeftBarButtonItem];
    self.navigationItem.leftBarButtonItem = [self addRightBarButtonItem];
    
    
    UIScreen *mainScreen = [UIScreen mainScreen];
    CGFloat scale = ([mainScreen respondsToSelector:@selector(scale)] ? mainScreen.scale : 1.0f);
    CGFloat pixelHeight = (CGRectGetHeight(mainScreen.bounds) * scale);
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        if (scale == 2.0f)
        {
            if (pixelHeight == 960.0f)
            {
                [self.baseView setFrame:CGRectMake(0, 7, 320, 480)];
                
            }
            
            else if (pixelHeight == 1136.0f)
            {
                [self.baseView setFrame:CGRectMake(0, 7, 320, 568)];
            }
        }
        
    }
}

-(void) viewDidAppear:(BOOL)animated
{
    [ParseClass incrementView];
}

-(void)viewWillAppear:(BOOL)animated
{
    i=0;
    notificationBadgeCount = 0;
    
    if (noticeBoardBadgeCount == 0)
    {
        [noBoardBadgeLabel setHidden:YES];
    }
    else
    {
        [noBoardBadgeLabel setHidden:NO];
        noBoardBadgeLabel.text = [NSString stringWithFormat:@"%D",noticeBoardBadgeCount];
    }
    
    if (notificationBadgeCount == 0)
    {
        [notificationBadgeLabel setHidden:YES];
    }
    else
    {
        [notificationBadgeLabel setHidden:NO];
        
        notificationBadgeLabel.text = [NSString stringWithFormat:@"%D",notificationBadgeCount];
    }
    if (communicationBadgeCount ==0)
    {
        [inboxCountLabel setHidden:YES];
    }
    else
    {
        [inboxCountLabel setHidden:NO];
        inboxCountLabel.text = [NSString stringWithFormat:@"%D",communicationBadgeCount];
    }
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(UIBarButtonItem *)addLeftBarButtonItem
{
    UIButton *backButton = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 25.0f, 25.0f)];
    //UIImage *backImage = [UIImage imageNamed:@"back button icon.png"] ;
    //[backButton setBackgroundImage:backImage  forState:UIControlStateNormal];
    //[backButton addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    return backButtonItem;
}
-(UIBarButtonItem *)addRightBarButtonItem
{
    UIButton *backButton = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 35.0f, 30.0f)];
    UIImage *backImage = [UIImage imageNamed:@"setting.png"] ;
    [backButton setBackgroundImage:backImage  forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(settingButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    return backButtonItem;
}
-(void)backButtonClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)handleTap:(id)sender
{
    NSLog(@"...............%@",isSettingClicked);
    if([isSettingClicked isEqualToString:@"YES"])
    {
        i++;
        [UIView animateWithDuration:0.3
                         animations:^{
                             self.logoutView.frame = theOldFrame;
                         }];
        isSettingClicked=@"NO";
    }
}

-(void)settingButtonClicked
{  // theOldFrame = [self.logoutView frame];
    //    theOldFrame.origin.x=-160;
    
    if(i %2==0)
    {
        i++;
        theNewFrame = [self.logoutView frame];
        theNewFrame.origin.x = 0;
        isSettingClicked=@"YES";
        //self.logoutView.frame = theFrame;
        //geopointView.frame = // somewhere offscreen, in the direction you want it to appear from
        [UIView animateWithDuration:0.3
                         animations:^{
                             self.logoutView.frame = theNewFrame;
                         }];
    }
    else
    {
        isSettingClicked=@"NO";
        i++;
        [UIView animateWithDuration:0.3
                         animations:^{
                             self.logoutView.frame = theOldFrame;
                         }];
    }
    
    // NSLog(@"setting Button Clicked");
    //ChangePasswordViewController *objChangePasswordViewController=[[ChangePasswordViewController alloc]initWithNibName:@"ChangePasswordViewController" bundle:nil];
    //[self.navigationController pushViewController:objChangePasswordViewController animated:YES];
    //[self presentViewController:objChangePasswordViewController animated:YES completion:nil];
    
}

#pragma rizwan
- (IBAction)videoButtonClicked:(id)sender
{
    int k=0;
    if(k==0)
    {
        NSString *urlStr = [[NSBundle mainBundle] pathForResource:@"parentAppVideo.mp4" ofType:nil];
        
        NSURL *url = [NSURL fileURLWithPath:urlStr];
        moviePlayer = [[MPMoviePlayerController alloc] initWithContentURL:url];
        [self.view addSubview:moviePlayer.view];
        
        UIScreen *mainScreen = [UIScreen mainScreen];
        CGFloat scale = ([mainScreen respondsToSelector:@selector(scale)] ? mainScreen.scale : 1.0f);
        CGFloat pixelHeight = (CGRectGetHeight(mainScreen.bounds) * scale);
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
        {
            if (scale == 2.0f)
            {
                if (pixelHeight == 960.0f)
                {
                    [moviePlayer.view setFrame:CGRectMake(0, 65, 320, 480-114)];
                    
                }
                
                else if (pixelHeight == 1136.0f)
                {
                    [moviePlayer.view setFrame:CGRectMake(0, 65, 320, 568-114)];
                }
            }
            
        }
        
        // moviePlayer.view.frame = CGRectMake(0, 0, 300, 400);
        [moviePlayer play];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(moviePlaybackComplete:)
                                                     name:MPMoviePlayerPlaybackDidFinishNotification
                                                   object:moviePlayer];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(doneButtonClick:)
                                                     name:MPMoviePlayerWillExitFullscreenNotification
                                                   object:nil];
    }
    
}

-(void)doneButtonClick:(NSNotification*)aNotification
{
    MPMoviePlayerViewController *moviePlayerController = [aNotification object];
    [moviePlayerController.view removeFromSuperview];
    
}

- (void)moviePlaybackComplete:(NSNotification *)notification
{
    MPMoviePlayerViewController *moviePlayerController = [notification object];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:MPMoviePlayerPlaybackDidFinishNotification
                                                  object:moviePlayerController];
    [moviePlayerController.view removeFromSuperview];
    i++;
    
}

//***********************************


- (IBAction)logoutButtonClicked:(UIButton *)sender
{
    //    [UIView animateWithDuration:0.2
    //                     animations:^{
    //                         self.logoutView.frame = theOldFrame;
    //
    //                     }];
    
    
    LogoutWebservice *objLogoutWebservice=[[LogoutWebservice alloc]init];
    objLogoutWebservice.delegate=self;
    [objLogoutWebservice logoutFromServerWithUsername:[[parentArrayCoreData objectAtIndex:0] user_name]];
    
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    
    // Regiser for HUD callbacks so we can remove it from the window at the right time
    HUD.delegate = self;
    
    // Show the HUD while the provided method executes in a new thread
    //[HUD showWhileExecuting:@selector(myTask) onTarget:self withObject:nil animated:YES];
    [HUD show:YES];
    
}

- (IBAction)cancelButtonClicked:(UIButton *)sender
{
    i++;
    [UIView animateWithDuration:0.3
                     animations:^{
                         self.logoutView.frame = theOldFrame;
                     }];
    
}
- (IBAction)changepassButtonClicked:(UIButton *)sender
{
    
    ChangePasswordViewController *objChangePasswordViewController=[[ChangePasswordViewController alloc]initWithNibName:@"ChangePasswordViewController" bundle:nil];
    //[self.navigationController pushViewController:objChangePasswordViewController animated:YES];
    [self presentViewController:objChangePasswordViewController animated:YES completion:nil];
}

- (IBAction)faqButtonClicked:(id)sender {
    NoticePDFViewController *objNoticePDFViewController=[[NoticePDFViewController alloc]initWithNibName:@"NoticePDFViewController" bundle:nil];
    objNoticePDFViewController.pdfNmaeNotice=@"FAQ";
    objNoticePDFViewController.delegate= self;
    [self.navigationController pushViewController:objNoticePDFViewController animated:YES];
}

- (IBAction)menuButtonClicked:(UIButton *)sender
{
    switch (sender.tag)
    {
        case 1:
        {
            AttendanceViewController *objAttendanceViewController=[[AttendanceViewController alloc] initWithNibName:@"AttendanceViewController" bundle:nil];
            objAttendanceViewController.delegate=self;
            [self.navigationController pushViewController:objAttendanceViewController animated:YES];
            break;
        }
            
        case 2:
        {
            isFeeViewController=@"NO";
            ResultViewController *objResultViewController=[[ResultViewController alloc] initWithNibName:@"ResultViewController" bundle:nil];
            objResultViewController.flag = @"NO";
            [objResultViewController setTitle:@"Result"];
            
            objResultViewController.delegate=self;
            
            [self.navigationController pushViewController:objResultViewController animated:YES];
            break;
        }
        case 3:
            
        {
            isFeeViewController=@"YES";
            ResultViewController *objResultViewController=[[ResultViewController alloc] initWithNibName:@"ResultViewController" bundle:nil];
            objResultViewController.flag = @"YES";
            [objResultViewController setTitle:@"Fee"];
            
            objResultViewController.delegate=self;
            
            [self.navigationController pushViewController:objResultViewController animated:YES];
            break;
        }
        case 4:
        {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Real time school bus tracking will be available in next release" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            
//            SBTrackerViewController *objSBTrackerViewController=[[SBTrackerViewController alloc]initWithNibName:@"SBTrackerViewController" bundle:nil];
//            
//            [self.navigationController pushViewController:objSBTrackerViewController animated:YES];
            break;
        }
        case 5:
        {
            NSLog(@"Button Clicked %ld",(long)sender.tag);
            NoticeboardViewController *objNoticeboardViewController=[[NoticeboardViewController alloc]initWithNibName:@"NoticeboardViewController" bundle:nil];
            objNoticeboardViewController.delegate=self;
            [self.navigationController pushViewController:objNoticeboardViewController animated:YES];
            
            break;
        }
        case 6:
        {
            NSLog(@"Info Button Clicked %ld",(long)sender.tag);
            InfoViewController *objInfoViewController=[[InfoViewController alloc]initWithNibName:@"InfoViewController" bundle:nil];
            [self.navigationController  pushViewController:objInfoViewController animated:YES];
            
            break;
        }
        case 7:
        {
            //            FeedbackViewController *objFeedbackViewController=[[FeedbackViewController alloc]initWithNibName:@"FeedbackViewController" bundle:nil];
            //
            //            objFeedbackViewController.delegate=self;
            //            [self.navigationController pushViewController:objFeedbackViewController animated:YES];
            //            break;
            
            if([[[parentArrayCoreData objectAtIndex:0] user_name]isEqualToString:@"12345"])
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"This feature is available only for valid users." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alert show];
                
            }
            else
            {
                
                studentInboxController *objstudentInboxController = [[studentInboxController alloc]initWithNibName:@"studentInboxController" bundle:nil];
                objstudentInboxController.delegate=self;
                [self.navigationController pushViewController:objstudentInboxController animated:YES];
            }
            break;
        }
            
        default:
            break;
    }
}

#pragma mark Logout webservice delegate
-(void)serverResponseForLogoutWebservice:(NSMutableDictionary*)responseArray
{
    i++;
    
    [HUD hide:YES];
    NSLog(@"%@",responseArray);
    if (responseArray!=nil)
    {
        NSLog(@"RESPONSE : %@",responseArray);
        
        if([[responseArray valueForKey:@"success"] intValue])
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"NOTE" message:@"Logout successfully" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            alert.delegate=self;
            alert.tag=1;
            [alert show];
            
            NSUserDefaults *preference=[NSUserDefaults standardUserDefaults];
            [preference setValue:@"NO" forKey:@"login"];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"NOTE" message:@"Logout not successful" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:@"Cancel", nil];
            alert.delegate=self;
            alert.tag=0;
            
            [alert show];
        }
        
    }
    
}
-(void)serverFailResponseForLogoutWebservice
{
    i++;
    
    [HUD hide:YES];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Server response failed" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:@"Cancel", nil];
    alert.delegate=self;
    alert.tag=10;
    
    [alert show];
    
}
#pragma mark Alertview Delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    // the user clicked one of the Ok/Cancel buttons
    if(alertView.tag==1)
    {
        [self.logoutView removeFromSuperview];
        [self.navigationController popViewControllerAnimated:YES];
    }
    if(alertView.tag==0)
    {
        //[self.logoutView removeFromSuperview];
        //[self.navigationController popViewControllerAnimated:YES];
        if(buttonIndex==1)
        {
            if([isSettingClicked isEqualToString:@"YES"])
            {
                i++;
                [UIView animateWithDuration:0.3
                                 animations:^{
                                     self.logoutView.frame = theOldFrame;
                                 }];
                isSettingClicked=@"NO";
                
            }
        }
        
    }
    if(alertView.tag==10)
    {
        // [self.logoutView removeFromSuperview];
        //[self.navigationController popViewControllerAnimated:YES];
        if(buttonIndex==1)
        {
            if([isSettingClicked isEqualToString:@"YES"])
            {
                i++;
                [UIView animateWithDuration:0.3
                                 animations:^{
                                     self.logoutView.frame = theOldFrame;
                                 }];
                isSettingClicked=@"NO";
                
            }
        }
    }
}
#pragma mark MenuButtonDelegate
-(void)didSelectRow:(NSInteger)rowcount
{
    UIButton *btn=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    btn.tag=rowcount;
    [self menuButtonClicked:btn];
}

#pragma mark MenuButtonCommunicationDelegate
-(void)didSelectCommunicationRow:(NSInteger)rowcount
{
    UIButton *btn=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    btn.tag=rowcount;
    [self menuButtonClicked:btn];
}


#pragma mark MenuButtonNoticeDelegate
-(void)didSelectRowNoticeboard:(NSInteger)rowcount
{
    UIButton *btn=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    btn.tag=rowcount;
    [self menuButtonClicked:btn];
}

#pragma mark MenuButtonInfoDelegate
-(void)didSelectRowInfo:(NSInteger)rowcount
{
    UIButton *btn=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    btn.tag=rowcount;
    [self menuButtonClicked:btn];
    
}

#pragma mark MenuButtonResultDelegate
-(void)didSelectRowResult:(NSInteger)rowcount
{
    UIButton *btn=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    btn.tag=rowcount;
    [self menuButtonClicked:btn];
    
}
-(void)didSelectRowFeedback:(NSInteger)rowcount
{
    UIButton *btn=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    btn.tag=rowcount;
    [self menuButtonClicked:btn];
    
}
-(void)didSelectRowNoticeboardDetail:(NSInteger)rowcount
{
    
    UIButton *btn=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    btn.tag=rowcount;
    [self menuButtonClicked:btn];
    
}
-(void)didSelectRowFees:(NSInteger)rowcount
{
    
    UIButton *btn=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    btn.tag=rowcount;
    [self menuButtonClicked:btn];
    
    
}
-(void)didSelectRowExamGrade:(NSInteger)rowcount
{
    UIButton *btn=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    btn.tag=rowcount;
    [self menuButtonClicked:btn];
    
}
-(void)didSelectRowSubjectGrade:(NSInteger)rowcount
{
    UIButton *btn=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    btn.tag=rowcount;
    [self menuButtonClicked:btn];
    
    
}
-(void)didSelectRowCalenderAllView:(NSInteger)rowcount
{
    UIButton *btn=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    btn.tag=rowcount;
    [self menuButtonClicked:btn];
}
-(void)didSelectRowCalenderSingleView:(NSInteger)rowcount
{
    UIButton *btn=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    btn.tag=rowcount;
    [self menuButtonClicked:btn];
}
#pragma rizwan

-(void)didSelectRowInboxDetail:(NSInteger)rowcount
{
    UIButton *btn=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    btn.tag=rowcount;
    [self menuButtonClicked:btn];
}

-(void)didSelectRowMail:(NSInteger)rowcount
{
    UIButton *btn=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    btn.tag=rowcount;
    [self menuButtonClicked:btn];
}
//********************
-(void)didSelectSliderMenuButton:(NSInteger)rowcount
{
    
}
@end
