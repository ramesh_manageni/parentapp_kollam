//
//  ComposeMessage.h
//  ParentApp
//
//  Created by Redbytes on 22/08/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "ConnectionManager.h"
#import "StudentDetails.h"


@protocol ComposeMessageDelegate <NSObject>

-(void)serverResponseForComposeMessage:(NSArray *)responseArray;
-(void)serverFailResponseForIComposeMessage;

@end
//********************************************************************

@class ConnectionManager;

@interface ComposeMessage : NSObject
{
    id _delegate;
    NSMutableDictionary *resultDictionary;
    ConnectionManager *objConnectionManager;
    
}


- (id)delegate;
- (void)setDelegate:(id)new_delegate;
-(void)composeMessgaeFrom:(NSString *)from  toMessage:(NSString *)toMessage SubjectName:(NSString *)subjectName message:(NSString *)message withSubject:(NSString *)subject withstudent_ID:(NSString *) student_id withStatus:(BOOL ) status ;

@end



