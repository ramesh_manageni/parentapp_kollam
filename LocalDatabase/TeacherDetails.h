//
//  TeacherDetails.h
//  ParentApp
//
//  Created by Redbytes on 27/08/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class StudentDetails;

@interface TeacherDetails : NSManagedObject

@property (nonatomic, retain) NSString * login_name;
@property (nonatomic, retain) NSString * subject;
@property (nonatomic, retain) NSString * teacher;
@property (nonatomic, retain) NSString * teacher_email;
@property (nonatomic, retain) StudentDetails *teacherToStudent;

@end
