//
//  Parent.m
//  ParentApp
//
//  Created by Redbytes on 05/02/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "Parent.h"
#import "StudentDetails.h"


@implementation Parent

@dynamic detailed_name;
@dynamic id_parent;
@dynamic institution_id;
@dynamic institution_name;
@dynamic login_name;
@dynamic login_pwd;
@dynamic passwords;
@dynamic profile_id;
@dynamic remarks;
@dynamic role_id;
@dynamic role_type_id;
@dynamic status_flag;
@dynamic style_id;
@dynamic updated_by;
@dynamic updated_date;
@dynamic user_id;
@dynamic user_name;
@dynamic user_role_id;
@dynamic valid_from;
@dynamic email;
@dynamic mobile1;
@dynamic parentToChild;

@end
