//
//  InfoCustomCell.m
//  ParentApp
//
//  Created by Redbytes on 19/02/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//
#define kFontName @"BradleyHandITCTT-Bold"

#import "InfoCustomCell.h"

@implementation InfoCustomCell
@synthesize NotificationText,NotificationTime,objImageViewBG,objImageViewStudent;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        NotificationText =[[UILabel alloc]initWithFrame:CGRectMake(130, 17, 150, 20)];
		[NotificationText setFont:[UIFont fontWithName:kFontName size:17]];
		[NotificationText setBackgroundColor:[UIColor clearColor]];
        [NotificationText setTextColor:[UIColor whiteColor]];
        [NotificationText setTextAlignment:NSTextAlignmentCenter];
        
        NotificationTime =[[UILabel alloc]initWithFrame:CGRectMake(130, 40, 150, 20)];
		[NotificationTime setFont:[UIFont fontWithName:kFontName size:12]];
		[NotificationTime setBackgroundColor:[UIColor clearColor]];
        [NotificationTime setTextColor:[UIColor whiteColor]];
        [NotificationTime setTextAlignment:NSTextAlignmentCenter];
        
        
        objImageViewBG=[[UIImageView alloc] initWithFrame:CGRectMake(25, 0, 270, 75)];
        [objImageViewBG setImage:[UIImage imageNamed:@"attendance tab.png"]];
        [objImageViewBG setBackgroundColor:[UIColor blackColor]];
        
        objImageViewStudent=[[UIImageView alloc] initWithFrame:CGRectMake(49, 12, 56, 52)];
        [objImageViewStudent setBackgroundColor:[UIColor clearColor]];
        [objImageViewStudent setImage:[UIImage imageNamed:@"AppIcon120n.png"]];
        
        
        [self.contentView addSubview:objImageViewBG];
        [self.contentView addSubview:objImageViewStudent];
        
        [self.contentView addSubview:NotificationText];
        [self.contentView addSubview:NotificationTime];
        [self.contentView setBackgroundColor:[UIColor blackColor]];
        

    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
