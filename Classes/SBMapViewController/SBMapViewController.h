//
//  SBMapViewController.h
//  ParentApp
//
//  Created by Redbytes on 22/01/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SBMapWebservice.h"
#import <MapKit/MapKit.h>
#import <GoogleMaps/GoogleMaps.h>

@interface SBMapViewController : UIViewController<SBMapDelegate,MKMapViewDelegate,GMSMapViewDelegate, CLLocationManagerDelegate>
{
    
    CLLocationManager *clLocManObj;
    GMSCameraPosition *camera;
    GMSMapView *mapView;
    GMSCircle *circle;
    GMSMutablePath *objpath;
    GMSMarker *marker, *marker1, *marker2;
    
    CLLocation *loc1, *loc2;
    CLLocationCoordinate2D cords1, cords2;
    
    NSMutableArray *latArray;
    NSMutableArray *longArray;

 
}
@property(nonatomic,strong)NSString *imei_number;

@property(strong,nonatomic)NSMutableArray *annotationArray;

@property(strong,nonatomic)NSMutableDictionary *pickupptsArray;

@end
