//
//  FeeWebservice.m
//  ParentApp
//
//  Created by Redbytes on 07/01/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "FeeWebservice.h"

#import "ConnectionManager.h"

@implementation FeeWebservice

-(id)init
{
	return self;
}
-(void)showFeesForUsername:(NSString *)username studentId:(NSString *)studentId
{
    NSMutableDictionary *localDictionary=[[NSMutableDictionary alloc]init];
    
    
    NSMutableString *strApplicationUUID=[[NSMutableString alloc]init];
    NSUUID *oNSUUID = [[UIDevice currentDevice] identifierForVendor];
    [strApplicationUUID setString:[oNSUUID UUIDString]];
    [localDictionary setObject:strApplicationUUID forKey:@"device_id"];

    [localDictionary setObject:username forKey:@"username"];
    [localDictionary setObject:studentId forKey:@"student_id_for_fee"];
    NSLog(@"Dict: %@",localDictionary);
    
    if (objConnectionManager!=nil)
    {
		objConnectionManager=nil;
	}
	objConnectionManager=[[ConnectionManager alloc]init];
    
    [objConnectionManager getWebData:self commandName:@"wsfees" Requestparameter:localDictionary];
}

#pragma mark connection manager delegate
- (void)receivedData:(NSArray *)responseArray
{
    if ([_delegate respondsToSelector:@selector(serverResponseForFeeWebservice:)])
    {
        [_delegate serverResponseForFeeWebservice:responseArray];
    }
    else
    {
        [NSException raise:NSInternalInconsistencyException format:@"Delegate doesn't respond to serverResponseForCaptchaWebSerivce:"];
    }
}
-(void)failToReciveData
{
    if ([_delegate respondsToSelector:@selector(serverFailResponseForFeeWebservice)])
    {
        [_delegate serverFailResponseForFeeWebservice];
    }
}
#pragma mark - Delegate methods
- (id)delegate
{
	return  _delegate;
}

- (void)setDelegate:(id)new_delegate
{
	_delegate = new_delegate;
}



@end
