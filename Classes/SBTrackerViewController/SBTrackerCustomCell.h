//
//  SBTrackerCustomCell.h
//  ParentApp
//
//  Created by Redbytes on 22/01/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SBTrackerCustomCell : UITableViewCell
{
    UILabel *labelName;
    UILabel *labelVehicleNumber;
    UILabel *labelVehicleImei;
    UIImageView *objImageView;
    
}
@property(strong,nonatomic)UILabel *labelName,*labelVehicleNumber,*labelVehicleImei;

@property(strong,nonatomic)UIImageView *objImageViewBG;
@property(strong,nonatomic)UIImageView *objImageViewStudent;

@end
