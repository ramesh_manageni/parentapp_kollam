//
//  CustomCellEvents.h
//  ParentApp
//
//  Created by RedBytes on 23/06/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomCellEvents : UITableViewCell
//@property (strong, nonatomic) UILabel *dateLabel;
@property (strong, nonatomic) UILabel *eventNameLabel;
@property (strong, nonatomic) UILabel *locationLabel;

@property (strong, nonatomic) UILabel *descriptionLabel;

@property (strong, nonatomic) UILabel *durationLabel;

@end
