//
//  ExamGradeViewController.h
//  ParentApp
//
//  Created by Redbytes on 03/01/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ResultCustomCell.h"


#import "DXSemiViewControllerCategory.h"
#import "DXSubClassSemiViewController.h"
#import "DXSubclassSemiTableViewController.h"
@protocol MenuButtonExamGradeDelegate <NSObject>

-(void)didSelectRowExamGrade:(NSInteger)rowcount;

@end


@interface ExamGradeViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,SliderMenuButtonDelegate>
{
    ResultCustomCell *objResultCustomCell;
    NSMutableArray *examNameArray;
    NSMutableArray *examGradeArray;
    UITableView *tableviewObject;
    NSDictionary *resultDict;
    
    id _delegate;
    DXSubclassSemiTableViewController *semiLeft;

}
@property(nonatomic,strong)	UITableView *tableviewObject;


- (id)delegate;
- (void)setDelegate:(id)new_delegate;


@end
