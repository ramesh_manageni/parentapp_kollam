//
//  ComposeMessage.m
//  ParentApp
//
//  Created by Redbytes on 22/08/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "ComposeMessage.h"
#import "Constant.h"

@implementation ComposeMessage

-(void)composeMessgaeFrom:(NSString *)from  toMessage:(NSString *)toMessage SubjectName:(NSString *)subjectName message:(NSString *)message withSubject:(NSString *)subject withstudent_ID:(NSString *) student_id withStatus:(BOOL ) status

{
    NSMutableDictionary *localDictionary=[[NSMutableDictionary alloc]init];
    
    
    //NSString *appVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    
    NSMutableString *strApplicationUUID=[[NSMutableString alloc]init];
    NSUUID *oNSUUID = [[UIDevice currentDevice] identifierForVendor];
    [strApplicationUUID setString:[oNSUUID UUIDString]];
    //[localDictionary setObject:strApplicationUUID forKey:@"device_id"];
    
    
    
	[localDictionary setObject:from forKey:@"From"];
	[localDictionary setObject:subjectName forKey:@"subject_name"];
	[localDictionary setObject:subject forKey:@"Subject"];
	[localDictionary setObject:message forKey:@"Message"];
    if(student_id != nil)
    {
    [localDictionary setValue:student_id forKey:@"student_id"];
    }
    else
    {
        [localDictionary setValue:@"" forKey:@"student_id"];

    }
	[localDictionary setValue:[NSNumber numberWithBool:status]forKey:@"success"];
    [localDictionary setObject:toMessage forKey:@"To"];
    [localDictionary setObject:institute_id forKey:@"institute_id"];
    
    
    
    //[localDictionary setValue:@"b7bc2f4a-e38e-4336-af7d-e6c392c2f817" forKey:@"MessageID"];
    //[[NSUserDefaults standardUserDefaults] objectForKey:@"apnsToken"]
    //[localDictionary setValue:[NSString stringWithFormat:@"%@",[NSDate date]] forKey:@"Date"];
    //[localDictionary setObject:appVersion forKey:@"app_version"];
    //Add message data to global file and save it in to data base
    
    sentMessageDict=localDictionary;
    
    NSLog(@"Dict: %@",localDictionary);
    
    if (objConnectionManager!=nil)
    {
		objConnectionManager=nil;
	}
	objConnectionManager=[[ConnectionManager alloc]init];
    // objConnectionManager.isThisUrl=@"http://schoolccesoftware.com/";
    
    objConnectionManager.isComposeMessageWS=@"YES";
    [objConnectionManager getWebData:self commandName:@"phorum/compose" Requestparameter:localDictionary];
    
    //http://schoolccesoftware.com/oxforddemo/login/Phorum/Inbox
}
#pragma mark connection manager delegate
- (void)receivedData:(NSArray *)responseArray
{
    if ([_delegate respondsToSelector:@selector(serverResponseForComposeMessage:)])
    {
        [_delegate serverResponseForComposeMessage:responseArray];
    }
    else
    {
        [NSException raise:NSInternalInconsistencyException format:@"Delegate doesn't respond to serverResponseForCaptchaWebSerivce:"];
    }
}
-(void)failToReciveData
{
    if ([_delegate respondsToSelector:@selector(serverFailResponseForIComposeMessage)])
    {
        [_delegate serverFailResponseForIComposeMessage];
    }
}
#pragma mark - Delegate methods
- (id)delegate
{
	return  _delegate;
}

- (void)setDelegate:(id)new_delegate
{
	_delegate = new_delegate;
}

@end
