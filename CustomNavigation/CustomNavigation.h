//
//  CustomNavigation.h
//  ParentApp
//
//  Created by Redbytes on 06/01/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CustomNavigation : NSObject
{
    
}

@property(strong,nonatomic)UIViewController *tempVC;
-(UIBarButtonItem *)addLeftBarButtonItem:(id)sender;
-(UIBarButtonItem *)addRightBarButtonItem:(id)sender;
-(void)backButtonClicked;
-(void)settingButtonClicked;

@end
