//
//  NoticePDFViewController.h
//  ParentApp
//
//  Created by Redbytes on 14/02/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>


#import "DXSemiViewControllerCategory.h"
#import "DXSubClassSemiViewController.h"
#import "DXSubclassSemiTableViewController.h"

@protocol MenuButtonNoticeBordDetailDelegate <NSObject,SliderMenuButtonDelegate>

-(void)didSelectRowNoticeboardDetail:(NSInteger)rowcount;

@end

@interface NoticePDFViewController : UIViewController
{
    id _delegate;
    DXSubclassSemiTableViewController *semiLeft;
}

@property (nonatomic, strong) UIWebView *webView;
@property(nonatomic,strong) NSString *pdfNmaeNotice;


- (id)delegate;
- (void)setDelegate:(id)new_delegate;




@end
