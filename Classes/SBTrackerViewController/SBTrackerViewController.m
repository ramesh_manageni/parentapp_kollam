//
//  SBTrackerViewController.m
//  ParentApp
//
//  Created by Redbytes on 22/01/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "SBTrackerViewController.h"
#import "Constant.h"
#import "StudentDetails.h"
#import "SBMapViewController.h"
#import "NSData+Base64.h"
@interface SBTrackerViewController ()

@end

@implementation SBTrackerViewController

@synthesize vehicleInfoArray,tableviewObject;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        SBTrackerWebservice *objSBTrackerWebservice=[[SBTrackerWebservice alloc]init];
        objSBTrackerWebservice.delegate=self;
        
        for(int i=0;i<[studentArrayCoreData count];i++)
        {
            
            [objSBTrackerWebservice getVehicleInfoForStudent:[[studentArrayCoreData objectAtIndex:i] admission_no]];
            
        }
        
        [objSBTrackerWebservice getVehicleInfoForStudent:@"21859/06"];

        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setTitle:@"School Bus Tracker"];

    
    vehicleInfoArray=[[NSMutableArray alloc] init];
    self.navigationController.navigationBarHidden=NO;
    [self.navigationItem setHidesBackButton:YES];
    //self.navigationItem.rightBarButtonItem = [self addRightBarButtonItem];
    self.navigationItem.leftBarButtonItem = [self addLeftBarButtonItem];
    
    // Do any additional setup after loading the view from its nib.
    [self.view setBackgroundColor:[UIColor blackColor]];
    
    
    UIScreen *mainScreen = [UIScreen mainScreen];
    CGFloat scale = ([mainScreen respondsToSelector:@selector(scale)] ? mainScreen.scale : 1.0f);
    CGFloat pixelHeight = (CGRectGetHeight(mainScreen.bounds) * scale);
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        if (scale == 2.0f)
        {
            if (pixelHeight == 960.0f)
            {
                tableviewObject=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, 320, 480-49) style:UITableViewStyleGrouped];
            }
            
            else if (pixelHeight == 1136.0f)
            {
                tableviewObject=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, 320, 568-49) style:UITableViewStyleGrouped];
            }
        }
    }
    
    [tableviewObject setBackgroundColor:[UIColor clearColor]];
    tableviewObject.separatorColor = [UIColor clearColor];
    tableviewObject.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    tableviewObject.delegate=self;
    tableviewObject.dataSource=self;
    [self.view addSubview:tableviewObject];
}

-(void) viewDidAppear:(BOOL)animated
{
    [ParseClass incrementView];
}

-(UIBarButtonItem *)addLeftBarButtonItem
{
    UIButton *backButton = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 30.0f, 30.0f)];
    UIImage *backImage = [UIImage imageNamed:@"back button icon.png"] ;
    [backButton setBackgroundImage:backImage  forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    return backButtonItem;
    
}
-(UIBarButtonItem *)addRightBarButtonItem
{
    
    
    UIButton *backButton = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 30.0f, 30.0f)];
    UIImage *backImage = [UIImage imageNamed:@"home icon.png"] ;
    [backButton setBackgroundImage:backImage  forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(settingButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    return backButtonItem;
    
}
-(void)backButtonClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)settingButtonClicked
{
    [self.navigationController popToViewController:objHomeViewController animated:YES];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Table View Datasource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	//tableviewObject = (UITableView *)tableView;
    return 75;
	
	
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
	return [vehicleInfoArray count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
	return 1;
    
    
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    SBTrackerCustomCell *cell = (SBTrackerCustomCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[SBTrackerCustomCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
		[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
   // NSLog(@"responseDictionary imei : %@",[NSString stringWithFormat:@"%@",[[[[vehicleInfoArray objectAtIndex:indexPath.section] valueForKey:@"list"] objectAtIndex:0] valueForKey:@"student_name"] ]);
//
    if([[[vehicleInfoArray objectAtIndex:indexPath.section] valueForKey:@"message"] isEqualToString:@"data retrived successfully"])
    {
  
    
     cell.labelName.text=[NSString stringWithFormat:@"%@",[[[[vehicleInfoArray objectAtIndex:indexPath.section] valueForKey:@"student_detail"] objectAtIndex:0] valueForKey:@"student_name"]];

    
    cell.labelVehicleNumber.text=[NSString stringWithFormat:@"Vehicle no.:%@",[[[[vehicleInfoArray objectAtIndex:indexPath.section] valueForKey:@"student_detail"] objectAtIndex:0] valueForKey:@"vehicle_number"] ];
    
    cell.labelVehicleImei.text=[NSString stringWithFormat:@"Description:%@",[[[[vehicleInfoArray objectAtIndex:indexPath.section] valueForKey:@"student_detail"] objectAtIndex:0] valueForKey:@"description"] ];
        
        if([[studentArrayCoreData objectAtIndex:indexPath.section] photo].length>0)
        {
            //NSData *imageData = [NSData dataFromBase64String:[[studentArrayCoreData objectAtIndex:indexPath.section] photo]];
           // cell.objImageViewStudent.image=[UIImage imageWithData:imageData];
        }

    }
    else
    {
        cell.labelName.text=[[studentArrayCoreData objectAtIndex:indexPath.section] local_name];
        cell.labelVehicleNumber.text=@"Vehicle no.: Not Available";
        cell.labelVehicleImei.text=@"IMEI: Not Available";
        if([[studentArrayCoreData objectAtIndex:indexPath.section] photo].length>0)
        {
            NSData *imageData = [NSData dataFromBase64String:[[studentArrayCoreData objectAtIndex:indexPath.section] photo]];
            cell.objImageViewStudent.image=[UIImage imageWithData:imageData];
        }

    }
    return cell;
}

#pragma mark Table View Delegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SBTrackerCustomCell *cell = (SBTrackerCustomCell *)[tableView cellForRowAtIndexPath:indexPath];
   
    if([cell.labelVehicleImei.text isEqualToString:@"Description: Not Available"])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"No information available" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    else
    {
        
        SBMapViewController *objSBMapViewController=[[SBMapViewController alloc]initWithNibName:@"SBMapViewController" bundle:nil];
     //   objSBMapViewController.imei_number=[[[[vehicleInfoArray objectAtIndex:indexPath.section] valueForKey:@"student_detail"] objectAtIndex:0] valueForKey:@"imei"];
        
        objSBMapViewController.pickupptsArray=[[vehicleInfoArray objectAtIndex:indexPath.section] valueForKey:@"pickup_points"];
        

        objSBMapViewController.imei_number=@"813071010848080";

        
        [self.navigationController pushViewController:objSBMapViewController animated:YES];

    }
    
}

#pragma mark TrackerWebservice delegate
-(void)serverResponseForSBTrackerWebservice:(NSMutableDictionary*)responseArray
{
    NSLog(@"ResponseArray: %@",responseArray);
    
     [vehicleInfoArray addObject:responseArray];
     [self.tableviewObject reloadData];
    
    NSLog(@"VehicleInfoArray:%@",vehicleInfoArray);
    
    vehicleInfoWithPickupArray=vehicleInfoArray;
   // NSLog(@"responseDictionary : %@",[[[vehicleInfoDict valueForKey:@"list"] objectAtIndex:0] valueForKey:@"student_name"]);
    
}
-(void)serverFailResponseForSBTrackerWebservice
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Server response failed" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];

}


@end
