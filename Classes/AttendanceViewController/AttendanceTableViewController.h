//
//  AttendanceTableViewController.h
//  ParentApp
//
//  Created by Redbytes on 08/01/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>


#import "DXSemiViewControllerCategory.h"
#import "DXSubClassSemiViewController.h"
#import "DXSubclassSemiTableViewController.h"
@protocol MenuButtonCalenderSingleViewDelegate <NSObject>

-(void)didSelectRowCalenderSingleView:(NSInteger)rowcount;

@end


@interface AttendanceTableViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    int sundayCount;
    id _delegate;
    DXSubclassSemiTableViewController *semiLeft;
}

@property (strong, nonatomic) IBOutlet UIView *baseViewTabbar;
@property (strong, nonatomic) IBOutlet UITableView *_tableView;
@property (weak, nonatomic) IBOutlet UIView *eventsCustomBottomView;

- (id)delegate;
- (void)setDelegate:(id)new_delegate;

@end
