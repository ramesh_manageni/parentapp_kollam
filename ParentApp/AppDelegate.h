//
//  AppDelegate.h
//  ParentApp
//
//  Created by Redbytes on 02/01/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoginViewController.h"
#import "MBProgressHUD.h"
//#import <GoogleMaps/GoogleMaps.h>
#import <Parse/Parse.h>

//AIzaSyCxqShJAFpLR3s_wi33A_IAc-bpJz8GKyc

@interface AppDelegate : UIResponder <UIApplicationDelegate,UITabBarControllerDelegate>
{
    NSMutableArray *notificationArray;
    int countForSliderMenu;
}

@property(strong,nonatomic)    MBProgressHUD *HUD;

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

@property(strong,nonatomic) LoginViewController *objLoginViewController;
@property(strong,nonatomic)UINavigationController *objNvc;
@property(strong,nonatomic)UITabBarController *objTabBarController;

@property(strong,nonatomic) NSString *tabbarURLString;


@end
