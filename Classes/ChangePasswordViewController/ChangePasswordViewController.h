//
//  ChangePasswordViewController.h
//  ParentApp
//
//  Created by Redbytes on 03/01/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChangePasswordWebservice.h"
#import "MBProgressHUD.h"
@interface ChangePasswordViewController : UIViewController<UITextViewDelegate,ChangePasswordDelegate,MBProgressHUDDelegate,UIAlertViewDelegate>
{
    MBProgressHUD *HUD;

}
@property (strong, nonatomic) IBOutlet UITextField *oldpasswordText;
@property (strong, nonatomic) IBOutlet UITextField *newpasswordText;
@property (strong, nonatomic) IBOutlet UITextField *confirmpasswordText;
@property (strong, nonatomic) IBOutlet UIButton *saveButton;
- (IBAction)saveButtonClicked:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *customNavBar;

@end
