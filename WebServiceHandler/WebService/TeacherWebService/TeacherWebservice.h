//
//  TeacherWebservice.h
//  ParentApp
//
//  Created by RedBytes on 23/06/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol TeacherDelegate <NSObject>

-(void)serverResponseForTeacherWebservice:(NSArray*)responseArray;
-(void)serverFailResponseForTeacherWebservice;
@end
@class ConnectionManager;

@interface TeacherWebservice : NSObject
{
    id _delegate;
    NSMutableDictionary *resultDictionary;
    ConnectionManager *objConnectionManager;
}
- (id)delegate;
- (void)setDelegate:(id)new_delegate;
-(void)showTeacherForUsername:(NSString *)username studentId:(NSString *)studentId;

@end
