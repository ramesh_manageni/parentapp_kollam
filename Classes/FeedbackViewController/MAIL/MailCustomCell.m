//
//  MailCustomCell.m
//  ParentApp
//
//  Created by RedBytes on 05/08/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//
#define kFontName @"BradleyHandITCTT-Bold"

#import "MailCustomCell.h"

@implementation MailCustomCell
@synthesize mailContentLabel,mailDateLabel,tickButton,mailTimeLabel,mailSubjectLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        tickButton = [[UIButton alloc] initWithFrame:CGRectMake(5,14,35,35)];
        [tickButton setBackgroundColor:[UIColor clearColor]];
        //tickButton.image = [UIImage imageNamed:@"unselected checkbox@2x.png"];
        [tickButton setImage:[UIImage imageNamed:@"check.png"] forState:UIControlStateNormal];
        //uncheck@2x.png,unselected checkbox@2x.png
        
        mailContentLabel =[[UILabel alloc]initWithFrame:CGRectMake(43,03,160,35)];
		[mailContentLabel setFont:[UIFont systemFontOfSize:17]];
		[mailContentLabel setBackgroundColor:[UIColor clearColor]];
        [mailContentLabel setTextColor:[UIColor blackColor]];
        [mailContentLabel setTextAlignment:NSTextAlignmentLeft];
       
        
        mailSubjectLabel =[[UILabel alloc]initWithFrame:CGRectMake(43,30,160,17)];
		[mailSubjectLabel setFont:[UIFont systemFontOfSize:14]];
		//[mailSubjectLabel setBackgroundColor:[UIColor clearColor]];
        [mailSubjectLabel setTextColor:[UIColor darkGrayColor]];
        [mailSubjectLabel setTextAlignment:NSTextAlignmentLeft];
        
        mailDateLabel =[[UILabel alloc]initWithFrame:CGRectMake(231,15,75,15)];
		[mailDateLabel setFont:[UIFont systemFontOfSize:12]];
		[mailDateLabel setBackgroundColor:[UIColor clearColor]];
        [mailDateLabel setTextColor:[UIColor lightGrayColor]];
        [mailDateLabel setTextAlignment:NSTextAlignmentLeft];
        
        mailTimeLabel =[[UILabel alloc]initWithFrame:CGRectMake(231,30,75,17)];
		[mailTimeLabel setFont:[UIFont systemFontOfSize:10]];
		[mailTimeLabel setBackgroundColor:[UIColor clearColor]];
        [mailTimeLabel setTextColor:[UIColor lightGrayColor]];
        [mailTimeLabel setTextAlignment:NSTextAlignmentLeft];
        
//        UIImageView* separatorLineView = [[UIImageView alloc] initWithFrame:CGRectMake(00,45,320, 4)];/// change size as you need.
//        separatorLineView.backgroundColor = [UIColor clearColor];
//        separatorLineView.image = [UIImage imageNamed:@"line@2x.png"];
        // you can also put image here
        //[cell.contentView addSubview:separatorLineView];
        
        //[self.contentView addSubview:separatorLineView];
        [self.contentView addSubview:tickButton];
        [self.contentView addSubview:mailContentLabel];
        [self.contentView addSubview:mailDateLabel];
        [self.contentView addSubview:mailTimeLabel];
        [self.contentView addSubview:mailSubjectLabel];

        
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
