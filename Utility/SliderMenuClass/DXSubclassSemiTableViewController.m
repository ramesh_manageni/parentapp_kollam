//
//  DXSubclassSemiTableViewController.m
//  DXSemiSideDemo
//
//  Created by 谢 凯伟 on 13-7-8.
//  Copyright (c) 2013年 谢 凯伟. All rights reserved.
//

#import "DXSubclassSemiTableViewController.h"

@interface DXSubclassSemiTableViewController ()

@end

@implementation DXSubclassSemiTableViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
   
    self.semiTableView.alpha = 1.0f;
    [self.semiTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.semiTableView setBackgroundColor:[UIColor clearColor]];
    for (int i = 0; i < 8; i++) {
        [self.dateSourceArray addObject:[NSString stringWithFormat:@"icon%d", i]];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dateSourceArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    static NSString *identifier = @"cellId";
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
//    if (!cell) {
//        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
//    }
//    cell.textLabel.text = self.dateSourceArray[indexPath.row];
    
    static NSString *CellIdentifier = @"Cell";
    
    SliderMenuCustomCell *cell = (SliderMenuCustomCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[SliderMenuCustomCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
		[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
        cell.objImageViewBG.image=[UIImage imageNamed:[NSString stringWithFormat:@"icon0%ld",(long)indexPath.row]];
       return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self dismissSemi:nil];
    if ([_delegate respondsToSelector:@selector(didSelectSliderMenuButton:)])
    {
        [_delegate didSelectSliderMenuButton:indexPath.row];
    }

   
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (id)delegate
{
	return  _delegate;
}

- (void)setDelegate:(id)new_delegate
{
	_delegate = new_delegate;
}

@end
