//
//  ResultWebservice.h
//  ParentApp
//
//  Created by Redbytes on 07/01/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ResultDelegate <NSObject>

-(void)serverResponseForResultWebSerivce:(NSArray*)responseArray;
-(void)serverFailResponseForResultWebSerivce;

@end
//********************************************************************

@class ConnectionManager;
@interface ResultWebservice : NSObject
{
    id _delegate;
    NSMutableDictionary *resultDictionary;
    ConnectionManager *objConnectionManager;
    
    NSString *currentdateFromUserdefaults;
    
    NSString *student_Id;
    NSUserDefaults *lastUpdaeDateForResults;
    NSString *currentDate;

}
- (id)delegate;
- (void)setDelegate:(id)new_delegate;
-(void)showResultForUsername:(NSString *)username studentId:(NSString *)studentId examId:(NSString *)examId ;
@end
