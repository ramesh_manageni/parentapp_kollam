//
//  readInboxMessage.m
//  ParentApp
//
//  Created by Redbytes on 22/08/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "readInboxMessage.h"

@implementation ReadInboxMessage
-(void)readInboxMessageWithUsername:(NSString *)username pmId:(NSString *)pmID;
{
    
    NSMutableDictionary *localDictionary=[[NSMutableDictionary alloc]init];
    
    
    //NSString *appVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    
    
    NSMutableString *strApplicationUUID=[[NSMutableString alloc]init];
    NSUUID *oNSUUID = [[UIDevice currentDevice] identifierForVendor];
    [strApplicationUUID setString:[oNSUUID UUIDString]];
    
    // [localDictionary setObject:strApplicationUUID forKey:@"device_id"];
    
    
	[localDictionary setObject:username forKey:@"username"];
	[localDictionary setObject:pmID forKey:@"pm_id"];
    //[localDictionary setObject:appVersion forKey:@"app_version"];
    
    
    NSLog(@"Dict: %@",localDictionary);
    
    if (objConnectionManager!=nil)
    {
		objConnectionManager=nil;
	}
	objConnectionManager=[[ConnectionManager alloc]init];
    // objConnectionManager.isThisUrl=@"http://schoolccesoftware.com/";
    
    objConnectionManager.isReadInboxMessageWS=@"YES";
    [objConnectionManager getWebData:self commandName:@"phorum/readFromDevice" Requestparameter:localDictionary];
    
    //http://schoolccesoftware.com/oxforddemo/login/Phorum/Inbox
}
#pragma mark connection manager delegate
- (void)receivedData:(NSArray *)responseArray
{
    if ([_delegate respondsToSelector:@selector(serverResponseForReadInboxMessage:)])
    {
        [_delegate serverResponseForReadInboxMessage:responseArray];
    }
    else
    {
        [NSException raise:NSInternalInconsistencyException format:@"Delegate doesn't respond to serverResponseForCaptchaWebSerivce:"];
    }
}
-(void)failToReciveData
{
    if ([_delegate respondsToSelector:@selector(serverFailResponseForIReadInboxMessage)])
    {
        [_delegate serverFailResponseForIReadInboxMessage];
    }
}
#pragma mark - Delegate methods
- (id)delegate
{
	return  _delegate;
}

- (void)setDelegate:(id)new_delegate
{
	_delegate = new_delegate;
}




@end
