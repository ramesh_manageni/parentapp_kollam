//
//  SentItemWebservices.m
//  ParentApp
//
//  Created by Redbytes on 11/09/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "SentItemWebservices.h"
#import "ConnectionManager.h"

@implementation SentItemWebservices

-(void)SentItemMethodForUsername:(NSString *)username
{
    NSMutableDictionary *localDictionary=[[NSMutableDictionary alloc]init];
    
    NSMutableString *strApplicationUUID=[[NSMutableString alloc]init];
    NSUUID *oNSUUID = [[UIDevice currentDevice] identifierForVendor];
    [strApplicationUUID setString:[oNSUUID UUIDString]];
    
    
    
	[localDictionary setObject:username forKey:@"username"];
    [localDictionary setObject:institute_id forKey:@"institute_id"];

    
    NSLog(@"Dict: %@",localDictionary);
    
    if (objConnectionManager!=nil)
    {
		objConnectionManager=nil;
	}
	objConnectionManager=[[ConnectionManager alloc]init];
    
    objConnectionManager.isSentWS=@"YES";
    [objConnectionManager getWebData:self commandName:@"phorum/SendItems" Requestparameter:localDictionary];
}
    
#pragma mark connection manager delegate
- (void)receivedData:(NSArray *)responseArray
{
    if ([_delegate respondsToSelector:@selector(serverResponseForSentItemWebservice:)])
        {
            [_delegate serverResponseForSentItemWebservice:responseArray];
        }
    else
        {
            [NSException raise:NSInternalInconsistencyException format:@"Delegate doesn't respond to serverResponseForCaptchaWebSerivce:"];
        }
}
-(void)failToReciveData
{
    if ([_delegate respondsToSelector:@selector(serverFailResponseForSentItemwebservice)])
        {
            [_delegate serverFailResponseForSentItemwebservice];
        }
}

#pragma mark - Delegate methods
-(id)delegate
{
    return  _delegate;
}
-(void)setDelegate:(id)new_delegate
{
    _delegate = new_delegate;
}


@end

