//
//  NoticeCountWebservice.m
//  ParentApp
//
//  Created by Imtiyaz on 8/11/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "NoticeCountWebservice.h"
#import "ConnectionManager.h"

@implementation NoticeCountWebservice

-(void)GetNoticeCountForProfileId:(NSString *)profileId
{
    NSMutableDictionary *localDictionary=[[NSMutableDictionary alloc]init];
    
    [localDictionary setObject:profileId forKey:@"profile_id"];
    
    NSLog(@"Dict: %@",localDictionary);
    
    if (objConnectionManager!=nil)
    {
		objConnectionManager=nil;
	}
	objConnectionManager=[[ConnectionManager alloc]init];
    //  objConnectionManager.isSBTracker=@"YES";
    objConnectionManager.isNoticeCountWS=@"YES";
    [objConnectionManager getWebData:self commandName:@"circular/NoticeCount" Requestparameter:localDictionary];
}

#pragma mark connection manager delegate
- (void)receivedData:(NSArray *)responseArray
{
    if ([_delegate respondsToSelector:@selector(serverResponseForNoticeCountWebservice:)])
    {
        [_delegate serverResponseForNoticeCountWebservice:(NSMutableDictionary *)responseArray];
    }
    else
    {
        [NSException raise:NSInternalInconsistencyException format:@"Delegate doesn't respond to serverResponseForCaptchaWebSerivce:"];
    }
}
-(void)failToReciveData
{
    if ([_delegate respondsToSelector:@selector(serverFailResponseForNoticeCountWebservice)])
    {
        [_delegate serverFailResponseForNoticeCountWebservice];
    }
}
#pragma mark - Delegate methods
- (id)delegate
{
	return  _delegate;
}

- (void)setDelegate:(id)new_delegate
{
	_delegate = new_delegate;
}


@end
