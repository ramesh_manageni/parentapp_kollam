//
//  NoticeReadfromDeviceWebservice.h
//  ParentApp
//
//  Created by Imtiyaz on 8/12/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ConnectionManager.h"


@protocol NoticeReadfromDeviceDelegate <NSObject>

-(void)serverResponseForNoticeReadfromDeviceWebservice:(NSMutableDictionary*)responseArray;
-(void)serverFailResponseForNoticeReadfromDeviceWebservice;

@end
//********************************************************************

@class ConnectionManager;
@interface NoticeReadfromDeviceWebservice : NSObject
{
    id _delegate;
    NSMutableDictionary *resultDictionary;
    ConnectionManager *objConnectionManager;
}
- (id)delegate;
- (void)setDelegate:(id)new_delegate;
-(void)GetNoticeReadForProfileId:(NSString *)profileId circular_id:(NSString *)circular_id;

@end
