//
//  SBMapViewController.m
//  ParentApp
//
//  Created by Redbytes on 22/01/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "SBMapViewController.h"
#import "Constant.h"
@interface SBMapViewController ()

@end

@implementation SBMapViewController

@synthesize imei_number;
@synthesize annotationArray;
@synthesize pickupptsArray;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization

          }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setTitle:@"School Bus Tracker"];

    
    
    NSLog(@"pickup points: %@",pickupptsArray);
    
    SBMapWebservice *objSBMapWebservice=[[SBMapWebservice alloc]init];
    objSBMapWebservice.delegate=self;
    [objSBMapWebservice getMapDetailsForIMEI:imei_number];
    
    self.navigationController.navigationBarHidden=NO;
    [self.navigationItem setHidesBackButton:YES];
    //self.navigationItem.rightBarButtonItem = [self addRightBarButtonItem];
    self.navigationItem.leftBarButtonItem = [self addLeftBarButtonItem];
    self.navigationItem.rightBarButtonItem = [self addRightBarButtonItem];
    
    // Do any additional setup after loading the view from its nib.
    [self.view setBackgroundColor:[UIColor blackColor]];
    

}

- (void)loadView
{
    clLocManObj = [[CLLocationManager alloc]init];
    if([CLLocationManager locationServicesEnabled])
    {
        clLocManObj.delegate = self;
        [clLocManObj startUpdatingLocation];
    }
    // Create a GMSCameraPosition that tells the map to display the
    // coordinate 21.250000, 79.150000 at zoom level 12.
    //camera = [GMSCameraPosition cameraWithLatitude:21.250000 longitude:79.150000 zoom:12];
    
    camera=[GMSCameraPosition cameraWithLatitude:21.250000 longitude:79.150000 zoom:12];

    mapView = [GMSMapView mapWithFrame:CGRectZero camera:camera];
    mapView.settings.compassButton = YES;
    mapView.settings.myLocationButton = YES;
    mapView.myLocationEnabled = YES;
    mapView.delegate = self;
    NSLog(@"%f---%f",mapView.myLocation.coordinate.latitude,mapView.myLocation.coordinate.longitude);
    marker = [[GMSMarker alloc] init];
    marker.position = CLLocationCoordinate2DMake(cords2.latitude, cords2.longitude);
    NSLog(@"%f---%f",cords2.latitude,cords2.longitude);
    marker.title = @"Nagpur";
    marker.snippet = @"India";
    marker.appearAnimation = kGMSMarkerAnimationPop;
    //marker.map = mapView;
    
    circle = [GMSCircle circleWithPosition:CLLocationCoordinate2DMake(21.250000, 79.150000) radius:1000];
    circle.fillColor = [UIColor colorWithHue:0.625 saturation:0.0 brightness:1 alpha:0.5];
    circle.strokeColor = [UIColor darkGrayColor];
    circle.strokeWidth = 5;
   // circle.map = mapView;
    
        
    self.view = mapView;
    
     

}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    loc1 = [[CLLocation alloc]initWithLatitude:oldLocation.coordinate.latitude longitude:oldLocation.coordinate.longitude];
    loc2 = [[CLLocation alloc]initWithLatitude:newLocation.coordinate.latitude longitude:newLocation.coordinate.longitude];
    
    cords1.latitude = oldLocation.coordinate.latitude;
    cords1.longitude = oldLocation.coordinate.longitude;
    
    cords2.latitude = newLocation.coordinate.latitude;
    cords2.longitude = newLocation.coordinate.longitude;
}
-(UIBarButtonItem *)addLeftBarButtonItem
{
    UIButton *backButton = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 30.0f, 30.0f)];
    UIImage *backImage = [UIImage imageNamed:@"back button icon.png"] ;
    [backButton setBackgroundImage:backImage  forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    return backButtonItem;
    
}
-(UIBarButtonItem *)addRightBarButtonItem
{
    
    
    UIButton *backButton = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 30.0f, 30.0f)];
    UIImage *backImage = [UIImage imageNamed:@"home icon.png"] ;
    [backButton setBackgroundImage:backImage  forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(settingButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    return backButtonItem;
    
}
-(void)backButtonClicked
{
    [self.navigationController popViewControllerAnimated:YES];
//    
//    NSArray *arr=[self.navigationController viewControllers];
//    [UIView transitionWithView:self.view.window                     duration:1.0f
//                       options:UIViewAnimationOptionTransitionFlipFromLeft
//                    animations:^{
//                        [self.navigationController popViewControllerAnimated:NO];
//                    }
//                    completion:NULL];
}
-(void)settingButtonClicked
{
    [self.navigationController popToViewController:objHomeViewController animated:YES];
    
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark SBMapWebservice delegate

-(void)serverResponseForSBMapWebservice:(NSMutableDictionary *)responseArray
{
    NSLog(@"RESPONSE : %@",responseArray);
    
    latArray=[[NSMutableArray alloc]init];
    longArray=[[NSMutableArray alloc]init];
    
    annotationArray=[[NSMutableArray alloc]init];
    annotationArray=[responseArray valueForKey:@"list"];
    
    objpath = [GMSMutablePath path];
    GMSMutablePath *objpath1 = [GMSMutablePath path];

    
    [mapView animateToLocation:CLLocationCoordinate2DMake([[responseArray valueForKey:@"dest_lat"] doubleValue], [[responseArray valueForKey:@"dest_long"] doubleValue])];
    [mapView animateToZoom:9];
    loc1 = [[CLLocation alloc]initWithLatitude:[[responseArray valueForKey:@"dest_lat"] doubleValue] longitude:[[responseArray valueForKey:@"dest_long"] doubleValue]];
    marker1 = [[GMSMarker alloc] init];
    marker1.position = CLLocationCoordinate2DMake([[responseArray valueForKey:@"dest_lat"] doubleValue], [[responseArray valueForKey:@"dest_long"] doubleValue]);
    marker1.title = [responseArray  valueForKey:@"destination"];
    //marker1.snippet = @"India";
    marker1.appearAnimation = kGMSMarkerAnimationPop;
    marker1.icon=[GMSMarker markerImageWithColor:[UIColor redColor]];
    marker1.map = mapView;
    
  

    for(int i=0;i<[annotationArray count];i++)
    {
    
        
        
        [mapView animateToLocation:CLLocationCoordinate2DMake([[[annotationArray objectAtIndex:i] valueForKey:@"loc_latitude"] doubleValue], [[[annotationArray objectAtIndex:i] valueForKey:@"loc_longitude"] doubleValue])];
        [mapView animateToZoom:7];

        loc1 = [[CLLocation alloc]initWithLatitude:[[[annotationArray objectAtIndex:i] valueForKey:@"loc_latitude"] doubleValue] longitude:[[[annotationArray objectAtIndex:i] valueForKey:@"loc_longitude"] doubleValue]];
        marker1 = [[GMSMarker alloc] init];
        marker1.position = CLLocationCoordinate2DMake([[[annotationArray objectAtIndex:i] valueForKey:@"loc_latitude"] doubleValue], [[[annotationArray objectAtIndex:i] valueForKey:@"loc_longitude"] doubleValue]);
        marker1.title = [[annotationArray objectAtIndex:i] valueForKey:@"calc_current_address"];
        marker1.snippet =[NSString stringWithFormat:@"Time:%@",[[annotationArray objectAtIndex:i] valueForKey:@"loc_current_time"]];
        marker1.appearAnimation = kGMSMarkerAnimationPop;
        if(i==0)
        {
            marker1.icon=[GMSMarker markerImageWithColor:[UIColor greenColor]];

        }
        else
        {
            marker1.icon=[GMSMarker markerImageWithColor:[UIColor blueColor]];

        }
        marker1.map = mapView;


        [objpath addLatitude:[[[annotationArray objectAtIndex:i] valueForKey:@"loc_latitude"] doubleValue] longitude:[[[annotationArray objectAtIndex:i] valueForKey:@"loc_longitude"] doubleValue]];
        
        if(i==[annotationArray count]-1)
        {
            [objpath1 addLatitude:[[[annotationArray objectAtIndex:i] valueForKey:@"loc_latitude"] doubleValue] longitude:[[[annotationArray objectAtIndex:i] valueForKey:@"loc_longitude"] doubleValue]];
            
        }
        [latArray addObject:[[annotationArray objectAtIndex:i] valueForKey:@"loc_latitude"] ];
        [longArray addObject:[[annotationArray objectAtIndex:i] valueForKey:@"loc_longitude"]];

       GMSPolyline *polyline = [GMSPolyline polylineWithPath:objpath];
        polyline.strokeColor = [UIColor blueColor];
        polyline.strokeWidth = 5.f;
        polyline.map = mapView;
        //objpath=nil;
       // polyline=nil;


    }

    [objpath1 addLatitude:[[responseArray valueForKey:@"dest_lat"] doubleValue] longitude:[[responseArray valueForKey:@"dest_long"] doubleValue]];

    GMSPolyline *polyline1 = [GMSPolyline polylineWithPath:objpath1];
    polyline1.strokeColor = [UIColor redColor];
    polyline1.strokeWidth = 5.f;
    polyline1.map = mapView;
    
    
    [latArray addObject:[responseArray valueForKey:@"dest_lat"] ];
    [longArray addObject:[responseArray valueForKey:@"dest_long"]];
   // [self drawLine];

}
-(void)drawLine
{
    int color=25;

    for(int i=0;i<[longArray count];i++)
    {
        GMSMutablePath  *objpathNew = [GMSMutablePath path];
if(i<[longArray count]-1)
{
        [objpathNew addLatitude:[[latArray objectAtIndex:i] doubleValue] longitude:[[longArray objectAtIndex:i] doubleValue]];
        [objpathNew addLatitude:[[latArray objectAtIndex:i+1] doubleValue] longitude:[[longArray objectAtIndex:i+1] doubleValue]];
}

        GMSPolyline *polyline1 = [GMSPolyline polylineWithPath:objpathNew];
        color=color*2;
        if(color>250)
        {
            color=25;
        }
        NSLog(@"COLOR : %d",color);
        polyline1.strokeColor = [UIColor colorWithRed:(CGFloat)color green:0 blue:0  alpha:1.0];
     
        //  [[NSString stringWithFormat:@"%d55",i] floatValue]
       
        polyline1.strokeWidth = 5.f;
        polyline1.map = mapView;
        objpathNew=nil;
        polyline1=nil;
    }
   

    
    
    

}
-(void)serverFailResponseForSBMapWebservice
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Server response failed" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
    
}
#pragma mark MapView Delegate
- (MKAnnotationView *)mapView:(MKMapView *)map viewForAnnotation:(id <MKAnnotation>)annotation
{
    
    if (annotation == map.userLocation)
        return nil;
    
    MKPinAnnotationView *pin = (MKPinAnnotationView *) [map dequeueReusableAnnotationViewWithIdentifier: @"asdf"];
    
    if (pin == nil)
        pin = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier: @"asdf"];
    else
        pin.annotation = annotation;
    pin.userInteractionEnabled = YES;
    pin.pinColor = MKPinAnnotationColorRed;
    pin.animatesDrop = YES;
    [pin setEnabled:YES];
    [pin setCanShowCallout:YES];
    UIButton *buttonForPin = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    
   // [buttonForPin addTarget:self action:@selector(btnclicked) forControlEvents:UIControlEventTouchUpInside];
    pin.rightCalloutAccessoryView = buttonForPin;
    
    
    return pin;
}
-(MKOverlayView *)mapView:(MKMapView *)mapView viewForOverlay:(id <MKOverlay>)overlay{
    if ([overlay isKindOfClass:[MKPolyline class]])
    {
        MKPolylineView *lineview=[[MKPolylineView alloc] initWithOverlay:overlay] ;
        lineview.strokeColor=[[UIColor blueColor] colorWithAlphaComponent:0.5];
        lineview.lineWidth=2.0;
        return lineview;
    }
    return nil;
}
@end
