//
//  FeeViewController.m
//  ParentApp
//
//  Created by Redbytes on 03/01/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "FeeViewController.h"
#import "ChangePasswordViewController.h"
#import "Constant.h"
#import "FeeDetails.h"
#import "StudentDetails.h"
#import "AppDelegate.h"
#import "NSData+Base64.h"
@interface FeeViewController ()

@end

@implementation FeeViewController
@synthesize index;
@synthesize student_id,username,student_name;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self  setTitle:@"Fee Details"];
    self.studentName.text=student_name;
    self.navigationController.navigationBarHidden=NO;
    [self.navigationItem setHidesBackButton:YES];
    self.navigationItem.rightBarButtonItem = [self addRightBarButtonItem];
    self.navigationItem.leftBarButtonItem = [self addLeftBarButtonItem];
    
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
	[self.view addSubview:HUD];
	
	// Regiser for HUD callbacks so we can remove it from the window at the right time
	HUD.delegate = self;
	
	// Show the HUD while the provided method executes in a new thread
	//[HUD showWhileExecuting:@selector(myTask) onTarget:self withObject:nil animated:YES];
    [HUD show:YES];

    
    FeeWebservice *objFeeWebservice=[[FeeWebservice alloc]init];
    objFeeWebservice.delegate=self;
    [objFeeWebservice showFeesForUsername:username studentId:student_id];
    
    NSLog(@"UserName: %@ Id :%@",username,student_id);
    
    // Do any additional setup after loading the view from its nib.
}

-(void) viewDidAppear:(BOOL)animated
{
    [ParseClass incrementView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(UIBarButtonItem *)addLeftBarButtonItem
{
    UIButton *backButton = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 30.0f, 30.0f)];
    UIImage *backImage = [UIImage imageNamed:@"back button icon.png"] ;
    [backButton setBackgroundImage:backImage  forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    return backButtonItem;
    
}
-(UIBarButtonItem *)addRightBarButtonItem
{
    
    
    UIButton *backButton = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 30.0f, 30.0f)];
    UIImage *backImage = [UIImage imageNamed:@"list button icon.png"] ;
    [backButton setBackgroundImage:backImage  forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(settingButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    return backButtonItem;
    
}
-(void)backButtonClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)settingButtonClicked
{
    
    
    if(semiLeft!=nil)
    {
        [semiLeft dismissSemi:nil];
    }

    //[self.navigationController popToViewController:objHomeViewController animated:YES];
    semiLeft = [[DXSubclassSemiTableViewController alloc] init];
    semiLeft.delegate=self;
    
    semiLeft.semiTitleLabel.text = @"SemiLeftTableView";
    semiLeft.tableViewRowHeight = 80.0f/2;
    [semiLeft.view setBackgroundColor:[UIColor clearColor]];
    self.leftSemiViewController = semiLeft;
    
    
}
#pragma mark FeeWebservise Delegates
-(void)serverResponseForFeeWebservice:(NSArray*)responseArray
{
    NSLog(@"%@",responseArray);
    [HUD hide:YES];
    if([responseArray isKindOfClass:[NSArray class]])
    {
        [self loadData:responseArray];
        [self getEntityCount:@"StudentDetails"];
        [self updateFeeTable];
    }
    else if(![[responseArray valueForKey:@"success"] intValue])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Fee not published yet" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
}
-(void)serverFailResponseForFeeWebservice
{
    [HUD hide:YES];

   // UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Server response failed\nAccessing local data" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
   // [alert show];

    [self getEntityCount:@"StudentDetails"];
    if([feeDetailsArray count]>0)
    {
     [self updateFeeTable];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Local database is empty" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
        
    


}
-(void)updateFeeTable
{

    self.labelActFee.text=[[feeDetailsArray objectAtIndex:0] act];
    self.labelAdminFee.text=[[feeDetailsArray objectAtIndex:0] adm];
    self.labelBusFee.text=[[feeDetailsArray objectAtIndex:0] bus];
    self.labelDuedate.text=[NSString stringWithFormat:@"Due date : %@",[[feeDetailsArray objectAtIndex:0] due_date]];
    self.labelTutionFee.text=[[feeDetailsArray objectAtIndex:0] tut];
    
    int act=[self.labelActFee.text intValue];
    int adm=[self.labelAdminFee.text intValue];
    int bus=[self.labelBusFee.text intValue];
    int tut=[self.labelTutionFee.text intValue];
    
    int total=act+adm+bus+tut;
    int dueAmount = [[[feeDetailsArray objectAtIndex:0] due_amount] intValue];
    
    
    //NSLog(@"%d",total);
    //NSLog(@"%d",dueAmount);
    
    self.labelOtherFee.text= [NSString stringWithFormat:@"%d.00", dueAmount-total];
    self.labelTotalFee.text=[[feeDetailsArray objectAtIndex:0] due_amount];
    
    
    
}

- (void) deleteAllObjects: (NSString *) entityDescription
{
    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [delegate managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityDescription inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    NSError *error;
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    
    
    for (NSManagedObject *managedObject in items) {
    	[context deleteObject:managedObject];
    	NSLog(@"%@ object deleted",entityDescription);
    }
    if (![context save:&error]) {
    	NSLog(@"Error deleting %@ - error:%@",entityDescription,error);
    }
    
}
-(void)getEntityCount:(NSString *)entityName
{
    entityCount = 0;
    feeDetailsArray=[[NSMutableArray alloc] init];
    
    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [delegate managedObjectContext];
    
    NSFetchRequest *fetchReq = [[NSFetchRequest alloc]init];
    
    [fetchReq setEntity:[NSEntityDescription entityForName:entityName inManagedObjectContext:context]];
    NSError *error;
    
    NSMutableArray *resultArray = [[NSMutableArray alloc]initWithArray:[context executeFetchRequest:fetchReq error:&error]];
    NSLog(@"Result Array: %@",resultArray);
    if(error==nil)
    {
        NSSet *set;

        for(StudentDetails *stddetails in resultArray)
        {
            if([stddetails.id_student isEqualToString:student_id])
            {
                set=stddetails.studentToFee;
                for(FeeDetails *fee in [set allObjects])
                {
                     NSLog(@"Fee details %@",fee.student_id);
                    [feeDetailsArray addObject:fee];

                }
                
            }

            
        }
        
    }
    
//    if(error==nil)
//    {
//        NSSet *arr;
//        for(StudentDetails *stdobject in resultArray)
//        {
//            
//            if([stdobject.id_student isEqualToString:stdId])
//            {
//                arr=stdobject.studentToAttendance;
//                NSLog(@"SET = %@",[arr allObjects]);
//                
//                for(AttendanceDetail *dates in [arr allObjects])
//                {
//                    NSMutableDictionary *tempDict=[[NSMutableDictionary alloc]init];
//                    [tempDict setObject:dates.date_absent forKey:@"date_absent"];
//                    [datesArrayCoreData addObject:tempDict];
//                    [datesArrayForCalender addObject:dates.date_absent];
//                    NSLog(@"Dates: %@",dates.date_absent);
//                }
//                absentDatesArrayCoreData=datesArrayForCalender;
//                NSLog(@"%@ %d std_id:%@",datesArrayForCalender,datesArrayForCalender.count,stdId);
//                break;
//            }
    
}
-(void)loadData:response
{
    
    StudentDetails *std=[self getStudentEntityWithId];

    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [delegate managedObjectContext];
    
    FeeDetails *feedetail = [NSEntityDescription insertNewObjectForEntityForName:@"FeeDetails" inManagedObjectContext:context];
   // StudentDetails *std = [NSEntityDescription insertNewObjectForEntityForName:@"StudentDetails" inManagedObjectContext:context];
    
    if([[response objectAtIndex:0] valueForKey:@"act"]!=(NSString *)[NSNull null])
    {
        feedetail.act=[[response objectAtIndex:0] valueForKey:@"act"];
    }
    
    if([[response objectAtIndex:0] valueForKey:@"adm"]!=(NSString *)[NSNull null])
    {
        feedetail.adm=[[response objectAtIndex:0] valueForKey:@"adm"];
    }
    
    if([[response objectAtIndex:0] valueForKey:@"bus"]!=(NSString *)[NSNull null])
    {
        feedetail.bus=[[response objectAtIndex:0] valueForKey:@"bus"];
    }
    
    if([[response objectAtIndex:0] valueForKey:@"due_amount"]!=(NSString *)[NSNull null])
    {
        feedetail.due_amount=[[response objectAtIndex:0] valueForKey:@"due_amount"];
    }
    
    if([[response objectAtIndex:0] valueForKey:@"due_date"]!=(NSString *)[NSNull null])
    {
        feedetail.due_date=[[response objectAtIndex:0] valueForKey:@"due_date"];
    }
    
    if([[response objectAtIndex:0] valueForKey:@"hos"]!=(NSString *)[NSNull null])
    {
        feedetail.hos=[[response objectAtIndex:0] valueForKey:@"hos"];
    }
    
    if([[response objectAtIndex:0] valueForKey:@"id"]!=(NSString *)[NSNull null])
    {
        feedetail.id_fee=[[response objectAtIndex:0] valueForKey:@"id"];
    }
    
    if([[response objectAtIndex:0] valueForKey:@"student_id"]!=(NSString *)[NSNull null])
    {
        feedetail.student_id=[[response objectAtIndex:0] valueForKey:@"student_id"];
    }
    
    if([[response objectAtIndex:0] valueForKey:@"tc"]!=(NSString *)[NSNull null])
    {
        feedetail.tc=[[response objectAtIndex:0] valueForKey:@"tc"];
    }
    
    if([[response objectAtIndex:0] valueForKey:@"tut"]!=(NSString *)[NSNull null])
    {
        feedetail.tut=[[response objectAtIndex:0] valueForKey:@"tut"];
    }
    [std addStudentToFeeObject:feedetail];
    

    
    NSError *err;
    
    if( ! [context save:&err] ){
        NSLog(@"Cannot save data: %@", [err localizedDescription]);
    }
    
}
-(StudentDetails *)getStudentEntityWithId
{
    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [delegate managedObjectContext];
    
    NSFetchRequest *fetchReq = [[NSFetchRequest alloc]init];
    
    [fetchReq setEntity:[NSEntityDescription entityForName:@"StudentDetails" inManagedObjectContext:context]];
    NSError *error;
    
    NSMutableArray *resultArray = [[NSMutableArray alloc]initWithArray:[context executeFetchRequest:fetchReq error:&error]];
    if(error==nil)
    {
        for(StudentDetails *stdDetails in resultArray)
        {
            if([stdDetails.id_student isEqualToString:student_id])
            {
                [stdDetails removeStudentToFee:stdDetails.studentToFee];
                return stdDetails;
            }
        }
        
    }
    
    return nil;
}

#pragma mark SliderMenuDelegate
-(void)didSelectSliderMenuButton:(NSInteger)rowcount
{
    // NSLog(@"ROWCOUNT=== %d",rowcount);
    if(rowcount==0)
    {
        [self.navigationController popToViewController:objHomeViewController animated:YES];
        
    }
    else
    {
        [self.navigationController popToViewController:objHomeViewController animated:NO];
        
    }
    if ([_delegate respondsToSelector:@selector(didSelectRowFeedback:)])
    {
        [_delegate didSelectRowFeedback:rowcount];
    }
    
}

#pragma mark Delegate

- (id)delegate
{
	return  _delegate;
}

- (void)setDelegate:(id)new_delegate
{
	_delegate = new_delegate;
}


@end
