//
//  StudentDetails.h
//  ParentApp
//
//  Created by Redbytes on 27/08/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class AttendanceDetail, FeeDetails, Parent, StudentResult, TeacherDetails;

@interface StudentDetails : NSManagedObject

@property (nonatomic, retain) NSString * admission_no;
@property (nonatomic, retain) NSString * due_amount;
@property (nonatomic, retain) NSString * due_date;
@property (nonatomic, retain) NSString * id_student;
@property (nonatomic, retain) NSString * local_name;
@property (nonatomic, retain) NSString * photo;
@property (nonatomic, retain) Parent *childToParent;
@property (nonatomic, retain) NSSet *studentToAttendance;
@property (nonatomic, retain) NSSet *studentToFee;
@property (nonatomic, retain) NSSet *studentToResult;
@property (nonatomic, retain) NSSet *studentToTeacher;
@end

@interface StudentDetails (CoreDataGeneratedAccessors)

- (void)addStudentToAttendanceObject:(AttendanceDetail *)value;
- (void)removeStudentToAttendanceObject:(AttendanceDetail *)value;
- (void)addStudentToAttendance:(NSSet *)values;
- (void)removeStudentToAttendance:(NSSet *)values;

- (void)addStudentToFeeObject:(FeeDetails *)value;
- (void)removeStudentToFeeObject:(FeeDetails *)value;
- (void)addStudentToFee:(NSSet *)values;
- (void)removeStudentToFee:(NSSet *)values;

- (void)addStudentToResultObject:(StudentResult *)value;
- (void)removeStudentToResultObject:(StudentResult *)value;
- (void)addStudentToResult:(NSSet *)values;
- (void)removeStudentToResult:(NSSet *)values;

- (void)addStudentToTeacherObject:(TeacherDetails *)value;
- (void)removeStudentToTeacherObject:(TeacherDetails *)value;
- (void)addStudentToTeacher:(NSSet *)values;
- (void)removeStudentToTeacher:(NSSet *)values;

@end
