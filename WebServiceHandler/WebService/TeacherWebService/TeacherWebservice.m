//
//  TeacherWebservice.m
//  ParentApp
//
//  Created by RedBytes on 23/06/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "TeacherWebservice.h"
#import "ConnectionManager.h"

@implementation TeacherWebservice

-(id)init
{
	return self;
}

-(void)showTeacherForUsername:(NSString *)username studentId:(NSString *)studentId
{
    NSMutableDictionary *localDictionary=[[NSMutableDictionary alloc]init];
    
    [localDictionary setObject:username forKey:@"username"];
    [localDictionary setObject:studentId forKey:@"student_id"];
    NSLog(@"Dict: %@",localDictionary);
    
    if (objConnectionManager!=nil)
    {
		objConnectionManager=nil;
	}
	objConnectionManager=[[ConnectionManager alloc]init];
    
    [objConnectionManager getWebData:self commandName:@"wssubteachrs" Requestparameter:localDictionary];
}

#pragma mark connection manager delegate
- (void)receivedData:(NSArray *)responseArray
{
    if ([_delegate respondsToSelector:@selector(serverResponseForTeacherWebservice:)])
    {
        [_delegate serverResponseForTeacherWebservice:responseArray];
    }
    else
    {
        [NSException raise:NSInternalInconsistencyException format:@"Delegate doesn't respond to serverResponseForCaptchaWebSerivce:"];
    }
}
-(void)failToReciveData
{
    if ([_delegate respondsToSelector:@selector(serverFailResponseForTeacherWebservice)])
    {
        [_delegate serverFailResponseForTeacherWebservice];
    }
}

#pragma mark - Delegate methods
- (id)delegate
{
	return _delegate;
}

- (void)setDelegate:(id)new_delegate
{
	_delegate = new_delegate;
}

@end
