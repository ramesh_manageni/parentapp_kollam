//
//  NoticeCountWebservice.h
//  ParentApp
//
//  Created by Imtiyaz on 8/11/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol NoticeCountDelegate <NSObject>

-(void)serverResponseForNoticeCountWebservice:(NSMutableDictionary*)responseArray;
-(void)serverFailResponseForNoticeCountWebservice;

@end
//********************************************************************

@class ConnectionManager;


@interface NoticeCountWebservice : NSObject
{
    id _delegate;
    NSMutableDictionary *resultDictionary;
    ConnectionManager *objConnectionManager;
}
- (id)delegate;
- (void)setDelegate:(id)new_delegate;
-(void)GetNoticeCountForProfileId:(NSString *)profileId;

@end
