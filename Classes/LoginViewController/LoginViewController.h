//
//  LoginViewController.h
//  ParentApp
//
//  Created by Redbytes on 02/01/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MenuViewController.h"
#import "LoginWebservice.h"
#import "Constant.h"
#import "MBProgressHUD.h"
#import "RegistrationWebservice.h"
#import "ForgotPasswordWebservice.h"
#import <MediaPlayer/MediaPlayer.h>

@interface LoginViewController : UIViewController<UITextFieldDelegate,LoginDelegate,MBProgressHUDDelegate,RegistrationWebserviceDelegate,ForgotPasswordDelegate,UIAlertViewDelegate>
{
    IBOutlet UILabel *forgotPasswordLabel;
    
    IBOutlet UITextField *objTextPassword;
    IBOutlet UITextField *objTextId;
    IBOutlet UIScrollView *objScrollView;
    
    MenuViewController *objMenuViewController;
    MBProgressHUD *HUD;
    
    NSArray *fetchedObjectsFromCoredata;
    
    NSMutableArray *studentArray;
    NSMutableArray *parentArray;
    
    MPMoviePlayerController *moviePlayer;
	NSURL *movieURL;
   // int i;
    int entityCount;

}
@property (strong, nonatomic) IBOutlet UIImageView *imageViewEdsys;
- (IBAction)cancelButtonClicked:(id)sender;
- (IBAction)submitButtonClicked:(id)sender;
- (void)forgotPassButtonClicked;

- (IBAction)videoButtonClicked:(id)sender;

@end
