//
//  NoticeBoardWebservice.m
//  ParentApp
//
//  Created by Redbytes on 24/01/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "NoticeBoardWebservice.h"
#import "ConnectionManager.h"

@implementation NoticeBoardWebservice
-(id)init
{
	return self;
}

-(void)GetNoticeForUsername:(NSString *)username profileId:(NSString *)profileId
{
    NSMutableDictionary *localDictionary=[[NSMutableDictionary alloc]init];
   
    
    NSMutableString *strApplicationUUID=[[NSMutableString alloc]init];
    NSUUID *oNSUUID = [[UIDevice currentDevice] identifierForVendor];
    [strApplicationUUID setString:[oNSUUID UUIDString]];
    [localDictionary setObject:strApplicationUUID forKey:@"device_id"];

    [localDictionary setObject:username forKey:@"username"];
   // [localDictionary setObject:profileId forKey:@"profile_id"];
    
    NSLog(@"Dict: %@",localDictionary);
    
    if (objConnectionManager!=nil)
    {
		objConnectionManager=nil;
	}
	objConnectionManager=[[ConnectionManager alloc]init];
  //  objConnectionManager.isSBTracker=@"YES";
    [objConnectionManager getWebData:self commandName:@"wscircular" Requestparameter:localDictionary];
}
#pragma mark connection manager delegate
- (void)receivedData:(NSArray *)responseArray
{
    if ([_delegate respondsToSelector:@selector(serverResponseForNoticeBoardWebservice:)])
    {
        [_delegate serverResponseForNoticeBoardWebservice:(NSMutableDictionary *)responseArray];
    }
    else
    {
        [NSException raise:NSInternalInconsistencyException format:@"Delegate doesn't respond to serverResponseForCaptchaWebSerivce:"];
    }
}
-(void)failToReciveData
{
    if ([_delegate respondsToSelector:@selector(serverFailResponseForNoticeBoardWebservice)])
    {
        [_delegate serverFailResponseForNoticeBoardWebservice];
    }
}
#pragma mark - Delegate methods
- (id)delegate
{
	return  _delegate;
}

- (void)setDelegate:(id)new_delegate
{
	_delegate = new_delegate;
}

@end
