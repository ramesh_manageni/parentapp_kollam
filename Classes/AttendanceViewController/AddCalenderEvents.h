//
//  AddCalenderEvents.h
//  AddCalendarEvents
//
//  Created by Chetan Zalake on 13/01/14.
//  Copyright (c) 2014 Chetan Zalake. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <EventKit/EventKit.h>

@interface AddCalenderEvents : UIViewController

- (void) addEventsToCalender: (NSString*)eventTitle Description: (NSString *)descriptionNote Date: (NSArray*)absentDate;

@end
