//
//  SentMessageDetail.m
//  ParentApp
//
//  Created by Redbytes on 29/08/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "SentMessageDetail.h"


@implementation SentMessageDetail

@dynamic from;
@dynamic message_id;
@dynamic messgae_sent;
@dynamic subject_sent_item;
@dynamic to_address;
@dynamic date_sent;
@dynamic time_sent;

@end
