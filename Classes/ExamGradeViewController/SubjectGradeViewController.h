//
//  SubjectGradeViewController.h
//  ParentApp
//
//  Created by Redbytes on 08/01/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ResultCustomCell.h"


#import "DXSemiViewControllerCategory.h"
#import "DXSubClassSemiViewController.h"
#import "DXSubclassSemiTableViewController.h"
@protocol MenuButtonSubjectGradeDelegate <NSObject>

-(void)didSelectRowSubjectGrade:(NSInteger)rowcount;

@end
@interface SubjectGradeViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    ResultCustomCell *objResultCustomCell;
     UITableView *tableviewObject;
    id _delegate;
    DXSubclassSemiTableViewController *semiLeft;

}
@property(nonatomic,strong)	UITableView *tableviewObject;
@property(nonatomic,strong)	NSMutableArray *subjectArray;

- (id)delegate;
- (void)setDelegate:(id)new_delegate;
@end
