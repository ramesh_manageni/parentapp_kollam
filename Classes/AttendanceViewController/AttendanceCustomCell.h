//
//  AttendanceCustomCell.h
//  ParentApp
//
//  Created by Redbytes on 03/01/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AttendanceCustomCell : UITableViewCell
{
    UILabel *labelName;
    UILabel *labelAdminNumber;
    UILabel *labelAttendance;
    UIImageView *objImageView;

}
@property(strong,nonatomic)UILabel *labelName,*labelAdminNumber,*labelAttendance;

@property(strong,nonatomic)UIImageView *objImageViewBG;
@property(strong,nonatomic)UIImageView *objImageViewStudent;

@end
