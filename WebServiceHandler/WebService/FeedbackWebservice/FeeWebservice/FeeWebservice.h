//
//  FeeWebservice.h
//  ParentApp
//
//  Created by Redbytes on 07/01/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol FeeDelegate <NSObject>

-(void)serverResponseForFeeWebservice:(NSArray*)responseArray;
-(void)serverFailResponseForFeeWebservice;

@end
//********************************************************************

@class ConnectionManager;
@interface FeeWebservice : NSObject
{
    id _delegate;
    NSMutableDictionary *resultDictionary;
    ConnectionManager *objConnectionManager;
}
- (id)delegate;
- (void)setDelegate:(id)new_delegate;
-(void)showFeesForUsername:(NSString *)username studentId:(NSString *)studentId;
@end
