//
//  ResultWebservice.m
//  ParentApp
//
//  Created by Redbytes on 07/01/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "ResultWebservice.h"


#import "ConnectionManager.h"

@implementation ResultWebservice

-(id)init
{
	return self;
}
-(void)showResultForUsername:(NSString *)username studentId:(NSString *)studentId examId:(NSString *)examId
{
    student_Id=studentId;

    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    
    currentDate = [formatter stringFromDate:[NSDate date]];
    
    lastUpdaeDateForResults=[NSUserDefaults standardUserDefaults];
    
    currentdateFromUserdefaults=[lastUpdaeDateForResults valueForKey:[NSString stringWithFormat:@"last_update_date_Result_%@",student_Id]];
    
    NSLog(@"DATE currentdateFromUserdefaults : %@",currentdateFromUserdefaults);

    
    NSMutableDictionary *localDictionary=[[NSMutableDictionary alloc]init];
	
    
    
    NSMutableString *strApplicationUUID=[[NSMutableString alloc]init];
    NSUUID *oNSUUID = [[UIDevice currentDevice] identifierForVendor];
    [strApplicationUUID setString:[oNSUUID UUIDString]];
    [localDictionary setObject:strApplicationUUID forKey:@"device_id"];

    [localDictionary setObject:username forKey:@"username"];
    [localDictionary setObject:studentId forKey:@"student_id_for_result"];
    [localDictionary setObject:examId forKey:@"exam_id"];
    if(currentdateFromUserdefaults==nil)
    {
        currentdateFromUserdefaults=@"";
    }
    [localDictionary setObject:currentdateFromUserdefaults forKey:@"last_update_date"];
    
    
    
    

    NSLog(@"Dict: %@",localDictionary);
    
    if (objConnectionManager!=nil)
    {
		objConnectionManager=nil;
	}
	objConnectionManager=[[ConnectionManager alloc]init];
    
    [objConnectionManager getWebData:self commandName:@"wsresults" Requestparameter:localDictionary];
}

#pragma mark connection manager delegate
- (void)receivedData:(NSArray *)responseArray
{
    [lastUpdaeDateForResults setValue:currentDate forKey:[NSString stringWithFormat:@"last_update_date_Result_%@",student_Id]];

    if ([_delegate respondsToSelector:@selector(serverResponseForResultWebSerivce:)])
    {
        [_delegate serverResponseForResultWebSerivce:responseArray];
    }
    else
    {
        [NSException raise:NSInternalInconsistencyException format:@"Delegate doesn't respond to serverResponseForCaptchaWebSerivce:"];
    }
}
-(void)failToReciveData
{
    if ([_delegate respondsToSelector:@selector(serverFailResponseForResultWebSerivce)])
    {
        [_delegate serverFailResponseForResultWebSerivce];
    }
}
#pragma mark - Delegate methods
- (id)delegate
{
	return  _delegate;
}

- (void)setDelegate:(id)new_delegate
{
	_delegate = new_delegate;
}



@end
