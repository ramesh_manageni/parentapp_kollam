//
//  InfoViewController.h
//  ParentApp
//
//  Created by Redbytes on 19/02/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InfoCustomCell.h"


#import "DXSemiViewControllerCategory.h"
#import "DXSubClassSemiViewController.h"
#import "DXSubclassSemiTableViewController.h"

@protocol MenuButtonInfoDelegate <NSObject>

-(void)didSelectRowInfo:(NSInteger)rowcount;

@end

@interface InfoViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    UITableView *tableviewObject;
    NSMutableArray *NotificationArray;
    
    id _delegate;
    DXSubclassSemiTableViewController *semiLeft;
}

@property(nonatomic,strong)	UITableView *tableviewObject;

- (id)delegate;
- (void)setDelegate:(id)new_delegate;


@end
