//
//  AttendanceViewController.h
//  ParentApp
//
//  Created by Redbytes on 03/01/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AttendanceCustomCell.h"
#import "AttendanceWebservice.h"
#import "AttendanceTableViewController.h"
#import "MBProgressHUD.h"

#import "DXSemiViewControllerCategory.h"
#import "DXSubClassSemiViewController.h"
#import "DXSubclassSemiTableViewController.h"

@protocol MenuButtonDelegate <NSObject>

-(void)didSelectRow:(NSInteger)rowcount;

@end

@interface AttendanceViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,AttendanceDelegate,MBProgressHUDDelegate,SliderMenuButtonDelegate>
{
    AttendanceCustomCell *objAttendanceCustomCell;
    UITableView *tableviewObject;
    AttendanceTableViewController *objAttendanceTableViewController;
    MBProgressHUD *HUD;
    int entityCount;
    NSMutableArray *datesArrayCoreData;
    NSMutableArray *datesArrayForCalender;

    NSString *stdName;
    NSString *stdId;
    
    id _delegate;
    
    DXSubclassSemiTableViewController *semiLeft;

}
@property(nonatomic,strong)	UITableView *tableviewObject;
- (id)delegate;
- (void)setDelegate:(id)new_delegate;

@end
