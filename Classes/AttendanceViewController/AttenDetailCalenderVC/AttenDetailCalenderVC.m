//
//  AttenDetailCalenderVC.m
//  ParentApp
//
//  Created by Redbytes on 04/01/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "AttenDetailCalenderVC.h"

@interface AttenDetailCalenderVC ()

@end

@implementation AttenDetailCalenderVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
