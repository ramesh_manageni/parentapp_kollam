//
//  DXSubclassSemiTableViewController.h
//  DXSemiSideDemo
//
//  Created by 谢 凯伟 on 13-7-8.
//  Copyright (c) 2013年 谢 凯伟. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DXSemiTableViewController.h"
#import "SliderMenuCustomCell.h"

@protocol SliderMenuButtonDelegate <NSObject>

-(void)didSelectSliderMenuButton:(NSInteger)rowcount;

@end

@interface DXSubclassSemiTableViewController : DXSemiTableViewController
{
    id _delegate;

    SliderMenuCustomCell *objSliderMenuCustomCell;
}
- (id)delegate;
- (void)setDelegate:(id)new_delegate;

@end
