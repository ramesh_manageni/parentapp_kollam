//
//  MenuViewController.h
//  ParentApp
//
//  Created by Redbytes on 03/01/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AttendanceViewController.h"
#import "ResultViewController.h"
#import "FeedbackViewController.h"
#import "FeeViewController.h"
#import "ChangePasswordViewController.h"
#import "NoticeboardViewController.h"
#import "SBTrackerViewController.h"
#import "LogoutWebservice.h"
#import "InfoViewController.h"
#import "NoticePDFViewController.h"
#import "ExamGradeViewController.h"
#import "SubjectGradeViewController.h"
#import "AttenCalenderVC.h"
#import "AttendanceTableViewController.h"
#import <MediaPlayer/MediaPlayer.h>
#import "InboxDetailViewController.h"
#import "studentInboxController.h"
@interface MenuViewController : UIViewController<UIGestureRecognizerDelegate,
LogoutWebserviceDelegate,
UIAlertViewDelegate,
MBProgressHUDDelegate,
MenuButtonDelegate,
MenuButtonNoticeDelegate,
MenuButtonResultDelegate,
MenuButtonInfoDelegate,
MenuButtonFeedbackDelegate,
MenuButtonNoticeBordDetailDelegate,
MenuButtonExamGradeDelegate,
MenuButtonSubjectGradeDelegate,
MenuButtonCalenderAllViewDelegate,
MenuButtonCalenderSingleViewDelegate,
InboxDetailDelegate,
MenuButtonCommunicationDelegate>
{
    int i;
    CGRect theNewFrame;
    CGRect theOldFrame;
    NSString *isSettingClicked;
    MBProgressHUD *HUD;

    MPMoviePlayerController *moviePlayer;
	NSURL *movieURL;
    
}
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property(strong,nonatomic)IBOutlet UIView *logoutView;

@property (strong, nonatomic) IBOutlet UIView *baseView;

- (IBAction)menuButtonClicked:(UIButton *)sender;
- (IBAction)logoutButtonClicked:(UIButton *)sender;
- (IBAction)cancelButtonClicked:(UIButton *)sender;
- (IBAction)changepassButtonClicked:(UIButton *)sender;
- (IBAction)faqButtonClicked:(id)sender;
- (IBAction)videoButtonClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *noBoardBadgeLabel;
@property (weak, nonatomic) IBOutlet UILabel *notificationBadgeLabel;
@property (weak, nonatomic) IBOutlet UILabel *inboxCountLabel;

@end
