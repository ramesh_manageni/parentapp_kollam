//
//  ParseClass.m
//  ParentApp
//
//  Created by RedBytes on 01/08/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "ParseClass.h"
#import <Parse/Parse.h>

@implementation ParseClass

+(void) incrementClick
{
    PFQuery *query = [PFQuery queryWithClassName:@"Abudhabi"];
    query.cachePolicy = kPFCachePolicyNetworkElseCache;

    [query getObjectInBackgroundWithId:@"lXmTuD0KUj" block:^(PFObject *object, NSError *error)
     {
         if (object != nil)
         {
             [object incrementKey:@"numOfClicks"];
             [object saveEventually];
         }
     }];
}

+(void) incrementView
{
    PFQuery *query = [PFQuery queryWithClassName:@"Abudhabi"];
    query.cachePolicy = kPFCachePolicyNetworkElseCache;
    
    [query getObjectInBackgroundWithId:@"lXmTuD0KUj" block:^(PFObject *object, NSError *error)
     {
         if (object != nil)
         {
             //[object incrementKey:@"numOfViews" byAmount:[NSNumber numberWithInt:Count]];
             [object incrementKey:@"numOfViews"];
             [object saveEventually];
         }
    }];
}

@end
