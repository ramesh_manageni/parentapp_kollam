//
//  ChangePasswordViewController.m
//  ParentApp
//
//  Created by Redbytes on 03/01/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "ChangePasswordViewController.h"
#import "Constant.h"
#import "Parent.h"
#import "StudentDetails.h"
@interface ChangePasswordViewController ()

@end

@implementation ChangePasswordViewController
@synthesize oldpasswordText,newpasswordText,confirmpasswordText;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBarHidden=NO;
    [self.navigationItem setHidesBackButton:YES];
//    self.navigationItem.rightBarButtonItem = [self addRightBarButtonItem];
//    self.navigationItem.leftBarButtonItem = [self addLeftBarButtonItem];

        UIButton *backButton = [[UIButton alloc] initWithFrame: CGRectMake(15, 30, 30.0f, 30.0f)];
        UIImage *backImage = [UIImage imageNamed:@"back button icon.png"] ;
        [backButton setBackgroundImage:backImage  forState:UIControlStateNormal];
        [backButton addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.customNavBar addSubview:backButton];
    
    [self.customNavBar setBackgroundColor:[UIColor darkGrayColor]];
    // Do any additional setup after loading the view from its nib.
    if ([oldpasswordText respondsToSelector:@selector(setAttributedPlaceholder:)])
    {
        UIColor *color = [UIColor lightGrayColor];
        oldpasswordText.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Old password" attributes:@{NSForegroundColorAttributeName: color}];
    }
    if ([newpasswordText respondsToSelector:@selector(setAttributedPlaceholder:)])
    {
        UIColor *color = [UIColor lightGrayColor];
        newpasswordText.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"New password" attributes:@{NSForegroundColorAttributeName: color}];
    }
    if ([confirmpasswordText respondsToSelector:@selector(setAttributedPlaceholder:)])
    {
        UIColor *color = [UIColor lightGrayColor];
        confirmpasswordText.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Retype new password" attributes:@{NSForegroundColorAttributeName: color}];
    }
}
-(void)backButtonClicked
{
    [self dismissViewControllerAnimated:YES completion:nil];

}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//-(UIBarButtonItem *)addLeftBarButtonItem
//{
//    UIButton *backButton = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 30.0f, 30.0f)];
//    UIImage *backImage = [UIImage imageNamed:@"back button icon.png"] ;
//    [backButton setBackgroundImage:backImage  forState:UIControlStateNormal];
//    [backButton addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
//    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
//    return backButtonItem;
//    
//}
//-(UIBarButtonItem *)addRightBarButtonItem
//{
//    
//    
//    UIButton *backButton = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 30.0f, 30.0f)];
//    UIImage *backImage = [UIImage imageNamed:@"list button icon.png"] ;
//    [backButton setBackgroundImage:backImage  forState:UIControlStateNormal];
//    [backButton addTarget:self action:@selector(settingButtonClicked) forControlEvents:UIControlEventTouchUpInside];
//    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
//    return backButtonItem;
//    
//}
//-(void)backButtonClicked
//{
//    [self.navigationController popViewControllerAnimated:YES];
//}
//-(void)settingButtonClicked
//{
//    NSLog(@"setting Button Clicked");
//    ChangePasswordViewController *objChangePasswordViewController=[[ChangePasswordViewController alloc]initWithNibName:@"ChangePasswordViewController" bundle:nil];
//    [self.navigationController pushViewController:objChangePasswordViewController animated:YES];
//    
//    
//}
//
- (IBAction)saveButtonClicked:(id)sender
{
    //[self dismissViewControllerAnimated:YES completion:nil];
 if([self validateFields])
  {

    
    ChangePasswordWebservice *objChangePasswordWebservice=[[ChangePasswordWebservice alloc] init];
    objChangePasswordWebservice.delegate=self;
    [objChangePasswordWebservice ChangePasswordForUsername:[[parentArrayCoreData objectAtIndex:0] user_name] withPassword:self.newpasswordText.text profileId:[[parentArrayCoreData objectAtIndex:0] passwords]];
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
	[self.view addSubview:HUD];
	
	// Regiser for HUD callbacks so we can remove it from the window at the right time
	HUD.delegate = self;
	
	// Show the HUD while the provided method executes in a new thread
	//[HUD showWhileExecuting:@selector(myTask) onTarget:self withObject:nil animated:YES];
    [HUD show:YES];
  }
    

    
}
-(BOOL)validateFields
{
    if (oldpasswordText.text.length == 0 || oldpasswordText == nil)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"NOTE" message:@"Please enter old password" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        return NO;
        
    }
    else if (newpasswordText.text.length == 0 || newpasswordText == nil)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"NOTE" message:@"Please enter new password" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        return NO;
        
    }
    else if (confirmpasswordText.text.length == 0 || confirmpasswordText == nil)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"NOTE" message:@"Please enter confirm password" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        return NO;
        
    }
    else if (![oldpasswordText.text isEqualToString:[[parentArrayCoreData objectAtIndex:0] passwords]])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Incorrect password" message:@"Please enter correct old password" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        return NO;
        
    }
    else if (newpasswordText.text.length<5 || confirmpasswordText.text.length<5)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"NOTE" message:@"Password length should be minimum five characters long" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        return NO;
        
    }
    else if (![newpasswordText.text isEqualToString:confirmpasswordText.text])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"NOTE" message:@"New password does not match confirm password" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        return NO;
        
    }


    
    return YES;
}
#pragma UITextField delegates
//- (void)textFieldDidBeginEditing:(UITextField *)textField
//{
//    UIScreen *mainScreen = [UIScreen mainScreen];
//    CGFloat scale = ([mainScreen respondsToSelector:@selector(scale)] ? mainScreen.scale : 1.0f);
//    CGFloat pixelHeight = (CGRectGetHeight(mainScreen.bounds) * scale);
//    
//    
//    if(objTextId==textField)
//    {
//        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
//        {
//            if (scale == 2.0f)
//            {
//                if (pixelHeight == 960.0f)
//                {
//                    [objScrollView setContentOffset:CGPointMake(0,120) animated:YES];
//                    
//                }
//                
//                else if (pixelHeight == 1136.0f)
//                {
//                    [objScrollView setContentOffset:CGPointMake(0,30) animated:YES];
//                    
//                }
//                
//            }
//            
//        }
//        
//        
//    }
//    if(objTextPassword==textField)
//    {
//        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
//        {
//            if (scale == 2.0f)
//            {
//                if (pixelHeight == 960.0f)
//                {
//                    [objScrollView setContentOffset:CGPointMake(0,120) animated:YES];
//                    
//                }
//                
//                else if (pixelHeight == 1136.0f)
//                {
//                    [objScrollView setContentOffset:CGPointMake(0,30) animated:YES];
//                    
//                }
//                
//            }
//            
//        }
//        
//    }
//    
//}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
   // [objScrollView setContentOffset:CGPointMake(0,0) animated:YES];
    
    return YES;
}
#pragma mark ChangePasswordWebservice Delegates
-(void)serverResponseForChangePasswordWebservice:(NSMutableDictionary*)responseArray
{
    [HUD hide:YES];
    if (responseArray!=nil)
    {
        NSLog(@"RESPONSE : %@",responseArray);
        
        if([[responseArray valueForKey:@"success"] intValue])
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"NOTE" message:@"Password changed successfully" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            alert.delegate=self;
            alert.tag=1;
            [alert show];

        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"NOTE" message:@"password change not successful" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            alert.delegate=self;
            alert.tag=0;
            
            [alert show];
        }
       
    }

}
-(void)serverFailResponseForChangePasswordWebservice
{
    [HUD hide:YES];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Server response failed" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    alert.delegate=self;
    alert.tag=10;
    
    [alert show];
 
}
#pragma mark Alert view Delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    // the user clicked one of the Ok/Cancel buttons
    if(alertView.tag==1)
    {
        [self dismissViewControllerAnimated:YES completion:nil];
        
    }
    
}

@end
