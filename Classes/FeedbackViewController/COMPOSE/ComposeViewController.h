//
//  ComposeViewController.h
//  ParentApp
//
//  Created by Redbytes on 22/08/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TeacherWebservice.h"
#import "MBProgressHUD.h"
#import "ComposeMessage.h"


@interface ComposeViewController : UIViewController<UIPickerViewDelegate,UITextFieldDelegate,UITextViewDelegate,UIPickerViewDataSource,ComposeMessageDelegate,MBProgressHUDDelegate>
{
    UIButton* customButton;
    MBProgressHUD *HUD;
    NSMutableArray *selectedStudents;
    NSString *isSelectAll;
    id _delegate;
    
    NSMutableArray *teacherDetailsArray;
    NSMutableArray * myRandomNumberArray;
    NSMutableArray * sentArray;

}


@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

@property (strong, nonatomic) IBOutlet UITextField *textFieldStudent;

@property (strong, nonatomic) IBOutlet UITextField *labelDefault;

@property (strong, nonatomic) IBOutlet UITextView *textViewMessage;

@property (strong, nonatomic) IBOutlet UITextField *textFieldSubject;

@property (strong, nonatomic) IBOutlet UITextField *textFieldAddTopic;



@property (retain, nonatomic) NSMutableArray *teacherNameArray;
@property (retain, nonatomic) NSMutableArray *subjectsNameArray;
@property (retain, nonatomic) NSString *student_id;


@property (retain, nonatomic) NSString *isFromReplyButton;
@property (retain, nonatomic) NSString *to_address;
@property (retain, nonatomic) NSString *to_subject;
@property (retain, nonatomic) NSString *to_message;


@property(strong,nonatomic)UITableView *myTableView;

- (IBAction)addTopicButtonClicked:(id)sender;
- (IBAction)sendButtonClicked:(id)sender;
- (IBAction)cancelButtonClicked:(id)sender;
@end
