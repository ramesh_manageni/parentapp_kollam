//
//  NoticePDFViewController.m
//  ParentApp
//
//  Created by Redbytes on 14/02/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "NoticePDFViewController.h"
#import "Constant.h"
@interface NoticePDFViewController ()<UIWebViewDelegate>

@end

@implementation NoticePDFViewController

@synthesize webView,pdfNmaeNotice;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //self.view.backgroundColor = [UIColor clearColor];
    
    [self setTitle:pdfNmaeNotice];
    self.navigationController.navigationBarHidden=NO;
    ///self.navigationController.navigationBar.translucent = NO;
    
    [self.navigationItem setHidesBackButton:YES];
    self.navigationItem.rightBarButtonItem = [self addRightBarButtonItem];
    self.navigationItem.leftBarButtonItem = [self addLeftBarButtonItem];
    
    
    //adjust imgage to screen size.
    UIScreen *mainScreen = [UIScreen mainScreen];
    CGFloat scale = ([mainScreen respondsToSelector:@selector(scale)] ? mainScreen.scale : 1.0f);
    CGFloat pixelHeight = (CGRectGetHeight(mainScreen.bounds) * scale);
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        if (scale == 2.0f)
        {
            if (pixelHeight == 960.0f)
            {
                webView = [[UIWebView alloc] initWithFrame:CGRectMake(0,64,320,416)];
            }
            else if (pixelHeight == 1136.0f)
            {
                webView = [[UIWebView alloc] initWithFrame:CGRectMake(0,64,320,504)];
            }
        }
    }
    [webView setBackgroundColor:[UIColor clearColor]];
     webView.scalesPageToFit = YES;
    //[[webView scrollView] setContentOffset:CGPointZero animated:YES];
    [webView.scrollView scrollRectToVisible:CGRectMake(0,64,320,504) animated:NO];
    [webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"window.scrollTo(0.0,50.0)"]];
    [self.view addSubview:webView];
    [self.webView setDelegate:self];
    
    if ([pdfNmaeNotice  isEqual: @"FAQ"])
    {
        NSString *filePath = [[NSBundle mainBundle] pathForResource:@"FAQ123" ofType:@"rtf"];
        NSURL *targetURL = [NSURL fileURLWithPath:filePath];
        NSURLRequest *request = [NSURLRequest requestWithURL:targetURL];
        [webView loadRequest:request];
    }
    else
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"/PDFFolder"];
        NSString *filePath = [dataPath stringByAppendingPathComponent:pdfNmaeNotice];
        NSURL *targetURL = [NSURL fileURLWithPath:filePath];
        NSURLRequest *request = [NSURLRequest requestWithURL:targetURL];
        [webView loadRequest:request];
    }
}

//- (void)webViewDidFinishLoad:(UIWebView *)webView
//{
//    CGRect newBounds = self.webView.bounds;
//    newBounds.size.height = self.webView.scrollView.contentSize.height+100;
//    self.webView.bounds = newBounds;
//}

-(void) viewDidAppear:(BOOL)animated
{
    [ParseClass incrementView];
}

-(UIBarButtonItem *)addLeftBarButtonItem
{
    UIButton *backButton = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 30.0f, 30.0f)];
    UIImage *backImage = [UIImage imageNamed:@"back button icon.png"] ;
    [backButton setBackgroundImage:backImage  forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    return backButtonItem;
    
}
-(UIBarButtonItem *)addRightBarButtonItem
{
    UIButton *backButton = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 30.0f, 30.0f)];
    UIImage *backImage = [UIImage imageNamed:@"list button icon.png"] ;
    [backButton setBackgroundImage:backImage  forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(settingButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    return backButtonItem;
    
}
-(void)backButtonClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)settingButtonClicked
{
    if(semiLeft!=nil)
    {
        [semiLeft dismissSemi:nil];
    }

    //[self.navigationController popToViewController:objHomeViewController animated:YES];
    semiLeft = [[DXSubclassSemiTableViewController alloc] init];
    semiLeft.delegate=self;
    
    semiLeft.semiTitleLabel.text = @"SemiLeftTableView";
    semiLeft.tableViewRowHeight = 80.0f/2;
    [semiLeft.view setBackgroundColor:[UIColor clearColor]];
    self.leftSemiViewController = semiLeft;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark SliderMenuDelegate
-(void)didSelectSliderMenuButton:(NSInteger)rowcount
{
    // NSLog(@"ROWCOUNT=== %d",rowcount);
    if(rowcount==0)
    {
        [self.navigationController popToViewController:objHomeViewController animated:YES];
        
    }
    else
    {
        [self.navigationController popToViewController:objHomeViewController animated:NO];
        
    }
    if ([_delegate respondsToSelector:@selector(didSelectRowNoticeboardDetail:)])
    {
        [_delegate didSelectRowNoticeboardDetail:rowcount];
    }
    
}

#pragma mark Delegate

- (id)delegate
{
	return  _delegate;
}

- (void)setDelegate:(id)new_delegate
{
	_delegate = new_delegate;
}

@end
