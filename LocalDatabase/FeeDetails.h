//
//  FeeDetails.h
//  ParentApp
//
//  Created by RedBytes on 09/06/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class StudentDetails;

@interface FeeDetails : NSManagedObject

@property (nonatomic, retain) NSString * act;
@property (nonatomic, retain) NSString * adm;
@property (nonatomic, retain) NSString * bus;
@property (nonatomic, retain) NSString * due_amount;
@property (nonatomic, retain) NSString * due_date;
@property (nonatomic, retain) NSString * hos;
@property (nonatomic, retain) NSString * id_fee;
@property (nonatomic, retain) NSString * student_id;
@property (nonatomic, retain) NSString * tc;
@property (nonatomic, retain) NSString * tut;
@property (nonatomic, retain) NSString * otherFee;
@property (nonatomic, retain) StudentDetails *feeToStudent;

@end
