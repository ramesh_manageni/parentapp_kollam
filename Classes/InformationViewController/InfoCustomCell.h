//
//  InfoCustomCell.h
//  ParentApp
//
//  Created by Redbytes on 19/02/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface InfoCustomCell : UITableViewCell
{
    UILabel *NotificationText;
    UILabel *NotificationTime;
    
}
@property(strong,nonatomic)UILabel *NotificationText;
@property(strong,nonatomic)UILabel *NotificationTime;
@property(strong,nonatomic)UIImageView *objImageViewBG;
@property(strong,nonatomic)UIImageView *objImageViewStudent;

@end