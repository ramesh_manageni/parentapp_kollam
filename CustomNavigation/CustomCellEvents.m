//
//  CustomCellEvents.m
//  ParentApp
//
//  Created by RedBytes on 23/06/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "CustomCellEvents.h"

@implementation CustomCellEvents
@synthesize eventNameLabel,locationLabel,descriptionLabel,durationLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        // Initialization code
        eventNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(03,02, 246, 25)];
        eventNameLabel.backgroundColor = [UIColor darkGrayColor];
        eventNameLabel.textColor = [UIColor whiteColor];
        eventNameLabel.textAlignment = NSTextAlignmentCenter;
        eventNameLabel.font = [UIFont systemFontOfSize:16];
        
        locationLabel = [[UILabel alloc] initWithFrame:CGRectMake(03,27, 246, 15)];
        locationLabel.backgroundColor = [UIColor lightGrayColor];
        locationLabel.textAlignment = NSTextAlignmentLeft;
        locationLabel.font = [UIFont systemFontOfSize:13];
        
        descriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(03,42,246,15)];
        descriptionLabel.backgroundColor = [UIColor lightGrayColor];
        descriptionLabel.textAlignment = NSTextAlignmentLeft;
        descriptionLabel.font = [UIFont systemFontOfSize:13];
        
        durationLabel = [[UILabel alloc] initWithFrame:CGRectMake(03,57,246,15)];
        durationLabel.backgroundColor = [UIColor lightGrayColor];
        durationLabel.textAlignment = NSTextAlignmentLeft;
        durationLabel.font = [UIFont systemFontOfSize:13];
        
        //[self.contentView addSubview:dateLabel];
        [self.contentView addSubview:eventNameLabel];
        [self.contentView addSubview:locationLabel];
        [self.contentView addSubview:descriptionLabel];
        [self.contentView addSubview:durationLabel];

    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
