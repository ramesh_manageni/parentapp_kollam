//
//  SBMapWebservice.h
//  ParentApp
//
//  Created by Redbytes on 22/01/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol SBMapDelegate <NSObject>

-(void)serverResponseForSBMapWebservice:(NSMutableDictionary*)responseArray;
-(void)serverFailResponseForSBMapWebservice;

@end
//********************************************************************

@class ConnectionManager;


@interface SBMapWebservice : NSObject
{
    id _delegate;
    NSMutableDictionary *resultDictionary;
    ConnectionManager *objConnectionManager;
}
- (id)delegate;
- (void)setDelegate:(id)new_delegate;
-(void)getMapDetailsForIMEI:(NSString *)imei_number;
@end
