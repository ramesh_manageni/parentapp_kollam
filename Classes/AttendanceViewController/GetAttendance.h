//
//  GetAttendance.h
//  ParentApp
//
//  Created by Redbytes on 16/01/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AttendanceWebservice.h"
@interface GetAttendance : UIViewController<AttendanceDelegate>
{
    int absentCount;
}

-(void)getAttendanceForUser:(NSString *)username stdId:(NSString *)stdId;
@end
