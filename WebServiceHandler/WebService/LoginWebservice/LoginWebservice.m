//
//  LoginWebservice.m
//  ParentApp
//
//  Created by Redbytes on 07/01/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "LoginWebservice.h"


#import "ConnectionManager.h"

@implementation LoginWebservice
-(id)init
{
	return self;
}
-(void)loginToServserWithId:(NSString *)userId :(NSString *)password
{
    NSMutableDictionary *localDictionary=[[NSMutableDictionary alloc]init];
    
    
    NSString *appVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];

    
    NSMutableString *strApplicationUUID=[[NSMutableString alloc]init];
    NSUUID *oNSUUID = [[UIDevice currentDevice] identifierForVendor];
    [strApplicationUUID setString:[oNSUUID UUIDString]];
    
    [localDictionary setObject:strApplicationUUID forKey:@"device_id"];

	[localDictionary setObject:userId forKey:@"username"];
	[localDictionary setObject:password forKey:@"password"];
	[localDictionary setObject:appVersion forKey:@"app_version"];
    
    
    NSLog(@"Dict: %@",localDictionary);
    
    if (objConnectionManager!=nil)
    {
		objConnectionManager=nil;
	}
	objConnectionManager=[[ConnectionManager alloc]init];
    
    [objConnectionManager getWebData:self commandName:@"WSlogin" Requestparameter:localDictionary];
    
    
}

#pragma mark connection manager delegate
- (void)receivedData:(NSArray *)responseArray
{
    if ([_delegate respondsToSelector:@selector(serverResponseForLoginWebservice:)])
    {
        [_delegate serverResponseForLoginWebservice:responseArray];
    }
    else
    {
        [NSException raise:NSInternalInconsistencyException format:@"Delegate doesn't respond to serverResponseForCaptchaWebSerivce:"];
    }
}
-(void)failToReciveData
{
    if ([_delegate respondsToSelector:@selector(serverFailResponseForLoginWebservice)])
    {
        [_delegate serverFailResponseForLoginWebservice];
    }
}
#pragma mark - Delegate methods
- (id)delegate
{
	return  _delegate;
}

- (void)setDelegate:(id)new_delegate
{
	_delegate = new_delegate;
}



@end
