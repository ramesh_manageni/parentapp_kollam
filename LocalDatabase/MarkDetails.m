//
//  MarkDetails.m
//  ParentApp
//
//  Created by Redbytes on 05/02/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "MarkDetails.h"
#import "StudentResult.h"


@implementation MarkDetails

@dynamic batch_id;
@dynamic entry_status;
@dynamic exam;
@dynamic exam_id;
@dynamic grade;
@dynamic mark;
@dynamic mark_t;
@dynamic max_mark_t;
@dynamic order_id;
@dynamic student_id;
@dynamic subject;
@dynamic subject_id;
@dynamic subject_mark_id;
@dynamic marksToResult;

@end
